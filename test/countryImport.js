import 'babel-polyfill';
import chai from 'chai';
import chaiHttp from 'chai-http';
import chaiAsPromised from 'chai-as-promised';
import Promise from 'promise';
import { testDb, testAqlQuery } from '../db/arangoDB/arangodbTest';

import {compareObj } from '../db/arangoDB/utils/compareObj'
import {checkExists, checkEdgeExists, checkExistsMultipleColl } from '../db/arangoDB/utils/checkExists'

const municipalities = testDb.collection('legislativeBody');
const headEdge = testDb.edgeCollection('headOfBody');
const peopleColl = testDb.collection('people');
const people2Coll = testDb.collection('people2');
const people3Coll = testDb.collection('people3');
const hasSameNameEdge = testDb.edgeCollection('hasSameName');

//Test1 and test2 are arguments for compareObj
//Compare objects are assumed to be preprocessed by getMunicipalityData in dutchMunicipalityImporter.js
const test1 = {
    "item":  "http://www.wikidata.org/entity/Q491999",
    "coords": "Point(5.22 52.75)",
    "itemLabel":"Andijk2"
};

const test2 = {
    "item":  "http://www.wikidata.org/entity/Q491999",
    "endDate":  "2011-01-01T00:00:00Z",
    "coords": "Point(5.22 52.75)",
    "itemLabel": ["Andijk", "Andijk2"],
    "replacedByLabel": "Medemblik"
};

//Unlike test1 and test2 this is raw data as it would be retrieved from Wikidata
const municipalityTEST = {
    "item": {
        "type": "uri",
        "value": "http://www.wikidata.org/entity/Q9899"
    },
    "coords": {
        "datatype": "http://www.opengis.net/ont/geosparql#wktLiteral",
        "type": "literal",
        "value": "Point(4.883333333 52.366666666)"
    },
    "mayor": {
        "type": "uri",
        "value": "http://www.wikidata.org/entity/Q1279554"
    },
    "itemLabel": {
        "xml:lang": "nl",
        "type": "literal",
        "value": "Amsterdam"
    },
    "mayorLabel": {
        "xml:lang": "nl",
        "type": "literal",
        "value": "Eberhard van der Laan"
    },
    "populationLabel": {
        "type": "literal",
        "value": "834119"
    }
};

const should = chai.should();
const expect = chai.expect;

const wikidataEndpoint = 'https://query.wikidata.org';
const wikidataPath = '/sparql?format=json&query=SELECT%20%3Fitem%20%3FitemLabel%20%20%20WHERE%20%7B%0A%20%20%0A%20%20%3Fitem%20wdt%3AP463%20wd%3AQ1065%20.%0A%20%20%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22en%2C%20nl%22%20.%20%7D%0A%7D%20ORDER%20BY%20%3FitemLabel%0A'

chai.use(chaiHttp);
chai.use(chaiAsPromised);

describe('Wikidata availability', () => {
    it('should receive a 200 status code when accessing Wikidata endpoint', function () {
        chai.request(wikidataEndpoint)
            .get(wikidataPath)
            .end((err, res) => {
                var result = JSON.stringify(res.text);
                // console.log(result);
                res.should.have.status(200);
                result.should.be.json;
                result.results.should.be.a('array');
                should.not.exist(err);
                done()
            })
    })
});

describe('Utility functions', () => {
    it('Compare obj be equal to expected result', () => {
        var result = compareObj(test2, test1 );
        result.item.should.equal('http://www.wikidata.org/entity/Q491999');
        result.endDate.should.equal('2011-01-01T00:00:00Z');
        result.coords.should.equal('Point(5.22 52.75)');
        result.itemLabel[0].should.equal('Andijk');
        result.itemLabel[1].should.equal('Andijk2');
        result.replacedByLabel.should.equal('Medemblik');
        result.should.be.an('object');
    });

    it('Check exists should return a person', (done) => {
        checkExists('iemandAlone', people2Coll, testDb, testAqlQuery).then(res => {
            expect(res.length).to.equal(1);
            expect(res).to.be.instanceof(Array);
            expect(res[0]._id).to.equal('people2/821674');
            done();
        },err => {
            assert.fail(err);
            done();
        });
    });
    
    it('Check edge exists should return an edge', (done) => {
        checkEdgeExists('people/821604', 'people2/821645', hasSameNameEdge, testDb, testAqlQuery).then(res => {
            expect(res.length).to.equal(1);
            expect(res).to.be.instanceof(Array);
            expect(res[0]._id).to.equal('hasSameName/824649');
            done()
        },err => {
            assert.fail(err);
            done();
        })
    });
    
    it('Check exists multiple collections should return two persons', (done) => {
        checkExistsMultipleColl('iemand', [peopleColl, people2Coll, people3Coll], testDb, testAqlQuery).then(res => {
            expect(res.length).to.equal(2);
            expect(res).to.be.instanceof(Array);
            done();
        })
    });
    
    
});

describe('DB workings', () => {
    it('Municipality should not yet be created', (done) => {
        checkExists(municipalityTEST.itemLabel.value, municipalities, testDb, testAqlQuery).then(res => {
            expect(res).to.be.an('undefined');
            done()
        }, err => {
            assert.fail(err);
            done();
        });
    });
    it('Municipality should be created', (done) => {
        municipalities.save({name: municipalityTEST.itemLabel.value}).then(res => {
            console.log('res in mun should be created');
            console.log(res);
            expect(res).to.be.an('object');
            done();
        },err => {
            assert.fail('error in municipality should be created');
            done();
        });
    });
    it('Municipality should be deleted', (done) => {
        var id;
        var shouldExist = checkExists(municipalityTEST.itemLabel.value, municipalities, testDb, testAqlQuery).then(
            res => {
                id = res[0]._id;
                // console.log('id: '+id);
                municipalities.remove(id).then(() => {
                    checkExists(municipalityTEST.itemLabel.value, municipalities, testDb, testAqlQuery).then(removed => {
                        // console.log('removed: '+removed);
                        if (!removed) done();
                        else {
                            assert.fail('not deleted');
                            done();
                        }
                    })
                })
            }
        )
    })
});


//TODO create test for asyncLoop