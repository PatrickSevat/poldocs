#start with current LTS of node. Specific version rather than latest to lower chance of errors
FROM node:6.9.1

#create less priviledged user poldocsUser rather than root
RUN useradd --user-group --create-home --shell /bin/false poldocsUser

#set ENV to DEV for now #TODO change for prod
ENV HOME=/home/poldocsUser

#Add package.json and change owner of $DEV and all subdirs\folders to poldocsUser
COPY package.json $HOME/poldocs/
RUN chown -R poldocsUser:poldocsUser $HOME/*

#TODO figure out how to use poldocsUser and still be able to run webpack inside the container
WORKDIR $HOME/poldocs

RUN npm install && \
    npm cache clean

COPY . $HOME/poldocs/

EXPOSE 3000

#USER poldocsUser

CMD ["npm", "start"]

