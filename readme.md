**Hi welcome to the Poldocs repo!**

*General remarks*

I've made some personal progress during this project. This means that the code in the older parts of this repo is significantly worse than the newer parts.

* I started with filling the database with some data from Wikidata, so that's the worst part
* After that, I started working on importing the documents from Eurlex, where you'll find some unnecessary large function which I still need to refactor
* During this time I also wrote most of the database utility functions, some of which could definitely be better. More and smaller functions and replacing some callbacks with async await
* Learning from that, I tried to write smaller and more function for the Officiele Bekendmakingen import
* Other utility functions have later become obsolete when I decided I shouldn't be reinventing the wheel. So in utility functions you'll find some files that will be replaced with moment.js and lodash for example. However, you'll still find many references to these older utility functions in the imports section. Still need to refactor
* Having written some working import scripts (including extracting text from PDFs) and having a reasonably filled db, I moved on to front-end
* In general the front-end should be better structured as I have more experience writing front-end than back-end
* One exception: the Redux reducers and actions. I know these shouldn't be this large and deep. Reason for this is that in a previous project I had too many reducers which led to other problems like sharing data from state, so my line of thinking was: I'll create one big reducer first and refactor later.

If you have any questions, like: "Why the hell did he use X here?!", you can send me an email: p.m.c.sevat@gmail.com give me a call: 0642978706


*Structure:*


    +--app (front end, entrypoint: main.js)

    |   +---actions (redux actions)

    |   +--ajax (just a folder with a general ajax function)

    |   +--components (all front-end components. To see what is rendered when, check app/routes/Routes.jsx)

    |   +--Reducers (Redux reducer, will be refactored later in multiple reducers)

    |   +--Routes (React Router)

    +--db

    |   +--ArangoDB

    |   |   +--importers (import scripts for Eurlex, Officiële Bekendmakingen and Open Raadsinformatie)

    |   |   |   +--Eurlex

    |   |   |   |   +--EurlexDocuments.js (importer for documents\legislation)

    |   |   |   |   +--EurlexDossiers.js (importer for dossiers\events)

    |   |   |   +--OB (best structured importer)

    |   |   |   |   +--officieleBekendmakingen (main entry point)

    |   |   |   +--OpenRaad (not yet implemented, just some tests to see if it works)

    |   |   |   +--Other folders (not really interesting, mainly used for additional db content gathering)

    |   |   +--Queries (several db queries which are used for certain paths to retrieve data front-end)

    |   |   +--sources (test data)

    |   |   +--sort-of-schema (overview for myself of the most common datastructures)

    |   |   +--utils (variety of custom utilities, mostly db related, many non db-related are now replaced by lodash)

    |   |   +--arangodb.js \ arangodbTest.js (connecting to db \ test db)

    |   |   +--users (db user verification)

    +--messages (just some error\success messages for front end)

    +--public (resource root)

    +--tests (will contain the tests later now)

    +--index.js (node entry point, compiles server.js)

    +--server.js (express server)

TODO server.js:

* move authentication code to seperate file
* move data retrieval routes to seperate file
* Build API with auth token verification
* Build client auth token renewal
* ~~remove unccessary files and folders such as scripts/, server_routes/~~
* ~~gitignore files like bundle.js, ideas.txt~~
* Learn to use Docker


















===Personal notes===

textract requires xpdf: https://github.com/dbashford/textract

make sure to set PATH environment variable to your folder with xpdf .exe's. Also make sure your xpdfrc is properly configured

Test by typing $ pdftotext -v in command line

Following IDs have slashes which have been replaced by dashes:
AdviesRvS: W03.14.0273/II/K/B => W03.14.0273-II-K-B

Dossiers Eurlex: 	2016/0258/NLE => 	2016-0258-NLE

Actieplan_open_overheid_2016-2017.pdf:
"Alle gemeenten hebben deze standaard in 2017 in gebruik genomen. Daarmee is in 2017 alle
 raadsinformatie, welke nu ongestructureerd openbaar is, op een uniforme en geïntegreerde
 manier als open data beschikbaar gesteld. "