const express = require('express');

const ReactEngine = require('react-engine');
const reactRoutesFilePath = join(__dirname, '..', '/app/routes/Routes.jsx');
const routes = require('./../app/routes/Routes.jsx');

import {join, normalize } from 'path';
import favicon from 'serve-favicon';
import passport from 'passport';
import bodyParser from 'body-parser';
const jwt = require('jsonwebtoken');

import apiRoutes from './api'
import { tokenSecret } from '../secret/jwtToken';
import {createNewUser} from '../db/arangoDB/users';
import { getErrorMessage } from '../messages/failure';
import {initializeLocalStrategy} from './auth/initiateAuthenticationMethods'
import {getHomepageSuggest} from '../db/arangoDB/queries/homepageSuggest'
import getHomepageSuggestES from '../db/arangoDB/queries/homepageSuggestES'
import {getRecentDocuments, getRecentDossiers} from '../db/arangoDB/queries/getRecent'
import {getDocumentAndDossierFileTypes} from '../db/arangoDB/queries/getDocumentAndDossierFileTypes'
import {getAllDocumentAuthors} from '../db/arangoDB/queries/getAllDocumentAuthors'
import {searchData} from '../db/arangoDB/queries/searchData'

const app = express();

// expose public folder as static assets
app.use('/static',express.static(join(__dirname, '..', 'public')));
app.use('/static/img',express.static(join(__dirname, '..', 'public/img')));
app.use('/static/pdf',express.static(join(__dirname, '..', 'public/pdf')));

app.use(favicon(join(__dirname, '..', 'public/favicon.ico')));

//Engine uses Routes.jsx to send incoming requests to the Router builder
var engine = ReactEngine.server.create({
    routes: routes,
    routesFilePath: normalize(reactRoutesFilePath)
});

//set engine file extention, call engine
app.engine('.jsx', engine);

// Set view path
app.set('views', join(__dirname, '..', 'app/components'));

// set jsx or js as the view engine
// (without this you would need to supply the extension to res.render())
// ex: res.render('index.jsx') instead of just res.render('index').
app.set('view engine', 'jsx');

// finally, set the custom view
app.set('view', ReactEngine.expressView);



//ACAO for client independent requests, define allowed methods and allowed headers
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
});

app.use(bodyParser.urlencoded({ extended: true }));

// const LocalStrategy = passportLocal.Strategy;
//
app.use(passport.initialize());
initializeLocalStrategy(passport);

app.use('/api', apiRoutes);

app.post('/signup', (req, res, next) => {
    console.log(req.body);
    let newUser = {
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
    };
    console.log('newUser in \\signup');
    console.dir(newUser);
    createNewUser(newUser).then((success) => {
        res.status(201).json({message: 'Account succesfully created'});
    }, (err) => {
        let message = getErrorMessage(err);
        //TODO base error messages on accept-languages
        res.status(409).json({message})
    })
});

//Verify token middleware
const verify = (req, res, next) => {
    let token = req.body.token || req.query.token || req.headers['x-access-token'] || req.headers.authorization;
    if (token) {
        jwt.verify(token, tokenSecret, (err, decoded) => {
            if (err) {
                return res.status(401).json({message: 'failed authentication'})
            }
            else {
                next()
            }
        })
    }
    else return res.status(401).json({message: 'failed authentication'})
};

// add our app routes & request data
// app.get('/api*', function(req, res) {
    // if (req.query.id) {
    //     console.log('req.query.id = '+req.query.id);
    //     Model.findById(req.query.id, function (err, doc) {
    //         if (err) throw err;
    //         res.type('json');
    //         res.json(doc);
    //     })
    // }
    // else if (req.query.q) {
    //     console.log('req.originalUrl = '+req.originalUrl);
    //     console.log('req.query.q = '+req.query.q);
    //     Model.find(req.query.q, function (err, docs) {
    //         if (err) throw err;
    //         res.json(docs);
    //         })
    // }
// });

app.get('/homepageSuggest', function (req, res) {
    if (!req.query.q) {
        console.log('no query');
        res.status(204)
    }
    // console.log(req.query.q);
    // getHomepageSuggest(req.query.q).then((data) => {
    getHomepageSuggestES(req.query.q).then((data) => {
        //IMPORTANT! Due to the appending we need to retrieve data[0] here for the correct results
        res.status(200).json(JSON.stringify(data));
        res.status(200).json(JSON.stringify(data[0]));
    }, (err) => {
        console.log(err);
        res.status(500).json('Error retrieving data')
    })
});

app.get('/recentDocuments', function (req, res) {
    let start = Date.now();
    // console.log(req.query.q);
    getRecentDocuments().then((data) => {
        console.log('/recentDocuments response in: '+(Date.now()-start));
        res.status(200).json(JSON.stringify(data));
    }, (err) => {
        console.log(err);
        res.status(500).json('Error retrieving data')
    })
});

app.get('/recentDossiers', function (req, res) {
    let start = Date.now();
    // console.log(req.query.q);
    getRecentDossiers().then((data) => {
        console.log('/recentDossiers response in: '+(Date.now()-start));
        res.status(200).json(JSON.stringify(data));
    }, (err) => {
        console.log(err);
        res.status(500).json('Error retrieving data')
    })
});

app.get('/getDocumentAndDossierFileTypes', function (req, res) {
    let start = Date.now();
    // console.log(req.query.q);
    getDocumentAndDossierFileTypes().then((data) => {
        res.status(200).json(JSON.stringify(data));
    }, (err) => {
        console.log(err);
        res.status(500).json('Error retrieving data')
    })
});

app.get('/getAllAuthors', function (req, res) {
    let start = Date.now();
    // console.log(req.query.q);
    getAllDocumentAuthors().then((data) => {
        res.status(200).json(JSON.stringify(data));
    }, (err) => {
        console.log(err);
        res.status(500).json('Error retrieving data')
    })
});

app.post('/searchData', function (req, res) {
    // let start = Date.now();
    searchData(req.body.query).then((data) => {
        res.status(200).json(JSON.stringify(data));
    }, (err) => {
        console.log(err);
        res.status(500).json('Error retrieving data')
    })
});

//TODO add different titles and metadescription for rendering routes
app.get('*', function(req, res) {
    console.log('getting request on: '+req.url);
        res.render(req.url, {
            path: req.path
        });
});

app.use(function(err, req, res, next) {
    console.error(err);

    // http://expressjs.com/en/guide/error-handling.html
    if (res.headersSent) {
        return next(err);
    }

    if (err._type && err._type === ReactEngine.reactRouterServerErrors.MATCH_REDIRECT) {
        return res.redirect(302, err.redirectLocation);
    }
    else if (err._type && err._type === ReactEngine.reactRouterServerErrors.MATCH_NOT_FOUND) {
        return res.status(404).send('Route Not Found!');
    }
    else {
        // for ReactEngine.reactRouterServerErrors.MATCH_INTERNAL_ERROR or
        // any other error we just send the error message back
        return res.status(500).send(err.message);
    }
});

app.listen(3000, function(){
    console.log('poldocs listening on port 3000');
});