const router = require('express').Router();
import auth from './api/auth/authRoutes';

//    TODO we can also use a token middleware here


router.use('/auth', auth);

export default router;