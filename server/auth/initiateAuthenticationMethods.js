import passport from 'passport';
import passportLocal from 'passport-local';
import {getAuthenticated} from '../../db/arangoDB/users';
import { getErrorMessage } from '../../messages/failure';

const LocalStrategy = passportLocal.Strategy;

//TODO check if this also works with imported passport
function initializeLocalStrategy(passport) {
    passport.use(new LocalStrategy((username, password, done) => {
        getAuthenticated(username, password).then((user) => {
            return done(null, user);
        }, (err) => {
            if (err) {
                console.error('Err in passport.use LocalStrategy: \n'+err);
                if (err === 'unknownUsername' || err === 'wrongPassword' || err === 'lockedOut')
                    return done(null, false, getErrorMessage(err));
                else return done(err)
            }
        })
    }));
}

export {initializeLocalStrategy}