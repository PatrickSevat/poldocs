const router = require('express').Router();
import local from './local';
import {redirectToLinkedInAuth, processLinkedInAuthCallback} from './linkedIn';

export default router
        .post('/authenticate/local', local)
        .get('/authenticate/linkedin', redirectToLinkedInAuth)
        .post('/authenticate/processLinkedIn', processLinkedInAuthCallback)
