import { tokenSecret } from '../../../secret/jwtToken';
const jwt = require('jsonwebtoken');

function serialize(req,res,next) {
    req.user.username = req.user._key;
    delete req.user.password;
    delete req.user._key;
    delete req.user._rev;
    delete req.user.lockedUntil;
    delete req.user.logInAttempts;
    next();
    /*
     * The serialize function is pretty much the core of the whole thing. It has basically three main jobs to do, two of them are only needed if you are using external authentication services:

     1. Create users, which are authenticated, but not in your database. This only happens if you use passport strategies for external services,
     like the Google or Facebook login (if someone authenticates with their google account they may have never been on your service and use it for the first time).
     2. Update the user data: Like #1, but now updates an already known user (a Facebook user could have switched his name in Facebook, you may want to update this in here too).
     3. Complete the user data in your req.user object: If you need additional information which aren't inside the authentication process, you can store it req.users (and therefore in your token) in here.
     */
}

function generateToken(req, res, next) {
    req.token = jwt.sign({
        id: req.user.id
    }, tokenSecret, {
        expiresIn: '2 days'
    });
    console.log('token generated, next!');
    next();
}

function respond(req, res) {
    console.log('responding');
    res.status(200).json({
        user: req.user,
        token: req.token,
        message: 'Log in successful'
    });
}

export {serialize, generateToken, respond}
