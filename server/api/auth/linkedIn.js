import superagent from 'superagent';

import {checkExistsAndCreateNodeByKey} from '../../../db/arangoDB/utils/checkExistsAndCreate';
import {db, aqlQuery} from '../../../db/arangoDB/arangodb';
const usersColl = db.collection('users');
import { clientSecret } from '../../../secret/linkedInClientSecretKey';
import {generateToken, respond} from './util'

const linkedInConfig = {
    clientID: '77cvzfts0k9mb4',
    clientSecret: clientSecret,
    callbackURL: "http%3A%2F%2Flocalhost%3A3000%2Fauthenticate%2FlinkedinCallback",
    scope: 'r_basicprofile r_emailaddress',
    state: '7uMJnHUaGt'
};

function redirectToLinkedInAuth(req, res) {
    res.redirect(`https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=${linkedInConfig.clientID}&redirect_uri=${linkedInConfig.callbackURL}&state=${linkedInConfig.state}&scope=${linkedInConfig.scope}`);
}

function processLinkedInAuthCallback(req, res, next) {
    let authCode = req.query.code;
    console.log('authCode: '+authCode);
    let state = req.query.state;
    console.log('res state: '+state);
    if (state !== linkedInConfig.state) {
        res.status(401).body('Verification between Poldocs server and LinkedIn server failed. CONTACT WEBSITE ADMINISTRATOR IMMEDIATELY! \nIf this is not on the side of Poldocs it might be that your computer is infected with harmful malware');
    }
    if (typeof req.query.error !== 'undefined') {
        res.status(401).body('User cancelled authorization');
    }
    else {
        superagent.post('https://www.linkedin.com/oauth/v2/accessToken')
            .set('Content-Type', 'application/x-www-form-urlencoded')
            .send('grant_type=authorization_code')
            .send(`code=${authCode}`)
            .send(`redirect_uri=${linkedInConfig.callbackURL}`)
            .send(`client_id=${linkedInConfig.clientID}`)
            .send(`client_secret=${linkedInConfig.clientSecret}`)
            .end(function (err, linkedInRes) {
                if (err) res.status(400).send('Error retrieving LinkedIn Access Token response');
                let accessToken = linkedInRes.body.access_token;
                superagent.get('https://api.linkedin.com/v1/people/~:(id,picture-url,positions,headline,formatted-name,public-profile-url,email-address)?format=json')
                    .set('Authorization', 'Bearer '+accessToken)
                    .set('Connection', 'Keep-Alive')
                    .end((err, userRes) => {
                        if (err) res.status(400).send('Error retrieving LinkedIn Profile response');
                        let user = userRes.body;
                        console.log(user);
                        checkExistsAndCreateNodeByKey(user.id, usersColl, db, aqlQuery, {
                            username: user.formattedName,
                            description: user.headline,
                            picture: user.pictureUrl,
                            link: user.publicProfileUrl,
                            email: user.emailaddress
                        }).then((savedUser) => {
                            req.user = savedUser;
                            generateToken(req, res, next);
                            respond(req, res);
                        }, (err) => {
                            res.status(400).send('Error retrieving user profile from database');
                        })
                    })
            })
    }
}

export {redirectToLinkedInAuth, processLinkedInAuthCallback}