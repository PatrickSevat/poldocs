import passport from 'passport';
import {serialize, generateToken, respond} from './util'

export default function local(req, res, next) {
    console.log('local auth called');

    passport.authenticate('local', {session: false}, function (err, user, info) {
        if (err) {
            console.log(err);
            return next(err);
        }
        else if (!user) {
            return res.status(401).send(info);
        }
        else if (user) {
            req.user = user;
            serialize(req, res, next);
            generateToken(req, res, next);
            respond(req, res, next);
        }
    })(req, res, next);
};