import React from 'react';
import { connect } from 'react-redux';
import {typeofUndefined} from '../../../db/arangoDB/utils/typeofUndefined';
import {SuggestionMetaItem} from './SuggestionMetaItem.jsx'

let _Suggestion = React.createClass({
    componentDidMount() {
        this.hoverAnimations();
    },
    getIcon(val) {
        let obj = {
            'country': 'fa-globe',
            'document': 'fa-file-text',
            'dossiers': 'fa-folder-open',
            'executive bodies': 'fa-sitemap',
            'legislative bodies':'fa-sitemap',
            'other bodies': 'fa-sitemap',
            'IGO': 'fa-sitemap',
            'judiciaries': 'fa-gavel',
            'people': 'fa-user',
            'political parties': 'fa-users'
        };
        return obj[val];
    },
    getMetaArr(obj) {
        let returnArr = [];
        for (let attr in obj) {
            if (obj[attr] !== '') {
                returnArr.push([attr, obj[attr]]);
            }
        }
        return returnArr;
    },
    hoverAnimations() {
        $(".homepageSuggestion").hover(over, out);
        function over(){
            TweenMax.to(this, 0.5, {backgroundColor:"rgb(225, 245, 254)"});
        }
        function out(){
            TweenMax.to(this, 0.5, {backgroundColor:"rgb(245, 245, 245)"});
        }
    },
    render: function () {
        // console.log('this.props.data');
        // console.log(this.props.data);
        let icon = this.getIcon(this.props.data.type);
        let metaObj = {};
        metaObj.Title = typeofUndefined(this.props.data, 'title');
        metaObj.Name = typeofUndefined(this.props.data, 'name');
        metaObj.Extract = typeofUndefined(this.props.data, 'extract');
        metaObj.Subtype = typeofUndefined(this.props.data, 'subType');
        metaObj.Subtitle = typeofUndefined(this.props.data, 'subTitle');

        let metaArr = this.getMetaArr(metaObj);
        console.log('metaArr', metaArr);
        return (
            <a
                className="no-styling-link"
                href={'/'+this.props.data.id}>
                <div
                    className="homepageSuggestion">
                    <div
                        className="result-icon">
                        <i className={'fa '+icon}/>
                    </div>
                    <div
                        className="result-meta">
                        {metaArr.map((metaItem) => {
                            return <SuggestionMetaItem 
                                title={metaItem[0]}
                                value={metaItem[1]}
                            />
                        })}
                    </div>
                </div>    
            </a>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const Suggestion = connect(mapStateToProps, mapDispatchToProps)(_Suggestion);

exports.Suggestion = Suggestion;