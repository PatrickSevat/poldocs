import React from 'react';
import { connect } from 'react-redux';

const _SuggestionMetaItem = React.createClass({
    // findSearchVal(str) {
    //     let string = Array.isArray(str) ? str[0] : str;
    //     let regex = new RegExp(this.props.state.appReducer.query, 'gi');
    //     let replaced = string.replace(regex, '&&&');
    //
    //     if (replaced.search('&&&') == -1) {
    //         return <span className="suggestionMetaText">{replaced}</span>;
    //     }
    //
    //     let splitArr = replaced.split('&&&');
    //     return (
    //         <span className="suggestionMetaText">
    //         {splitArr[0]}<b className="searchHit">{this.props.state.appReducer.query}</b>{splitArr[1]}
    //     </span>
    //     )
    // },
    
    render: function () {
        // let value = this.findSearchVal(this.props.value);
        return (
            <p className="suggestionMetaItem">
                <b className="suggestionMetaItemTitle">{this.props.title}: </b>{this.props.value}
            </p>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const SuggestionMetaItem = connect(mapStateToProps, mapDispatchToProps)(_SuggestionMetaItem);

exports.SuggestionMetaItem = SuggestionMetaItem;