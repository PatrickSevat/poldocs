import React from 'react';
import { connect } from 'react-redux';
import { hideLogInModal, showError, logIn, showLinkedInModal, homeSuggestionsReceived, query, updatePath } from '../../actions/actions';
import { LinkedInModal } from './../modals/loginLinkedIn.jsx';
import { getData } from '../../ajax/makeAjaxCall';
import { Suggestions } from './Suggestions.jsx';

const _Home = React.createClass({
    componentWillMount () {
        this.props.updatePath('/');
    },
    componentDidMount() {
        if (location.search.search('code') > -1 && location.search.search('state') > -1) {
            this.props.linkedInModal();
        }
        TweenMax.to('#h1-home', 1, {
            text:{
                value: "Poldocs", 
                delimiter: ''
            },
            ease:Linear.easeNone});
        
        TweenMax.to('#tagline', 2.5, {
            text:{
                value: "Find political documents on EU, National and Local levels", 
                delimiter: ''
            },
            delay: 1,
            ease:Linear.easeNone});
    },
    componentWillUnmount() {
        this.props.dataReceived(null);
    },
    search() {
        //TODO remove characters which might interfere with regexp
        let searchVal = document.getElementById('home-search-input').value;
        if (searchVal.length > 2) {
            let start = Date.now();
            getData('/homepageSuggest?q='+searchVal).then((data) => {
                // console.dir(JSON.parse(data));
                console.log('Time between: '+(Date.now()-start));
                // this.processData(JSON.parse(data));
                this.props.dataReceived(JSON.parse(data));
                this.props.query(searchVal);
            }, (err) => {
                console.log(err);
            })
        }
        else if (searchVal.length == 0)
            this.props.dataReceived(null);
    },
    processData(data) {
    //      weights are defined in db\arangoDB\queries\homepageSuggest.js
        let arr = [];
        for (let attr in data) {
            if (!data.hasOwnProperty(attr)) {}
            else {
                if (data[attr].length > 0) {
                    data[attr].forEach((item) => {
                        arr.push(item);
                    })
                }
            }
        }
        this.props.dataReceived(arr);
    },
    render: function () {
        let modal;
        if (this.props.state.appReducer.showLinkedInModal) {
            console.log('showing modal');
            modal = <LinkedInModal />;
        }
        let suggestionsClass = this.props.state.appReducer.homepageSuggestionData === null ? null : 'suggestionsAvailable';
        return (
            <div id="content-container-home">
                <div id="home-modal">
                    <h1 className="display-none">
                        Poldocs
                    </h1>
                    <h2 className="display-none">
                        Find political documents on EU, National and Local levels
                    </h2>
                    <span id="h1-home"></span>
                    <span id="tagline"></span>
                    
                    <div id="front-search">
                        <input
                            type="text"
                            placeholder='Search for text or identifier'
                            onChange={this.search}
                            id="home-search-input"
                            aria-label="Searchfield, enter a search query to find political documents"
                            className={suggestionsClass}
                        />
                        <div
                            id="home-search-submit"
                            aria-label="Submit your search by clicking this button"
                            className={suggestionsClass}
                        >
                            <i className="fa fa-search" />
                        </div>
                    </div>
                    <Suggestions />

                    <p
                        className={"hint "+suggestionsClass}
                    >
                        You can search for identifiers (<em>kst-33885-30</em>, <em>52016XC0427</em>, or <em>C/2016/5661</em>),
                        words and terms (<em>milk products sector</em>, <em>geneesmiddelen</em>, or <em>motie</em>),
                        people (<em>Geert Wilders</em> or <em>Jean-Claude Juncker</em>),
                        and document types (<em>Kamervragen</em> or <em>Directive</em>).
                        <br/>If you have a more advanced query, <a href="/advanced-search">advanced search</a> will suit your needs
                    </p>
                </div>

                { modal }
            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        dataReceived: (data) => {
            dispatch(homeSuggestionsReceived(data));
        },
        LogIn: () => {
            dispatch(logIn());
        },
        linkedInModal: () => {
            dispatch(showLinkedInModal());
        },
        query: (q) => {
            dispatch(query(q));
        },
        updatePath: (url) => {
        dispatch(updatePath(url));
        }
    }
};

const Home = connect(mapStateToProps, mapDispatchToProps)(_Home);

module.exports = Home;