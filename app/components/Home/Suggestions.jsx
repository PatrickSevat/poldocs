import React from 'react';
import { connect } from 'react-redux';
import { Suggestion } from './Suggestion.jsx'

let _Suggestions = React.createClass({
    componentDidMount() {
    },
    render: function () {
        let className = 'display-none';
        let sortedResults = [];
        if (this.props.state.appReducer.homepageSuggestionData !== null) {
            className = 'homepageSuggestions-wrapper';
            console.log('homepageSuggestionData: '+this.props.state.appReducer.homepageSuggestionData.length);
            console.dir(this.props.state.appReducer.homepageSuggestionData);
            // sortedResults = this.props.state.appReducer.homepageSuggestionData.sort((a, b) => b.weight - a.weight);
            // sortedResults = sortedResults.slice(0,10);
            console.dir(sortedResults);
        }


        return (
            <div
                className={className}
            >
                {sortedResults.map((result) => {
                    return <Suggestion
                                data={result}
                            />
                })}
            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const Suggestions = connect(mapStateToProps, mapDispatchToProps)(_Suggestions);

exports.Suggestions = Suggestions;