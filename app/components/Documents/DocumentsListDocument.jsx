import React from 'react';
import { connect } from 'react-redux';
import { Parser } from 'html-to-react';
import {setResultFullView, updateFullViewFileType} from '../../actions/actions';
import {ResultItemTitle} from '../AllResultPages/FullViewElements/ResultItemTitle.jsx';
import {ResultItemSubTitle} from '../AllResultPages/FullViewElements/ResultItemSubTitle.jsx';
import {ResultItemKey} from '../AllResultPages/FullViewElements/ResultItemKey.jsx';
import {DocumentDates} from './DocumentFullViewElements/DocumentDates.jsx';
import {ResultItemAuthors} from '../AllResultPages/FullViewElements/ResultItemAuthors.jsx';
import {DocumentDossiers} from './DocumentFullViewElements/DocumentDossiers.jsx';
import {ResultItemType} from '../AllResultPages/FullViewElements/ResultItemType.jsx';
import {ResultItemCategories} from '../AllResultPages/FullViewElements/ResultItemCategories.jsx';
import {DocumentLinks} from './DocumentFullViewElements/DocumentLinks.jsx';
import {DocumentRelationships} from './DocumentFullViewElements/DocumentRelationships.jsx';

const htmlToReactParser = new Parser();

const _DocumentsListDocument = React.createClass({
    selectFileType(fileType) {
        console.log('fileType: '+fileType);
        this.props.updateFileType(fileType);
    },
    componentDidMount () {
        this.masonry();
    },
    componentDidUpdate () {
        this.masonry();
    },
    componentWillUnmount () {
        const masonry = $('#list-wrapper').data('masonry');
        if (masonry)
            $('#list-wrapper').masonry('destroy');
    },
    masonry () {
        if (this.props.state.appReducer.resultsData != null || this.props.state.appReducer.resultsDataFiltered != null) {
            $('#list-wrapper').masonry({
                itemSelector: '.list-item',
                columnWidth: '#list-item-width', 
                gutter: '#gutter',
                percentPosition: true
            })
        }
    },
    getFullView (fullView) {
        let returnObj = {
            fullText: null,
            viewChoices: []
        };

        //fullViewData for normal document, e.g. in recent documents list
        let fullViewData = fullView;
        //fullViewData for document full view within a dossier
        if (this.props.state.appReducer.showDocumentInDossierTimeline != null)
            fullViewData = this.props.state.appReducer.showDocumentInDossierTimeline;

        //viewType & returnObj.viewChoices needed for determining which iframe will be loaded: pdf, html, etc
        let viewType = this.props.state.appReducer.documentFullViewFileType;

        //Eurlex documents
        if (fullViewData && fullViewData._id == this.props.data._id && fullViewData.level === 'international') {
            //TODO too many viewChoices are represented
            for (let fileType in fullViewData.documentLinks) {
                returnObj.viewChoices.push(fileType)
            }
            if (viewType === 'html')
                returnObj.fullText = <iframe src={fullViewData.documentLinks.html} >Your browser does not support iframes. Unfortunately we cannot show full text documents.</iframe>;
            else if (viewType === 'pdf')
                returnObj.fullText = <iframe src={fullViewData.documentLinks.pdf} >Your browser does not support iframes. Unfortunately we cannot show full text documents.</iframe>;
        }
        //OB documents
        if (fullViewData && fullViewData._id == this.props.data._id && fullViewData.level === 'national') {
            if (this.props.data.documentStatus == 'Onopgemaakt')
                returnObj.fullText = <iframe src={fullViewData.documentLinks.pdf} />
            else {
                let opgemaaktHtml = htmlToReactParser.parse('<div id="opgemaakt-html">'+this.props.data.html+'</div>');
                returnObj.fullText = opgemaaktHtml;
            }
        }
        return returnObj;
    },
    render: function() {
        //determines if this view is part of dossier timeline, if so then add a className
        let partOfDossierTimeline = this.props.state.appReducer.showDocumentInDossierTimeline !== null;

        //Determine if fullView should be shown
        let active = this.props.state.appReducer.resultsFullViewID === this.props.index;
        let resultsFullView = this.props.state.appReducer.resultsFullView != null ? this.props.state.appReducer.resultsFullView : null;

        //fullText contains the iframe with pdf\html with full text document
        let fullView = this.getFullView(resultsFullView);

        return(
            <div 
                className={
                    "list-item"
                    +(active ? ' list-item-active' : '')
                    +(partOfDossierTimeline ? ' timeline-document-full-view' : '')
                }
                id={'doc-list-doc-'+this.props.index}
            >
                {fullView.fullText}
                <table className={"search-result"+(active ? ' search-result-active' : '')} >
                    <ResultItemTitle
                        data={this.props.data}
                        index={this.props.index}
                        active={active}
                        partOfDossierTimeline={partOfDossierTimeline}
                    />
                    <ResultItemType
                        data={this.props.data}
                    />
                    <ResultItemSubTitle
                        data={this.props.data}
                    />
                    <ResultItemKey
                        data={this.props.data}
                    />
                    <DocumentDates
                        data={this.props.data}
                        index={this.props.index}
                    />
                    <ResultItemAuthors
                        data={this.props.data}
                        index={this.props.index}
                    />
                    <DocumentDossiers
                        data={this.props.data}
                        index={this.props.index}
                    />
                    <DocumentRelationships
                        data={this.props.data}
                        index={this.props.index}
                    />
                    <DocumentLinks
                        data={this.props.data}
                        index={this.props.index}
                        fileTypes={fullView.viewChoices}
                    />
                    <ResultItemCategories
                        data={this.props.data}
                        index={this.props.index}
                    />
                </table>
            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        hideDocumentFullView: () => {
            dispatch(setResultFullView(null))
        },
        updateFileType: (filetype) => {
            dispatch(updateFullViewFileType(filetype))
        }
    }
};

const DocumentsListDocument = connect(mapStateToProps, mapDispatchToProps)(_DocumentsListDocument);

exports.DocumentsListDocument = DocumentsListDocument;