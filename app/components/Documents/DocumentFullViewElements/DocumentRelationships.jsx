import React from 'react';
import { connect } from 'react-redux';
import {} from '../../../actions/actions';
import {DocumentRelationshipsItem} from './DocumentRelationshipsItem.jsx';
import {DocumentRelationshipsExternalItem} from './DocumentRelationshipsExternalItem.jsx';

const _DocumentRelationships = React.createClass({
    goToDoc (docID) {
        console.log('go to doc: '+docID);
    },
    render: function() {
        let hasContent = false;
        let relationships = this.props.data.documentRelationships;
        let fullView = (this.props.index === this.props.state.appReducer.resultsFullViewID);
        let relationshipsDisplay = [];

        //Check if there are any relationships
        for (let key in relationships) {
            if (relationships.hasOwnProperty(key) && relationships[key].length > 0) {
                hasContent = true;
                
                if (key === 'externalReferences') {
                    relationshipsDisplay.push(
                        <DocumentRelationshipsExternalItem
                            label={key}
                            items={relationships[key]}
                        />)    
                }
                else {
                    relationshipsDisplay.push(
                        <DocumentRelationshipsItem
                            label={key}
                            items={relationships[key]}
                            fullView={fullView}
                            data={this.props.data}
                        />)
                }
            }
        }
        
        if (!hasContent) {
            return null
        }

        return(
            <tr className="">
                <th className="">
                    Document relationships
                </th>
                <td>
                    {relationshipsDisplay}
                </td>
            </tr>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const DocumentRelationships = connect(mapStateToProps, mapDispatchToProps)(_DocumentRelationships);

exports.DocumentRelationships = DocumentRelationships;