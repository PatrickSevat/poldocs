import React from 'react';
import { connect } from 'react-redux';
import {} from '../../../actions/actions';
import lodash from 'lodash';

const _DocumentRelationshipsItem = React.createClass({
    goToDoc (docID) {
        console.log('go to doc: '+docID);
    },
    setResultFullViewID () {
        this.props.setResultFullView(this.props.data);
        this.props.setResultFullViewID(this.props.data._id)
    },
    render: function() {
        //Make sure that items is an array, if it is a single str, add it to Array
        let items = this.props.items;
        if (!Array.isArray(items))
            items = [items];

        let uniqueItems = lodash.uniq(items);
        let moreSpan, uniqueItemsTotalLength;
        if (!this.props.fullView && uniqueItems.length > 1) {
            uniqueItemsTotalLength = uniqueItems.length;
            uniqueItems = uniqueItems.slice(0,1);
            moreSpan = (
                <span onClick={this.setResultFullViewID}
                      className="looks-like-link">
                    <br />{'+'+(uniqueItemsTotalLength-1)+' more'}
                </span>
            )
        }

        return(
            <p>
                {this.props.label+': '}
                {uniqueItems.map((item, index) => {
                    if (index != uniqueItems.length-1) {
                        return (
                            <span>
                                <span
                                    onClick={() => this.goToDoc(item)}
                                    className="looks-like-link"
                                    key={index}
                                >
                                    {item}
                                </span>
                                <span>, </span>
                            </span>
                        )
                    }
                    else {
                        return (
                            <span>
                                <span
                                    onClick={() => this.goToDoc(item)}
                                    className="looks-like-link"
                                    key={index}
                                >
                                    {item}
                                </span>
                                {moreSpan}
                            </span>
                        )
                    }
                })}
            </p>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const DocumentRelationshipsItem = connect(mapStateToProps, mapDispatchToProps)(_DocumentRelationshipsItem);

exports.DocumentRelationshipsItem = DocumentRelationshipsItem;