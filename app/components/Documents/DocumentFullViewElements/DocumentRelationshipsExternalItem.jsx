import React from 'react';
import { connect } from 'react-redux';
import {} from '../../../actions/actions';
import lodash from 'lodash';

const _DocumentRelationshipsExternalItem = React.createClass({
    trimUrl (str) {
        if (str.search('http://www.') > -1) {
            return str.substr(11, 21)+'...';
        }
        else if (str.search('https://www.') > -1) {
            return str.substr(12, 22)+'...';
        }
        else if (str.search('http://') > -1) {
            return str.substr(7, 17)+'...';
        }
        else if (str.search('https://') > -1) {
            return str.substr(8, 18)+'...';
        }
        else
            return str.substr(0,10)+'...';
    },
    render: function() {
        let uniqueItems = lodash.uniq(this.props.items);
        return(
            <span>
                {this.props.label+': '}
                {uniqueItems.map((item, index) => {
                    if (typeof item === 'undefined')
                        return null;
                    if (index != uniqueItems.length-1) {
                        return (
                            <span>
                                <a href={item}
                                    className="looks-like-link"
                                    key={index}
                                >
                                    {item.length > 10 ? this.trimUrl(item) : item}
                                </a>
                                <span>, </span>
                            </span>
                        )
                    }
                    else {
                        return (
                            <a href={item}
                               className="looks-like-link"
                               key={index}
                            >
                                {item.length > 10 ? this.trimUrl(item) : item}
                            </a>
                        )
                    }
                })}
            </span>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const DocumentRelationshipsExternalItem = connect(mapStateToProps, mapDispatchToProps)(_DocumentRelationshipsExternalItem);

exports.DocumentRelationshipsExternalItem = DocumentRelationshipsExternalItem;