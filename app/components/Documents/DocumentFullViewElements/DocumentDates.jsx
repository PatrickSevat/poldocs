import React from 'react';
import { connect } from 'react-redux';
import {} from '../../../actions/actions';

const _DocumentDates = React.createClass({
    getDisplayName(oldName) {
        let displayNameObj = {
            dateImported: 'Date imported',
            documentDate: 'Document date',
            dateModified: 'Last modified',
            availableDate: 'Available since',
            dateCreated: 'Created on',
            entryIntoForceDate: 'Will enter into force on',
            endOfValidityDate: 'Will no longer be in force on',
            deadline: 'Deadline',
            dateMeeting: 'Date of meeting in which document is discussed'
        };
        return displayNameObj[oldName];
    },
    render: function() {
        if (this.props.state.appReducer.resultListType !== 'documents')
            return null;
        
        let display = [];
        let dateNames = ['availableDate', 'documentDate', 'dateImported', 'dateModified', 'dateCreated',
            'entryIntoForceDate', 'endOfValidityDate', 'deadline', 'dateMeeting'];

        dateNames.map((dateName) => {
            if (typeof this.props.data[dateName] != 'undefined' && this.props.data[dateName] != ''
                && this.props.data[dateName] != null) {
                    let isoString = new Date(this.props.data[dateName]);
                    let readableStr = isoString.toLocaleDateString();

                    display.push({
                        label: this.getDisplayName(dateName),
                        value: readableStr
                    })
            }
        });

        if (display.length > 2 && this.props.state.appReducer.resultsFullViewID !== this.props.index)
            display = display.slice(0,2);

        return(
            <tr className="">
                <th className="">
                    Dates
                </th>
                <td>
                    {display.map((item, index) => {
                        if (index !== display.length-1) {
                            return <span>{item.label}: {item.value}<br /></span>
                        }
                        else return <span>{item.label}: {item.value}</span>
                    })}
                </td>
            </tr>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const DocumentDates = connect(mapStateToProps, mapDispatchToProps)(_DocumentDates);

exports.DocumentDates = DocumentDates;