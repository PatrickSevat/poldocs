import React from 'react';
import { connect } from 'react-redux';
import {} from '../../../actions/actions';

const _DocumentDossiers = React.createClass({
    goToDossier (dossierName) {
        console.log('go to dossier: '+dossierName);
    },
    render: function() {
        let dossiers = [];

        if (this.props.data.level === 'national') {
            if (Array.isArray(this.props.data.dossier)) dossiers = this.props.data.dossier;
            else if (typeof this.props.data.dossier == 'undefined' || this.props.data.dossier == '') return null;
            else dossiers.push(this.props.data.dossier);
        }
        else if (this.props.data.level === 'international') {
            if (Array.isArray(this.props.data.dossierIDDashed) && this.props.data.dossierIDDashed.length > 0) dossiers = this.props.data.dossierIDDashed;
            else if (typeof this.props.data.dossierIDDashed == 'undefined' || this.props.data.dossierIDDashed == '') return null;
            else dossiers.push(this.props.data.dossierIDDashed);
        }

        return(
            <tr className="">
                <th className="">
                    Dossiers
                </th>
                <td>
                    {dossiers.map((dossier, index) => {
                        let dossierTitle = (typeof this.props.data.dossierTitle !== 'undefined' && this.props.data.dossierTitle !== '') ? ' ('+this.props.data.dossierTitle+')' : '';
                        if (dossierTitle.length > 100 && this.props.state.appReducer.resultsFullViewID !== this.props.index ) {
                            dossierTitle = dossierTitle.slice(0,97)+'...';
                        }
                        let dossierID = this.props.data.level === 'international' ? dossier.replace(/-/g, '/') : dossier;
                        if (index !== dossiers.length-1) {
                            return (
                                <span>
                                    <span
                                        className="looks-like-link"
                                        onClick={() => this.goToDossier(dossier)}
                                        key={index}
                                    >
                                        {dossierID+dossierTitle}
                                    </span>
                                    <span>, </span>
                                </span>
                            )
                        }
                        else {
                            return (
                                <span className="looks-like-link"
                                      onClick={() => this.goToDossier(dossier)}
                                      key={index}
                                >
                                    {dossierID+dossierTitle}
                                </span>
                            )
                        }
                    })}
                </td>
            </tr>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const DocumentDossiers = connect(mapStateToProps, mapDispatchToProps)(_DocumentDossiers);

exports.DocumentDossiers = DocumentDossiers;