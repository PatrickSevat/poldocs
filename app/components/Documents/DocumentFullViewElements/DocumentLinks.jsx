import React from 'react';
import { connect } from 'react-redux';
import {updateFullViewFileType, setResultFullView, setResultFullViewID} from '../../../actions/actions';

const _DocumentLinks = React.createClass({
    openFullText(fileType) {
        if (this.props.state.appReducer.resultsFullView == null) {
            this.props.setDocumentFullView(this.props.data);
            this.props.setDocumentFullViewID(this.props.index);
            // TweenMax.to('#doc-list-doc-'+this.props.index, 1, {width:"66vw", height: '75vh'})
        }
        this.props.updateFileType(fileType);
    },
    render: function() {
        if (typeof this.props.data.documentLinks === 'undefined' || this.props.data.documentLinks=== '') {
            return null
        }


        //TODO replace level with source once db is reimported

        let viewChoices = [];

        if (this.props.data.level === 'international' && Array.isArray(this.props.data.documentLinks)) {
            for (let fileType in this.props.state.appReducer.resultsFullView.documentLinks) {
                viewChoices.push(fileType)
            }
        }
        else if (this.props.data.source == 'officieleBekendmakingen' && this.props.data.documentStatus == 'Onopgemaakt')
            viewChoices.push('pdf');
        else
            viewChoices.push('pdf', 'html');

        // console.log(viewChoices);

        //Eurlex has obj
        //OB has link(s] which depending on documentStatus is PDF or HTML + PDF

        return(
            <tr className="">
                <th className="">
                    Full text
                </th>
                <td className="">
                    {viewChoices.map((type) => {
                        return (
                            <i
                                        onClick={() => this.openFullText(type)}
                                        className={"file-type-icon icon-icon--document-file-"+type} />
                        )
                    })}
                </td>
            </tr>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateFileType: (filetype) => {
            dispatch(updateFullViewFileType(filetype))
        },
        setDocumentFullView: (data) => {
            dispatch(setResultFullView(data));
        },
        setDocumentFullViewID: (ID) => {
            console.log('setFullViewID: '+ID);
            dispatch(setResultFullViewID(ID))
        }
    }
};

const DocumentLinks= connect(mapStateToProps, mapDispatchToProps)(_DocumentLinks);

exports.DocumentLinks= DocumentLinks;