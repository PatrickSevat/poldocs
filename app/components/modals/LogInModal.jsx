import React from 'react';
import { Modal, Button, FormGroup, ControlLabel, FormControl, Image } from 'react-bootstrap';
import { connect } from 'react-redux';
import { hideLogInModal, showError, logIn, showLinkedInModal } from '../../actions/actions';
import { WellAlert } from '../WellAlert.jsx';

const _LogInModal = React.createClass({
    submitLogIn() {
        let username = document.getElementById('loginUsername').value;
        let password = document.getElementById('loginPassword').value;
        let data = { 'username': username, 'password': password};
        this.submitRequest('/api/auth/authenticate/local', 'POST', data);
    },
    logInLinkedIn() {
        window.location = '/api/auth/authenticate/linkedin';
    },
    submitRequest(url, type, data) {
        $.ajax({
            url,
            type,
            data: data,
            success: (data) => {
                console.log('login successful, data: ');
                console.log(data);
                window.localStorage.setItem('user_id', data.user['_id']);
                window.localStorage.setItem('username', data.user.username);
                window.localStorage.setItem('token', data.token);
                this.props.LogIn();
                setTimeout(this.props.hideLogInModal, 1000);
            },
            error: (err) => {
                console.log('login failed: err');
                console.log(err);
                this.props.ShowError(err);
            }
        });
    },
    componentDidMount() {
    },
    render: function () {
        console.log('rendering loginmodal');
        let wellMessage, wellStyle, wellIcon;
        if (this.props.state.appReducer.loggedIn) {
            wellMessage = 'Succesfully logged in';
            wellStyle= 'success';
        }
        else {
            wellMessage = 'Click here to sign up';
            wellStyle = 'info';
            wellIcon = 'fa-user-plus'
        }
        if (this.props.state.appReducer.error) {
            console.log('well error: '+this.props.state.appReducer.error);
            wellMessage = this.props.state.appReducer.error;
            wellStyle = 'warning';
            wellIcon = 'fa-exclamation';
        }
        return (
            <Modal show={this.props.state.appReducer.showLogInModal} onHide={this.props.hideLogInModal} className="poldocs-modal">
                <Modal.Header closeButton>
                    <Modal.Title>LOG IN</Modal.Title>
                </Modal.Header>
                <Modal.Body id="modal-body-log-in">
                    <WellAlert 
                        wellMessage={wellMessage}
                        wellStyle={wellStyle}
                        wellIcon={wellIcon}
                        showWell={true} />
                    <div id="modal-body-log-in-options">
                        <form id="log-in-form" >
                            <FormGroup>
                                <ControlLabel>Username</ControlLabel>
                                <FormControl type="text" id="loginUsername" name="username" />
                            </FormGroup>
                            <FormGroup>
                                <ControlLabel>Password</ControlLabel>
                                <FormControl type="password" id="loginPassword" name="password"/>
                            </FormGroup>
                            <Button
                                id="login-submit"
                                className="poldocs-button"
                                onClick={this.submitLogIn}
                            >
                                Submit
                            </Button>
                            <Button
                                onClick={this.props.hideLogInModal}
                                className="poldocs-button"
                            >
                                Close
                            </Button>
                        </form>
                        <div id="modal-body-log-in-social-strategies">
                            <div id="linkedInButtonWrapper">
                                <Image src="/static/img/Sign-In-Small---Default.png" id="linkedInLogIn" onClick={this.logInLinkedIn}/>
                            </div>
                        </div>
                    </div>

                </Modal.Body>
            </Modal>
        )
    }

});


const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        LogIn: () => {
            dispatch(logIn());
        },
        linkedInModal: () => {
            dispatch(showLinkedInModal());  
        },
        hideLogInModal: () => {
            dispatch(hideLogInModal())
        },
        ShowError: (err) => {
            dispatch(showError(err))
        }
    }
};

const LogInModal = connect(mapStateToProps, mapDispatchToProps)(_LogInModal);

exports.LogInModal = LogInModal;