import React from 'react';
import { Modal, Button, FormGroup, ControlLabel, FormControl, Image } from 'react-bootstrap';
import { connect } from 'react-redux';
import { hideLinkedInModal, showError, logIn } from '../../actions/actions';
import { WellAlert } from '../WellAlert.jsx';

const _loginLinkedIn = React.createClass({
    componentDidMount() {
        $.ajax({
            url: '/api/auth/authenticate/processLinkedIn'+location.search,
            type: 'POST',
            success: (data) => {
                console.log('login successful, data: ');
                console.log(data);
                window.localStorage.setItem('user_id', data.user['_id']);
                window.localStorage.setItem('username', data.user.username);
                window.localStorage.setItem('token', data.token);
                this.props.LogIn();
                setTimeout(this.props.hideLinkedInModal, 1000);
            },
            error: (err) => {
                console.log('login failed: err');
                console.log(err);
                this.props.ShowError(err);
            }
        });
    },
    render: function () {
        let wellMessage;
        let wellStyle;
        if (this.props.state.appReducer.loggedIn) {
            wellMessage = 'Succesfully logged in';
            wellStyle= 'success';

        }
        else {
            wellMessage = 'Processing...';
            wellStyle = 'info'
        }
        return (
            <Modal 
                show={this.props.state.appReducer.showLinkedInModal} 
                onHide={this.props.hideLinkedInModal}
                className="poldocs-modal"
            >
                <Modal.Header closeButton>
                    <Modal.Title>Processing user data from LinkedIn</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <WellAlert wellMessage={wellMessage} wellStyle={wellStyle} showWell={true} />
                        <Button
                            onClick={this.props.hideLinkedInModal}
                            className="poldocs-button"
                        >
                            Close
                        </Button>
                </Modal.Body>
            </Modal>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        LogIn: () => {
            dispatch(logIn());
        },
        hideLinkedInModal: () => {
            window.location = '/';
            dispatch(hideLinkedInModal())
        },
        ShowError: (err) => {
            dispatch(showError(err))
        }
    }
};

const loginLinkedIn = connect(mapStateToProps, mapDispatchToProps)(_loginLinkedIn);

exports.LinkedInModal = loginLinkedIn;