/**
 * Created by Patrick on 06/04/2016.
 */
var React = require('react');
var Griddle = React.createFactory(require('griddle-react'));
var resultsPerPage = 1;
import {Modal} from 'react-bootstrap';

console.log('Table.jsx called');

var Table = React.createClass({
    getInitialState(){
        return {
            loading: true,
            data: []
        }
    },
    componentDidMount() {
        console.log('component did mount');
        console.log('this.props.params.query');
        console.log(this.props.params.query);
        this.serverRequest = $.get('http://localhost:3000/api?'+this.props.params.query, function (result) {
            this.setState({
                loading: false,
                data: result
            });
        }.bind(this));
    },
    componentWillUnmount: function() {
        this.serverRequest.abort();
    },
    render: function () {
        console.log('rendering Table');
        console.log('params.query in Table.jsx');
        console.log(this.props.params.query);
        console.log('state:');
        console.log(this.state);
        return (
            <div id="table-area">
                <Modal backdrop="static" show={this.state.loading ? true : false} bsSize="small">
                    <Modal.Header>Loading</Modal.Header>
                </Modal>
                <Griddle results={this.state.data}
                         resultsPerPage={resultsPerPage}
                />
            </div>
        )
    }
});

module.exports = Table;