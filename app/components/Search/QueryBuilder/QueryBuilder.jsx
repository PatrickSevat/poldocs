import React from 'react';
import { connect } from 'react-redux';
import { selectQueryBuilderPage } from '../../../actions/actions';
import { QueryBuilderPaginator } from './QueryBuilderPaginator.jsx';
import { QueryBuilderPage1 } from './QueryBuilderPages/QueryBuilderPage1.jsx';
import { QueryBuilderPage2 } from './QueryBuilderPages/QueryBuilderPage2.jsx';
import { QueryBuilderPage3 } from './QueryBuilderPages/QueryBuilderPage3.jsx';
import { QueryBuilderPage4 } from './QueryBuilderPages/QueryBuilderPage4.jsx';
import { QueryBuilderPage5 } from './QueryBuilderPages/QueryBuilderPage5.jsx';

const _QueryBuilder = React.createClass({
    componentWillMount () {
        
    },
    componentDidMount() {
        if (this.props.state.appReducer.queryBuilderPage == null) {
            this.props.selectQueryBuilderPage(1);
        }
    },
    componentWillUnmount: function() {
    },
    render: function() {
        //TODO Idea for date in savedSearches: run since last visit
        let page;
        switch (this.props.state.appReducer.queryBuilderPage) {
            case 1:
                page = <QueryBuilderPage1 />;
                break;
            case 2:
                page = <QueryBuilderPage2 />;
                break;
            case 3:
                page = <QueryBuilderPage3 />;
                break;
            case 4:
                page = <QueryBuilderPage4 />;
                break;
            case 5:
                page = <QueryBuilderPage5 />;
                break;
            default:
                page = <QueryBuilderPage1 />
        }
            
        
        
        return(
            <div id="query-builder" className={'query-builder'+(this.props.loggedIn ? '-logged-in' : '')}>
                <div className="query-builder-title">
                    <h1 className="">
                        Query builder
                        <i className="fa fa-search" />
                    </h1>
                </div>
                <div id="query-builder-page-container">
                    <div id="query-builder-page-wrapper">
                        <QueryBuilderPaginator />
                        {page}
                    </div>
                    
                </div>
            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        selectQueryBuilderPage: (pageNum) => {
            dispatch(selectQueryBuilderPage(pageNum));
        }
    }
};

const QueryBuilder = connect(mapStateToProps, mapDispatchToProps)(_QueryBuilder);

exports.QueryBuilder = QueryBuilder;