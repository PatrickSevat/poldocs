import React from 'react';
import { connect } from 'react-redux';
import { selectQueryBuilderPage } from '../../../actions/actions';

const _QueryBuilderPaginator = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
    },
    componentWillUnmount: function() {
    },
    render: function() {
        let numbers = [];
        for (let i = 1; i < 6; i++) {
            let active;
            if ((i == 1 && this.props.state.appReducer.queryBuilderPage == null) || (i == this.props.state.appReducer.queryBuilderPage))
                active = true;

            numbers.push(
                <div 
                    className={"query-builder-paginator-circle"+ (active ? " query-builder-paginator-circle-active" : "")}
                    onClick={() => this.props.selectQueryBuilderPage(i)}
                    key={i}
                >
                    <span>{i}</span>
                </div>
            )
        }

        return(
            <div id="query-builder-paginator">
                <div id="query-builder-paginator-line"></div>
                {numbers}
            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        selectQueryBuilderPage: (pageNum) => {
            dispatch(selectQueryBuilderPage(pageNum));
        }
    }
};

const QueryBuilderPaginator = connect(mapStateToProps, mapDispatchToProps)(_QueryBuilderPaginator);

exports.QueryBuilderPaginator = QueryBuilderPaginator;