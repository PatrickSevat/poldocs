import React from 'react';
import { connect } from 'react-redux';
import { queryBuilderShowDocumentTypes } from '../../../../actions/actions';
import {Page3SelectorSearchForType } from './QueryBuilderPagesElements/Page3SelectorSearchForType.jsx'
import { QueryBuilderPageButtons } from './QueryBuilderPageButtons.jsx'

const _QueryBuilderPage3 = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
    },
    componentWillUnmount: function() {
    },
    render: function() {
        let documentTypes = this.props.state.appReducer.queryBuilderDocumentTypesVisible ? <Page3SelectorSearchForType /> : null;
        let disabled = false;
        
        return(
            <div id="query-builder-selector">
                <h2>
                    Subtype(s)<br/>
                    <small>(optional)</small>
                </h2>
                <div id="query-builder-selector-page-3-select-document-type">
                    <div aria-hidden="true"><span>Would you like to specify the document type (e.g. 'Kamerstuk' or 'Preparatory act')?</span></div>
                    <div className="on-off-slider">
                        <input
                            type="checkbox"
                            name="document-type-toggle"
                            id="document-type-toggle"
                            onClick={this.props.queryBuilderDocumentTypeToggle}
                        />
                        <label htmlFor="document-type-toggle">
                            <span className="sr-only">Would you like to specify the document type (e.g. 'Kamerstuk' or 'Preparatory act'?</span>
                        </label>    
                    </div>
                    

                    <br/>
                </div>
                { documentTypes }
                <QueryBuilderPageButtons />        
            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        queryBuilderDocumentTypeToggle: () => {
            dispatch(queryBuilderShowDocumentTypes())
        }
    }
};

const QueryBuilderPage3 = connect(mapStateToProps, mapDispatchToProps)(_QueryBuilderPage3);

exports.QueryBuilderPage3 = QueryBuilderPage3;