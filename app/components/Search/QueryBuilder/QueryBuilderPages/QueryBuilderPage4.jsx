import React from 'react';
import { connect } from 'react-redux';
import { queryBuilderSelectDate, queryBuilderSetDate } from '../../../../actions/actions';
import { QueryBuilderPageButtons } from './QueryBuilderPageButtons.jsx'
const DatePicker = require('react-datepicker');
import moment from 'moment'


const _QueryBuilderPage4 = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
    },
    componentWillUnmount: function() {
    },
    humanReadableLabel(label) {
        let datesHumanReadable = {
            dateImported: 'Date imported',
            documentDate: 'Document date',
            dateModified: 'Last modified',
            availableDate: 'Available since',
            dateCreated: 'Created on',
            entryIntoForceDate: 'Will enter into force on',
            endOfValidityDate: 'Will no longer be in force on',
            deadline: 'Deadline',
            dateMeeting: 'Date of meeting in which document is discussed'
        };
        return datesHumanReadable[label];
    },
    setDatePicker() {
        let args = Array.from(arguments);
        this.props.setDate(args[0], args[1], args[2])
    },
    render: function() {
        let disabled = false;
        let disabledText = null;
        let datesMachineLabel = ['dateImported', 'documentDate', 'dateModified', 'availableDate', 'dateCreated', 'entryIntoForceDate',
        'endOfValidityDate', 'deadline', 'dateMeeting'];

        return(
            <div id="query-builder-selector">
                <h2>Dates<br/>
                    <small>(optional)</small></h2>
                <div id="query-builder-selector-page-4">
                    <div id="date-selectors-container">
                        {datesMachineLabel.map((dateLabel, dateLabelIndex) => {
                            let selected = this.props.state.appReducer.queryBuilderValues.dates[dateLabel].selected;
                            let datePickers;

                            if (selected)
                                datePickers = (
                                    <div className="date-pickers">
                                        <DatePicker
                                            dateFormat="DD/MM/YYYY"
                                            selected={this.props.state.appReducer.queryBuilderValues.dates[dateLabel].startDate}
                                            selectsStart
                                            startDate={this.props.state.appReducer.queryBuilderValues.dates[dateLabel].startDate}
                                            endDate={this.props.state.appReducer.queryBuilderValues.dates[dateLabel].endDate}
                                            showMonthDropdown
                                            showYearDropdown
                                            dropdownMode="select"
                                            isClearable={true}
                                            onChange={this.setDatePicker.bind(this, dateLabel, 'start')}
                                        />
                                        <DatePicker
                                            dateFormat="DD/MM/YYYY"
                                            selected={this.props.state.appReducer.queryBuilderValues.dates[dateLabel].endDate}
                                            selectsEnd
                                            startDate={this.props.state.appReducer.queryBuilderValues.dates[dateLabel].startDate}
                                            endDate={this.props.state.appReducer.queryBuilderValues.dates[dateLabel].endDate}
                                            showMonthDropdown
                                            showYearDropdown
                                            dropdownMode="select"
                                            isClearable={true}
                                            onChange={this.setDatePicker.bind(this, dateLabel, 'end')}
                                        />
                                    </div>
                                )

                            return(
                                <div
                                    className="date-selector-wrapper"
                                    key={"date-selector-"+dateLabelIndex}
                                >
                                    <input
                                        type="checkbox"
                                        name={this.humanReadableLabel(dateLabel)}
                                        id={this.humanReadableLabel(dateLabel)}
                                        onClick={() => this.props.selectDate(dateLabel)}
                                    />
                                    <label htmlFor={this.humanReadableLabel(dateLabel)}>
                                        {this.humanReadableLabel(dateLabel)}
                                    </label>
                                    {datePickers}
                                </div>
                            )
                        })}
                    </div>
                    <QueryBuilderPageButtons
                        disabled={disabled}
                        warningText={disabledText}
                    />
                </div>


            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        selectDate: (dateLabel) => {
            dispatch(queryBuilderSelectDate(dateLabel));
        },
        setDate: (dateLabel, startEndStr, moment) => {
            dispatch(queryBuilderSetDate(dateLabel, startEndStr, moment))
        }
    }
};

const QueryBuilderPage4 = connect(mapStateToProps, mapDispatchToProps)(_QueryBuilderPage4);

exports.QueryBuilderPage4 = QueryBuilderPage4;