import React from 'react';
import { connect } from 'react-redux';
import { selectQueryBuilderPage, executeQuery, resultsReceived } from '../../../../actions/actions';
import { queryConstructor } from './queryConstructor';
import { getSearchData } from '../../../../ajax/makeAjaxCall';

const _QueryBuilderPageButtons = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
    },
    componentWillUnmount: function() {
    },
    startSearch (disabled) {
        if (!disabled) {
            console.log('start search with these values:');
            console.log(this.props.state.appReducer.queryBuilderValues);
            let query = queryConstructor(this.props.state.appReducer.queryBuilderValues);
            this.props.executeQuery(query);
            let start = Date.now();
            getSearchData(query).then((data) => {
                // console.log('ajax data:');
                // console.log(data);
                //TODO handle dossier results
                let parsed = JSON.parse(data);
                this.props.resultsReceived(parsed[0][0]['documentsGroup0'].results, 'search', parsed[0][0]['documentsGroup0'].count);
                console.log(Date.now() - start);
            }, (err) => {
                console.log('ajax err:');
                console.log(err);
            })
        }
    },
    nextPage (disabled) {
        if (!disabled)
            this.props.selectQueryBuilderPage(this.props.state.appReducer.queryBuilderPage+1);
    },
    render: function() {
        //TODO perhaps better to make a bool in reducer for disabled, so that paginator can also use it
        let disabled = this.props.disabled;
        let disabledText = <div className="warning-text">
            {this.props.warningText}
        </div>;
        let previous, next;
        if (this.props.state.appReducer.queryBuilderPage > 1)
            previous = (
                <span
                    className={"float-left poldocs-button"+ (disabled ? " poldocs-button-disabled" : "")}
                    onClick={() => this.startSearch(disabled)}
                >
                        Go back to previous step
                </span>
            );

        if (this.props.state.appReducer.queryBuilderPage < 5)
            next = (
                <span
                    className={"poldocs-button"+ (disabled ? " poldocs-button-disabled" : "")}
                    onClick={() => this.nextPage(disabled)}
                >
                        Continue query builder
                </span>
            );

        return(
            <div className="query-builder-buttons">
                {disabled ? disabledText : null}
                {previous}
                    <span
                        className={"poldocs-button"+ (disabled ? " poldocs-button-disabled" : "")}
                        onClick={() => this.startSearch(disabled)}
                    >
                        Start searching with these filters
                    </span>
                {next}
            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        selectQueryBuilderPage: (pageNum) => {
            dispatch(selectQueryBuilderPage(pageNum));
        },
        executeQuery: (query) => {
            dispatch(executeQuery(query));
        },
        resultsReceived: (data, type, count) => {
            dispatch(resultsReceived(data, type, count))
        }
    }
};

const QueryBuilderPageButtons = connect(mapStateToProps, mapDispatchToProps)(_QueryBuilderPageButtons);

exports.QueryBuilderPageButtons = QueryBuilderPageButtons;