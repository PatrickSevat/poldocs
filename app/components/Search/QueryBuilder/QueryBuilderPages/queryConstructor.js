function queryConstructor(queryBuilderValues, offset = 0, limit = 24) {
    let query = '';


    let collections = queryBuilderValues.collections; //Object {documents: bool, dossiers: bool}
    let text = queryBuilderValues.text; //Array [{elements: [{choice: 'String', extras: ['Str'], relationship: String, searchValue: String}], relationship: bool, searchAttributes: []}]
    let groupQueryNameArr = [];

    //Start with collections as they will be used as first-level subquery
    for (let collection in queryBuilderValues.collections) {
        if (queryBuilderValues.collections.hasOwnProperty(collection) && queryBuilderValues.collections[collection] == true) {
            text.map((group, groupIndex) => {
                //TODO currently work for group.relationship OR. If AND => filter
                //elementQueryNameArr stores name of each second-level subquery
                let elementQueryNameArr = [];

                //Group first-level subquery per collection
                query += `let ${collection+'Group'+groupIndex} = (`;
                groupQueryNameArr.push(collection+'Group'+groupIndex);

                //Determine if any elements within a group has an 'Or'-relationship with a previous element
                let elementContainsOrRelationship = groupContainsElementWithOrRelationship(group.elements);

                //If there is an 'Or'-relationship in the element, the element is split in multiple second-level sub queries per searchAttr:
                //This means that E1: 'milk && juice' AND E2 '-beer*' OR E3  'teeth*,dental*,tooth*' AND E4 'candy,|sweets'
                //Becomes subquery1: ('milk,juice,-beer') subquery2: ('candy,|sweets,teeth*,dental*,tooth*)
                if (elementContainsOrRelationship) {
                    let orRelationship = createElementWithOrRelationship(group, collection);
                    query += orRelationship.query;
                    orRelationship.elementQueryNames.map((name) => elementQueryNameArr.push(name))
                }
                //No 'Or'-relationship means a relatively straightforward second-level subquery per searchAttr
                else {
                    let withoutOrRelationship = createElementWithoutOrRelationship(group, collection);
                    query += withoutOrRelationship.query;
                    withoutOrRelationship.elementQueryNames.map((name) => elementQueryNameArr.push(name))
                }

                // if (collection == 'dossiers') {
                //     console.log('elementQueryNameArr');
                //     console.log(elementQueryNameArr);
                //
                // }
                // Filter elementQueryName arr with undefined value from fullText (which is not available in dossiers)
                elementQueryNameArr = elementQueryNameArr.filter((item) => {
                    if (typeof item != 'undefined') return item
                });

                //Group all second level subqueries
                query += `\nlet union${collection+'Group'+groupIndex} = union(${elementQueryNameArr.join(', ')})`;
                //Slice results based on offset
                query += `\nlet union${collection+'Group'+groupIndex}Sliced = SLICE(union${collection+'Group'+groupIndex}, ${offset}, ${limit})`;
                //Return sliced results + total count
                query += `\nreturn {${collection+'Group'+groupIndex}: {results: union${collection+'Group'+groupIndex}Sliced, count: length(union${collection+'Group'+groupIndex})}}\n)\n\n`;
            });
        }
    }
    query+= `\n return union(${groupQueryNameArr.join(', ')})`;

    
    // console.log('query:');
    // console.log(query);
    return query;
}

function createElementWithOrRelationship(group, collection) {
    //Or-relationship are handled as splitters, as soon as an Or-relationship is encountered, a new subquery is created
    if (group.elements.length ==1 ) {
        return createElementWithoutOrRelationship(group, collection)
    }
    else {
        //Split the elements as soon as a 'Or'-relationship is encountered
        //This means that E1: 'milk && juice' AND E2 '-beer*' OR E3  'teeth*,dental*,tooth*' AND E4 'candy,|sweets'
        //Becomes subquery1: ('milk,juice,-beer') subquery2: ('candy,|sweets,teeth*,dental*,tooth*)
        let indicesOfElementsWithOrRelationship = [];
        group.elements.map((element, elementIndex) => {
            if (element.relationship == 'Or') {
                indicesOfElementsWithOrRelationship.push(elementIndex);
            }
        });

        //Split elements group
        let elementGroups= splitElementsGroup(group, indicesOfElementsWithOrRelationship);
        
        let aggregatedQuery = '';
        let aggregatedElementQueryNames = [];
        elementGroups.map((element, index) => {
            let groupWithSlicedElement = Object.assign({}, group, {elements: element});
            let sliceElementObj = createElementWithoutOrRelationship(groupWithSlicedElement, collection, index);
            aggregatedQuery += sliceElementObj.query;
            aggregatedElementQueryNames.push(sliceElementObj.elementQueryNames);
        });

        return {query: aggregatedQuery, elementQueryNames: aggregatedElementQueryNames == '' ? null : aggregatedElementQueryNames}
    }
}

function createElementWithoutOrRelationship(group, collection, index) {
    let elementQueryNameArr = [];
    let query = '';
    let searchValuesPrefixed = sortAggregateAndPrefixSearchValues(group.elements);
    // output: {valuesWithExtra: [{searchValue: 'String prefixed', extras: extras}], valuesOnly: ['String prefixed']}

    //Create a subQuery for each searchAttr ('title', 'fulltext', etc.)
    group.searchAttributes.map((searchAttr) => {
        let searchAttrMachine = getSearchAttrMachineReadable(searchAttr);
        let elementSubQueryObj = createElementSubQuery(searchValuesPrefixed, searchAttrMachine, collection, index);
        query += elementSubQueryObj.query;
        elementQueryNameArr.push(elementSubQueryObj.elementName);
    });

    return {query, elementQueryNames: elementQueryNameArr == '' ? null : elementQueryNameArr }
}

function createElementSubQuery(searchValuesObj, searchAttrMachine, collection, index) {
    let returnObj = {query: '', elementName: ''};
    let elementName = typeof index == 'undefined' ? 'elementAttribute'+searchAttrMachine : 'elementAttribute'+searchAttrMachine+index;

    //Exclude fulltext in dossiers
    if (searchAttrMachine == 'fullText' && collection == 'dossiers') {}
    else {
        returnObj.query += `\n    let ${elementName} = (\n    for element in FULLTEXT(${collection}, '${searchAttrMachine}', '${searchValuesObj.valuesOnly.join(',')}')`;
        returnObj.query += getExtrasFilter(searchValuesObj.valuesWithExtra, searchAttrMachine);
        returnObj.query += `\n    return element\n    )`;
        returnObj.elementName = elementName;
    }
    return returnObj;
}

function groupContainsElementWithOrRelationship(elements) {
    let returnBool = false;
    elements.map((element) => {
        if (element.relationship == 'Or') {
            returnBool = true
        }
    });

    return returnBool;
}

function getSearchValues(searchValue, extras) {
    if (extras.indexOf('Exact match') === -1) {
        return searchValue.split(' ');
    }
    else
        return [searchValue]
}

function getPrefixedSearchValues(searchValueArr, extras, choice) {
    let searchValueArrPrefixed =[];
    searchValueArr.map((searchValue, searchValueIndex) => {
        if (extras.indexOf('Starts with') > -1) {
            searchValue = 'prefix:'+searchValue
        }
        if (choice === 'Or' && searchValueIndex != 0)
            searchValue = '|'+searchValue;
        if (choice === 'Exclude')
            searchValue = '-'+searchValue;
        searchValueArrPrefixed.push(searchValue)
    });

    let searchValuesPrefixedJoined = searchValueArrPrefixed.join(',');

    return {valuesWithExtra: {searchValue: searchValuesPrefixedJoined, searchValueRaw: searchValueArr, extras}, valuesOnly: searchValuesPrefixedJoined}
}

function getSearchAttrMachineReadable(searchAttr) {
    let translateObj = {
        "Title": 'title',
        'Subtitle': 'subTitle',
        'ID': '_key',
        'Full text': 'fullText'
    };
    return translateObj[searchAttr]
}

//This function aims to lower to number of subQueries by aggregating queries where possible
function sortAggregateAndPrefixSearchValues(elements) {

    //In order for query to work properly, Choice == 'Or' values need to be added first, so sort on choice
    let elementsSorted = elements.sort((a,b) => {
        if (a.choice == 'Or')
            return -1;
        else
            return 0;
    });

    let searchValuesPrefixed =[];
    elementsSorted.map((element, elementIndex) => {
        //Split element.searchValue if necessary & prefix if necessary
        let searchValuesArr = getSearchValues(element.searchValue, element.extras);
        searchValuesPrefixed.push(getPrefixedSearchValues(searchValuesArr, element.extras, element.choice));
    });

    let searchValuesOnly = [];
    let searchValuesWithExtras = [];
    searchValuesPrefixed.map((element) => {
        searchValuesOnly.push(element.valuesOnly);
        searchValuesWithExtras.push(element.valuesWithExtra);
    });

    return {valuesWithExtra: searchValuesWithExtras, valuesOnly: searchValuesOnly}
}

function getExtrasFilter(elementsWithExtrasArr, searchAttrMachine) {
    let useFilter = false;
    let filterArr = [];

    elementsWithExtrasArr.map((element) => {
        if (element.extras.indexOf('Case insensitive') == -1 && element.extras.indexOf('Exact match') > -1) {
            useFilter = true;
            filterArr.push(`REGEX_TEST(element.${searchAttrMachine}, '${element.searchValueRaw}', false)`)
        }
        else {
            if (element.extras.indexOf('Case insensitive') == -1) {
                useFilter = true;
                filterArr.push(`REGEX_TEST(element.${searchAttrMachine}, '${element.searchValueRaw}', false)`)
            }
            else if (element.extras.indexOf('Exact match') > -1) {
                useFilter = true;
                filterArr.push(`REGEX_TEST(element.${searchAttrMachine}, '${element.searchValueRaw}', true)`)
            }
        }
    });
    if (useFilter) {
        let filterJoined = filterArr.join(' && ');
        return '\n    filter '+filterJoined;
    }
    else return ''
}

function splitElementsGroup(group, indicesOfElementsWithOrRelationship) {
    let elementGroups=[];
    indicesOfElementsWithOrRelationship.map((index, indexIndex) => {
        if (indicesOfElementsWithOrRelationship.length == 1) {
            elementGroups.push(group.elements.slice(0, index));
            elementGroups.push(group.elements.slice(index));
        }
        else if (indexIndex == indicesOfElementsWithOrRelationship.length-1) {
            elementGroups.push(group.elements.slice(index));
        }
        else {
            if (indexIndex == 0) {
                elementGroups.push(group.elements.slice(0, index));
                elementGroups.push(group.elements.slice(index, indicesOfElementsWithOrRelationship[indexIndex+1]));
            }
            else
                elementGroups.push(group.elements.slice(index, indicesOfElementsWithOrRelationship[indexIndex+1]));
        }
    });
    return elementGroups;
}

exports.queryConstructor = queryConstructor;