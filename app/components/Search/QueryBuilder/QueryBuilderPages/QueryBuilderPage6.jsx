import React from 'react';
import { connect } from 'react-redux';
import { setQueryBuilderAuthors, setQueryBuilderAuthorInput } from '../../../../actions/actions';
import { QueryBuilderPageButtons } from './QueryBuilderPageButtons.jsx'
import {getAllAuthorsQuery} from '../../../../../db/arangoDB/queries/getAllDocumentAuthors'
import {getData} from '../../../../ajax/makeAjaxCall';
import {Page5SelectedAuthors} from './QueryBuilderPagesElements/Page5SelectedAuthors.jsx';
import {Page5SuggestedAuthors} from './QueryBuilderPagesElements/Page5AuthorSuggestions.jsx';

const _QueryBuilderPage6 = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
        
    },
    componentWillUnmount: function() {
    },
    render: function() {
        let disabled = false;
        let disabledText = null;

        //TODO build categories
        return(
            <div id="query-builder-selector">
                
            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        setQueryBuilderAuthors: (authorArr) => {
            dispatch(setQueryBuilderAuthors(authorArr));
        },
        updateAuthorInput: (event) => {
            let searchText = event.target.value;
            dispatch(setQueryBuilderAuthorInput(searchText));
        }
    }
};

const QueryBuilderPage6 = connect(mapStateToProps, mapDispatchToProps)(_QueryBuilderPage6);

exports.QueryBuilderPage6 = QueryBuilderPage5;