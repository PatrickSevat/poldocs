import React from 'react';
import { connect } from 'react-redux';
import { toggleQueryBuilderAdvancedSearch, queryBuilderAdvancedSearchAddGroup } from '../../../../actions/actions';
import { QueryBuilderPageButtons } from './QueryBuilderPageButtons.jsx'
import { Page1AdvancedSearch } from './QueryBuilderPagesElements/Page1AdvancedSearch.jsx'
import { Page1SimpleSearch } from './QueryBuilderPagesElements/Page1SimpleSearch.jsx'

const _QueryBuilderPage1 = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
        this.props.addGroup();
    },
    componentWillUnmount: function() {
    },
    render: function() {
        let disabled = false;
        let disabledText = null;

        return(
            <div id="query-builder-selector">
                <h2>Text search<br/> 
                    <small>(optional)</small></h2>
                <div id="query-builder-selector-page-1">
                    <div aria-hidden="true"><span>Would you like to use the advanced search?</span></div>
                    <div className="on-off-slider">
                        <input
                            type="checkbox"
                            name="document-type-toggle"
                            id="document-type-toggle"
                            onClick={this.props.advancedSearchToggle}
                        />
                        <label htmlFor="document-type-toggle">
                            <span className="sr-only">Would you like to use the advanced search?</span>
                        </label>
                    </div>
                    { this.props.state.appReducer.queryBuilderAdvancedSearch ? <Page1AdvancedSearch /> : <Page1SimpleSearch/>}
                    <div className="search-suggestions">
                        <ul>
                            <li>Suggestion 1</li>
                            <li>Suggestion 2</li>
                            <li>Suggestion 3</li>
                        </ul>
                    </div>
                    <QueryBuilderPageButtons
                        disabled={disabled}
                        warningText={disabledText}
                    />
                </div>


            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        advancedSearchToggle: () => {
            dispatch(toggleQueryBuilderAdvancedSearch());
        },
        addGroup: () => {
            dispatch(queryBuilderAdvancedSearchAddGroup());
        }
    }
};

const QueryBuilderPage1 = connect(mapStateToProps, mapDispatchToProps)(_QueryBuilderPage1);

exports.QueryBuilderPage1 = QueryBuilderPage1;