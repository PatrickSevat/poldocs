import React from 'react';
import { connect } from 'react-redux';
import { queryBuilderAdvancedSearchDeleteElement, searchFieldChange } from '../../../../../actions/actions';
import { Page1AdvancedSearchGroupElementChoices } from './Page1AdvancedSearchGroupElementChoices.jsx';
import { Page1AdvancedSearchRelationChoice} from './Page1AdvancedSearchRelationChoices.jsx';

const _Page1AdvancedSearchGroupElement = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
    },
    componentWillUnmount: function() {
    },
    textChange(event) {
        this.props.textChange(this.props.groupIndex, this.props.elementIndex, event.target.value)
    },
    render: function() {
        return (
            <div 
                id=""
                className="advanced-search-group-element"
            >
                <h5>{"Element "+(this.props.elementIndex+1)}</h5>
                <button
                    className="advanced-search-delete-group"
                    onClick={() => this.props.deleteElement(this.props.groupIndex, this.props.elementIndex)}
                >Delete element</button>
                <Page1AdvancedSearchRelationChoice
                    groupIndex={this.props.groupIndex}
                    elementIndex={this.props.elementIndex}
                    groupOrElement="element"
                />
                <div className="advanced-search-group-elements">
                    <label htmlFor={'group-'+this.props.index+'element-'+'1'}>
                        Search for:
                    </label>
                    <input
                        type="text"
                        id={'group-'+this.props.groupIndex+'element-'+'1'}
                        className="advanced-search-group-element-text"
                        onChange={this.textChange}
                    />
                    <Page1AdvancedSearchGroupElementChoices
                        groupIndex={this.props.groupIndex}
                        elementIndex={this.props.elementIndex}
                    />
                </div>
            </div>
                
            

        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        deleteElement: (groupIndex, elementIndex) => {
            dispatch(queryBuilderAdvancedSearchDeleteElement(groupIndex, elementIndex));
        },
        textChange: (groupIndex, elementIndex, value) => {
            dispatch(searchFieldChange(groupIndex, elementIndex, value))
        }
    }
};

const Page1AdvancedSearchGroupElement = connect(mapStateToProps, mapDispatchToProps)(_Page1AdvancedSearchGroupElement);

exports.Page1AdvancedSearchGroupElement = Page1AdvancedSearchGroupElement;