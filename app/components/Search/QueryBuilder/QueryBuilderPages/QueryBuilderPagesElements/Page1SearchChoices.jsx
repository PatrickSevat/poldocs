import React from 'react';
import { connect } from 'react-redux';
import { searchFieldChange, searchCollectionChange } from '../../../../../actions/actions';

const _Page1SearchChoices = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
    },
    componentWillUnmount: function() {
    },
    render: function() {
        let choices = ['Title', 'Subtitle', 'ID', 'Full text'];

        return (
            <div className="page1-simple-search-choices">
                {choices.map((choice) => {
                    return (
                        <div
                            className="page1-search-attribute-choice"
                            key={this.props.index+'-'+choice}
                        >
                            <input
                                type="checkbox"
                                id={this.props.index+'-'+choice}
                                name={this.props.index+'-'+choice}
                                onClick={() => this.props.searchChoiceChange(this.props.index, choice)}
                            />
                            <label 
                                htmlFor={this.props.index+'-'+choice}
                            >
                                {choice}
                            </label>
                        </div>
                    )
                })}
            </div>

        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        searchChoiceChange: (searchFieldIndex, value) => {
            dispatch(searchCollectionChange(searchFieldIndex, value));
        }
    }
};

const Page1SearchChoices = connect(mapStateToProps, mapDispatchToProps)(_Page1SearchChoices);

exports.Page1SearchChoices = Page1SearchChoices;