import React from 'react';
import { connect } from 'react-redux';
import {  } from '../../../../../actions/actions';

const _Page1AdvancedSearchGroupPreview = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
    },
    componentWillUnmount: function() {
    },
    searchAttributesToMachineReadable (attr) {
        let translateObj = {
            'Title': 'title',
            'Subtitle': 'subTitle',
            'ID': '_key, subTitle',
            'Full text': 'fullText'
        };
        return translateObj[attr]
    },
    extrasString(extrasArr, value) {
        let regex, caseInsensitive, exactMatch, startsWith;
        caseInsensitive = extrasArr.indexOf('Case insensitive') > -1;
        exactMatch = extrasArr.indexOf('Exact match') > -1;
        startsWith = extrasArr.indexOf('Starts with') > -1;

        //TODO add more cases
        if (caseInsensitive && !exactMatch && !startsWith)
            return value;
        else if (caseInsensitive && !exactMatch && startsWith) {
            return value+'*';
        }
        else {
            return '"'+value+'"';
        }
    },
    choicesJoinedWithPrefix(valuesArr, choice, extrasArr) {
        let withExtrasArr = [];
        valuesArr.map((value) => {
            withExtrasArr.push(this.extrasString(extrasArr, value))
        });

        if (choice == 'Contains')
            return withExtrasArr.join(',');
        else if (choice == 'Or')
            return withExtrasArr.join("|");
        else if (choice == 'Exclude')
            return withExtrasArr.map((value) => {
                return '-'+value
            })

    },
    toLogicOperator(value) {
        let translateObj = {
            'And': '&&',
            'Or': '||',
            'Exclude': '&& !'
        };
        return translateObj[value];
    },
    render: function() {
        let previewDataObj = this.props.state.appReducer.queryBuilderValues.text[this.props.index];

        let previewStr = '(';
        previewDataObj.elements.map((element, elementIndex) => {
            if (typeof element.searchValue != 'undefined' && element.searchValue != '') {
                //Operator between elements
                if (elementIndex > 0)
                    previewStr += ' '+this.toLogicOperator(element.relationship)+' ';

                //Element opening bracket
                previewStr += '(';
                //Array of element values or value in case of exact match
                let elementSplit = (element.extras.indexOf('Exact match') === -1) ? element.searchValue.split(' ') : [element.searchValue];
                //Formatted value(s)
                previewStr += this.choicesJoinedWithPrefix(elementSplit, element.choice, element.extras);
                //Element closing bracket
                previewStr += ')';
            }
        });


        previewStr += ` in ${previewDataObj.searchAttributes.join(',')})`;
        
        return (
            <div
                className="advanced-search-group-preview"
            >
                Preview:
                <br/>
                {previewStr}
            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        // relationChoiceSelect: (index, choice) => {
        //     dispatch(queryBuilderAdvancedSearchGroupRelationChoiceSelect(index, choice))
        // }
    }
};

const Page1AdvancedSearchGroupPreview = connect(mapStateToProps, mapDispatchToProps)(_Page1AdvancedSearchGroupPreview);

exports.Page1AdvancedSearchGroupPreview = Page1AdvancedSearchGroupPreview;