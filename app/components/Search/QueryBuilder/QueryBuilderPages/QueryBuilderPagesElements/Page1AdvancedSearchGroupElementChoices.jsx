import React from 'react';
import { connect } from 'react-redux';
import { queryBuilderAdvancedSearchElementChoiceSelect, queryBuilderAdvancedSearchElementExtraSelect } from '../../../../../actions/actions';

const _Page1AdvancedSearchGroupElementChoices = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
    },
    componentWillUnmount: function() {
    },
    render: function() {
        //TODO determine if fuzzy matching is a must have
        let choices = ['Contains', 'Or', 'Exclude'];
        let extras = ['Case insensitive', 'Exact match', 'Starts with'];
        return (
            <div className="advanced-search-group-element-choices-wrapper">
                <div className="advanced-search-group-element-choices">
                    <span>Choices:</span>
                    <br/>
                    {choices.map((choice) => {
                        return (
                            <div className="choice-radio-wrapper">
                                <input
                                    type="radio"
                                    id={'group-'+this.props.groupIndex+'-element-'+this.props.elementIndex+'-choice-'+choice}
                                    onChange={() => this.props.choiceSelect(this.props.groupIndex, this.props.elementIndex, choice)}
                                    checked={choice==this.props.state.appReducer.queryBuilderValues.text[this.props.groupIndex].elements[this.props.elementIndex].choice}
                                />
                                <label
                                    htmlFor={'group-'+this.props.groupIndex+'-element-'+this.props.elementIndex+'-choice-'+choice}
                                    className="advanced-search-group-element-choice"
                                >
                                    {choice}
                                </label>
                            </div>

                        )
                    })}
                </div>
                <div className="advanced-search-group-element-extras">
                    <span>Extras:</span>
                    <br/>
                    {extras.map((extra) => {
                        let elementIndex = this.props.elementIndex;
                        return (
                            <div className="extra-checkbox-wrapper">
                                <input
                                    type="checkbox"
                                    id={'group-'+this.props.groupIndex+'-element-'+this.props.elementIndex+'-extra-'+extra}
                                    className="advanced-search-group-extra"
                                    onClick={() => this.props.extraSelect(this.props.groupIndex, elementIndex, extra)}
                                    checked={this.props.state.appReducer.queryBuilderValues.text[this.props.groupIndex].elements[this.props.elementIndex].extras.indexOf(extra) > -1}
                                />
                                <label htmlFor={'group-'+this.props.groupIndex+'-element-'+this.props.elementIndex+'-extra-'+extra}>
                                    {extra}
                                </label>
                            </div>
                        )
                    })}
                </div>
            </div>

        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        choiceSelect: (groupIndex, elementIndex, value) => {
            dispatch(queryBuilderAdvancedSearchElementChoiceSelect(groupIndex, elementIndex, value));
        },
        extraSelect: (groupIndex, elementIndex, value) => {
            dispatch(queryBuilderAdvancedSearchElementExtraSelect(groupIndex, elementIndex, value));
        }
    }
};

const Page1AdvancedSearchGroupElementChoices = connect(mapStateToProps, mapDispatchToProps)(_Page1AdvancedSearchGroupElementChoices);

exports.Page1AdvancedSearchGroupElementChoices = Page1AdvancedSearchGroupElementChoices;