import React from 'react';
import { connect } from 'react-redux';
import { queryBuilderAdvancedSearchDeleteGroup, queryBuilderAdvancedSearchAddElement, queryBuilderAdvancedSearchGroupRelationChoiceSelect } from '../../../../../actions/actions';
import { Page1AdvancedSearchGroupElement } from './Page1AdvancedSearchGroupElement.jsx';
import { Page1SearchChoices} from './Page1SearchChoices.jsx';
import { Page1AdvancedSearchRelationChoice} from './Page1AdvancedSearchRelationChoices.jsx';
import { Page1AdvancedSearchGroupPreview} from './Page1AdvancedSearchGroupPreview.jsx';

const _Page1AdvancedSearchGroup = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
    },
    componentWillUnmount: function() {
    },
    render: function() {
        let groupElements = [];
        for (let i = 0; i < this.props.state.appReducer.queryBuilderValues.text[this.props.index].elements.length; i++) {
            groupElements.push(
                <Page1AdvancedSearchGroupElement
                    groupIndex={this.props.index}
                    elementIndex={i}
                />
            )
        }

        return (
            <div className="page1-advanced-search-group">
                <div className="advanced-search-group-controls">
                    <h4>
                        {"Group "+(this.props.index+1)}
                        <br/><small>Should search in:</small>
                    </h4>
                    <button
                        className="advanced-search-edit-group"
                        onClick={() => this.props.addElement(this.props.index)}
                    >Add element</button>
                    <button
                        className="advanced-search-edit-group"
                        onClick={() => this.props.editGroup(this.props.index)}
                    >Edit group</button>
                    <button
                        className="advanced-search-delete-group"
                        onClick={() => this.props.deleteGroup(this.props.index)}
                    >Delete group</button>
                    <Page1SearchChoices
                        index={this.props.index}
                        advanced={true}
                    />
                </div>
                <Page1AdvancedSearchRelationChoice
                    groupIndex={this.props.index}
                    elementIndex={false}
                    groupOrElement="group"
                />
                <div className="advanced-search-group-elements-wrapper">
                    {groupElements}
                </div>
                <Page1AdvancedSearchGroupPreview
                    index={this.props.index}
                    data={this.props.state.appReducer.queryBuilderValues.text[this.props.index]}
                />
            </div>

        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        editGroup: () => {
            // dispatch(queryBuilderAdvancedSearchAddGroup());
        },
        relationChoiceSelect: (index, choice) => {
            dispatch(queryBuilderAdvancedSearchGroupRelationChoiceSelect(index, choice))  
        },
        deleteGroup: (index) => {
            dispatch(queryBuilderAdvancedSearchDeleteGroup(index));
        },
        addElement: (index) => {
            dispatch(queryBuilderAdvancedSearchAddElement(index));
        }
    }
};

const Page1AdvancedSearchGroup = connect(mapStateToProps, mapDispatchToProps)(_Page1AdvancedSearchGroup);

exports.Page1AdvancedSearchGroup = Page1AdvancedSearchGroup;