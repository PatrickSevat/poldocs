import React from 'react';
import { connect } from 'react-redux';
import { searchFieldChange, searchCollectionChange } from '../../../../../actions/actions';
import { Page1SearchChoices } from './Page1SearchChoices.jsx'

const _Page1SimpleSearch = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
    },
    componentWillUnmount: function() {
    },
    searchFieldChange (event) {

        let value = event.target.value;
        // console.log(value);
        this.props.searchFieldChange(0, 0, value);
    },
    render: function() {
        return (
            <div className="page1-simple-search">
                <h3>
                    Search for:
                    <br/><small>(Each word included, for exact phrase use advanced search)</small>
                </h3>
                <span>'Apple tree' will match both <em>An apple tree</em> and <em>An apple grows on a tree</em></span>
                <div className="page1-simple-search-text-field-container">
                    <div className="page1-simple-search-text-field-wrapper">
                        <textarea
                            id="page1-simple-search-text-field"
                            onChange={this.searchFieldChange}
                            
                        />
                        <label
                            htmlFor="page1-simple-search-text-field"
                            className="sr-only"
                        >Search for:</label>
                    </div>
                    <Page1SearchChoices
                        index={0}
                        advanced={false}
                    />
                </div>
            </div>

        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        searchFieldChange: (searchFieldIndex, elementIndex, value) => {
            dispatch(searchFieldChange(searchFieldIndex, elementIndex, value));
        },
        searchChoiceChange: (searchFieldIndex, value) => {
            dispatch(searchCollectionChange(searchFieldIndex, value));
        }
    }
};

const Page1SimpleSearch = connect(mapStateToProps, mapDispatchToProps)(_Page1SimpleSearch);

exports.Page1SimpleSearch= Page1SimpleSearch;