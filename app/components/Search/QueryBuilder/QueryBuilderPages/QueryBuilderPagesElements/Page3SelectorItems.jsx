import React from 'react';
import { connect } from 'react-redux';
import { toggleDocumentsTypesItem } from '../../../../../actions/actions';

const _Page3SelectorItems = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
    },
    componentWillUnmount: function() {
    },
    render: function() {
        //this.props.data contains array of valueObjects with keys: checked, label, visible
        //this.props.searchForType contains documents or dossiers
        //this.props.level contains national or international
        return (
            <div className="page2-selector-items">
                {this.props.data.map((item) => {
                    if (item.label === '')
                        item.label = 'Unspecified type';
                    if (item.visible)
                        return (
                            <div>
                                <input
                                    type="checkbox"
                                    name={item.label}
                                    id={item.label}
                                    onClick={() => this.props.toggleDocumentTypesItem(this.props.searchForType, this.props.level, item.label)}
                                />
                                <label htmlFor={item.label}
                                       className="page2-selector-item"
                                >
                                    {item.label} <i className={'fa '+(item.checked ? 'fa-check' : 'fa-times')}/>
                                </label>
                            </div>
                            )
                })}
            </div>

        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        toggleDocumentTypesItem: (searchForType, level, itemLabel) => {
            dispatch(toggleDocumentsTypesItem(searchForType, level, itemLabel));
        }
    }
};

const Page3SelectorItems = connect(mapStateToProps, mapDispatchToProps)(_Page3SelectorItems);

exports.Page3SelectorItems= Page3SelectorItems;