import React from 'react';
import { connect } from 'react-redux';
import { queryBuilderAdvancedSearchGroupRelationChoiceSelect } from '../../../../../actions/actions';

const _Page1AdvancedSearchGroupRelationChoices = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
    },
    componentWillUnmount: function() {
    },
    render: function() {
        if ((this.props.groupIndex == 0 && this.props.groupOrElement == 'group') ||
            (this.props.elementIndex == 0 && this.props.groupOrElement == 'element'))
            return null;
        
        let relationChoices = ['And', 'Or'];
        return (
            <div
                className={"advanced-search-"+this.props.groupOrElement+"-relation"}
            >
                    <span>
                        {'Relation with previous '+(this.props.groupOrElement)+': '}
                    </span>
                {relationChoices.map((choice) => {
                    let id = this.props.groupOrElement == 'group' ? 'group-'+this.props.groupIndex+'-relation-choice-'+choice : 'group-'+this.props.groupIndex+'-element-'+this.props.elementIndex+'-relation-choice-'+choice;
                    let checkedGroup = choice==this.props.state.appReducer.queryBuilderValues.text[this.props.groupIndex].relationship;
                    let checkedElement;
                    if (this.props.elementIndex)
                        checkedElement = choice==this.props.state.appReducer.queryBuilderValues.text[this.props.groupIndex].elements[this.props.elementIndex].relationship;
                    return (
                        <div className={this.props.groupOrElement+"-relation-choice-radio-wrapper"}>
                            <input
                                type="radio"
                                id={id}
                                onClick={() => this.props.relationChoiceSelect(choice, this.props.groupIndex, this.props.elementIndex)}
                                checked={this.props.groupOrElement == 'group' ? checkedGroup : checkedElement }
                            />
                            <label
                                htmlFor={id}
                                className={"advanced-search-"+this.props.groupOrElement+"-relation-choice"}
                            >
                                {choice}
                            </label>
                        </div>
                    )})
                }
            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        relationChoiceSelect: (choice, groupIndex, elementIndex) => {
            dispatch(queryBuilderAdvancedSearchGroupRelationChoiceSelect(choice, groupIndex, elementIndex))
        }
    }
};

const Page1AdvancedSearchGroupRelationChoices = connect(mapStateToProps, mapDispatchToProps)(_Page1AdvancedSearchGroupRelationChoices);

exports.Page1AdvancedSearchRelationChoice = Page1AdvancedSearchGroupRelationChoices;