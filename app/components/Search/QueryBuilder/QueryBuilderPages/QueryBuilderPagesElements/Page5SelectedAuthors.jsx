import React from 'react';
import { connect } from 'react-redux';
import { setQueryBuilderAuthors } from '../../../../../actions/actions';

const _Page5SelectedAuthors = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
    },
    componentDidUpdate () {

    },
    masonry () {

    },
    componentWillUnmount: function() {
    },
    render: function() {
        if (this.props.state.appReducer.queryBuilderValues.authors.selected.length == 0)
            return null;

        //TODO see if masonry work if selected author item is put in its own component

        let items = this.props.state.appReducer.queryBuilderValues.authors.selected.map((author) => {
            let altNames = typeof author.nameID[0] != 'undefined' ? author.nameID[0].name : null;
            if (Array.isArray(altNames))
                altNames = altNames.join(', ');
            return (
                <div className="selected-author-item">
                            <span>
                                {author.author}
                                <br/>
                                <small>{altNames}</small>
                            </span>

                </div>
            )
        });

        return(
                <div id="selected-authors">
                    <div className="selected-author-item-width"></div>
                    <div className="selected-author-item-gutter"></div>
                    {items}
                </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        setQueryBuilderAuthors: (authorArr) => {
            dispatch(setQueryBuilderAuthors(authorArr));
        }
    }
};

const Page5SelectedAuthors = connect(mapStateToProps, mapDispatchToProps)(_Page5SelectedAuthors);

exports.Page5SelectedAuthors= Page5SelectedAuthors;