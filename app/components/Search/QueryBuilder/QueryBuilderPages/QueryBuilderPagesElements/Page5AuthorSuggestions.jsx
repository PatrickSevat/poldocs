import React from 'react';
import { connect } from 'react-redux';
import { setQueryBuilderAuthors, queryBuilderSelectAuthor } from '../../../../../actions/actions';
import lodash from 'lodash'

const _Page5SuggestedAuthors = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {

    },
    componentWillUnmount: function() {
    },
    render: function() {
        const authors = this.props.state.appReducer.queryBuilderValues.authors;
        if (authors.searchText == null || authors.authorValues == null)
            return null;

        let suggestions = [];
        this.props.state.appReducer.queryBuilderValues.authors.authorValues.map((author) => {
            let combinedNameArr = [author.author];
            if (typeof author.nameID[0] != 'undefined')
                combinedNameArr = combinedNameArr.concat([author.nameID[0].name]);
            combinedNameArr = lodash.flatten(combinedNameArr);
            combinedNameArr.map((name) => {
                if (name.toLowerCase().search(authors.searchText.toLowerCase()) > -1)
                    suggestions.push(author);
            })
        });

        suggestions = lodash.uniqWith(suggestions, lodash.isEqual);
        if (suggestions.length > 5)
            suggestions = suggestions.slice(0,4);
        
        return(
            <div id="suggested-authors">
                {suggestions.map((author) => {
                    return (
                        <div 
                            className="suggested-author-item"
                            onClick={() => this.props.selectAuthor(author)}
                        >
                            <span>
                                {author.author}
                            </span>
                        </div>
                    )
                })}
            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        setQueryBuilderAuthors: (authorArr) => {
            dispatch(setQueryBuilderAuthors(authorArr));
        },
        selectAuthor: (authorObj) => {
            console.log('select author:');
            console.log(authorObj);
            dispatch(queryBuilderSelectAuthor(authorObj));
        }
    }
};

const Page5SuggestedAuthors = connect(mapStateToProps, mapDispatchToProps)(_Page5SuggestedAuthors);

exports.Page5SuggestedAuthors = Page5SuggestedAuthors;