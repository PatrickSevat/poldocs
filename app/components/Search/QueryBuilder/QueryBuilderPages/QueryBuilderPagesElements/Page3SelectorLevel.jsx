import React from 'react';
import { connect } from 'react-redux';
import { toggleCutOffQueryBuilderSelectorTypes } from '../../../../../actions/actions';
import { Page3SelectorItems } from './Page3SelectorItems.jsx';

const _Page3SelectorLevel = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
    },
    componentWillUnmount: function() {
    },
    levelToSource (level, display) {
        if (!display)
            return level == 'international' ? 'Eurlex' : 'officieleBekendmakingen';
        else
            return level == 'international' ? 'Eurlex' : 'Officiële Bekendmakingen'
    },
    render: function() {
        //this.props.data contains an object with keys: international && national
        //international && national contain obj with keys cutOff => bool && values => arr
        let display = [];
        for (let key in this.props.data) {
            if (this.props.data.hasOwnProperty(key) && this.props.state.appReducer.queryBuilderValues.collections[this.props.searchForType] &&
                this.props.state.appReducer.queryBuilderValues.source[this.levelToSource(key)]) {
                console.log('level');
                let cutOff = this.props.data[key].cutOff;
                let cutOffDisplay;
                if (cutOff)
                    cutOffDisplay = (
                        <span
                            className="poldocs-button page2-selector-level-toggle-cut-off"
                            onClick={() => this.props.toggleCutOffValues(this.props.searchForType, key)}
                        >
                        More
                    </span>
                    );
                
                display.push(
                    <div className="page2-selector-level">
                        <span className="page2-selector-level-label">
                            {key}
                        </span>
                        <Page3SelectorItems
                            data={this.props.data[key].values}
                            searchForType={this.props.searchForType}
                            level={key}
                        />    
                        {cutOffDisplay}
                    </div>
                )
            }
        }
        return (
            <div className="page2-selector-level-wrapper">
                {display}
            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        toggleCutOffValues: (searchForType, level) => {
            console.log('searchForType: '+searchForType);
            console.log('level: '+level);
            dispatch(toggleCutOffQueryBuilderSelectorTypes(searchForType, level));
        }
    }
};

const Page3SelectorLevel = connect(mapStateToProps, mapDispatchToProps)(_Page3SelectorLevel);

exports.Page3SelectorLevel = Page3SelectorLevel;