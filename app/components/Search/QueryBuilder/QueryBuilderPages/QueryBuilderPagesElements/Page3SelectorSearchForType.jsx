import React from 'react';
import { connect } from 'react-redux';
import { setQueryBuilderTypes } from '../../../../../actions/actions';
import { getData } from '../../../../../ajax/makeAjaxCall';
import { Page3SelectorLevel } from './Page3SelectorLevel.jsx';

const _Page3SelectorSearchForTypes = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
        getData('/getDocumentAndDossierFileTypes').then((data) => {
            let start = Date.now();
            console.log('Time between: '+(Date.now()-start));
            let parsed = JSON.parse(data);
            this.props.setQueryBuilderTypes(parsed[0])

        }, (err) => {
            console.log(err);
        })
    },
    componentWillUnmount: function() {
    },
    documentTypeToggle (event) {
    },
    render: function() {
        if (this.props.state.appReducer.queryBuilderValues.types == null)
            return null;

        //TODO build type selector
        //key here contains the search-for-type (documents or dossier)
        //this.props.state.appReducer.queryBuilderValues.types[key] contains object with keys international and level, 
        // each of which contains an array with actual values
        let displayTypes = [];
        for (let key in this.props.state.appReducer.queryBuilderValues.types) {
            if (this.props.state.appReducer.queryBuilderValues.types.hasOwnProperty(key) && this.props.state.appReducer.queryBuilderValues.collections[key]) {
                displayTypes.push(
                    <div className="page3-selector-collection">
                        <span className="search-for-type">{key}</span>
                        <Page3SelectorLevel
                            data={this.props.state.appReducer.queryBuilderValues.types[key]}
                            searchForType={key}
                        />
                    </div>
                )    
            }
        }
        return (
        <div className="page3-selector-collection-wrapper">
            {displayTypes}
        </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        setQueryBuilderTypes: (typesObj) => {
            dispatch(setQueryBuilderTypes(typesObj));
        }
    }
};

const Page3SelectorSearchForType = connect(mapStateToProps, mapDispatchToProps)(_Page3SelectorSearchForTypes);

exports.Page3SelectorSearchForType = Page3SelectorSearchForType;