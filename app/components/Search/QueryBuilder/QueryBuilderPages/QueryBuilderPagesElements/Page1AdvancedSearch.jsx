import React from 'react';
import { connect } from 'react-redux';
import { queryBuilderAdvancedSearchAddGroup } from '../../../../../actions/actions';
import {Page1AdvancedSearchGroup} from './Page1AdvancedSearchGroup.jsx'

const _Page1AdvancedSearch = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
    },
    componentWillUnmount: function() {
        
    },
    render: function() {
        let groups = [];
        for (let i = 0; i < this.props.state.appReducer.queryBuilderValues.text.length; i++) {
            groups.push(
                <Page1AdvancedSearchGroup
                    index={i}
                    relation={i > 0}
                />
            )
        }
        
        return (
            <div id="page1-advanced-search">
                <div id="advanced-search-controls">
                    <h3>Advanced search groups</h3>
                    <button 
                        id="advanced-search-new-group"
                        onClick={this.props.addGroup}
                    >Add group</button>
                </div>
                <div id="advanced-search-wrapper">
                    {groups}
                </div>
            </div>

        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        addGroup: () => {
            dispatch(queryBuilderAdvancedSearchAddGroup());
        }
    }
};

const Page1AdvancedSearch = connect(mapStateToProps, mapDispatchToProps)(_Page1AdvancedSearch);

exports.Page1AdvancedSearch= Page1AdvancedSearch;