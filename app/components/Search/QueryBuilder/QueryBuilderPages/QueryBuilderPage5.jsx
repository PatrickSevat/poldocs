import React from 'react';
import { connect } from 'react-redux';
import { setQueryBuilderAuthors, setQueryBuilderAuthorInput } from '../../../../actions/actions';
import { QueryBuilderPageButtons } from './QueryBuilderPageButtons.jsx'
import {getAllAuthorsQuery} from '../../../../../db/arangoDB/queries/getAllDocumentAuthors'
import {getData} from '../../../../ajax/makeAjaxCall';
import {Page5SelectedAuthors} from './QueryBuilderPagesElements/Page5SelectedAuthors.jsx';
import {Page5SuggestedAuthors} from './QueryBuilderPagesElements/Page5AuthorSuggestions.jsx';

const _QueryBuilderPage5 = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
        let start = Date.now();
        getData('/getAllAuthors').then((res) => {
            console.log('Via node authors retrieval success in' + (Date.now() - start));
            this.props.setQueryBuilderAuthors(JSON.parse(res))
        }, (err) => {
            console.log('err retrieving authors:');
            console.log(err);
        });
    },
    componentWillUnmount: function() {
    },
    render: function() {
        let disabled = false;
        let disabledText = null;

        let suggestedNullValues;
        if (this.props.state.appReducer.queryBuilderValues.authors.selected.length > 0)
            suggestedNullValues = (
                <div id="allow-null-values-checkbox">
                    <div aria-hidden="true"><span>Also retrieve documents which do not have an author specified?</span></div>
                    <div
                        id="allow-null-values-checkbox"
                        className="on-off-slider">
                        <input
                            type="checkbox"
                            id="authors-allow-null"
                        />
                        <label htmlFor="authors-allow-null">
                            <span className="sr-only">Also etrieve documents which do not have an author specified?</span>
                        </label>
                    </div>
                </div>
            )

        return(
            <div id="query-builder-selector">
                <h2>Authors<br/>
                    <small>(optional)</small></h2>
                <div id="query-builder-selector-page-5">
                    {this.props.state.appReducer.queryBuilderValues.authors.selected.length > 0 ? <Page5SelectedAuthors /> : null}
                    <div id="author-search-field-wrapper">
                        <label htmlFor="author-search-field">
                            <h3>Search for author</h3>
                        </label>
                        <input
                            type="text"
                            id="author-search-field"
                            onChange={this.props.updateAuthorInput}
                            disabled={!this.props.state.appReducer.queryBuilderValues.authors.searchable}
                        />
                    </div>
                    <Page5SuggestedAuthors/>
                    {suggestedNullValues}
                </div>
                <QueryBuilderPageButtons
                    disabled={disabled}
                    warningText={disabledText}
                />
            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        setQueryBuilderAuthors: (authorArr) => {
            dispatch(setQueryBuilderAuthors(authorArr));
        },
        updateAuthorInput: (event) => {
            let searchText = event.target.value;
            dispatch(setQueryBuilderAuthorInput(searchText));
        }
    }
};

const QueryBuilderPage5 = connect(mapStateToProps, mapDispatchToProps)(_QueryBuilderPage5);

exports.QueryBuilderPage5 = QueryBuilderPage5;