import React from 'react';
import { connect } from 'react-redux';
import { selectQueryBuilderPage, queryBuilderToggleCollection, queryBuilderToggleSource } from '../../../../actions/actions';
import { QueryBuilderPageButtons } from './QueryBuilderPageButtons.jsx'

const _QueryBuilderPage2 = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
    },
    componentWillUnmount: function() {
    },
    render: function() {
        //TODO perhaps better to make a bool in reducer for disabled, so that paginator can also use it
        let disabled = false;
        let queryBuilderValues = this.props.state.appReducer.queryBuilderValues;
        if ((!queryBuilderValues.collections.documents && !queryBuilderValues.collections.dossiers)
        || (!queryBuilderValues.source.Eurlex && !queryBuilderValues.source.officieleBekendmakingen))
            disabled= true;

        let disabledText = 'Please select source(s) and type(s)';
        

        return(
            <div id="query-builder-selector">
                <h2>Specify type(s) and source(s)<br/> 
                    <small>(required)</small></h2>
                <div id="query-builder-selector-page-1-collections">
                    <h3>Search in type:</h3>
                    <br/>
                    <div className="icons">
                        <input
                            type="checkbox"
                            name="collection"
                            id="collection-documents"
                            onClick={() => this.props.toggleCollections('documents')}
                        />
                        <label htmlFor="collection-documents" className="collection-item">
                            <i className="fa fa-file-text display-block" />
                            <span className="collection-item">Documents</span>
                        </label>

                        <input
                            type="checkbox"
                            name="collection"
                            id="collection-dossiers"
                            onClick={() => this.props.toggleCollections('dossiers')}
                        />
                        <label htmlFor="collection-dossiers" className="collection-item">
                            <i className="fa fa-folder-open display-block" />
                            <span className="collection-item">Dossiers</span>
                        </label>

                    </div>
                </div>
                <div id="query-builder-selector-page-1-select-source">
                    <h3>Select source(s):</h3>
                    <br/>
                    <div className="icons">
                        <input
                            type="checkbox"
                            name="source"
                            id="source-Eurlex"
                            onClick={() => this.props.toggleSource('Eurlex')}
                        />
                        <label htmlFor="source-Eurlex" className="f32 collection-item">
                            <i className="flag _European_Union display-block"/>
                            <span className="source-item">Eurlex (European Union)</span>
                        </label>

                        <input
                            type="checkbox"
                            name="source"
                            id="source-officieleBekendmakingen"
                            onClick={() => this.props.toggleSource('officieleBekendmakingen')}
                        />
                        <label htmlFor="source-officieleBekendmakingen" className="f32 collection-item">
                            <i className="f32 flag nl display-block"/>
                            <span className="source-item">Officiële Bekendmakingen (Netherlands, national)</span>
                        </label>

                    </div>
                </div>
                <QueryBuilderPageButtons
                    disabled={disabled}
                    warningText={disabledText}
                />

            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        selectQueryBuilderPage: (pageNum) => {
            dispatch(selectQueryBuilderPage(pageNum));
        },
        toggleCollections: (collectionName) => {
            dispatch(queryBuilderToggleCollection(collectionName))
        },
        toggleSource: (sourceName) => {
            console.log('toggle source: '+sourceName);
            dispatch(queryBuilderToggleSource(sourceName))
        }
    }
};

const QueryBuilderPage2 = connect(mapStateToProps, mapDispatchToProps)(_QueryBuilderPage2);

exports.QueryBuilderPage2 = QueryBuilderPage2;