import React from 'react';
import { connect } from 'react-redux';
import { } from '../../../actions/actions';
import { SearchListItem } from './SearchListItem.jsx';

const _SavedSearches = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
    },
    componentWillUnmount: function() {
    },
    render: function() {
        return(
            <div id="saved-searches">
                <div id="saved-searches-title">
                    Saved Searches    
                </div>
                <div id="saved-searches-list">
                    {this.props.state.appReducer.savedSearches.map((searchItem, searchListItemIndex) => {
                        return <SearchListItem 
                            data={searchItem}
                            index={searchListItemIndex}
                        />
                    })}
                </div>
            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        // documentsReceived: (data) => {
        //     console.log('doc received in documentsList');
        //     dispatch(resultsReceived(data));
        // }
    }
};

const SavedSearches = connect(mapStateToProps, mapDispatchToProps)(_SavedSearches);

exports.SavedSearches = SavedSearches;