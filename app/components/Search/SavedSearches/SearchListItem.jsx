import React from 'react';
import { connect } from 'react-redux';
import { } from '../../../actions/actions';

const _SearchListItem = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
    },
    componentWillUnmount: function() {
    },
    editSearchItem(index) {
    },
    copySearchItem(index) {
    },
    executeSearchItem(index) {
    },
    executeSearchItemSinceLast(index) {
    },
    render: function() {
        return(
            <div className="search-list-item">
                <div className="search-list-item-title">
                    {this.props.data.title}
                </div>
                <div className="search-list-item-icons">
                    <i
                        className="fa fa-files-o"
                        onClick={() => this.copySearchItem(this.props.index)}
                    />
                    <i
                        className="fa fa-pencil"
                        onClick={() => this.editSearchItem(this.props.index)}
                    />
                    <i
                        className="fa fa-refresh"
                        onClick={() => this.executeSearchItemSinceLast(this.props.index)}
                    />
                    <i
                        className="fa fa-play"
                        onClick={() => this.executeSearchItem(this.props.index)}
                    />
                </div>
                <div className="search-list-item-data">
                    <span>Query: {this.props.data.query}</span>
                    <span>Last run: {new Date(this.props.data.lastRun).toLocaleString()}</span>
                </div>
            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        // documentsReceived: (data) => {
        //     console.log('doc received in documentsList');
        //     dispatch(resultsReceived(data));
        // }
    }
};

const SearchListItem = connect(mapStateToProps, mapDispatchToProps)(_SearchListItem);

exports.SearchListItem = SearchListItem;