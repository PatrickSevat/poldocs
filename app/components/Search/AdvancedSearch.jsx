import React from 'react';
import { connect } from 'react-redux';
import {resultsReceived, resetAdvancedSearchState} from '../../actions/actions';
import {SavedSearches} from './SavedSearches/SavedSearches.jsx';
import {QueryBuilder} from './QueryBuilder/QueryBuilder.jsx';
import List from '../AllResultPages/List.jsx'
import {Loader} from '../AllResultPages/Loader.jsx';
import SecondSidebar from '../Sidebars/SecondSidebar.jsx'

const _AdvancedSearch = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
    },
    componentWillUnmount: function() {
        this.props.resetAdvancedSearchState()
    },
    render: function() {
        // console.log('rendering advanced search')
        let loading = this.props.state.appReducer.loading;
        
        if (this.props.state.appReducer.query != null && this.props.state.appReducer.resultsData != null) {
            return (
                    <div id="outer-container">
                        {loading == true ? <Loader/> : null}
                        <List />
                        <SecondSidebar />
                    </div>
                )
        }
        
        let loggedIn = this.props.state.appReducer.loggedIn;
        let savedSearches = loggedIn ? <SavedSearches /> : null;
        
        return(
            <div id="outer-container">
                {loading == true ? <Loader/> : null}
                <div id="search-container">
                    {savedSearches}
                    <QueryBuilder
                        loggedIn={loggedIn}
                    />
                </div>
            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        documentsReceived: (data) => {
            console.log('doc received in documentsList');
            dispatch(resultsReceived(data));
        },
        resetAdvancedSearchState: () => {
            dispatch(resetAdvancedSearchState());
        }
    }
};

const AdvancedSearch = connect(mapStateToProps, mapDispatchToProps)(_AdvancedSearch);

module.exports = AdvancedSearch;