import React from 'react';
import { connect } from 'react-redux';
import {getData} from '../../ajax/makeAjaxCall';
import {resultsReceived, updatePath, setResultListType, clearResults} from '../../actions/actions';

const _DossiersOuter = React.createClass({
    componentWillMount() {
        // this.props.clearResults();
        this.props.setResultListType('dossiers');
        // console.log('appReducer pre mount');
        // console.log(this.props.state.appReducer);
    },
    componentDidMount() {
        let start = Date.now();
        getData('/recentDossiers').then((data) => {
            console.log('Time between: '+(Date.now()-start));
            let parsed = JSON.parse(data);
            this.props.documentsReceived(parsed);

        }, (err) => {
            console.log(err);
        });
    },
    componentWillUnmount: function() {
        this.props.clearResults();
    },
    render: function() {
        return(
            <div id="outer-container">
                {this.props.children}
            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        documentsReceived: (data) => {
            dispatch(resultsReceived(data, 'dossiers'));
        },
        updatePath: (url) => {
            dispatch(updatePath(url));
        },
        setResultListType: (str) => {
            dispatch(setResultListType(str));
        },
        clearResults: () => {
            dispatch(clearResults());
        }
    }
};

const DossiersOuter = connect(mapStateToProps, mapDispatchToProps)(_DossiersOuter);

module.exports = DossiersOuter;