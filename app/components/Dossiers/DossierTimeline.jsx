import React from 'react';
import { connect } from 'react-redux';
import { DossierEventDocuments } from './DossierFullViewElements/DossierEventDocuments.jsx';
import { ResultItemAuthors } from '../AllResultPages/FullViewElements/ResultItemAuthors.jsx';

const _DossierTimeline = React.createClass({
    goToAuthor (author) {
        console.log('go to author: '+author)
    },
    getFlag(country) {
        if (typeof country == 'undefined')
            return null;
        if (country.toLowerCase() == 'nl')
            return <span className="f32"><span className="flag nl"/></span>;
        else if (country.toLowerCase() == 'eu')
            return <span className="f32"><span className="flag _European_Union"/></span>
    },
    render: function() {
        console.log('DossierTimeline props.data');
        console.log(this.props.data);

        let events = this.props.data.events;
        this.props.data.relatedDossiers.map((relatedDossierWhole) => {
            if (typeof relatedDossierWhole.relatedDossier.events != 'undefined')
                relatedDossierWhole.relatedDossier.events.map((event) => {
                    events.push(event)
                })
        });

        //Sort date oldest to newest
        events.sort((a, b) => {
            return new Date(a.date) - new Date(b.date)
        });
//TODO add author to events for national events
        return(
            <ul
                id="timeline"
            >
                <li className="timeline-header">
                    Timeline
                </li>
                {events.map((event, eventIndex) => {
                    let authorData = {};
                    if (typeof event.eventSubOrganisation != 'undefined') {
                        authorData.authors = event.eventSubOrganisation == '' ? event.eventOrganisation : event.eventSubOrganisation;
                        if (!Array.isArray(authorData.authors))
                            authorData.authors = [authorData.authors]
                    }
                    else
                        authorData.authors = event.eventOrganisation;

                    console.log('authordata');
                    console.log(authorData);

                    let isoString = new Date(event.date);
                    let readableString = isoString.toLocaleDateString();

                    let decision;
                    if (typeof event.decision != 'undefined' && event.decision != '')
                        decision = (
                            <div className="timeline-item-data-item timeline-author">
                                Decision: <span>{event.decision}</span>
                            </div>);

                    // <div className="timeline-item-data-item timeline-author">
                    //     Author: <span
                    //     className="looks-like-link"
                    //     onClick={() => this.goToAuthor(author)}
                    // >{author}</span>
                    // </div>

                    let flag = this.getFlag(this.props.data.country);

                    return (
                        <li
                            className="timeline-item"
                        >
                            <div className="timeline-item-data">
                                <div className="timeline-item-data-item timeline-label">
                                    {event.eventLabel}
                                    {flag}
                                </div>
                                <div className="timeline-item-data-item timeline-date">
                                    Date: <date className="timeline-event-date">{readableString}</date>
                                </div>
                                <ResultItemAuthors
                                    data={authorData}
                                    index={this.props.index}
                                />
                                {decision}
                                <DossierEventDocuments
                                    data={event}
                                    index={eventIndex}
                                />
                            </div>
                        </li>
                    )
                })}
            </ul>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const DossierTimeline = connect(mapStateToProps, mapDispatchToProps)(_DossierTimeline);

exports.DossierTimeline = DossierTimeline;