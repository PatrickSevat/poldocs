import React from 'react';
import { connect } from 'react-redux';
import {} from '../../actions/actions';
import {setResultFullView} from '../../actions/actions';
import {ResultItemTitle} from '../AllResultPages/FullViewElements/ResultItemTitle.jsx';
import {ResultItemSubTitle} from '../AllResultPages/FullViewElements/ResultItemSubTitle.jsx';
import {ResultItemKey} from '../AllResultPages/FullViewElements/ResultItemKey.jsx';
import {ResultItemAuthors} from '../AllResultPages/FullViewElements/ResultItemAuthors.jsx';
import {ResultItemType} from '../AllResultPages/FullViewElements/ResultItemType.jsx';
import {ResultItemCategories} from '../AllResultPages/FullViewElements/ResultItemCategories.jsx';
import {DossierModifiedDate} from './DossierFullViewElements/DossierModifiedDate.jsx';
import {DossierRelationships} from './DossierFullViewElements/DossierRelationships.jsx';
import {DossierTimeline} from './DossierTimeline.jsx';
import {DocumentsListDocument} from '../Documents/DocumentsListDocument.jsx';


const _DossiersListDossier = React.createClass({
    componentDidMount () {
        this.masonry();
    },
    componentDidUpdate () {
        this.masonry();
    },
    componentWillUnmount () {
        console.log('unmount doc');
        const masonry = $('#list-wrapper').data('masonry');
        if (masonry)
            $('#list-wrapper').masonry('destroy');
    },
    masonry () {
        if (this.props.state.appReducer.resultsData != null) {
            $('#list-wrapper').masonry({
                itemSelector: '.list-item',
                columnWidth: '#list-item-width',
                gutter: '#gutter',
                percentPosition: true
            })
        }  
    },
    render: function() {
        //Set variables needed for fullview of Dossier (full width & show timeline)
        let active = this.props.state.appReducer.resultsFullViewID === this.props.index;
        let fullView = this.props.state.appReducer.resultsFullView != null ? this.props.state.appReducer.resultsFullView : null;

        //Set variable for opening a document, if you click on an document within an event, it is displayed here
        let showDocumentInDossierTimeline;
        if (this.props.state.appReducer.showDocumentInDossierTimeline != null && active) {
            showDocumentInDossierTimeline = (
                <DocumentsListDocument
                    data={this.props.state.appReducer.showDocumentInDossierTimeline}
                    index={this.props.state.appReducer.resultsFullViewID}
                />
            )
        }

        let dossierTimeline;
        if (fullView)
            dossierTimeline = (<DossierTimeline
                data={this.props.data}
                index={this.props.index}
                country={this.props.data.country}
            />);
        return(
            <div
                className={"list-item"
                    +(active ? ' list-item-active' : '')
                    +(showDocumentInDossierTimeline != null ? ' dossier-list-contains-document-full-view' : '')
                    }
                id={'doc-list-doc-'+this.props.index}>
                <div className="timeline-and-table-wrapper">
                    {active ? dossierTimeline : null}
                    <table className={"search-result"+(active ? ' search-result-active' : '')} >
                        <ResultItemTitle
                            data={this.props.data}
                            index={this.props.index}
                            active={active}
                        />
                        <ResultItemType
                            data={this.props.data}
                            index={this.props.index}
                        />
                        <ResultItemSubTitle
                            data={this.props.data}
                        />
                        <ResultItemKey
                            data={this.props.data}
                        />
                        <DossierModifiedDate
                            data={this.props.data}
                        />
                        <ResultItemAuthors
                            data={this.props.data}
                            index={this.props.index}
                        />
                        <DossierRelationships
                            data={this.props.data}
                            index={this.props.index}
                        />
                        <ResultItemCategories
                            data={this.props.data}
                            index={this.props.index}
                        />
                    </table>
                </div>
                {showDocumentInDossierTimeline}
            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        hideDocumentFullView: () => {
            dispatch(setResultFullView(null))
        },
        updateFileType: (filetype) => {
            dispatch(updateFullViewFileType(filetype))
        }
    }
};

const DossiersListDossier = connect(mapStateToProps, mapDispatchToProps)(_DossiersListDossier);

exports.DossiersListDossier = DossiersListDossier;