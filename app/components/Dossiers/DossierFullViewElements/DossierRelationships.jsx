import React from 'react';
import { connect } from 'react-redux';
import {} from '../../../actions/actions';
import {DocumentRelationshipsItem} from '../../Documents/DocumentFullViewElements/DocumentRelationshipsItem.jsx'

const _DossierRelationships = React.createClass({
    render: function() {
        let fullView = (this.props.index === this.props.state.appReducer.resultsFullViewID);

        let relationshipsArr = [['Adopted act', []], ['Initiated by', []], ['Contains documents', []],
            ['Legal basis', []], ['Related dossier', []]];
        //Eurlex adopted act
        if (typeof this.props.data.adoptedAct != 'undefined' && this.props.data.adoptedAct != '')
            this.props.data.adoptedAct.map((item) => relationshipsArr[0][1].push(item));

        // let initiatedArr = [];
        //Eurlex initiatedBy
        if (typeof this.props.data.initiatedByCelex != 'undefined' && this.props.data.initiatedByCelex != '')
            this.props.data.initiatedByCelex.map((item) => relationshipsArr[1][1].push(item));

        let workArr = [];
        //Eurlex event contains work && National document added to dossier
        this.props.data.events.map((event) => {
            if (typeof event.eventContainsWork != 'undefined' && event.eventContainsWork != '')
                event.eventContainsWork.map((work) => relationshipsArr[2][1].push(work));
            else if (typeof event.documentID != 'undefined' && event.documentID != '')
                relationshipsArr[2][1].push(event.documentID)
        });

        let legalBasisArr = [];
        //Eurlex legal basis
        if (typeof this.props.data.legalBasis != 'undefined' && this.props.data.legalBasis != '') {
            if (Array.isArray(this.props.data.legalBasis))
                this.props.data.legalBasis.map((item) => relationshipsArr[3][1].push(item));
            else
                relationshipsArr[3][1].push(this.props.data.legalBasis);
        }

        let relatedDossierArr = [];
        //Related dossiers
        if (typeof this.props.data.relatedDossiers != 'undefined' && this.props.data.relatedDossiers.length > 0) {
            this.props.data.relatedDossiers.map((dossier) => {
                relationshipsArr[4][1].push(dossier.relatedDossier.title);
            })
        }

        return(
            <tr className="">
                <th className="">
                    Relationships
                </th>
                <td>
                    <span>
                {relationshipsArr.map((rel, i)=> {
                    if (rel[1].length > 0) {
                        return <DocumentRelationshipsItem
                            label={rel[0]}
                            items={rel[1]}
                            fullView={fullView}
                            data={this.props.data}
                        />
                    }
                })}
            </span>
                </td>
            </tr>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const DossierRelationships = connect(mapStateToProps, mapDispatchToProps)(_DossierRelationships);

exports.DossierRelationships = DossierRelationships;