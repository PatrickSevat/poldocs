import React from 'react';
import { connect } from 'react-redux';
import {} from '../../../actions/actions';

const _DossierModifiedDate = React.createClass({
    render: function() {
        if (this.props.state.appReducer.resultListType !== 'dossiers')
            return null;

        let isoString = new Date(this.props.data.dateImported);
        let humanReadable = isoString.toLocaleDateString();
        
        return(
            <tr className="">
                <th className="">
                    Date modified
                </th>
                <td>
                    {humanReadable}
                </td>
            </tr>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const DossierModifiedDate = connect(mapStateToProps, mapDispatchToProps)(_DossierModifiedDate);

exports.DossierModifiedDate = DossierModifiedDate;