import React from 'react';
import { connect } from 'react-redux';
import lodash from 'lodash';
import { setDocumentInDossierTimeLine } from '../../../actions/actions';

const _DossierEventDocuments = React.createClass({
    openDoc (docData) {
        this.props.openDocument(docData)
    },
    openExternalDoc (id) {

    },
    getDocs (work) {
        let workFiltered = lodash.uniqWith(work, lodash.isEqual);
        let documentIDArr = this.props.data.documentID;
        if (!Array.isArray(documentIDArr))
            documentIDArr = [documentIDArr];

        let docs = [];
        // We are missing all docs
        if (workFiltered.length != documentIDArr.length && workFiltered.length == 0) {
            console.log('length mismatch no works, index: '+this.props.index);
            console.log(this.props.data);
            documentIDArr.map((docID) => {
                if (docID)
                    docs.push({
                        label: docID,
                        data: null,
                        warning: true
                    })
            })
        }
        else if (workFiltered.length != documentIDArr.length && workFiltered.length > 0) {
            console.log('length mismatch, workFiltered length: '+workFiltered.length+', docIDArr length: '+documentIDArr.length)
            console.log('workFiltered:');
            console.log(workFiltered);
            console.log('documentIDArr:');
            console.log(documentIDArr);
        }

        //    We have all docs
        else
            workFiltered.map((workItem) => {
                docs.push({
                    label: workItem.title,
                    data: workItem,
                    warning: false
                })
            });
        return docs;
    },
    render: function() {
        //Work item can contain empty items => filter these
        let work = this.props.data.work;

        let docs = this.getDocs(work);

        if (docs.length == 0) {
            console.log('docs length 0, index: '+this.props.index);
                return (
                    <div className="timeline-item-data-item timeline-document-id">
                        <span>This event contains no documents</span>
                    </div>
                )
            }

        return(
            <div className="timeline-item-data-item timeline-document-id">
                <span>Documents: </span>
                <ul>
                    {docs.map((doc, docIndex) => {
                        if (!doc.warning)
                            return (<li>
                                <span
                                    className="looks-like-link"
                                    onClick={() => this.openDoc(doc.data)}
                                >
                                    {doc.label}
                                </span>
                                <br /><small>
                                    {typeof doc.data.subTitle != 'undefined' ? doc.data.subTitle : doc.data._key}
                                </small>
                            </li>);
                        else
                            return (
                                <li
                                    className="looks-like-link warning"
                                    onClick={() => this.openExternalDoc(doc.label)}>
                                    <i className="fa fa-exclamation-triangle"
                                />
                                    {doc.label}

                                </li>

                            );
                    })}
                </ul>

            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        openDocument: (doc) => {
            console.log('opening document, key: '+doc._key);
            dispatch(setDocumentInDossierTimeLine(doc))
        }
    }
};

const DossierEventDocuments = connect(mapStateToProps, mapDispatchToProps)(_DossierEventDocuments);

exports.DossierEventDocuments = DossierEventDocuments;