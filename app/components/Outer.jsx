import React from 'react';
import {Sidebar} from './Sidebars/MenuSidebar.jsx'
import { Provider } from 'react-redux';
import { applyMiddleware, createStore, combineReducers } from 'redux';
import { appReducer } from '../reducers/appReducer';


let combinedReducer = combineReducers({
    appReducer
});

const store = createStore(
    combinedReducer
);

//TODO add Open Graph & Twitter Cards meta tags based on route => get route from express
//FB & Linkedin use Open Graph, Twitter uses OG as fallback

//TODO add a footer with made by cr-8.nl and photo credit, also credits to sources

const Outer = React.createClass({
    componentWillMount () {
        
    },
    componentDidMount() {
        
    },
    render: function () {
        return (
            <html>
            <head>
                <title>Poldocs - Search and find political documents on EU, national and local levels</title>
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <meta name="description" content='Poldocs searches documents from the EU, Dutch government and Dutch local government. Find parliamentary and legislative documents, find politicians.' />
                <meta httpEquiv="Content-Type" content="text/html; charset=UTF-8" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"/>
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/8.5.1/nouislider.min.css" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.4/css/bootstrap.min.css" />
                <link rel="stylesheet" href="/static/hawcons.css" />
                <link rel="stylesheet" href="/static/flags32.css" />
                <link rel="stylesheet" href="/static/react-datepicker.min.css" />
                <link href="https://fonts.googleapis.com/css?family=Exo:100,200,300,400,500,600,700|Racing+Sans+One|Raleway:100,200,300,400,500,600,700&subset=latin-ext" rel="stylesheet" />
                <link rel="stylesheet" href="/static/app.css"/>
            </head>
            <body>
            <Provider store={store}>
                <div id="main-container" className={this.props.path}>
                    <Sidebar
                        path={this.props.path}
                    />
                    {this.props.children}
                    {this.props.main}
                    {this.props.secondarySidebar}
                </div>
            </Provider>
            {/*Remember, public folder is already set to static in server.js so no need to reference to public folder in src='' */}
            <script src='/static/bundle.js'></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenMax.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/plugins/TextPlugin.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/8.5.1/nouislider.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/masonry/4.1.1/masonry.pkgd.min.js"></script>
            </body>
            </html>

        )
    }
});

module.exports = Outer;