import React from 'react';
import { connect } from 'react-redux';
import { MenuItem } from './MenuItem.jsx';

const _MenuItems = React.createClass({
    render: function() {
        let loggedIn = this.props.state.appReducer.loggedIn;
        let userID = (typeof localStorage !== 'undefined' && loggedIn) ? localStorage.user_id : null;
        let menuItemsArr = [
            {
                label: 'View your profile',
                icon: 'fa-user',
                navigate: true,
                loginRequired: true,
                navigateTo: '/'+userID
            },{
                label: 'Navigate to home',
                icon: 'fa-home',
                navigate: true,
                loginRequired: false,
                navigateTo: '/'
            },{
                label: 'Log in',
                icon: 'fa-sign-in',
                navigate: false,
                loginRequired: false
            },{
                label: 'Log out',
                icon: 'fa-sign-out',
                navigate: false,
                loginRequired: true
            },{
                label: 'Go to (advanced) search',
                icon: 'fa-search',
                navigate: true,
                navigateTo: '/search',
                loginRequired: false
            },{
                label: 'Find (recent) documents',
                icon: 'fa-file-text',
                navigate: true,
                navigateTo: '/documents',
                loginRequired: false
            },{
                label: 'Find a dossier',
                icon: 'fa-folder-open',
                navigate: true,
                navigateTo: '/dossiers',
                loginRequired: false
            },{
                label: 'About Poldocs',
                icon: 'fa-info',
                navigate: true,
                navigateTo: '/about',
                loginRequired: false
            },{
                label: 'Poldocs API',
                icon: 'fa-code',
                navigate: true,
                navigateTo: '/dev',
                loginRequired: false
            }
        ];

        return (
            <div id="menu-items-wrapper">
                {menuItemsArr.map((item, index) => {
                    return (
                        <MenuItem
                            key = {'menu-item-'+index}
                            label={item.label}
                            icon={item.icon}
                            navigate={item.navigate}
                            loginRequired={item.loginRequired}
                            navigateTo={typeof item.navigateTo !== 'undefined' ? item.navigateTo : null}
                            />
                    )
                })}
            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

let MenuItems = connect(mapStateToProps, mapDispatchToProps)(_MenuItems);

exports.MenuItems = MenuItems;