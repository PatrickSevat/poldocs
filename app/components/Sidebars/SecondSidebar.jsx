import React from 'react';
import {openCloseSecondSidebar} from '../../actions/actions'
import { connect } from 'react-redux';
import { Facet } from './Facets/Facet.jsx';

//'Authors', 'Document type', 'Parliamentary year', 'Part of dossier', 'Categories'

const _SecondSidebar = React.createClass({
    openCloseSidebar () {
        this.props.openCloseSidebar();
        TweenMax.to('.second-sidebar-closed', .25, {width: '33vw', ease: "Power0.easeNone"});
        TweenMax.to('.second-sidebar-open', .25, {width: '25px', ease: "Power0.easeNone"});
    },
    render: function () {
        let sidebarHiderText = this.props.state.appReducer.showSecondSidebar ? 'Close filters' : 'Open filters';
        let sidebarHiderIcon = this.props.state.appReducer.showSecondSidebar ? ' fa-caret-left' : ' fa-caret-right';
        let sidebarType = this.props.state.appReducer.resultListType;
        return (
            <div
                className={"second-sidebar"+(this.props.state.appReducer.showSecondSidebar ? ' second-sidebar-open' : ' second-sidebar-closed')}
                >
                <div
                    id="sidebar-hider"
                    onClick={this.openCloseSidebar}>
                    <span id="hider-text">{sidebarHiderText}<i className={'fa'+sidebarHiderIcon} /></span>
                </div>
                <div className="second-sidebar-wrapper">
                    <div id="second-sidebar-title-wrapper">
                        <h3 id="second-sidebar-title">
                            Filters
                        </h3>
                    </div>
                    
                    <div id="second-sidebar-facets-wrapper">
                        {this.props.state.appReducer.resultFacets.map((item, index) => {
                            if (item.facetVisible && item.usedIn.indexOf(sidebarType) > -1)
                                return <Facet 
                                    data={item} 
                                    key={index} 
                                    listType={this.props.state.appReducer.resultListType} />
                        })}
                    </div>
                    
                </div>

            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        openCloseSidebar: () => {
            dispatch(openCloseSecondSidebar())
        }
    }
};

let SecondSidebar = connect(mapStateToProps, mapDispatchToProps)(_SecondSidebar);

module.exports = SecondSidebar;