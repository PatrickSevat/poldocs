import React from 'react';
import { connect } from 'react-redux';
import { logIn, logOut, showLogInModal } from '../../actions/actions';
import jwtDecode from 'jwt-decode';

import { CreateNewUserModal } from './../modals/CreateNewUserModal.jsx';
import { LogInModal } from './../modals/LogInModal.jsx';
import { SocialMenuItems} from './SocialMenuItems.jsx';
import { MenuItems } from './MenuItems.jsx';


const _Sidebar = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
        this.hoverAnimations();
        this.props.checkToken();
    },
    componentDidUpdate() {
        this.hoverAnimations()
    },
    hoverAnimations() {
        $(".sidebar-block, .sidebar-block-social").hover(over, out);
        function over(){
            TweenMax.to(this, 0.5, {backgroundColor:"rgba(0, 0, 0, 0.7)", color: 'rgb(245, 245, 245)'});
        }
        function out(){
            TweenMax.to(this, 0.5, {backgroundColor:"rgba(245, 245, 245, 0.7)", color: 'rgb(0, 0, 0)'});
        }

        $("#icon-circle").hover(overIcon, outIcon);
        function overIcon(){
            TweenMax.to(this, 0.5, {backgroundColor:"rgba(0, 0, 0, 0.85)", color: 'rgb(245, 245, 245)'});
        }
        function outIcon(){
            TweenMax.to(this, 0.5, {backgroundColor:"rgba(245, 245, 245, 0.85)", color: 'rgb(0,0,0)'});
        }
        
        $(".poldocs-button").hover(over, out);
        function over(){
            TweenMax.to(this, 0.5, {backgroundColor:"rgb(0,0,0)", color: 'rgb(245, 245, 245)'});
        }
        function out(){
            TweenMax.to(this, 0.5, {backgroundColor:"rgb(245, 245, 245)", color: 'rgb(0,0,0)'});
        }
    },
    navigateTo(url) {
    },
    render: function () {
        let modal, secondarySidebar;
        if (this.props.state.appReducer.showCreateNewUserModal) modal = <CreateNewUserModal />;
        if (this.props.state.appReducer.showLogInModal) modal = <LogInModal />;
        
        return (
            <div id="sidebar-wrapper">
                <div id="icon">
                    <div id="icon-circle">
                        <span id="icon-text">pd</span>
                    </div>
                </div>
                <MenuItems
                    
                />
                <SocialMenuItems />
                {modal}
            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        logOut: () => {
            console.log('logging out');
            localStorage.clear();
            dispatch(logOut());
        },
        checkToken: function() {
            if (typeof window !== 'undefined') {
                if (window.localStorage.token !== undefined) {
                    //check that the token is not expired
                    //THIS IS NOT VERIFICATION!
                    // Merely intended to not give a false sense of the user being logged in
                    let now = Date.now();
                    let expiryStamp;
                    let decoded;
                    decoded = jwtDecode(window.localStorage.token);
                    expiryStamp = decoded.exp;
                    console.log('Expiry: '+expiryStamp );
                    if (now > (expiryStamp*1000)) {
                        console.log('token expired');
                        this.logOut()
                    }
                    else if (now < (expiryStamp*1000)) {
                        dispatch(logIn());
                    }
                }    
            }
        }
    }
};

let Sidebar = connect(mapStateToProps, mapDispatchToProps)(_Sidebar);

exports.Sidebar = Sidebar;

