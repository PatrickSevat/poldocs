import React from 'react';
import { connect } from 'react-redux';

const _SocialMenuItems = React.createClass({
        render: function () {
            return (
                <div className="sidebar-block-bottom">
                    <div className="sidebar-block-half">
                        <div className="sidebar-block-social">
                            <i className="fa fa-twitter" />
                        </div>
                        <div className="sidebar-block-social">
                            <i className="fa fa-linkedin" />
                        </div>
                    </div>
                    <div className="sidebar-block-half">
                        <div className="sidebar-block-social">
                            <i className="fa fa-facebook" />
                        </div>
                        <div className="sidebar-block-social">
                            <i className="fa fa-envelope" />
                        </div>
                    </div>
                </div>
            )
        }
    });

exports.SocialMenuItems = _SocialMenuItems;

