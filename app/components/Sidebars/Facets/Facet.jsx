import React from 'react';
import { connect } from 'react-redux';
import {FacetChoiceCheckbox} from './FacetChoiceCheckbox.jsx'
import {FacetChoiceDateSlider} from './FacetChoiceDateSlider.jsx'
import { toggleFacetValuesVisible, setFacetState, toggleFacetCutOffValues, resultsReceived, toggleAllFacetValues} from '../../../actions/actions'

const _Facet = React.createClass({
    getMetaDataAttr (title) {
        switch (title) {
            case 'Authors':
                return 'authors';
            case 'Document type':
                return 'type';
        }
    },
    componentDidMount () {

    },
    toggleAll(facetTitle) {
        this.props.deselectAll(facetTitle);
        
    },
    render: function () {
        //let choices will contain the individual choices for each facet
        let choices, choicesCutOffDiv, toggleAll;

        //Render individual choices for facet type checkbox and maximize them to 10 + show more more button
        if (this.props.data.type == 'checkbox' && this.props.data.values !== null) {
            //Individual choices rendered by FacetChoiceCheckbox
            choices = this.props.data.values.map((item, index) => {
                let label = typeof item.count === 'undefined' ? item.label : item.label+` (`+item.count+`)`;
                return <FacetChoiceCheckbox
                    facetTitle={this.props.data.title}
                    internalLabel={this.props.data.internalLabel}
                    internalValueLabel={item.internalLabel}
                    type={this.props.data.type} 
                    label={label}
                    checked={item.checked}
                    visible={item.visible}
                    listType={this.props.data.listType}
                    key={index}
                />
            });
            // Rendering Show more \ show less buttons if applicable
            if (this.props.data.values.length > 10 && this.props.data.numberOfVisibleChoicesReduced ) {
                choicesCutOffDiv = (
                    <span
                        className="poldocs-button facet-options-toggle-cut-off"
                        onClick={() => this.props.toggleCutOffValues(this.props.data.title)}
                    >
                        More
                    </span>
                )
            }
            else if (this.props.data.values.length > 10 && !this.props.data.numberOfVisibleChoicesReduced ) {
                choicesCutOffDiv = (
                    <span
                        className="poldocs-button facet-options-toggle-cut-off"
                        onClick={() => this.props.toggleCutOffValues(this.props.data.title)}
                    >
                        Less
                    </span>
                )
            }

        //    Toggle all button
            toggleAll = (<span
                className="poldocs-button"
                onClick={() => this.toggleAll(this.props.data.title)}
            >Toggle all </span>)
        }
        //    Render facet range type, ex date when data is available
        else if (this.props.data.type =='range' && this.props.data.values != null) {
            choices = <div id="date-range" >
                <div id="date-slider">
                    <FacetChoiceDateSlider
                        data={this.props.data}
                    />
                </div>
                <span>Start: <span id="date-slider-start"/></span>
                <br/><span >End (incl): <span id="date-slider-end"/></span>
                <div id="date-picker"></div>
            </div>
        }
        //    retrieve the data needed for date range (needs to be done async after rendering on due to library used for slider)
        else if (this.props.state.appReducer.resultsData !== null &&
            this.props.data.type =='range' && this.props.data.values == null) {
            this.getDateRange();
        }

        return (
            <div className="facet">
                <div className="facet-accordion">
                    <span 
                        className="facet-title" 
                        onClick={() => this.props.toggleFacetValuesVisible(this.props.data.title)}>
                        {this.props.data.title}
                    </span>
                    <i 
                        className={'fa '+(this.props.data.valuesVisible ? 'fa-caret-up' : 'fa-caret-down')}
                        onClick={() => this.props.toggleFacetValuesVisible(this.props.data.title)}
                    />
                    <div className={"facet-options facet-options-visible-"+this.props.data.valuesVisible}>
                        {choices}
                        <div className="facet-options-cut-off-toggle-wrapper">
                            {choicesCutOffDiv}
                            {toggleAll}
                        </div>

                    </div>
                </div>
            </div>
        )
    },
    getDateRange () {
        this.props.setFacetState(this.props.data.title, {
            rangeMin: this.props.state.appReducer.resultsData[this.props.state.appReducer.resultsData.length-1].dateImported || 0,
            rangeMax: this.props.state.appReducer.resultsData[0].dateImported,
            selectedMin: this.props.state.appReducer.resultsData[this.props.state.appReducer.resultsData.length-1].dateImported,
            selectedMax: this.props.state.appReducer.resultsData[0].dateImported
        });
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        toggleFacetValuesVisible: (facetTitle) => {
            dispatch(toggleFacetValuesVisible(facetTitle))
        },
        setFacetState: (facetTitle, facetValuesArr) => {
            dispatch(setFacetState(facetTitle, facetValuesArr))
        },
        toggleCutOffValues: (facetTitle) => {
            dispatch(toggleFacetCutOffValues(facetTitle))
        },
        deselectAll: (facetTitle) => {
            dispatch(toggleAllFacetValues(facetTitle))
        }
    }
};

let Facet= connect(mapStateToProps, mapDispatchToProps)(_Facet);

exports.Facet = Facet;