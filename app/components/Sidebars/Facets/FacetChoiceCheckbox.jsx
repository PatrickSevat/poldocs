import React from 'react';
import { connect } from 'react-redux';
import { updateResultFacet } from '../../../actions/actions'

const _FacetChoiceCheckbox = React.createClass({
    checkboxChange() {
        // console.log('Checkbox status for: '+this.props.type+' - ');
        this.props.updateDocumentFacets({
            title: this.props.facetTitle,
            internalLabel: this.props.internalLabel,
            internalValueLabel: this.props.internalValueLabel,
            valuesLabel: this.props.label,
            value: !this.props.checked
        })
    },
    render: function () {
        //TODO check appReducer functions for generating authors and types
        //TODO Deselect all option
        return (
            <div
                className={"facet-option facet-option-checked-"+this.props.checked+" facet-option-visible-"+this.props.visible}
                onClick={this.checkboxChange}>
                    <div className="facet-option-icon-wrapper">
                        <i className={"fa "+(this.props.checked ? "fa-check-square-o" : "fa-square-o")} />
                    </div>
                    <div className="facet-option-label-wrapper">
                        <span className="facet-option-label">
                            {this.props.label}
                        </span>
                    </div>
            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateDocumentFacets: (obj) => {
            dispatch(updateResultFacet(obj));
        }
    }
};

let FacetChoiceCheckbox= connect(mapStateToProps, mapDispatchToProps)(_FacetChoiceCheckbox);

exports.FacetChoiceCheckbox = FacetChoiceCheckbox;