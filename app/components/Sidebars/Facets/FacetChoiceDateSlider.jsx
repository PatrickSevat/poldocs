import React from 'react';
import { connect } from 'react-redux';
import { updateResultFacetDateRange } from '../../../actions/actions'
import { timestampToYYYYMMDD } from '../../../../db/arangoDB/utils/dateFormat'

const _FacetChoiceDateSlider = React.createClass({
    updateDateRange (selectedMinTimestamp, selectedMaxTimestamp) {
        this.props.updateDocumentFacetDateRange({
            facetTitle: this.props.data.title,
            internalLabel: this.props.data.internalLabel,
            values: {
                rangeMin: this.props.data.values.rangeMin,
                rangeMax: this.props.data.values.rangeMax,
                selectedMin: timestampToYYYYMMDD(selectedMinTimestamp, '-'),
                selectedMax: timestampToYYYYMMDD(selectedMaxTimestamp, '-')
            }
        });
    },
    componentDidMount () {
        let dateSlider = document.getElementById('date-slider');
        if (typeof noUiSlider !== 'undefined') {
            let timestampMin = new Date(this.props.data.values.rangeMin).getTime();
            let timestampMax = new Date(this.props.data.values.rangeMax).getTime();

            //Min and max cannot be qual, TODO check if we need to improve 1 by whole day in ms
            if (timestampMin === timestampMax)
                timestampMax += 1;

            noUiSlider.create(dateSlider, {
                range: {
                    min: timestampMin,
                    max: timestampMax
                },
                step: 24 * 60 * 60 * 1000,
                start: [new Date(this.props.data.values.selectedMin).getTime(),
                    new Date(this.props.data.values.selectedMax).getTime()]
            });

            const dateValues = [
                document.getElementById('date-slider-start'),
                document.getElementById('date-slider-end')
            ];

            const self = this;

            dateSlider.noUiSlider.on('update', function( values, handle) {
                let selectedMinTimestamp = new Date(+values[0]);
                let selectedMaxTimestamp = new Date(+values[1]);
                self.updateDateRange(selectedMinTimestamp, selectedMaxTimestamp);

                dateValues[handle].innerHTML = new Date(+values[handle]).toDateString();
            });
        }
    },
    render: function () {
        // console.log('label in choice checkbox: '+this.props.label);
        return (
            null
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateDocumentFacetDateRange: (obj) => {
            // console.log('updating date range: ');
            // console.log(obj);
            dispatch(updateResultFacetDateRange(obj));
        }
    }
};

let FacetChoiceDateSlider = connect(mapStateToProps, mapDispatchToProps)(_FacetChoiceDateSlider);

exports.FacetChoiceDateSlider = FacetChoiceDateSlider;