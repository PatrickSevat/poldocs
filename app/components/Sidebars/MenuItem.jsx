import React from 'react';
import { connect } from 'react-redux';
import { logIn, logOut, showLogInModal, updatePath } from '../../actions/actions';
import { browserHistory } from 'react-router';

const _MenuItem = React.createClass({
    navigateTo(url) {
        this.props.updatePath(url);
        browserHistory.push(url);

    },
    render: function() {
        let menuItem;
        if ((!this.props.loginRequired || (this.props.loginRequired && this.props.state.appReducer.loggedIn)) && this.props.navigateTo) {
            menuItem = (
                <div className="sidebar-block" onClick={() => this.navigateTo(this.props.navigateTo)}>
                    <i className={"fa "+this.props.icon} />
                    <div className="sidebar-block-extended showOnHover">
                        <span className="sidebar-block-extended-text">{this.props.label}</span>
                    </div>
                </div>
            )
        }
        else if ((!this.props.loginRequired || (this.props.loginRequired && this.props.state.appReducer.loggedIn)) && !this.props.navigateTo) {
            let func;
            if (this.props.label === 'Log in') func = this.props.showLogInModal.bind(this);
            else if (this.props.label === 'Log out') func = this.props.logOut.bind(this);

            menuItem = (
                <div className="sidebar-block" onClick={func}>
                    <i className={"fa "+this.props.icon} />
                    <div className="sidebar-block-extended showOnHover">
                        <span className="sidebar-block-extended-text">{this.props.label}</span>
                    </div>
                </div>
            );
            if (this.props.state.appReducer.loggedIn && this.props.label === 'Log in')
                menuItem = null;
        }

        return (
            <div className="menu-item">
                {menuItem}
            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        logOut: () => {
            localStorage.clear();
            dispatch(logOut());
        },
        showLogInModal: () => {
            dispatch(showLogInModal());
        },
        updatePath: (url) => {
            dispatch(updatePath(url));
        }
    }
};

let MenuItem = connect(mapStateToProps, mapDispatchToProps)(_MenuItem);

exports.MenuItem = MenuItem;