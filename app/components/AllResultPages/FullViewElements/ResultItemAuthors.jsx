import React from 'react';
import { connect } from 'react-redux';
import {} from '../../../actions/actions';
import lodash from 'lodash';

const _ResultItemAuthors = React.createClass({
    goToAuthor (authorName) {
          console.log('go to author: '+authorName);
    },
    render: function() {
        //need to check for null when switching pages
        // if (this.props.data.authors == null)
        //     return null;

        if (this.props.state.appReducer.resultListType == 'documents' && this.props.data.authors === '') {
            return null
        }

        let authorsArr = this.props.data.authors;
        authorsArr = lodash.uniq(authorsArr);

        //Cut off values when item has too many authors & no fullView
        let moreSpan;
        let authorsTotalLength = authorsArr.length;
        if (authorsArr.length > 3 && this.props.index != this.props.state.appReducer.resultsFullViewID) {
            authorsArr = authorsArr.slice(0,3);
            moreSpan = (
                <span 
                    onClick={this.setResultFullViewID}
                    className="looks-like-link"    >
                    <br />{'+'+(authorsTotalLength-3)+' authors'}
                </span>
            )
        }

        return(
            <tr className="">
                <th className="">
                    Authors
                </th>
                <td>
                    {authorsArr.map((author, index) => {
                        if (index !== authorsArr.length-1) {
                            return (
                                <span>
                                    <span
                                        className="looks-like-link"
                                        onClick={() => this.goToAuthor(author)}
                                        key={index}
                                    >
                                        {author}
                                    </span>
                                    <span>, </span>
                                </span>
                            )
                        } 
                        else {
                            return (
                                <span>
                                    <span className="looks-like-link"
                                          onClick={() => this.goToAuthor(author)}
                                          key={index}
                                    >
                                        {author}
                                    </span>
                                    {moreSpan}
                                </span>

                            )                            
                        }
                    })}
                </td>
            </tr>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const ResultItemAuthors = connect(mapStateToProps, mapDispatchToProps)(_ResultItemAuthors);

exports.ResultItemAuthors= ResultItemAuthors;