import React from 'react';
import { connect } from 'react-redux';
import {} from '../../../actions/actions';

const _ResultItemKey = React.createClass({
    render: function() {
        let label, value;
        if ((this.props.state.appReducer.resultListType == 'documents' || this.props.state.appReducer.resultListType == 'search')
            || (this.props.state.appReducer.resultListType === 'dossiers' && this.props.state.appReducer.showDocumentInDossierTimeline != null)) {
            if (this.props.data.level === 'international' && this.props.data.celexKey !== '') {
                label = 'Celex ID';
                value = this.props.data.celexKey;
            }
            else if (this.props.data.level === 'international') {
                label = 'Cellar ID';
                value = this.props.data._key;
            }
            else if (this.props.data.level == 'national') {
                label = 'ID';
                value = this.props.data._key;
            }
        }
        else if (this.props.state.appReducer.resultListType === 'dossiers') {
            if (this.props.data.source === 'Eurlex') {
                // console.log('Eurlex in itemKey');
                label = 'Interinstitutional reference';
                value = this.props.data.interinstitutionalReference.replace(/-/g, '/');
            }
            else if (this.props.data.source == 'officieleBekendmakingen') {
                label = 'Dossier ID';
                value = this.props.data._key;
            }
        }

        return(
            <tr className="">
                <th className="">
                    {label}
                </th>
                <td>
                    <span>{value}</span>
                </td>
            </tr>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const ResultItemKey= connect(mapStateToProps, mapDispatchToProps)(_ResultItemKey);

exports.ResultItemKey= ResultItemKey;