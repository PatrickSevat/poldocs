import React from 'react';
import { connect } from 'react-redux';
import {setResultFullView, setResultFullViewID} from '../../../actions/actions';
import lodash from 'lodash';

const _ResultItemCategories = React.createClass({
    goToCategory (categoryName) {
        console.log('go to category: '+categoryName);
    },
    setResultFullViewID () {
        this.props.setResultFullView(this.props.data);
        this.props.setResultFullViewID(this.props.data._id)
    },
    processCategories (categoriesObj) {
        let returnArr = [];
        categoriesObj.beleidsagenda.forEach((item) => {
            if (item != null)
                returnArr.push(item.top+' - '+item.sub)
        });
        returnArr = returnArr.concat(categoriesObj.eurovoc);

        returnArr = lodash.uniq(returnArr);
        returnArr = lodash.uniqWith(returnArr, lodash.isEqual);
        return returnArr;
    },
    render: function() {

        /**
         * this.props.data.categories as saved in db
         * */
        // "categories": {
        //     "beleidsagenda": [
        //         {
        //             "top": "Onderwijs en wetenschap",
        //             "sub": "Hoger onderwijs"
        //         }
        //     ],
        //         "eurovoc": []
        // }

        let categories = this.processCategories(this.props.data.categories);
        let moreSpan = null;
        let categoriesTotalLength = categories.length;

        //Restrict maximum displayed categories to 3
        if (categories.length > 3 && this.props.index != this.props.state.appReducer.resultsFullViewID) {
            categories = categories.slice(0,3);
            moreSpan = (
                <span onClick={this.setResultFullViewID}
                      className="looks-like-link">
                    <br />{'+'+(categoriesTotalLength-3)+' categories'}
                </span>
            )
        }

        if (categories.length == 0)
            return null;

        return(
            <tr className="">
                <th className="">
                    Categories
                </th>
                <td>
                    {categories.map((category, index) => {
                        return (
                            <span>
                                    <span
                                        className="looks-like-link"
                                        onClick={() => this.goToCategory(category)}
                                        key={index}
                                    >
                                        {category}
                                    </span>
                                    {index !== categories.length-1 ? <span>, </span> : null}
                                    {moreSpan}
                                </span>
                        )
                    })}
                </td>
            </tr>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        setDocumentFullView: (data) => {
            dispatch(setResultFullView(data));
        },
        setDocumentFullViewID: (ID) => {
            console.log('setFullViewID: '+ID);
            dispatch(setResultFullViewID(ID))
        }
    }
};

const ResultItemCategories = connect(mapStateToProps, mapDispatchToProps)(_ResultItemCategories);

exports.ResultItemCategories= ResultItemCategories;