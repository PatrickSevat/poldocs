import React from 'react';
import { connect } from 'react-redux';
import {setResultFullView, setResultFullViewID, setDocumentInDossierTimeLine} from '../../../actions/actions';

const _ResultItemTitle = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
    },
    openDocument() {
        this.props.setDocumentFullView(this.props.data);
        this.props.setDocumentFullViewID(this.props.index);
    },
    closeFullView() {
        console.log('close doc');

        //Make sure dossier full view does not close when only document should close
        if (this.props.state.appReducer.showDocumentInDossierTimeline === null) {
            this.props.setDocumentFullView(null);
            this.props.setDocumentFullViewID(null);
        }
        this.props.setDocumentInDossierTimeLine(null);
    },
    getFlag(country) {
        if (typeof country == 'undefined')
            return null;
        if (country.toLowerCase() == 'nl')
            return <span className="f32"><span className="flag nl"/></span>;
        else if (country.toLowerCase() == 'eu')
            return <span className="f32"><span className="flag _European_Union"/></span>
    },
    render: function() {
        let displayTitle;
        if (this.props.state.appReducer.resultListType == 'documents' || this.props.state.appReducer.resultListType == 'search')
            displayTitle = (typeof this.props.data.altTitle === 'undefined' || this.props.data.altTitle == '') ? this.props.data.title : this.props.data.altTitle[0];
        else if (this.props.state.appReducer.resultListType == 'dossiers')
            displayTitle = typeof this.props.data.title != 'undefined' ? this.props.data.title : this.props.data._key;

        if (Array.isArray(displayTitle))
            displayTitle = displayTitle[0];
        
        if (displayTitle.length > 100 && this.props.state.appReducer.resultsFullViewID !== this.props.index ) {
            displayTitle = displayTitle.slice(0,97)+'...';
        }

        //TODO add icon flags for categories adopted act etc.
        let icon;
        let flag = this.getFlag(this.props.data.country);


        let closeButton = this.props.active ?
            <td className="close-button-cell">
                    <span
                        onClick={() => this.closeFullView()}
                        className="float-right"
                    >X</span>
            </td> : null;
        return(
            <tr 
                className="list-item-title-row">
                <th className="">
                    Title
                </th>
                <td className="list-item-title-table-data">
                    {flag}
                    <h3
                        onClick={() => this.openDocument()}
                        className="list-item-title">
                        {displayTitle}
                    </h3>
                </td>
                {closeButton}
            </tr>    
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        setDocumentFullView: (data) => {
            dispatch(setResultFullView(data));
        },
        setDocumentFullViewID: (ID) => {
            console.log('setFullViewID: '+ID);
            dispatch(setResultFullViewID(ID))
        },
        setDocumentInDossierTimeLine: (data) => {
            dispatch(setDocumentInDossierTimeLine(data));
        }
    }
};

const ResultItemTitle = connect(mapStateToProps, mapDispatchToProps)(_ResultItemTitle);

exports.ResultItemTitle= ResultItemTitle;