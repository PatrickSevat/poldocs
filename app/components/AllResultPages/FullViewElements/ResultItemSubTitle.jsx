import React from 'react';
import { connect } from 'react-redux';
import {} from '../../../actions/actions';

const _ResultItemSubTitle = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
    },
    componentWillUnmount: function() {
    },
    render: function() {
        if (typeof this.props.data.subTitle === 'undefined' || this.props.data.subTitle === '') {
            return null
        }
        let displaySubTitle = this.props.data.subTitle;
        // if (this.props.state.appReducer.resultListType == 'documents')
        //     displaySubTitle = this.props.data.subTitle;
        // else if (this.props.state.appReducer.resultListType == 'dossiers')
        //     displaySubTitle = this.props.data.subTitle;
        
        
        return(
            <tr className="">
                <th className="">
                    Sub title
                </th>
                <td className="">
                    <span className="">{displaySubTitle}</span>
                </td>
            </tr>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const ResultItemSubTitle = connect(mapStateToProps, mapDispatchToProps)(_ResultItemSubTitle);

exports.ResultItemSubTitle= ResultItemSubTitle;