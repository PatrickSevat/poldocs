import React from 'react';
import { connect } from 'react-redux';
import {} from '../../../actions/actions';

const _ItemType = React.createClass({
    goToType(type) {
        //TODO account for documents \ dossiers
        console.log('go to type: '+type)
    },
    render: function() {
        let types = [];
        if (typeof this.props.data.type === 'undefined' || this.props.data.type === '') {
            return null
        }

        if (Array.isArray(this.props.data.type))
            types = this.props.data.type;
        else
            types.push(this.props.data.type);


        return(
            <tr className="">
                <th className="">
                    Type
                </th>
                <td className="">
                    {types.map((type, index) => {
                        if (index !== types.length-1) {
                            return (
                                <span>
                                    <span
                                        className="looks-like-link"
                                        onClick={() => this.goToType(type)}
                                        key={index}
                                    >
                                        {type}
                                    </span>
                                    <span>, </span>
                                </span>
                            )
                        }
                        else {
                            return (
                                <span className="looks-like-link"
                                      onClick={() => this.goToType(type)}
                                      key={index}
                                >
                                    {type}
                                </span>
                            )
                        }
                    })}
                </td>
            </tr>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const ItemType = connect(mapStateToProps, mapDispatchToProps)(_ItemType);

exports.ResultItemType= ItemType;