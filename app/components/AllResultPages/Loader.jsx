import React from 'react';
import { connect } from 'react-redux';

const _Loader = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
    },
    componentWillUnmount: function() {
    },
    render: function() {
        
        return(
            <div className="loader1">
                <div className="loader">
                    
                </div>

            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const Loader = connect(mapStateToProps)(_Loader);

exports.Loader = Loader;