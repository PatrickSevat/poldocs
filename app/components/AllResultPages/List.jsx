import React from 'react';
import TransitionsGroup from 'react-addons-css-transition-group';
import { connect } from 'react-redux';
import {updatePath} from '../../actions/actions';
import {ListPaginator} from './ListPaginator.jsx';
import {DocumentsListDocument} from './../Documents/DocumentsListDocument.jsx';
import {DossiersListDossier} from './../Dossiers/DossiersListDossier.jsx';

const _List = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
    },
    componentWillUnmount: function() {
        console.log('list unmount');
    },
    render: function() {
        let dataset = this.props.state.appReducer.resultsDataFiltered == null ? this.props.state.appReducer.resultsData : this.props.state.appReducer.resultsDataFiltered;
        let itemsLength = this.props.state.appReducer.resultsLength;

        //Used in 'Currently showing results 8 - 16'
        let showingItemsStart = (this.props.state.appReducer.resultsListPage-1)*this.props.state.appReducer.resultsPageSize;
        let showingItemsEnd = (this.props.state.appReducer.resultsListPage)*this.props.state.appReducer.resultsPageSize;

        //The displayed items
        let itemsList = dataset == null ? [] : dataset.slice(showingItemsStart, showingItemsEnd);

        let icon, title;
        switch (this.props.state.appReducer.resultListType) {
            case 'documents':
                icon = 'fa-file-text';
                title = 'Recent documents';
                break;
            case 'dossiers':
                icon = 'fa-folder-open';
                title = 'Recent dossiers';
                break;
            case 'search':
                icon = 'fa-search';
                title = 'Search results'
        }

        let items;
        if (itemsLength != 0 ) {
            items = itemsList.map((doc, index) => {
                // console.log('documents list index: '+index);
                // console.log(doc);
                if (this.props.state.appReducer.resultListType == 'documents' || this.props.state.appReducer.resultListType == 'search')
                    return  <DocumentsListDocument
                        key={index}
                        index={index}
                        data={itemsList[index]}
                    />;

                else if (this.props.state.appReducer.resultListType == 'dossiers')
                    return <DossiersListDossier
                        key={index}
                        index={index}
                        data={itemsList[index]}
                    />;
            });
        }

        let paginator;
        if (itemsLength >= this.props.state.appReducer.resultsPageSize && this.props.state.appReducer.resultsFullView == null) {
            paginator = <ListPaginator />;
        }

        return(
            <div id="list-container">
                <div id="list-header">
                    <h1 id="list-h1">
                        {title} <i className={"fa "+icon}  />
                    </h1>
                        <span>
                            Showing results {showingItemsStart+1} - {itemsLength < showingItemsEnd ? itemsLength : showingItemsEnd }, out of {itemsLength}
                        </span>
                </div>
                <TransitionsGroup
                    transitionName="list-loaded-animation"
                    component="div"
                    transitionEnterTimeout={500}
                    transitionLeaveTimeout={300}
                    transitionAppear={true}
                    transitionAppearTimeout={1500}
                    id="list-wrapper"
                >
                    <div id="gutter"></div>
                    <div id="list-item-width"></div>
                    {items}
                </TransitionsGroup>
                {paginator}
            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        updatePath: (url) => {
            dispatch(updatePath(url));
        }
    }
};

const List = connect(mapStateToProps, mapDispatchToProps)(_List);

module.exports = List;