import React from 'react';
import { connect } from 'react-redux';
import {selectResultPaginatorPage, retrievingData, retrievingDone, executeQuery, moreResultsReceived} from '../../actions/actions';
import { getSearchData } from '../../ajax/makeAjaxCall';
import { queryConstructor } from '../Search/QueryBuilder/QueryBuilderPages/queryConstructor';

const _ListPaginator = React.createClass({
    componentWillMount () {
    },
    componentDidMount() {
    },
    componentWillUnmount: function() {
    },
    selectPage (newPageNum) {
        let start = Date.now();
        let resultsLength = this.props.state.appReducer.resultsDataFiltered != null ? this.props.state.appReducer.resultsDataFiltered.length : this.props.state.appReducer.resultsData.length;
        if (newPageNum * this.props.state.appReducer.resultsPageSize <= resultsLength) {
            console.log('no new search query needed');
            this.props.selectPage(newPageNum);
        }
        else if (this.props.state.appReducer.resultListType == 'search') {
            
            this.props.selectPage(newPageNum);
            console.log('making new search');
            let query = queryConstructor(this.props.state.appReducer.queryBuilderValues, (newPageNum-1)* this.props.state.appReducer.resultsPageSize);
            this.props.executeSearchQuery(query);
            
            getSearchData(query).then((data) => {
                let parsed = JSON.parse(data);
                this.props.loadMore(parsed[0][0]['documentsGroup0'].results, 'search', parsed[0][0]['documentsGroup0'].count);
                console.log(Date.now() - start);
            }, (err) => {
                console.log('ajax err:');
                console.log(err);
            })
        }


    },
    render: function() {
        let leftToStart, leftBefore, rightNext, rightToEnd;
        let pages = [];

        //The active page
        let activeNum = this.props.state.appReducer.resultsListPage;

        //The last possible page
        let lastNum = this.props.state.appReducer.resultsListPageLast;

        //Determine the possible pages

        //To page 1 and one page back enabled
        if (activeNum > 1) {
            leftToStart =
                <div className="paginator-item"  onClick={() => this.selectPage(1)}>
                    <i className="fa fa-angle-double-left" />
                </div>;
            leftBefore =
                <div className="paginator-item" onClick={() => this.selectPage(activeNum-1)} >
                    <i className="fa fa-angle-left" />
                </div>;
        }

        //To last page and next page enabled
        if (activeNum < lastNum) {
            rightNext =
                <div className="paginator-item" onClick={() => this.selectPage(activeNum+1)} >
                    <i className="fa fa-angle-right"/>
                </div>;
            rightToEnd =
                <div className="paginator-item"  onClick={() => this.selectPage(lastNum)} >
                    <i className="fa fa-angle-double-right"/>
                </div>;
        }

        //Add page-2, page-1, page active, page+1, page+2
        for (let i = activeNum-2; i <= activeNum+2; i++ ) {
            if (i == activeNum) pages.push(<div className="paginator-item paginator-active" onClick={() => this.selectPage(i)} key={i}>{i}</div>);
            else if (i > 0 && i <= lastNum) pages.push(<div className="paginator-item" onClick={() => this.selectPage(i)} key={i}>{i}</div>)
        }
        return(
            <div id="doc-list-paginator-outer">
                <div id="doc-list-paginator-wrapper">
                    {leftToStart}
                    {leftBefore}
                    {pages}
                    {rightNext}
                    {rightToEnd}
                </div>

            </div>
        )
    }
});

const mapStateToProps = (state) => {
    return {state}
};

const mapDispatchToProps = (dispatch) => {
    return {
        selectPage: (pageNum) => {
            if (typeof $ !== 'undefined')
                $("html, body").animate({ scrollTop: 0 }, "slow");
            dispatch(selectResultPaginatorPage(pageNum));
        },
        retrievingData: () => {
            dispatch(retrievingData());
        },
        retrievingDone: () => {
            dispatch(retrievingDone());
        },
        executeSearchQuery: (query) => {
            dispatch(executeQuery(query));
        },
        loadMore: (data, type, count) => {
            dispatch(moreResultsReceived(data, type, count))
        }
    }
};

const ListPaginator = connect(mapStateToProps, mapDispatchToProps)(_ListPaginator);

exports.ListPaginator = ListPaginator;