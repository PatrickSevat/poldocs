/**
 * Created by Patrick on 19/07/2016.
 */
export const logIn = () => {
    return {
        type: 'LOGGED-IN'
    }
};

export const signUp = () => {
    return {
        type: 'SIGNED-UP'
    }
};

export const logOut = () => {
    return {
        type: 'LOGGED-OUT'
    }
};

export const hideLinkedInModal = () => {
    return {
        type: 'HIDE-LINKEDIN-MODAL'
    }
};

export const showLinkedInModal = () => {
    return {
        type: 'SHOW-LINKEDIN-MODAL'
    }
};

export const showCreateNewUserModal = () => {
    return {
        type: 'SHOW-CREATE-NEW-USER-MODAL'
    }
};

export const hideCreateNewUserModal = () => {
    return {
        type: 'HIDE-CREATE-NEW-USER-MODAL'
    }
};

export const showLogInModal = () => {
    return {
        type: 'SHOW-LOGIN-MODAL'
    }
};

export const hideLogInModal = () => {
    return {
        type: 'HIDE-LOGIN-MODAL'
    }
};

export const showError = (err) => {
    return {
        type: 'SHOW-ERROR',
        message: err
    }
};

export const homeSuggestionsReceived = (data) => {
    return {
        type: 'HOME-SUGGESTIONS-RECEIVED',
        data: data
    }
};

export const resultsReceived = (data, dataCollection, count) => {
    // console.log('action results received: ');
    // console.log('data:');
    // console.log(data);
    return {
        type: 'RESULTS-RECEIVED',
        data: data,
        dataCollection,
        count
    }
};

export const moreResultsReceived = (data, dataCollection, count) => {
    return {
        type: 'MORE-RESULTS-RECEIVED',
        data: data,
        dataCollection,
        count
    }
};

export const clearResults = () => {
    return {
        type: 'CLEAR-RESULTS'
    }
};

export const query = (q) => {
    return {
        type: 'QUERY-UPDATED',
        query: q
    }
};

export const updatePath = (path) => {
    return {
        type: 'PATH-UPDATED',
        path
    }
};

export const updateResultFacet = (obj) => {
    return {
        type: 'UPDATE-DOCUMENT-FACET',
        facet: obj
        // obj = {
        // title: 'Source'
        // internalLabel: 'source'
        // valuesLabel: 'EU (Eurlex)'
        // internalValueLabel: 'Eurlex'
        // value: true (checked or not)
        // }
    }
};

export const updateResultFacetDateRange = (obj) => {
    return {
        type: 'UPDATE-DOCUMENT-FACET-DATE-RANGE',
        facet: obj
        // obj = {
        // faceTitle: 'Date'
        // values: {
        //      rangeMin
        //      rangeMax
        //      selectedMin
        //      selectedMax
        //      }
        // }
    }
};

export const toggleFacetCutOffValues = (title) => {
    return {
        type: 'UPDATE-DOCUMENT-FACET-TOGGLE-CUTOFF-VALUES',
        facetTitle: title
    }
};

export const toggleFacetValuesVisible = (title) => {
    return {
        type: 'UPDATE-DOCUMENT-FACET-VALUES-VISIBLE',
        facetTitle: title
    }
};

export const setFacetState = (title, valuesArr) => {
    return {
        type: 'UPDATE-DOCUMENT-FACET-STATE',
        facetTitle: title,
        values: valuesArr
    }
};

export const toggleAllFacetValues = (facetTitle) => {
    return {
        type: 'FACET-TOGGLE-ALL',
        facetTitle
    }
};

export const selectResultPaginatorPage = (page) => {
    return {
        type: 'UPDATE-DOCUMENT-PAGINATOR-PAGE',
        page
    }
};

export const setResultFullView = (data) => {
    // console.log('setDocFullView called');
    return {
        type: 'UPDATE-DOCUMENT-FULL-VIEW',
        data
    }
};

export const updateFullViewFileType = (fileType) => {
    // console.log('setDocFullView called');
    return {
        type: 'UPDATE-DOCUMENT-FULL-VIEW-FILE-TYPE',
        fileType
    }
};

export const setResultFullViewID = (ID) => {
    // console.log('setDocFullView called');
    return {
        type: 'UPDATE-DOCUMENT-FULL-VIEW-ID',
        ID
    }
};

export const openCloseSecondSidebar = () => {
    // console.log('setDocFullView called');
    return {
        type: 'OPEN-CLOSE-SECOND-SIDEBAR'
    }
};

export const setResultListType = (str) => {
    return {
        type: 'SET-RESULT-LIST-TYPE',
        value: str
    }
};

export const setDocumentInDossierTimeLine= (data) => {
    return {
        type: 'SET-DOCUMENT-IN-DOSSIER-TIMELINE',
        data
    }
};

export const selectQueryBuilderPage = (pageNum) => {
    return {
        type: 'SELECT-QUERY-BUILDER-PAGE',
        pageNum
    }
};

export const queryBuilderToggleCollection = (collectionName) => {
    return {
        type: 'QUERY-BUILDER-TOGGLE-COLLECTION',
        collectionName
    }
};

export const queryBuilderToggleSource = (sourceName) => {
    return {
        type: 'QUERY-BUILDER-TOGGLE-SOURCE',
        sourceName
    }
};

export const queryBuilderShowDocumentTypes = () => {
    return {
        type: 'QUERY-BUILDER-SHOW-DOCUMENT-TYPES'
    }
};

export const setQueryBuilderTypes = (types) => {
    return {
        type: 'QUERY-BUILDER-SET-TYPES',
        types
    }
};

export const toggleCutOffQueryBuilderSelectorTypes = (searchForType, level) => {
    return {
        type: 'QUERY-BUILDER-TOGGLE-CUT-OFF-SELECTOR-TYPES',
        searchForType,
        level
    }
};

export const toggleDocumentsTypesItem = (searchForType, level, itemLabel) => {
    return {
        type: 'QUERY-BUILDER-TOGGLE-DOCUMENTS-TYPES-ITEM',
        searchForType,
        level,
        itemLabel
    }
};

export const toggleQueryBuilderAdvancedSearch = () => {
    return {
        type: 'QUERY-BUILDER-TOGGLE-ADVANCED-SEARCH'
    }
};

export const searchFieldChange = (groupIndex, elementIndex, value) => {
    return {
        type: 'QUERY-BUILDER-SEARCH-FIELD-CHANGE',
        groupIndex,
        elementIndex,
        value
    }
};

export const searchCollectionChange = (groupIndex, value) => {
    return {
        type: 'QUERY-BUILDER-SEARCH-COLLECTION-CHANGE',
        groupIndex,
        value
    }
};

export const queryBuilderAdvancedSearchAddGroup = () => {
    return {
        type: 'QUERY-BUILDER-ADVANCED-SEARCH-ADD-GROUP'
    }
};

export const queryBuilderAdvancedSearchDeleteGroup = (groupIndex) => {
    return {
        type: 'QUERY-BUILDER-ADVANCED-SEARCH-DELETE-GROUP',
        groupIndex
    }
};

export const queryBuilderAdvancedSearchAddElement = (groupIndex) => {
    return {
        type: 'QUERY-BUILDER-ADVANCED-SEARCH-ADD-ELEMENT',
        groupIndex
    }
};

export const queryBuilderAdvancedSearchDeleteElement = (groupIndex, elementIndex) => {
    return {
        type: 'QUERY-BUILDER-ADVANCED-SEARCH-DELETE-ELEMENT',
        groupIndex,
        elementIndex
    }
};

export const queryBuilderAdvancedSearchElementChoiceSelect = (groupIndex, elementIndex, value) => {
    return {
        type: 'QUERY-BUILDER-ADVANCED-SEARCH-ELEMENT-CHOICE-SELECT',
        groupIndex,
        elementIndex,
        value
    }
};

export const queryBuilderAdvancedSearchElementExtraSelect = (groupIndex, elementIndex, value) => {
    return {
        type: 'QUERY-BUILDER-ADVANCED-SEARCH-ELEMENT-EXTRA-SELECT',
        groupIndex,
        elementIndex,
        value
    }
};

export const queryBuilderAdvancedSearchGroupRelationChoiceSelect = (value, groupIndex, elementIndex) => {
    return {
        type: 'QUERY-BUILDER-ADVANCED-SEARCH-GROUP-RELATION-CHOICE-SELECT',
        groupIndex,
        elementIndex,
        value
    }
};

//Start queryBuilderValues.dates
export const queryBuilderSelectDate = (dateLabel) => {
    return {
        type: 'QUERY-BUILDER-SELECT-DATE',
        dateLabel
    }
};

export const queryBuilderSetDate = (dateLabel, startEndStr, moment) => {
    return {
        type: 'QUERY-BUILDER-SET-DATE',
        dateLabel,
        moment,
        startEndStr
    }
};

//End of queryBuilderValues.dates

//Start of queryBuilderValues.authors
export const setQueryBuilderAuthors = (authors) => {
    return {
        type: 'QUERY-BUILDER-SET-AUTHORS',
        authors
    }
};

export const setQueryBuilderAuthorInput = (input) => {
    return {
        type: 'QUERY-BUILDER-SET-AUTHOR-INPUT',
        input
    }
};

export const queryBuilderSelectAuthor= (selectedItem) => {
    return {
        type: 'QUERY-BUILDER-SELECT-AUTHOR',
        selectedItem
    }
};
//End of queryBuilderValues.authors
export const resetAdvancedSearchState = () => {
    return {
        type: 'RESET-ADVANCED-SEARCH-STATE'
    }
};

export const executeQuery = (query) => {
    return {
        type: 'EXECUTE-QUERY',
        query
    }
};

//TODO build spinners for loading data, use retrievingData and retrievingDone
export const retrievingData = () => {
    return {
        type: 'RETRIEVING-DATA'
    }
};

export const retrievingDone = () => {
    return {
        type: 'RETRIEVING-DONE'
    }
};
