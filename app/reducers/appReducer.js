import {dateFormatFunc} from '../../db/arangoDB/utils/dateFormat'
const deepFreeze = require('deep-freeze');
import lodash from 'lodash';
const moment = require('moment');
//TODO determine how much of the appReducer can be recycled for other views:
// --Dossiers, people, document types, search
// Extra field for types in facets
// Do we share documents received?
// Could be useful programming wise, also might be better for memory usage, slower in browsing though

const resultFacets = [{
    title: 'Source',
    internalLabel: 'source',
    usedIn: ['documents', 'dossiers', 'search'],
    type: 'checkbox',
    numberOfVisibleChoicesReduced: false,
    facetVisible: true,
    valuesVisible: true,
    values: [
        {
            label: 'EU (Eurlex)',
            internalLabel: 'Eurlex',
            checked: true,
            visible: true,
            count: 0
        },{
            label: 'Netherlands national (Officiële bekendmakingen)',
            internalLabel: 'officieleBekendmakingen',
            checked: true,
            visible: true,
            count: 0
        },{
            label: 'Netherlands municipal (Open Raadsinformatie)',
            internalLabel: 'openraad',
            checked: true,
            visible: true,
            count: 0
        }
    ]
},{
    title: 'Level',
    internalLabel: 'level',
    usedIn: ['documents', 'dossiers', 'search'],
    type: 'checkbox',
    facetVisible: false,
    valuesVisible: true,
    numberOfVisibleChoicesReduced: false,
    values: [
        {
            label: 'International',
            internalLabel: 'international',
            checked: true,
            visible: true,
            count: 0
        },{
            label: 'National',
            internalLabel: 'national',
            checked: true,
            visible: true,
            count: 0
        },{
            label: 'Local',
            internalLabel: 'local',
            checked: true,
            visible: true,
            count: 0
        }
    ]
},{
    title: 'Authors',
    internalLabel: 'authors',
    usedIn: ['documents', 'dossiers', 'search'],
    type: 'checkbox',
    facetVisible: true,
    valuesVisible: true,
    numberOfVisibleChoicesReduced: false,
    values: null
},{
    title: 'Document type',
    internalLabel: 'type',
    usedIn: ['documents', 'search'],
    type: 'checkbox',
    facetVisible: true,
    valuesVisible: true,
    numberOfVisibleChoicesReduced: false,
    values: null
},{
    title: 'Dossier type',
    internalLabel: 'type',
    usedIn: ['dossiers', 'search'],
    type: 'checkbox',
    facetVisible: true,
    valuesVisible: true,
    numberOfVisibleChoicesReduced: false,
    values: null
},{
    title: 'Date',
    internalLabel: 'dateImported',
    usedIn: ['documents', 'dossiers', 'search'],
    type: 'range',
    numberOfVisibleChoicesReduced: false,
    facetVisible: true,
    valuesVisible: true,
    values: null
}];

const initState = {
    showCreateNewUserModal: false,
    showLogInModal: false,
    showLinkedInModal: false,
    loggedIn: false,
    signedUp: false,
    error: false,
    loading: false,
    homepageSuggestionData: null,
    showSecondSidebar: false,
    resultsData: null,
    resultsDataFiltered: null,
    resultsLength: 0,
    resultsPageSize: 8,
    resultsListPage: 1,
    resultsListPageLast: null,
    resultsFullView: null,
    resultsFullViewID: null,
    showDocumentInDossierTimeline: null,
    resultListType: null,
    documentFullViewFileType: 'pdf',
    resultFacets: resultFacets,
    savedSearches: [{query: "abcd", title: "title 12345 abcdef met nog wat er achter", lastRun: 1475931741}],
    query: null,
    queryBuilderPage: null,
    queryBuilderValues: {
        text: [],
        collections: {
            documents: true,
            dossiers: true
        },
        source: {
            Eurlex: true,
            officieleBekendmakingen: true
        },
        types: null,
        dates: {
            dateImported: {selected: false},
            documentDate: {selected: false},
            dateModified: {selected: false},
            availableDate: {selected: false},
            dateCreated: {selected: false},
            entryIntoForceDate: {selected: false},
            endOfValidityDate: {selected: false},
            deadline: {selected: false},
            dateMeeting: {selected: false}
        },
        authors: {
            authorValues: null,
            selected: [],
            searchText: null,
            searchable: false,
            allowNull: false
        }
    },
    queryBuilderDocumentTypesVisible: false,
    queryBuilderAdvancedSearch: false
};

deepFreeze(initState);

function getUpdatedArr(newDocumentFacets, state) {
    let filters = [];
    newDocumentFacets.map((facet) => {
        if (facet.type === 'checkbox' && facet.usedIn.indexOf(state.resultListType) > -1) {
            let facetInternal = facet.internalLabel;
            facet.values.map((value) => {
                if (!value.checked)
                    filters.push([facetInternal, typeof value.internalLabel !== 'undefined' ? value.internalLabel : value.label])
            })
        }
    });

    // console.log(filters);

    let updatedArr = state.resultsData.filter((item) => {
        let filterItem = false;
        filters.map((filter) => {
            //TODO add daterange filtering
            if ((item[filter[0]] === filter[1] || item[filter[0]].indexOf(filter[1]) > -1))
                filterItem = true;
        });
        if (!filterItem)
            return item
    });

    if (updatedArr.length === state.resultsData.length)
        updatedArr = null;

    return updatedArr
}

//queryBuilderValues.text[i] == searchGroup
const searchGroup = {
    elements: [],
    searchAttributes: ['Title', 'Subtitle', 'ID', 'Full text'],
    relationship: null
};

const newSearchGroupElement = {
    searchValue: '',
    choice: 'Contains',
    extras: ['Case insensitive'],
    relationship: null
};

const appReducer = (state = initState, action) => {

    switch (action.type) {
        case 'SHOW-CREATE-NEW-USER-MODAL':
        case 'HIDE-CREATE-NEW-USER-MODAL':
            return Object.assign({}, state, {
                showCreateNewUserModal: !state.showCreateNewUserModal,
                showCreateNewPollModal: false,
                showLogInModal: false
            });
        case 'SHOW-LOGIN-MODAL':
        case 'HIDE-LOGIN-MODAL':
            return Object.assign({}, state, {
                showLogInModal: !state.showLogInModal,
                showCreateNewUserModal: false,
                showCreateNewPollModal: false
            });
        case 'SHOW-LINKEDIN-MODAL':
        case 'HIDE-LINKEDIN-MODAL':
            return Object.assign({}, state, {
                showLinkedInModal: !state.showLinkedInModal,
                showCreateNewUserModal: false,
                showCreateNewPollModal: false
            });
        case 'LOGGED-IN':
            return Object.assign({}, state, {
                loggedIn: true
            });
        case 'SIGNED-UP':
            return Object.assign({}, state, {
                signedUp: true
            });
        case 'LOGGED-OUT':
            return Object.assign({}, state, {
                loggedIn: false,
                form: 'logIn'
            });
        case 'SHOW-ERROR':
            return Object.assign({}, state, {
                error: action.message.responseText
            });
        case 'HOME-SUGGESTIONS-RECEIVED':
            return Object.assign({}, state, {
                homepageSuggestionData: action.data
            });
        case 'RESULTS-RECEIVED':
            // console.log('appreducer results received, count: '+action.count);

            //TODO think of a solution for this hack!
            //Problem is that facets are built based upon the returned results not all results.
            // Thus for search (and maybe resultListTypes later too) we need to recalculate on each retrieval
            // TODO a better way would be to append
            //TODO best way is to use Solr

            let newDocumentFacets0 = getNewDocumentFacetsOnResultsReceived(state.resultFacets, action);

            //Update reducer with newDocumentFacets & data
            return Object.assign({}, state, {
                resultFacets: newDocumentFacets0,
                resultsData: action.data,
                resultsListPageLast: action.count ? Math.ceil(action.count / state.resultsPageSize) : Math.ceil(action.data.length / state.resultsPageSize),
                resultsLength: action.count ? action.count : action.data.length,
                loading: false
            });
        
        case 'MORE-RESULTS-RECEIVED':
            let newActionData = [...state.resultsData, ...action.data];
            console.log('newActionData & .length');
            console.log(newActionData);
            console.log(newActionData.length);
            let newAction = Object.assign({}, action, {data: newActionData});
            let newDocumentFacets20 = getNewDocumentFacetsOnResultsReceived(resultFacets, newAction);
            
            return Object.assign({}, state, {
                resultFacets: newDocumentFacets20,
                resultsData: newAction.data,
                resultsListPageLast: newAction.count ? Math.ceil(newAction.count / state.resultsPageSize) : Math.ceil(newAction.data.length / state.resultsPageSize),
                resultsLength: newAction.count ? newAction.count : newActionData.length,
                loading: false
            });

        case 'CLEAR-RESULTS':
            console.log('CLEARED RESULTS');
            return Object.assign({}, state, {
                resultsData: null,
                resultsDataFiltered: null,
                resultsListPage: 1,
                resultsListPageLast: null,
                resultsFullView: null,
                resultsFullViewID: null,
                resultsListType: null,
                resultFacets: resultFacets,
                showDocumentInDossierTimeline: null
        });
        
        case 'QUERY-UPDATED':
            return Object.assign({}, state, {
                query: action.query
            });
        case 'PATH-UPDATED':
            return Object.assign({}, state, {
                path: action.path
            });
        case 'UPDATE-DOCUMENT-FACET':
            //Update the facet checkbox itself
            let facet, facetIndex, valuesIndex;

            state.resultFacets.map((item, index) => {
                if (item.internalLabel === action.facet.internalLabel && item.usedIn.indexOf(state.resultListType) > -1) {
                    facet = item;
                    facetIndex = index;
                }
            });

            // console.log('\naction in update doc facet:');
            // console.log(action);

            facet.values.some((item, index) => {
                if (item.internalLabel === action.facet.internalValueLabel) valuesIndex = index;
            });
            
            let newFacet = state.resultFacets[facetIndex];
            newFacet.values[valuesIndex].checked = !newFacet.values[valuesIndex].checked;

            let newDocumentFacets = [...state.resultFacets.slice(0,facetIndex), newFacet, ...state.resultFacets.slice(facetIndex+1)];

            //Update resultsData
            let updatedArr = getUpdatedArr(newDocumentFacets, state);

            // console.log('newdoc facets update doc facets:');
            // console.log(newDocumentFacets);
            
            return Object.assign({}, state, {
                resultFacets: newDocumentFacets,
                resultsDataFiltered: updatedArr,
                resultsListPage: 1,
                resultsListPageLast: updatedArr == null ? Math.round(state.resultsData.length / 10) : Math.round(updatedArr.length / 10)
            });

        case 'FACET-TOGGLE-ALL':
            //Update facet itself
            let facetIndex6;
            state.resultFacets.some((item, index)=> {
                if (item.title === action.facetTitle) {
                    facetIndex6 = index;
                }
            });
            let newFacet6 = Object.assign({}, state.resultFacets[facetIndex6]);
            
            newFacet6.values.map((facetValue) => {
                facetValue.checked = !facetValue.checked
            });

            let newDocumentFacets6 = [...state.resultFacets.slice(0,facetIndex6), newFacet6, ...state.resultFacets.slice(facetIndex6+1)];

            let resultsArr6 = getUpdatedArr(newDocumentFacets6, state);

            return Object.assign({}, state, {
                resultFacets: newDocumentFacets6,
                resultsDataFiltered: resultsArr6,
                resultsListPage: 1,
                resultsListPageLast: resultsArr6 == null ? Math.round(state.resultsData.length / 10) : Math.round(resultsArr6.length / 10)
            });

        case 'UPDATE-DOCUMENT-FACET-VALUES-VISIBLE':
            let facetIndex2;
            let resultFacets2 = state.resultFacets;
            resultFacets2.some((item, index) => {
                if (item.title === action.facetTitle) {
                    facetIndex2 = index;
                }
            });

            let newFacet2 = resultFacets2[facetIndex2];
            newFacet2.valuesVisible = !newFacet2.valuesVisible;

            let newDocumentFacets2 = [...resultFacets2.slice(0,facetIndex2), newFacet2, ...resultFacets2.slice(facetIndex2+1)];
            return Object.assign({}, state, {resultFacets: newDocumentFacets2});

        case 'UPDATE-DOCUMENT-FACET-STATE':
            // console.log('UPDATE-DOCUMENT-FACET-STATE, facetTitle: '+action.facetTitle+', values:');
            // console.log(action.values);

            let facetIndex3;

            for (let i = 0; i < state.resultFacets.length; i++) {
                if (state.resultFacets[i].title === action.facetTitle) {
                    facetIndex3 = i;
                }
            }

            let newFacet3 = state.resultFacets.slice(facetIndex3, facetIndex3+1);
            let numberOfVisibleChoicesReduced = false;

            if (action.values.length > 10) numberOfVisibleChoicesReduced = true;
            
            let newFacet3WithValues = Object.assign({}, newFacet3[0], {values: action.values, numberOfVisibleChoicesReduced});

            let newDocumentFacets3 = [...state.resultFacets.slice(0,facetIndex3), newFacet3WithValues, ...state.resultFacets.slice(facetIndex3+1)];

            // console.log('update doc facet state:');
            // console.log(newDocumentFacets3);

            return Object.assign({}, state, {resultFacets: newDocumentFacets3});

        case 'UPDATE-DOCUMENT-FACET-TOGGLE-CUTOFF-VALUES':
            let facetIndex4;
            state.resultFacets.some((item, index) => {
                if (item.title === action.facetTitle) {
                    facetIndex4 = index;
                }
            });

            let newFacet4 = state.resultFacets[facetIndex4];
            if (!newFacet4.numberOfVisibleChoicesReduced) {
                newFacet4.numberOfVisibleChoicesReduced = !newFacet4.numberOfVisibleChoicesReduced;
                newFacet4.values.map((item, index) => {
                    if (index > 10) item.visible = false;
                    return item;
                })
            }
            else {
                newFacet4.numberOfVisibleChoicesReduced = !newFacet4.numberOfVisibleChoicesReduced;
                newFacet4.values.map((item, index) => {
                    item.visible = true;
                    return item;
                })
            }

            let newDocumentFacets4 = [...state.resultFacets.slice(0,facetIndex4), newFacet4, ...state.resultFacets.slice(facetIndex4+1)];
            return Object.assign({}, state, {resultFacets: newDocumentFacets4});

        case 'UPDATE-DOCUMENT-FACET-DATE-RANGE':
            let facetIndex5;
            state.resultFacets.some((item, index) => {
                if (item.title === action.facet.facetTitle) {
                    facetIndex5 = index;
                }
            });

            let newFacet5 = state.resultFacets[facetIndex5];
            newFacet5.values = action.facet.values;

            let newDocumentFacets5 = [...state.resultFacets.slice(0,facetIndex5), newFacet5, ...state.resultFacets.slice(facetIndex5+1)];

            let updatedArr2 = state.resultsData.filter((item) => {
                if (new Date(item.dateImported).getTime() >= new Date(newFacet5.values.selectedMin).getTime()
                    && new Date(item.dateImported).getTime() <= new Date(newFacet5.values.selectedMax).getTime()+86399999)
                    return item
            });
            // console.log('selectedMin: '+new Date(newFacet5.values.selectedMin));
            // console.log('selectedMax +86399999: '+new Date(Number(new Date(newFacet5.values.selectedMax).getTime()) +86399999));

            if (updatedArr2.length === state.resultsData.length)
                updatedArr2 = null;

            // console.log('update doc facet date range:');
            // console.log(newDocumentFacets5);

            return Object.assign({}, state, {
                resultFacets: newDocumentFacets5,
                resultsDataFiltered: updatedArr2
            });

        case 'UPDATE-DOCUMENT-PAGINATOR-PAGE':
            return Object.assign({}, state, {resultsListPage: action.page});
        
        case 'UPDATE-DOCUMENT-FULL-VIEW':
            // console.log('update-document-full-view');
            return Object.assign({}, state, {resultsFullView: action.data});

        case 'UPDATE-DOCUMENT-FULL-VIEW-FILE-TYPE':
            // console.log('update-document-full-view-file-type');
            return Object.assign({}, state, {documentFullViewFileType: action.fileType});

        case 'UPDATE-DOCUMENT-FULL-VIEW-ID':
            return Object.assign({}, state, {resultsFullViewID: action.ID});

        case 'OPEN-CLOSE-SECOND-SIDEBAR':
            return Object.assign({}, state, {showSecondSidebar: !state.showSecondSidebar});

        case 'SET-RESULT-LIST-TYPE':
            return Object.assign({}, state, {resultListType: action.value});
        case 'SET-DOCUMENT-IN-DOSSIER-TIMELINE':
            return Object.assign({}, state, {showDocumentInDossierTimeline: action.data});

        case 'SELECT-QUERY-BUILDER-PAGE':
            return Object.assign({}, state, {queryBuilderPage: action.pageNum});

        case 'QUERY-BUILDER-TOGGLE-COLLECTION':
            let newCollectionsObj = Object.assign({}, state.queryBuilderValues.collections, {[action.collectionName]: !state.queryBuilderValues.collections[action.collectionName]} );
            let newQueryBuilderValues = Object.assign({}, state.queryBuilderValues, {collections: newCollectionsObj} );
            return Object.assign({}, state, {queryBuilderValues: newQueryBuilderValues} );

        case 'QUERY-BUILDER-SHOW-DOCUMENT-TYPES':
            return Object.assign({}, state, {queryBuilderDocumentTypesVisible: !state.queryBuilderDocumentTypesVisible});

        case 'QUERY-BUILDER-TOGGLE-SOURCE':
            let newSourceObj = Object.assign({}, state.queryBuilderValues.source, {[action.sourceName]: !state.queryBuilderValues.source[action.sourceName]} );
            let newQueryBuilderValues2 = Object.assign({}, state.queryBuilderValues, {source: newSourceObj} );
            return Object.assign({}, state, {queryBuilderValues: newQueryBuilderValues2});

        case 'QUERY-BUILDER-SET-TYPES':
            // {documents: {
            //      international:
            //          values: [
                //          {
                //              label: 'Legislation',
                //              visible: true,
                //              checked: false
                //          }
            //          ],
            //          cutOff: false
            //      }
            // }
            let newTypes = action.types;
            for (let key1 in newTypes) {
                if (newTypes.hasOwnProperty(key1)) {
                    for (let key2 in newTypes[key1]) {

                        if (newTypes[key1].hasOwnProperty(key2)) {
                            //Add cutOff boolean
                            newTypes[key1][key2] = {
                                values: newTypes[key1][key2],
                                cutOff: false
                            };
                            //Presort the valuesArray alphabetically
                            newTypes[key1][key2].values = newTypes[key1][key2].values.sort((a,b) => {
                                if (a.toLowerCase() < b.toLowerCase()) return -1;
                                else if (a.toLowerCase() > b.toLowerCase()) return 1;
                                else return 0
                            });

                            //Convert value to obj with keys label, visible & checked
                            newTypes[key1][key2].values = newTypes[key1][key2].values.map((item, itemIndex) => {
                                return {
                                    label: item,
                                    visible: (itemIndex < 10),
                                    checked: false
                                }
                            });
                            //Set cutOff boolean
                            if (newTypes[key1][key2].values.length > 9)
                                newTypes[key1][key2].cutOff = true;
                        }
                    }
                }
            }


            let newQueryBuilderValues3 = Object.assign({}, state.queryBuilderValues, {types: newTypes} );
            return Object.assign({}, state, {queryBuilderValues: newQueryBuilderValues3});

        case 'QUERY-BUILDER-TOGGLE-CUT-OFF-SELECTOR-TYPES':
            let newQueryBuilderValues4 = state.queryBuilderValues;
            newQueryBuilderValues4.types[action.searchForType][action.level].cutOff = !newQueryBuilderValues4.types[action.searchForType][action.level].cutOff;
            if (!newQueryBuilderValues4.types[action.searchForType][action.level].cutOff)
                newQueryBuilderValues4.types[action.searchForType][action.level].values = newQueryBuilderValues4.types[action.searchForType][action.level].values.map((item) => {
                    item.visible = true;
                    return item
                });
            else
                newQueryBuilderValues4.types[action.searchForType][action.level].values = newQueryBuilderValues4.types[action.searchForType][action.level].values.map((item, itemIndex) => {
                    item.visible = itemIndex < 9;
                    return item
                });

            return Object.assign({}, state, {queryBuilderValues: newQueryBuilderValues4});
        
        case 'QUERY-BUILDER-TOGGLE-DOCUMENTS-TYPES-ITEM':
            let newQueryBuilderValues5 = state.queryBuilderValues;
            newQueryBuilderValues5.types[action.searchForType][action.level].values = newQueryBuilderValues5.types[action.searchForType][action.level].values.map((item, index) => {
                if (item.label === action.itemLabel) {
                    item.checked = !item.checked
                }
                return item
            });
            return Object.assign({}, state, {queryBuilderValues: newQueryBuilderValues5});

        case 'QUERY-BUILDER-TOGGLE-ADVANCED-SEARCH':
            return Object.assign({}, state, {queryBuilderAdvancedSearch: !state.queryBuilderAdvancedSearch});

        //start state.queryBuilderValues.text
        //All lets are being built up from deepest to highest
        case 'QUERY-BUILDER-SEARCH-FIELD-CHANGE':
            let newElementObj = Object.assign({}, state.queryBuilderValues.text[action.groupIndex].elements[action.elementIndex], {searchValue: action.value});
            let newElementArr = [...state.queryBuilderValues.text[action.groupIndex].elements.slice(0, action.elementIndex), newElementObj, ...state.queryBuilderValues.text[action.groupIndex].elements.slice(action.elementIndex+1)];
            let newSearchGroup2 = Object.assign({}, state.queryBuilderValues.text[action.groupIndex], {elements: newElementArr});
            let newQueryBuilderValuesText2 = [...state.queryBuilderValues.text.slice(0, action.groupIndex), newSearchGroup2, ...state.queryBuilderValues.text.slice(action.groupIndex+1)];
            let newQueryBuilderValues6 = Object.assign({}, state.queryBuilderValues, {text: newQueryBuilderValuesText2});
            return Object.assign({}, state, {queryBuilderValues: newQueryBuilderValues6});

        case 'QUERY-BUILDER-SEARCH-COLLECTION-CHANGE':
            let newSearchAttributes;
            let indexOfCollection = state.queryBuilderValues.text[action.groupIndex].searchAttributes.indexOf(action.value);
            if (indexOfCollection == -1) 
                newSearchAttributes = [...state.queryBuilderValues.text[action.groupIndex].searchAttributes, action.value];
            else 
                newSearchAttributes = [...state.queryBuilderValues.text[action.groupIndex].searchAttributes.slice(0, indexOfCollection), ...state.queryBuilderValues.text[action.groupIndex].searchAttributes.slice(indexOfCollection+1)];
            
            let newSearchGroup3 = Object.assign({}, state.queryBuilderValues.text[action.groupIndex], {searchAttributes: newSearchAttributes});
            let newQueryBuilderValuesText3 = [...state.queryBuilderValues.text.slice(0, action.groupIndex), newSearchGroup3, ...state.queryBuilderValues.text.slice(action.groupIndex+1)];
            
            let newQueryBuilderValues7 = Object.assign({}, state.queryBuilderValues, {text: newQueryBuilderValuesText3});
            return Object.assign({}, state, {queryBuilderValues: newQueryBuilderValues7});
        
        case 'QUERY-BUILDER-ADVANCED-SEARCH-ADD-GROUP':
            let newSearchGroupElements = [newSearchGroupElement];
            let newSearchGroup0 = Object.assign({}, searchGroup, {elements: newSearchGroupElements});
            let newQueryBuilderValuesText = [...state.queryBuilderValues.text, newSearchGroup0];
            let newQueryBuilderValues8 = Object.assign({}, state.queryBuilderValues, {text: newQueryBuilderValuesText});
            return Object.assign({}, state, {queryBuilderValues: newQueryBuilderValues8, relationship: (newQueryBuilderValuesText.length > 1 ? 'And':null)});

        case 'QUERY-BUILDER-ADVANCED-SEARCH-DELETE-GROUP':
            let newQueryBuilderValuesText6 = [...state.queryBuilderValues.text.slice(0, action.groupIndex), ...state.queryBuilderValues.text.slice(action.groupIndex+1)];
            let newQueryBuilderValues10 = Object.assign({}, state.queryBuilderValues, {text: newQueryBuilderValuesText6});
            return Object.assign({}, state, {queryBuilderValues: newQueryBuilderValues10});

        case 'QUERY-BUILDER-ADVANCED-SEARCH-ADD-ELEMENT':
            let newElementArray = [...state.queryBuilderValues.text[action.groupIndex].elements, newSearchGroupElement];
            if (newElementArray.length > 1) {
                newElementArray[newElementArray.length-1].relationship = 'And'
            }

            let newSearchGroup = Object.assign({}, state.queryBuilderValues.text[action.groupIndex], {elements: newElementArray}); 
            let newQueryBuilderValuesText4 = [...state.queryBuilderValues.text.slice(0, action.groupIndex), newSearchGroup, ...state.queryBuilderValues.text.slice(action.groupIndex+1)];
            let newQueryBuilderValues9 = Object.assign({}, state.queryBuilderValues, {text: newQueryBuilderValuesText4});
            return Object.assign({}, state, {queryBuilderValues: newQueryBuilderValues9});

        case 'QUERY-BUILDER-ADVANCED-SEARCH-ELEMENT-EXTRA-SELECT':
            let newExtrasArr = [...state.queryBuilderValues.text[action.groupIndex].elements[action.elementIndex].extras];
            let indexOfExtraInElement = newExtrasArr.indexOf(action.value);
            
            if (indexOfExtraInElement == -1)
                newExtrasArr = [...newExtrasArr, action.value];
            else 
                newExtrasArr = [...newExtrasArr.slice(0, indexOfExtraInElement), ...newExtrasArr.slice(indexOfExtraInElement+1)];
            
            let newElement2 = Object.assign({}, state.queryBuilderValues.text[action.groupIndex].elements[action.elementIndex], {extras: newExtrasArr});
            let newElementArr3 = [...state.queryBuilderValues.text[action.groupIndex].elements.slice(0, action.elementIndex), newElement2, ...state.queryBuilderValues.text[action.groupIndex].elements.slice(action.elementIndex+1)];
            let newSearchGroup5 = Object.assign({}, state.queryBuilderValues.text[action.groupIndex], {elements: newElementArr3});
            let newQueryBuilderValuesText7 = [...state.queryBuilderValues.text.slice(0, action.groupIndex), newSearchGroup5, ...state.queryBuilderValues.text.slice(action.groupIndex+1)];
            let newQueryBuilderValues12 = Object.assign({}, state.queryBuilderValues, {text: newQueryBuilderValuesText7});
            return Object.assign({}, state, {queryBuilderValues: newQueryBuilderValues12});

        case 'QUERY-BUILDER-ADVANCED-SEARCH-ELEMENT-CHOICE-SELECT':
            let newElement1 = Object.assign({}, state.queryBuilderValues.text[action.groupIndex].elements[action.elementIndex], {choice: action.value});
            
            let newElementArr4 = [...state.queryBuilderValues.text[action.groupIndex].elements.slice(0, action.elementIndex), newElement1, ...state.queryBuilderValues.text[action.groupIndex].elements.slice(action.elementIndex+1)];
            let newSearchGroup6 = Object.assign({}, state.queryBuilderValues.text[action.groupIndex], {elements: newElementArr4});
            let newQueryBuilderValuesText8 = [...state.queryBuilderValues.text.slice(0, action.groupIndex), newSearchGroup6, ...state.queryBuilderValues.text.slice(action.groupIndex+1)];
            let newQueryBuilderValues13 = Object.assign({}, state.queryBuilderValues, {text: newQueryBuilderValuesText8});
            return Object.assign({}, state, {queryBuilderValues: newQueryBuilderValues13});

        case 'QUERY-BUILDER-ADVANCED-SEARCH-DELETE-ELEMENT':
            let newElementArray2 = [...state.queryBuilderValues.text[action.groupIndex].elements.slice(0, action.elementIndex), ...state.queryBuilderValues.text[action.groupIndex].elements.slice(action.elementIndex+1)];
            let newSearchGroup4 = Object.assign({}, state.queryBuilderValues.text[action.groupIndex], {elements: newElementArray2});
            let newQueryBuilderValuesText5 = [...state.queryBuilderValues.text.slice(0, action.groupIndex), newSearchGroup4, ...state.queryBuilderValues.text.slice(action.groupIndex+1)];
            let newQueryBuilderValues11 = Object.assign({}, state.queryBuilderValues, {text: newQueryBuilderValuesText5});
            return Object.assign({}, state, {queryBuilderValues: newQueryBuilderValues11});

        case 'QUERY-BUILDER-ADVANCED-SEARCH-GROUP-RELATION-CHOICE-SELECT':
            let newSearchGroup7;
            if (action.elementIndex) {
                let newElement3 = Object.assign({}, state.queryBuilderValues.text[action.groupIndex].elements[action.elementIndex], {relationship: action.value});
                let newElementArr5 = [...state.queryBuilderValues.text[action.groupIndex].elements.slice(0, action.elementIndex), newElement3, ...state.queryBuilderValues.text[action.groupIndex].elements.slice(action.elementIndex+1)];
                newSearchGroup7 = Object.assign({}, state.queryBuilderValues.text[action.groupIndex], {elements: newElementArr5});
            }
            else if (!action.elementIndex)
                newSearchGroup7 = Object.assign({}, state.queryBuilderValues.text[action.groupIndex], {relationship: action.value});
            console.log('relation choice newSearchGroup:');
            console.log(newSearchGroup7);
            let newQueryBuilderValuesText9 = [...state.queryBuilderValues.text.slice(0, action.groupIndex), newSearchGroup7, ...state.queryBuilderValues.text.slice(action.groupIndex+1)];
            let newQueryBuilderValues14 = Object.assign({}, state.queryBuilderValues, {text: newQueryBuilderValuesText9});
            return Object.assign({}, state, {queryBuilderValues: newQueryBuilderValues14});
        //End of queryBuilderValues.text

        //Start queryBuilderValues.dates
        case 'QUERY-BUILDER-SELECT-DATE':
            let newDateItem = Object.assign({}, state.queryBuilderValues.dates[action.dateLabel], {
                selected: !state.queryBuilderValues.dates[action.dateLabel].selected,
                startDate: moment().subtract(7, "days"),
                endDate: moment()
            });
            console.log('newDateItem:');
            console.log(newDateItem);
            let newDates = Object.assign({}, state.queryBuilderValues.dates, {[action.dateLabel]: newDateItem});
            console.log('newDates:');
            console.log(newDates);
            let newQueryBuilderValues15 = Object.assign({}, state.queryBuilderValues, {dates: newDates});
            return Object.assign({}, state, {queryBuilderValues: newQueryBuilderValues15});

        case 'QUERY-BUILDER-SET-DATE':
            let newDateItem2 = Object.assign({}, state.queryBuilderValues.dates[action.dateLabel], {
                startDate: action.startEndStr == 'start' ? action.moment : state.queryBuilderValues.dates[action.dateLabel].startDate,
                endDate: action.startEndStr == 'end' ? action.moment : state.queryBuilderValues.dates[action.dateLabel].endDate
            });
            let newDates2 = Object.assign({}, state.queryBuilderValues.dates, {[action.dateLabel]: newDateItem2});
            let newQueryBuilderValues16 = Object.assign({}, state.queryBuilderValues, {dates: newDates2});
            return Object.assign({}, state, {queryBuilderValues: newQueryBuilderValues16});
        //End queryBuilderValues.dates

        //    Start queryBuilderValues.authors
        case 'QUERY-BUILDER-SET-AUTHORS':
            let newAuthorsAttr = Object.assign({}, state.queryBuilderValues.authors, {
                authorValues: action.authors,
                searchable: !state.queryBuilderValues.authors.searchable
            });
            let newQueryBuilderValues17 = Object.assign({}, state.queryBuilderValues, {authors: newAuthorsAttr});
            return Object.assign({}, state, {queryBuilderValues: newQueryBuilderValues17});

        case 'QUERY-BUILDER-SET-AUTHOR-INPUT':
            let newAuthorsAttr2 = Object.assign({}, state.queryBuilderValues.authors, {
                searchText: action.input == '' ? null : action.input
            });
            let newQueryBuilderValues18 = Object.assign({}, state.queryBuilderValues, {authors: newAuthorsAttr2});
            return Object.assign({}, state, {queryBuilderValues: newQueryBuilderValues18});

        case 'QUERY-BUILDER-SELECT-AUTHOR':
            let newSelected;
            let selectedIndex = -1;
            state.queryBuilderValues.authors.selected.map((selectItem, selectItemIndex) => {
                if (lodash.isEqual(selectItem, action.selectedItem))
                    selectedIndex = selectItemIndex
            });
            
            if (selectedIndex != -1)
                newSelected = [...state.queryBuilderValues.authors.selected.slice(0, selectedIndex), ...state.queryBuilderValues.authors.selected.slice(selectedIndex +1)];
            else 
                newSelected = [...state.queryBuilderValues.authors.selected, action.selectedItem];
            
            let newAuthorsAttr3 = Object.assign({}, state.queryBuilderValues.authors, {
                selected: newSelected
            });
            let newQueryBuilderValues19 = Object.assign({}, state.queryBuilderValues, {authors: newAuthorsAttr3});
            return Object.assign({}, state, {queryBuilderValues: newQueryBuilderValues19});
        //End queryBuilderValues.authors

        case 'RESET-ADVANCED-SEARCH-STATE':
            return Object.assign({}, state, {
                queryBuilderValues: initState.queryBuilderValues, 
                queryBuilderDocumentTypesVisible: false,
                queryBuilderAdvancedSearch: false,
                queryBuilderPage: null
            });
        
        case 'EXECUTE-QUERY':
            return Object.assign({}, state, {
                query: action.query,
                loading: true, 
                resultListType: 'search'
            });

        case 'RETRIEVING-DATA':
            return Object.assign({}, state, {
                loading: true
            });

        case 'RETRIEVING-DONE':
            return Object.assign({}, state, {
                loading: false
            });
        
        default:
            return state
    }
};

exports.appReducer = appReducer;

function getNewDocumentFacetsOnResultsReceived(resultFacets, action) {
    let newDocumentFacets0 = [];

    //Updating resultFacets
    //Select each facet
    resultFacets.map((facet, index) => {
        let newFacet = resultFacets.slice(index, index+1);
        // console.log('facet.internalLabel in results receieved: '+facet.internalLabel);

        if (facet.type === 'checkbox' && facet.usedIn.indexOf(action.dataCollection) > -1) {
            let facetValuesArr = [];
            let facetValuesCountArr = [];

            //Push unique values to facetValuesArr, increment facetCountArr if duplicate
            action.data.map((doc) => {
                let docValue = doc[facet.internalLabel];

                // docValue can be array, if so push all items to facetValuesArr
                if (Array.isArray(docValue)) {
                    docValue.map((value) => {
                        if (value != null) {
                            let indexOfValue = facetValuesArr.indexOf(value);
                            if (indexOfValue === -1) {
                                facetValuesArr.push(value);
                                facetValuesCountArr.push(1);
                            }
                            else {
                                facetValuesCountArr[indexOfValue] += 1;
                            }
                        }
                    })
                }
                else {
                    if (docValue != null) {
                        if (facetValuesArr.indexOf(docValue) === -1) {
                            facetValuesArr.push(docValue);
                            facetValuesCountArr.push(1)
                        }
                        else
                            facetValuesCountArr[facetValuesArr.indexOf(docValue)] += 1
                    }
                }
            });

            //    Now we have all possible values+counts for a facet, let's rebuild facet.values
            let newFacetValues = [];

            //TODO running into error with line 333.

            // console.log('facetValuesArr:');
            // console.log(facetValuesArr);
            facetValuesArr.map((valueLabel, i) => {

                //Retrieve correct index to display correct label and internal label for Source & Level facets
                let valueLabelIndex;
                if (newFacet[0].values != null) {
                    newFacet[0].values.map((item, valueLabelIndexFromFacet) => {
                        // console.log('item.internalLabel');
                        // console.log(item.internalLabel);
                        if (item.internalLabel == valueLabel ) {
                            valueLabelIndex = valueLabelIndexFromFacet;
                            console.log('valueLabelIndex in values.map');
                            console.log(valueLabelIndex);
                        }

                    });
                }

                // console.log('newFacet[0].values');
                // console.log(newFacet[0].values);
                // console.log('valueLabelIndex');
                // console.log(valueLabelIndex);

                newFacetValues.push({
                    label: newFacet[0].values == null ? valueLabel : newFacet[0].values[valueLabelIndex].label,
                    internalLabel: newFacet[0].values == null ? valueLabel : newFacet[0].values[valueLabelIndex].internalLabel,
                    count: facetValuesCountArr[i],
                    checked: true,
                    visible: true
                })
            });

            let numberOfVisibleChoicesReduced = false;
            //    Sort the values, highest to lowest
            newFacetValues = newFacetValues.sort((a,b) => b.count - a.count);
            //  Limit visible values to 10
            if (newFacetValues.length > 10) {
                newFacetValues = newFacetValues.map((item, index) => {
                    if (index > 10) item.visible = false;
                    return item;
                });
                numberOfVisibleChoicesReduced = true;
            }

            const newFacetWithNewFacetValues = Object.assign({}, newFacet[0], {values: newFacetValues, numberOfVisibleChoicesReduced});
            //    Push the facet to newDocumentFacets
            newDocumentFacets0.push(newFacetWithNewFacetValues);
        }
        else
            newDocumentFacets0.push(newFacet[0]);
    });
    
    return newDocumentFacets0;
}