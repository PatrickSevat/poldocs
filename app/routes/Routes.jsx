const React = require('react');
import { Router, Route, Redirect, IndexRoute, IndexRedirect, browserHistory } from 'react-router'

//import components
const Outer = require('../components/Outer.jsx');
const AdvancedSearch = require('../components/Search/AdvancedSearch.jsx');
const DocumentOuter = require('../components/Documents/DocumentsOuter.jsx');
const List = require('../components/AllResultPages/List.jsx');
const DossiersOuter = require('../components/Dossiers/DossiersOuter.jsx');
const Home = require('../components/Home/Home.jsx');
const SecondSidebar = require('../components/Sidebars/SecondSidebar.jsx');
const NotFound = require('../components/404.jsx');

//TODO figure out how to handle advanced search results and also how to 

const Routes = (
    <Router history={browserHistory}>
        <Redirect from="/gohome" to="/" />
        <Route path="/" component={Outer}>
            <IndexRoute component={Home} />
            
            <Route path="authenticate/*" component={Home}/>
            
            <Route path="documents" components={{main: DocumentOuter, secondarySidebar: SecondSidebar}} >
                <IndexRoute component={List} listType="documents" />
            </Route>
            <Redirect from="documents/:id" to="search/?collection=documents&attr=_key&q=:id" />
            
            <Route path="dossiers" components={{main: DossiersOuter, secondarySidebar: SecondSidebar}} >
                <IndexRoute component={List} listType="dossiers" />
            </Route>
            <Redirect from="dossiers/:id" to="search/?collection=dossiers&attr=_key&q=:id" />
            
            <Route path="search/:query" component={List} listType="search" query=":query" />
            <Route path="search" component={AdvancedSearch} />
            
            <Route path="*" component={NotFound} />
        </Route>

    </Router>
);

module.exports = Routes;
