//todo build a class with a cancel all request method
//TODO replace with superagent

function getData(pathWithQuery) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: pathWithQuery,
            type: 'GET',
            success: (data) => {
                // console.log('ajax success');
                resolve(data)
            },
            error: (err) => {
                reject(err)
            }
        })    
    })
}

function getSearchData(query) {
    return new Promise((resolve, reject) => {
        // console.log('query in getSearchData');
        // console.log(query);
        $.ajax({
            url: '/searchData',
            type: 'POST',
            data: {query: query},
            success: (data) => {
                // console.log('ajax success');
                resolve(data)
            },
            error: (err) => {
                reject(err)
            }
        })
    })
}

export {getData, getSearchData};