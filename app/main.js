/**
 * Created by Patrick on 06/04/2016.
 */
//polyfill required for stuff like Object.assign
require('babel-polyfill');

'use strict';
//require sass
require("./app.sass");


var Routes = require('./routes/Routes.jsx');
var Client = require('react-engine/lib/client');

// boot options
var options = {
    routes: Routes,

    // supply a function that can be called
    // to resolve the file that was rendered.
    viewResolver: function(viewName) {
        return require('./components/' + viewName);
    }
};

document.addEventListener('DOMContentLoaded', function onLoad() {
    Client.boot(options);
});