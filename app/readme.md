**Front-end code**

TODO:

+ Check if it's feasible to use Solr\ElasticSearch\Azure Search for searching
+ Refactor reducers and actions
+ Figure out if it's possible to inject reducer at React-Router level, if so, implement it
* Make more and better use of React Routers Link component
+ Build user dashboard
+ Make advanced search fully function and user friendly
+ Make a nice 404 page
+ Add params to URL so that users can share URLs
+ Occasionally save state to db so upon next log-in same settings are loaded into localStorage\state
+ Add explanatory (i)'s for front-end user instructions
+ Nice to have: build some graph functionality
+ Write tests!