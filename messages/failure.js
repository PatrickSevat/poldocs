const failMessage = {
    usernameTaken: {
        NL: 'Deze gebruikersnaam is al bezet. Probeer het opnieuw met een andere gebruikersnaam of registreer via LinkedIn.',
        EN: 'This username is already taken. Please try again with a different username or register using LinkedIn.'
    },
    unknownUsername: {
        EN: 'The credentials you have entered are incorrect. Please try again',
        NL: 'De inloggegevens zijn incorrect. Gelieve het opnieuw te proberen'
    },
    wrongPassword: {
        EN: 'The credentials you have entered are incorrect. Please try again',
        NL: 'De inloggegevens zijn incorrect. Gelieve het opnieuw te proberen'
    },
    lockedOut: {
        EN: 'You have made 5 consecutive wrong log in attempts. For security reasons you are prohibited from logging in for 15 minutes.',
        NL: 'U heeft 5 keer achter elkaar een foute inlogpoging gedaan. Uit veiligheidsoverwegingen kunt u 15 minuten niet inloggen.'
    },
    unknownError: {
        NL: 'Er is een onbekende fout opgetreden. Neem contact op met de beheerder indien dit probleem aanhoudt.',
        EN: 'An unknown error has occurred. Contact the administrator if the problem persists.'
    }
};

function getErrorMessage(errorStr, lang = 'EN') {
    return typeof failMessage[errorStr] == 'undefined' ? failMessage.unknownError[lang] : failMessage[errorStr][lang]
}

export {getErrorMessage}
