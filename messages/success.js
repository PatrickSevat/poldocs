const successMessage = {
    accountCreated: {
        NL: 'Account aangemaakt!',
        EN: 'Account created!'
    },
    loggedIn: {
        NL: 'Succesvol aangemeld!',
        EN: 'Logged in!'
    },
    unknownError: {
        NL: 'Er is een onbekende fout opgetreden. Neem contact op met de beheerder indien dit probleem aanhoudt.',
        EN: 'An unknown error has occurred. Contact the administrator if the problem persists.'
    }
};

function getSuccessMessage(successStr, lang = 'EN') {
    return typeof successMessage[successStr] == 'undefined' ? successMessage.unknownError[lang] : successMessage[successStr][lang]
}

export {getSuccessMessage}
