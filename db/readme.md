TODO

* Reimport MEPs into people with data from ParlTrack.eu
* Build a single script which activates officieleBekendmakingen, Eurlex Docs, Eurlex Dossiers and save success\error to db
* Optimise queries, loading 250 dossiers = 10mb NOT ACCEPTABLE
* reimport all documents using a standardized scheme, which also takes OpenRaadsInformatie into account
* on reimport: only allow String values for identifiers, all others should be placed in arrays, current combination of strings and arrays is a mess
* Expand the relation detector
* Refactor the Eurlex import scripts, functions way to big now, make more managable like OB
* WRITE TESTS!
* Nice to have import all legislation
* Nice to have far far in the future, also import jurisprudence
