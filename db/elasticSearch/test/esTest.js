import addOBDocumentES from '../addDocumentElastic';

//TODO remove this file

const doc = {
    "_key": "stb-2016-158",
    "KIOtype": "",
    "KIWI": "",
    "altTitle": [
        "Amendement van het lid Bashir dat regelt dat het onmogelijk is om de inkomensafhankelijke component buiten de huursom te laten vallen"
    ],
    "askedBy": "F. Bashir",
    "askedTo": "",
    "audience": "",
    "authors": [
        "Tweede Kamer der Staten-Generaal",
        "F. Bashir"
    ],
    "availableDate": "2016-02-08",
    "categories": [
        {
            "top": "Huisvesting",
            "sub": "Huren en verhuren"
        }
    ],
    "conformsTo": "",
    "coverage": "",
    "dateCreated": "",
    "dateMeeting": "",
    "dateModified": "2016-02-08",
    "dateImported": "2016-02-08",
    "description": "",
    "documentDate": "2016-02-02",
    "documentLinks": "https://zoek.officielebekendmakingen.nl/kst-34373-15",
    "documentRelationships": {
        "attachment": "",
        "attachmentOf": "",
        "reprints": "",
        "reprintedBy": "",
        "corrects": "",
        "correctedBy": "",
        "citedWork": "",
        "relatedTo": "",
        "adviesRvS": "",
        "behandeldDossier": "",
        "implements": "",
        "legalBasis": ""
    },
    "documentStatus": "Opgemaakt na onopgemaakt",
    "dossier": "34373",
    "dossierTitle": "Wijziging van Boek 7 van het Burgerlijk Wetboek en enkele andere wetten in verband met het stellen van nadere huurmaatregelen tot verdere bevordering van de doorstroming op de huurmarkt (Wet doorstroming huurmarkt 2015)",
    "endOfValidityDate": "",
    "entryIntoForceDate": "",
    "fullText": "﻿\r\n\r\n  \r\n    \r\n  \r\n  \r\n    \r\n      Vergaderjaar 2015-2016\r\n      Tweede Kamer der Staten-Generaal\r\n      2\r\n      Kamerstukken\r\n    \r\n    \r\n      \r\n        34 373\r\n      \r\n      Wijziging van Boek 7 van het Burgerlijk Wetboek en enkele andere wetten in verband met het stellen van nadere huurmaatregelen tot verdere bevordering van de doorstroming op de huurmarkt (Wet doorstroming huurmarkt 2015)\r\n    \r\n    \r\n      Nr. 15\r\n      AMENDEMENT VAN HET LID BASHIR\r\n      Ontvangen 2 februari 2016\r\n      \r\n        De ondergetekende stelt het volgende amendement voor:\r\n        \r\n          In artikel VI, onderdeel D, vervalt onderdeel d in artikel 54, tweede lid.\r\n        \r\n        \r\n          \r\n            Toelichting\r\n          \r\n          \r\n            Het wetsvoorstel maakt het mogelijk om, wanneer daarover op lokaal niveau prestatieafspraken zijn gemaakt tussen huurders, woningcorporatie(s) en gemeente, buiten de huursombenadering om extra huurverhogingen mogelijk te maken op basis van inkomen.\r\n            De huren zijn de laatste jaren al erg omhoog gegaan en een verdere verhoging is ongewenst. Met dit amendement wordt onmogelijk gemaakt om de inkomensafhankelijke component buiten de huursom te laten vallen. De inkomensafhankelijke component zelf blijft mogelijk, zolang deze binnen de huursombenadering valt.\r\n            Wanneer deze inkomensafhankelijke huurverhogingen binnen de huursombenadering vallen, dan is de inkomstenderving van woningcorporaties gering. Namelijk 75 miljoen euro per jaar (zie de nota naar aanleiding van het verslag: Kamerstukken II 2015/16, 34 373, nr. 6, blz. 3).\r\n          \r\n        \r\n        \r\n          \r\n            \r\n              Bashir\r\n            \r\n          \r\n        \r\n      \r\n    \r\n  \r\n",
    "jurisdiction": "",
    "key": "kst-34373-15",
    "language": "nl",
    "level": "national",
    "manifestations": "",
    "organisationType": "",
    "parliamentaryYear": "2015-2016",
    "path": "\\2016\\02\\08\\kst\\kst-34373-15\\metadata_owms.xml",
    "questionID": "",
    "spatial": "",
    "subDossier": "15",
    "subject": "",
    "subTitle": "",
    "summary": "",
    "temporal": "",
    "title": "Wijziging van Boek 7 van het Burgerlijk Wetboek en enkele andere wetten in verband met het stellen van nadere huurmaatregelen tot verdere bevordering van de doorstroming op de huurmarkt (Wet doorstroming huurmarkt 2015); Amendement; Amendement van het lid Bashir dat regelt dat het onmogelijk is om de inkomensafhankelijke component buiten de huursom te laten vallen",
    "type": [
        "Amendement",
        "Kamerstuk"
    ],
    "version": ""
};

console.log('addOBDocumentES');

addOBDocumentES(doc).then((res) => {
    console.log('ES save success');
    console.log('res:\n', res);
}, (err) => {
    console.log('err\n', err);
})