import es from 'elasticsearch';

let sharedClient;

function getClient() {
    if (sharedClient != null)
        return sharedClient;
    else {
        sharedClient = new es.Client({
            host: 'localhost:9200',
            log: 'trace',
            apiVersion: '5.0'
        });
        return sharedClient;
    }
}

function closeClient() {
    if (sharedClient != null)
        sharedClient.close();
}

export {getClient, closeClient}