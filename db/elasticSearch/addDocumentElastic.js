import { getClient }from './elasticSearch';

let client = getClient();

export default async function addOBDocumentES(document) {
    try {
        console.log('addOBDocumentES called');
        let ESDocOB = {};
        OBfieldsForES.forEach((field) => {
            ESDocOB[field] = document[field];
        });

        const exists = await client.exists({
            index: 'documents',
            type: 'officielebekendmakingen',
            id: ESDocOB._key
        });

        if (exists) {
            return updateOBDocumentES(client, ESDocOB);
        }

        else {
            return createOBDocumentES(client, ESDocOB);
        }
    }
    catch (err) {
        console.log('error adding OB document to ES: \n'+err);
        return new Promise((resolve, reject) => reject(err));
    }
}

const OBfieldsForES = ['_key', 'documentDate', 'dateImported', 'dateModified', 'documentStatus', 'dossier',
    'dossierTitle', 'fullText', 'language', 'level', 'parliamentaryYear', 'subDossier', 'subTitle',
    'title', 'type'
];

function createOBDocumentES(client, doc) {
    return client.create({
        index: 'documents',
        type: 'officielebekendmakingen',
        id: doc._key,
        body: doc
    })
}

function updateOBDocumentES(client, doc) {
    return client.update({
        index: 'documents',
        type: 'officielebekendmakingen',
        id: doc._key,
        body: {doc}
    })
}