/**
 * Created by Patrick on 05/08/2016.
 */
const Database = require('arangojs').Database;
import { arangoUsername, arangoPassword } from '../../secret/arangoCredentials';

const db = new Database(`http://${arangoUsername}:${arangoPassword}@127.0.0.1:8529`);

db.useDatabase('poldocsTEST');

var aqlQuery = require('arangojs').aqlQuery;

exports.testDb = db;
exports.testAqlQuery = aqlQuery;