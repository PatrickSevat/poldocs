import { db, aqlQuery } from '../../arangodb';
import {parseString, Parser} from 'xml2js';
const parser = new Parser({explicitArray: false, ignoreAttrs: true});
import fs from 'fs';
import {typeofUndefined } from '../../utils/typeofUndefined';
import { checkExistsAndCreateNodeByKey } from '../../utils/checkExistsAndCreate';

const eurovocColl = db.collection('eurovoc');

/**
 * 
 * TODO: fix the async order in importAllEurovoc!
 * Run these parse functions manually one-by-one!!! or fix the order.
 * Uncommenting them all will lead to corrupted data items
 * 
 * */

async function importAllEurovoc() {
//    import thes-en
//     await parse('../sources/eurovocxml/thes_en.xml', 'thes', 'en');
// //    import desc-en
//     await parse('../sources/eurovocxml/desc_en.xml', 'desc', 'en');
// //    import dom-en
//     await parse('../sources/eurovocxml/dom_en.xml', 'dom', 'en');
// //    import sn-en
//     await parse('../sources/eurovocxml/sn_en.xml', 'sn', 'en');
// //    import uf-en
//     await parse('../sources/eurovocxml/uf_en.xml', 'uf', 'en');
//
//     //    import thes-nl
//     await parse('../sources/eurovocxml/thes_nl.xml', 'thes', 'nl');
// //    import desc-nl
//     await parse('../sources/eurovocxml/desc_nl.xml', 'desc', 'nl');
// //    import dom-nl
//     await parse('../sources/eurovocxml/dom_nl.xml', 'dom', 'nl');
// //    import sn-nl
//     await parse('../sources/eurovocxml/sn_nl.xml', 'sn', 'nl');
// //    import uf-nl
//     await parse('../sources/eurovocxml/uf_nl.xml', 'uf', 'nl');

    console.log('\nAll done importing Eurovoc NL and ENG');
}

const eurovocTypes = {
    desc: {
        topKey: 'DESCRIPTEUR',
        id: 'DESCRIPTEUR_ID',
        prefix: 'desc-'
    },
    thes: {
        topKey: 'THESAURUS',
        id: 'THESAURUS_ID',
        prefix: 'thes-'
    },  
    dom: {
        topKey: 'DOMAINES',
        id: 'DOMAINE_ID',
        prefix: 'dom-'
    },
    sn: {
        topKey: 'SCOPE_NOTE',
        id: 'DESCRIPTEUR_ID',
        keys: ['SN', 'HN'], // sometimes SN, other times HN, can also be multiple
        prefix: 'desc-'
    },
    uf: {
        topKey: 'USED_FOR',
        id: 'DESCRIPTEUR_ID',
        keys: [{'UF': 'UF_EL'}],
        prefix: 'desc-'
    }
};

let records;
let count = 0;

async function asyncLoop(iteration, type, eurovocType, lang) {
    if (iteration < records.length) {
        try {
            let record = records[iteration];
            console.log('');
            console.dir(record);
            await saveRecord(record, eurovocType, type, lang);   
            console.log('saved record');
            count++;
            asyncLoop(count, type, eurovocType, lang);
        }
        catch (err){
            throw new Error(err);
        }
    }
    else {
        return new Promise((resolve, reject) => {
            count = 0;
            console.log('Finished asyncLoop');
            resolve();
        })
    }
}

async function xml2json(xml) {
    return new Promise((resolve, reject) => {
        parser.parseString(xml, function (err, json) {
            if (err)
                reject(err);
            else
                resolve(json);
        });
    });
}

async function parse(path, type, lang) {
    try {
        let xml = await getXMLData(path);
        let eurovocType;
        let parsed = await xml2json(xml);
        eurovocType = eurovocTypes[type];
        records = parsed[eurovocType.topKey].RECORD;

        // console.log(records[0]);

        await asyncLoop(count, type, eurovocType, lang);
        return new Promise((resolve, reject) => {
            console.log(`Done processing ${type} in ${lang}`);
            resolve();
        })
    }
    catch (err) {
        throw new Error(err);
    }
}

function getXMLData(path) {
    return new Promise((resolve, reject) => {
        fs.readFile(path, function (err, data) {
            if (err) reject(err);
            console.log('data retrieved');
            // console.log(data);
            resolve(data);
        })
    })
}

async function saveRecord(subRecord, eurovocType, type, lang) {
    try {
        let saveObj = {};
        saveObj._key = (eurovocType.prefix+subRecord[eurovocType.id]);
        saveObj['label-'+lang] = typeofUndefined(subRecord, 'LIBELLE');
        saveObj['def-'+lang] = typeofUndefined(subRecord, 'DEF');
        saveObj['scopeNote-'+lang] = typeofUndefined(subRecord, 'SN');
        saveObj['historyNote-'+lang] = typeofUndefined(subRecord, 'HN');
        saveObj['altLabels-'+lang] = typeofUndefined(subRecord, 'UF', 'UF_EL');
        await checkExistsAndCreateNodeByKey(saveObj._key, eurovocColl, db, aqlQuery, saveObj);
        return new Promise((resolve, reject) => {
            console.log('saved:');
            console.dir(saveObj);
            resolve();
        })
    }
    catch (err) {
        throw new Error('err in saveRecord:\n'+err);
    }
}

importAllEurovoc();


