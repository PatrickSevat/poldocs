import { db, aqlQuery } from '../../arangodb';
import { decode } from '../../utils/decodeWindows1252';
import { getKey } from '../../utils/getKeyFromWikiDataURL';
import { compareObj } from '../../utils/compareObj';

import fs from 'fs';
import {parseString, Parser} from 'xml2js';
import superagent from 'superagent';
import { checkCreateNodeAndEdges, checkCreateNodeAndEdgesByKey, checkExistsEdgeOrCreateOrUpdate, checkExistsAndCreateEdgesTwoWay,
    checkCreateNodeAndEdgesMultipleColl, checkCreateNodeAndEdgesFromArrayByKey, checkExistsAndCreateEdgesTwoWayFromArray } from '../../utils/createNodeAndEdges';
import { checkExistsByName, checkEdgeExists, checkExistsByKey, checkExistsMultipleColl, checkExistsByAttr } from '../../utils/checkExists';
import { checkExistsAndCreateNodeByKey, checkExistsAndCreateNodeByName} from '../../utils/checkExistsAndCreate';
import  wdk  from 'wikidata-sdk';

//TODO use underscore string cleanDiacritics

const parser = new Parser;

const peopleColl = db.collection('people');
const politicalParties = db.collection('politicalParties');
const countriesColl = db.collection('countries');
const legislativeBodiesColl = db.collection('legislativeBodies');

const EPID = 'legislativeBodies/Q8889';

const partOf = db.edgeCollection('partOf');
const hasPart = db.edgeCollection('hasPart');

const personsWithOutWikiKeys = [];
const nationalPartiesWithOutWikiKeys = [];

const getData = () => {
    return new Promise((resolve, reject) => {
        fs.readFile('../sources/meps.xml', function (err, data) {
            if(err) reject(err);
            else {
                parser.parseString(data, function (err, result) {
                    resolve(result);
                    console.log('Done');
                });
            }
        });
    })
};

async function saveMEPs() {
    try {
        //Get array of MEPs from local XML file
        const data = await getData();
        let MEParray = data.meps.mep;

        // Iterate over MEPs
        for (let mep of MEParray) {
            const person = await getPersonData(mep);
            console.log('\nperson from getPersonData: \n'+JSON.stringify(person));

            let personMeta;
            let personID;
            let personKey;
            let nationalPartyKey;
            let nationalPartyMeta;
            let nationalPartyID;
            let skip = false;

            let countryObj = await checkExistsByName(person.country, countriesColl, db, aqlQuery);
            let languageCodeArr = countryObj[0].languageCodes;
            let countryKey = countryObj[0]._key;
            if (countryKey == 'Q55') {
                countryKey = 'Q29999';
                languageCodeArr = ['nl'];
            }
            console.log('\nLanguageCodeArr: '+languageCodeArr);

            let EPPartyID = await getEPPartyKey(person.EPPoliticalParty);

            //First check locally if MEP already exists in db
            personMeta = await checkExistsByName(person.name, peopleColl, db, aqlQuery);

            //Person exists, update it with additional data
            if (personMeta) {
                console.log('personMeta exists');
                personKey = personMeta[0]._key;
                personID = personMeta[0]._id;
                console.log('personID: '+personID);
                let personUpdated = compareObj(person, personMeta[0]);
                console.log('updated person: ');
                console.dir(personUpdated);
                peopleColl.replace(personID, personUpdated);
                // create edges connecting person to EP Party
                checkExistsAndCreateEdgesTwoWay(personID, EPPartyID, [partOf, {name: 'part of', type: 'MEP of', level: 'international'}],
                    [hasPart, {name: 'has part', type: 'has MEP', level: 'international'}], db, aqlQuery);
                console.log('person and edges updated');
            }
            // Person does not exist, retrieve wikidata key
            else {
                let personWikiObj = await getWikiPersonKey(person.name, person.country, countryKey);

                if (typeof personWikiObj.personKey === 'undefined') {
                    skip = true;
                    personsWithOutWikiKeys.push(person.name);
                }
                else personKey = personWikiObj.personKey;
                console.log('\nWiki key for person retrieved: '+personKey);

                if (!skip) {
                    //Key retrieved, now create person and create edges connecting person to EP Party
                    personMeta = await checkCreateNodeAndEdgesByKey(personKey, person.name, peopleColl, [partOf, {name: 'part of', level: 'international', type: 'MEP of'}],
                        [hasPart, {name: 'has part', level: 'international', type: 'has MEP'}], EPPartyID, db, aqlQuery, person );
                    personID = personMeta._id;
                    console.log('person and edges created');

                    //Sometimes partyKey is included, if so save it
                    if (typeof  personWikiObj.partyKey !== 'undefined') nationalPartyKey = personWikiObj.partyKey;
                }

            }

            // Now turn to national party
            if (skip) {}
            // If MEP is one the independents we can skip key retrieval & national party creation
            else if (person.nationalPoliticalParty === 'Sans étiquette' || person.nationalPoliticalParty === "Bezpartyjny"  || person.nationalPoliticalParty === "Sõltumatu"
                || person.nationalPoliticalParty === "Independent" || person.nationalPoliticalParty ===  "Bezpartyjna" || person.nationalPoliticalParty ===  'Sans étiquette'){
                console.log('independent');
                nationalPartyID = "politicalParties/independents";
            }
            // Not independent
            else {
                // First check if national party exists in db
                nationalPartyMeta = await checkExistsByName(person.nationalPoliticalParty, politicalParties, db, aqlQuery);
                if (nationalPartyMeta) {
                    console.log('\nParty already exists');
                    nationalPartyID = nationalPartyMeta[0]._id;
                }
                // Party does not yet exists in db
                else {
                    //check for key, else not needed because party key has already been retrieved during person key retrieval
                    if (!nationalPartyKey) {
                        nationalPartyKey = await getWikiNationalPartyKey(person.nationalPoliticalParty, languageCodeArr, person.country);
                        console.log('\nperson national party key '+nationalPartyKey);
                        if (!nationalPartyKey) {
                            skip = true;
                            nationalPartiesWithOutWikiKeys.push(person.nationalPoliticalParty);
                        }
                    }

                    //Now that we have a key, time to create national party
                    //Because the fragmention we'll create edges later
                    if (!skip) nationalPartyMeta = await checkExistsAndCreateNodeByKey(nationalPartyKey, politicalParties, db, aqlQuery,
                        {level: 'national', type: 'national political party', country: person.country, _key: nationalPartyKey, name: person.nationalPoliticalParty});
                }
                //Retrieving ID from meta
                if (!skip) nationalPartyID = nationalPartyMeta._id;
            }

            if (!skip && nationalPartyID) {
                //Create edge between national party and EP Party
                await checkExistsAndCreateEdgesTwoWay(nationalPartyID, EPPartyID, [partOf, {name: 'part of', level: 'international', type: 'member party of'}],
                    [hasPart, {name: 'has part', level: 'international', type:'has member party'}], db, aqlQuery);
                //Create edge between national party and MEP
                await checkExistsAndCreateEdgesTwoWay(nationalPartyID, personID, [hasPart, {name: 'has part', level: 'international', type:'has MEP'}],
                    [partOf, {name: 'part of', level: 'international', type: 'MEP of'}], db, aqlQuery);
            }
        }

        console.log('All done MEPs');
        console.log('Persons without keys: '+personsWithOutWikiKeys);
        console.log('National parties without keys: '+nationalPartiesWithOutWikiKeys);
    }
    catch (err) {
        throw new Error('Error in saveMEPs: \n'+err);
    }
}

const getEPPartyKey = (name) => {
    switch (name) {
        case 'Group of the European People\'s Party (Christian Democrats)':
            return 'politicalParties/Q635616';
        case 'Group of the Progressive Alliance of Socialists and Democrats in the European Parliament':
            return 'politicalParties/Q507343';
        case 'Confederal Group of the European United Left - Nordic Green Left':
            return 'politicalParties/Q753711';
        case 'Group of the Greens/European Free Alliance':
            return 'politicalParties/Q751935';
        case 'Group of the Alliance of Liberals and Democrats for Europe':
            return 'politicalParties/Q839097';
        case 'Europe of Nations and Freedom Group':
            return 'politicalParties/Q20113710';
        case 'European Conservatives and Reformists Group':
            return 'politicalParties/Q1345559';
        case 'Europe of Freedom and Direct Democracy Group':
            return 'politicalParties/Q1165759';
        case 'Non-attached Members':
            return 'politicalParties/Q1440626';
    }
};

async function getPersonData (person) {
    try {
        let decodedName = (person.fullName[0]);
        console.log('\n'+decodedName);
        let decodedNameArr = decodedName.split(' ');
        let firstNames = '';
        for (let i = 0; i<decodedNameArr.length-1; i++) {
            if (i < decodedNameArr.length-2) firstNames += (decodedNameArr[i]+ ' ');
            else if (i <decodedNameArr.length-1) firstNames += decodedNameArr[i];
        }
        console.log('firstNames: '+firstNames);
        let lastName = decodedNameArr[decodedNameArr.length-1].toLowerCase();
        lastName = lastName.substr(0,1).toUpperCase() + lastName.substr(1);
        console.log('lastName: '+lastName);
        let personObj = {
            firstNames,
            lastName,
            name: firstNames+' '+lastName,
            country: person.country[0],
            EPPoliticalParty: person.politicalGroup[0],
            nationalPoliticalParty: person.nationalPoliticalGroup[0],
            type: 'politician',
            subType: 'MEP'
        };
        return new Promise((resolve, reject) => {
            resolve(personObj);
        })
    }
    catch (err) {
        throw new Error('CAUGHT ERR in getPersonData \n'+err);
    }
}

async function getWikiPersonKey(personName, countryName, countryKey) {
    let result;
    let potentialKeyArr = [];
    let partyKeyArr = [];
    try {
        let query = wikiPersonUrl(personName, countryKey);
        console.log(query);
        result = await makeWikiDataRequest(query);
        console.log('person request made');
        let resultRefined = result.results.bindings;

        for (let person of resultRefined) {
            console.dir(person);
            let country = typeof person.personCountryLabel !== 'undefined' ? person.personCountryLabel.value : '';

            if ((countryName == country || countryName.indexOf(country) > -1 || country.indexOf(countryName) > -1) &&
                potentialKeyArr.indexOf(getKey(person.person.value)) === -1) {
                potentialKeyArr.push(getKey(person.person.value));
                if (typeof person.personParty !== 'undefined') partyKeyArr.push(getKey(person.personParty.value))
            }
        }

        if (potentialKeyArr.length > 0 && partyKeyArr.length > 0 ) return {personKey: potentialKeyArr[0], partyKey: partyKeyArr[0]};
        else if (potentialKeyArr.length > 0 && partyKeyArr.length === 0 ) return {personKey: potentialKeyArr[0]};
        else if (potentialKeyArr.length === 0 && partyKeyArr.length > 0 ) return {partyKey: partyKeyArr[0]};
        else {
            console.log('\nCANNOT FIND WIKI KEY FOR PERSON: '+personName);
            console.log('countryName: '+countryName);
            return {};
        }
    }
    catch (err) {
        throw new Error('CAUGHT ERR in getWikiDataPersonKey \n'+err);
    }
}

async function getWikiNationalPartyKey(partyName, languageCodeArr, countryName ) {
    let result;
    let potentialKeyArr = [];
    let pName = partyName.replace(/"/g, '');
    pName = pName.replace(/'/g, '');
    try {
        let query = wikiPartyUrl(pName, languageCodeArr);
        // console.log(query);
        result = await makeWikiDataRequest(query);
        console.log('party request made');
        let resultRefined = result.results.bindings;

        for (let party of resultRefined) {
            // console.dir(party);
            let country = typeof party.partyCountryLabel !== 'undefined' ? party.partyCountryLabel.value : '';
            let description = typeof party.partyDescription !== 'undefined' ? party.partyDescription.value : '';
            // console.log('country from wikiData: '+country);
            // console.log('description from wikiData: '+description);

            if (country !== ''
                && (country == countryName || country.indexOf(countryName) > -1 || countryName.indexOf(country) > -1)
                && potentialKeyArr.indexOf(getKey(party.party.value)) === -1) {
                console.log('hit in party country and countryName');
                potentialKeyArr.push(getKey(party.party.value));
                }
                
            else if (description !== '' && description.indexOf(countryName) > -1 && potentialKeyArr.indexOf(getKey(party.party.value)) === -1) {
                console.log('hit in party description and countryName');
                potentialKeyArr.push(getKey(party.party.value));
            }
        }

        if (potentialKeyArr.length > 0) return potentialKeyArr[0];
        else {
            console.log('\nCANNOT FIND WIKI KEY FOR PARTY: '+partyName);
            console.log('countryName: '+countryName);
            return null;  
        }
    }
    catch (err) {
        throw new Error('CAUGHT ERR in getWikiNationalPartyKey \n'+err);
    }
}

function makeWikiDataRequest(query) {
    return new Promise((resolve, reject) => {
        superagent
            .get(query).then(
            res => {
                resolve(JSON.parse(res.text));
            }, err => {
                throw new Error('ERROR in getWikiDataPerson \n'+err);
            }
        );
    })
}

function wikiPartyUrl(partyName, languageCodeArr) {
    let filter = ' filter( lang($partyLabel) = "en" || lang($partyLabel) = "de" || lang($partyLabel) = "fr" || ';
    for (let i=0; i<languageCodeArr.length; i++) {
        if (languageCodeArr.length > 1 && i < languageCodeArr.length-1) filter += ` lang($partyLabel) = "${languageCodeArr[i]}" ||`;
        else filter += ` lang($partyLabel) = "${languageCodeArr[i]}" )`
    }
    let query = `
        SELECT $party $partyLabel $partyAltLabel $partyDescription $partyCountryLabel WHERE {
            $party wdt:P31/wdt:P279* wd:Q7278 .
            $party rdfs:label $partyLabel ${filter} .
            OPTIONAL { $party schema:description $partyDescription filter (lang($partyDescription) = "en") . }
            OPTIONAL { $party skos:altLabel $partyAltLabel } 
            OPTIONAL { $party wdt:P17 $partyCountry .
                        $partyCountry rdfs:label $partyCountryLabel filter (lang($partyCountryLabel) = "en" ) . }
            FILTER (CONTAINS(LCASE($partyLabel), "${partyName.toLowerCase()}") || CONTAINS(LCASE($partyAltLabel), "${partyName.toLowerCase()}"))
        }`;
    console.log(query);
    let url = wdk.sparqlQuery(query);
    return url;
}

function wikiPersonUrl(personName, countryKey) {
    console.log('personNAme: '+personName+ ', countryKey: '+countryKey);
    let query = wdk.sparqlQuery(`
    SELECT DISTINCT $person $personLabel $personAltLabel $personCountryLabel $personParty WHERE {
  	$person wdt:P27 wd:${countryKey} . 
  	{ $person wdt:P106 wd:Q82955 } UNION{ $person wdt:P39 wd:Q27169 } .
  	$person wdt:P31 wd:Q5 .
	$person rdfs:label $personLabel filter (lang($personLabel) = "en" ).
		
	OPTIONAL { $person wdt:P102 $personParty . }
  	OPTIONAL { $person skos:altLabel $personAltLabel }  
  	OPTIONAL { $person wdt:P27 $personCountry .
             	$personCountry rdfs:label $personCountryLabel filter (lang($personCountryLabel) = "en" ) .
             }
	filter (CONTAINS(LCASE($personLabel), "${personName.toLowerCase()}") || CONTAINS(LCASE($personAltLabel), "${personName.toLowerCase()}"))
}
`);
    return query;
}

saveMEPs();
// getWikiNationalPartyKey('Parti chrétien social luxembourgeois', ['en', 'fr'], 'Luxembourg').then(res => console.dir(res));
// getWikiPersonKey('Johannes Cornelis van Baalen', 'Netherlands', 'Q29999').then(res => console.dir(res));
// console.log(JSON.stringify(getWikiPersonKey('Mireille D\'ornano', 'France', 'Q142')));

// console.log(getWikiPartyData());

// const test = {"fullName":["Lars ADAKTUSSON"],"country":["Sweden"],"politicalGroup":["Group of the European People's Party (Christian Democrats)"],"id":["124990"],"nationalPoliticalGroup":["Kristdemokraterna"]};
//
// getPersonData(test);
// console.log('test: \n'+JSON.stringify(test));
