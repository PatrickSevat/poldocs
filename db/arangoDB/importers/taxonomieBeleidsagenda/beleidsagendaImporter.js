import { db, aqlQuery } from '../../arangodb';
import {parseString, Parser} from 'xml2js';
const parser = new Parser({explicitArray: false, ignoreAttrs: false});
import fs from 'fs';
import {typeofUndefined } from '../../utils/typeofUndefined';
import { checkExistsAndCreateNodeByKey } from '../../utils/checkExistsAndCreate';
import { checkCreateNodeAndEdgesByKey } from '../../utils/createNodeAndEdges'
import underscoreStr from 'underscore.string'

const beleidsAgendaColl = db.collection('taxonomieBeleidsagenda');
const hasPartEdgeColl = db.edgeCollection('hasPart');
const partOfEdgeColl = db.edgeCollection('partOf');

function getXMLData(path) {
    return new Promise((resolve, reject) => {
        fs.readFile(path, 'utf8', function (err, data) {
            if (err) reject(err);
            console.log('data retrieved');
            // console.log(data);
            resolve(data);
        })
    })
}

async function xml2json(xml) {
    return new Promise((resolve, reject) => {
        parser.parseString(xml, function (err, json) {
            if (err)
                reject(err);
            else
                resolve(json);
        });
    });
}

async function parse(path) {
    try {
        let xml = await getXMLData(path);
        let parsed = await xml2json(xml);
        let values = parsed.cv.value;
        // console.dir(values);

        asyncLoop(values);
    }
    catch (err) {
        console.log('caught err in parse');
        console.log(err);
    }
}

function replaceSpaces(str) {
    let replaced = str.replace(/\s/g, '-');
    let cleaned = underscoreStr.cleanDiacritics(replaced);
    return cleaned;
}

async function asyncLoop(data) {
    try {
        for (let topLevel of data) {
            let topLevelName = replaceSpaces(topLevel['$'].prefLabel);
            let topLevelMeta = await checkExistsAndCreateNodeByKey(topLevelName, beleidsAgendaColl, db, aqlQuery, {
                type: 'top category',
                resourceIdentifier: topLevel['$'].resourceIdentifier,
                name: topLevelName,
                _key: topLevelName
            });
            let topLevelID = topLevelMeta._id;
            
            for (let subLevel of topLevel.value) {
                let subLevelName = replaceSpaces(subLevel['$'].prefLabel);
                let subLevelMeta = await checkCreateNodeAndEdgesByKey(topLevelName+'-'+subLevelName, 
                    subLevelName, beleidsAgendaColl, [partOfEdgeColl, {type: 'subcategory of'}], [hasPartEdgeColl, {type: 'has subcategory'}],
                    topLevelID, db, aqlQuery, {type: 'sub category', resourceIdentifier: subLevel['$'].resourceIdentifier, name: subLevelName})
            }
        }
        console.log('importing done');
    }
    catch (err) {
        console.log('err in asyncloop');
        console.error(err);
    }
}

parse('../sources/TaxonomieBeleidsagenda.xml');