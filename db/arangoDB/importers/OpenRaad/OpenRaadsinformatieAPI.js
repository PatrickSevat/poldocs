/**
 * Created by Patrick on 15/04/2016.
 */

/*
* endpoint:  http://api.openraadsinformatie.nl/v0
* GET example: http://api.openraadsinformatie.nl/v0/search?q=vergadering
* POST data example:
* {
 "query": "vergadering",
 "facets": {
 "collection": {},
 "date": {"interval": "day"}
 },
 "size": 1
 }'
*
*
* POST!!! getting all raadsleden: http://api.openraadsinformatie.nl/v0/combined_index/persons/search
*  {
 "query": "",
 "size": 100
 }
* Repeat until total is met
*
*
*
* */

var request = require('request');

var query = 'vergadering*+utrecht|amstelveen';

var bodyRequest = {
    "query": query, //operators: +,|,-,",*,()
    "facets": { //determines which facets should be returned
        
    },
    "size": 10, //number of docs to return
    "filters": {
        "classification": {
            "terms": ["Agenda"]
        },
        "start_date": {
            "from": "2016-01-01",
            "to": "2016-12-31"
        }
    }
        
    /* Other options:
    filter
    sort
    from //offset, #result to start displaying
    * */
};

var options =
    {
        uri: 'http://api.openraadsinformatie.nl/v0/search/events',
        method: 'POST',
        json: true,
        headers: {
            "content-type": "application/json"
        },
        body: bodyRequest
    };

request(options, function (err, obj, json) {
    if (err) throw err;
    
    console.log(obj.body);
});

/**/