'use strict';
require('babel-polyfill');
require('babel-register')({
    presets: ['es2015', 'react'],
    plugins: ["syntax-async-functions", "transform-regenerator"]
});

//TODO run 07 okt
require('./OB/officieleBekendmakingen');

/**
 * To improve upon:
 *
 * ALL
 * TODO Replace all asyncLoops with for loops
 * TODO Build use typeofUndefined whereever possible to combat corrupted data in Eurlex
 * TODO remove all edges which are derived from documents and redo them once data & process is improved
 * 
 * 
 * Countries
 * Add member states as part of \has part EU:
 * Add NL names
 *
 * Dutch Provinces:
 * Add executive body and legislative bodies to wikidata
 * Add dutch names
 *
 * Dutch ministries:
 * Add DG's
 * Add NL names
 * Done: diensten en instellingen
 * Add edges to diensten en instellingen
 *
 * COM:
 * DONE: Double check DGs & Dutch names
 * DONE: Kind of COM DG => executive body, comes closest to 'ministry'
 * Add dutch names of commissioners public offices
 * Add dutch names of DGs
 *
 * Council:
 * Add DGs
 * DONE: Determine what kind of body a DG of Council is => Supporting body? Administrative Body?
 * DONE: Added configs to wikidata
 * 
 * EP
 * Add DGs
 * National parties are added via MEPs
 * TODO Add committees
 * TODO enrich with Parltrack MEPs
 * 
 * National Parliaments
 * NL: Add committees
 *
 * DutchMinisters: public offices are sometimes part of NL \ sometimes not
 * Examples: secretary general, MEP
 * Add dutch names
 * 
 * Dutch municipality:
 * TODO Refactor
 *
 * Documents
 * TODO return null instead of '' for undefined or make sure the attr is not added at all, last preferable, will save data
 * TODO run AQL query on authors once all authors have been nicely imported
 * 
 * legislativeBodies
 * Add original name & dutch name
 *
 * executiveBodies
 * add original name & dutch name
 * add COM DG Legal Service
 *
 * otherBodies
 * add subbodies of CoR
* */