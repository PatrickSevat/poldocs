import { db, aqlQuery } from '../../arangodb';
import  wdk  from 'wikidata-sdk';
import superagent from 'superagent';
import { getKey } from '../../utils/getKeyFromWikiDataURL';
import { compareObj } from '../../utils/compareObj';
import { decode } from '../../utils/decodeWindows1252';
import { checkCreateNodeAndEdges, checkCreateNodeAndEdgesByKey, checkExistsEdgeOrCreateOrUpdate, checkExistsAndCreateEdgesTwoWay,
    checkCreateNodeAndEdgesMultipleColl, checkCreateNodeAndEdgesFromArrayByKey } from '../../utils/createNodeAndEdges';
import { checkExistsByName, checkEdgeExists, checkExistsByKey, checkExistsMultipleColl } from '../../utils/checkExists';
import { checkExistsAndCreateNodeByKey, checkExistsAndCreateNodeByName} from '../../utils/checkExistsAndCreate';

const partOf = db.edgeCollection('partOf');
const hasPart = db.edgeCollection('hasPart');

//Executive body governs over country and other way around
//Additional edges: partOf hasPart country
//Additional data: jurisdiction, type
const executiveBodiesColl = db.collection('executiveBodies');
const governsOverEdgeColl = db.edgeCollection('governsOver');
const governedByEdgeColl = db.edgeCollection('governedBy');

//Executive body controlled by legislative body and other way around
//Additional edges: partOf hasPart country
//Additional data: jurisdiction, type
const legislativeBodiesColl = db.collection('legislativeBodies');
const controlsEdgeColl = db.edgeCollection('controls');
const controlledByEdgeColl = db.edgeCollection('controlledBy');

//Judiciary checks both legislative and executive bodies
//Additional data: jurisdiction, type
const judiciariesColl = db.collection('judiciaries');
const judgesOver = db.edgeCollection('judgesOver');
const judgedOverBy = db.edgeCollection('judgedOverBy');

const EUID = 'IGO/Q458';
const EUcountryID = 'countries/Q458';
const EUCoJID = 'judiciaries/Q4951';

const url = wdk.sparqlQuery(`
SELECT DISTINCT ?eu ?euLabel ?executiveBody ?executiveBodyLabel ?DGCOMs ?DGCOMKeys ?DGLegs ?DGLegKeys WHERE {
  ?eu wdt:P209 wd:Q4951 .
  ?eu wdt:P208 ?executiveBody .
  
  OPTIONAL { ?eu wdt:P208 ?executiveBody }
  OPTIONAL  {
    SELECT ?executiveBody (GROUP_CONCAT(?DGCOMLabel; SEPARATOR=";") As ?DGCOMs) (GROUP_CONCAT(?DGCOM; SEPARATOR=";") As ?DGCOMKeys) WHERE {
      ?eu wdt:P208 ?executiveBody .
      ?DGCOM wdt:P31 wd:Q1485366 .
      ?DGCOM wdt:P361 ?executiveBody .
      SERVICE wikibase:label { bd:serviceParam wikibase:language "en" .
                             ?DGCOM rdfs:label ?DGCOMLabel}
    }
    GROUP BY ?executiveBody
  }
  
  SERVICE wikibase:label { bd:serviceParam wikibase:language "en" . }
} ORDER BY ?euLabel`);

let count = 0;
let data = [];

const getDGArray = (nameArr, urlArr) => {
    let returnArr = [];
    for (let i = 0; i < nameArr.length; i++) {
        returnArr.push({
            name: nameArr[i],
            _key: getKey(urlArr[i]),
            type: 'directorate-general',
            level: 'international',
            jurisdiction: 'European Union'
        })
    }
    return returnArr;
};

const getEUCOMData = (body) => {
    return {
        _key: getKey(body.executiveBody.value),
        name: body.executiveBodyLabel.value,
        wikiData: body.executiveBody.value,
        type: 'supranational organisation',
        level: 'international',
        jurisdiction: 'European Union'
    };
};

async function asyncLoop(iteration) {
    console.log('Iteration: '+iteration);
    if (iteration < data.length) {
        const EUCOMRaw = data[iteration];
        const EUCOMRefined = getEUCOMData(EUCOMRaw);
        console.log('Processing: '+EUCOMRefined.name);
        let EUCOMMeta;
        let EUCOMID;
        let DGMetaArr;

        let DGArray = getDGArray(EUCOMRaw.DGCOMs.value.split(';'), EUCOMRaw.DGCOMKeys.value.split(';'));

        try {
            // console.log('trying');
            //Create or update EUCOM
            EUCOMMeta = await checkExistsAndCreateNodeByKey(EUCOMRefined._key, executiveBodiesColl, db, aqlQuery, EUCOMRefined);

            EUCOMID = EUCOMMeta._id;

            //Create edges for EUCOM: part of\has part EU
            await checkExistsAndCreateEdgesTwoWay(EUCOMID, EUID, [partOf, {name: 'part of', type: 'executive body of'}], 
                [hasPart, {name: 'has part', type: 'has executive body'}], db, aqlQuery);

            //More edges for EUCOM, governs over EU countries
            await checkExistsAndCreateEdgesTwoWay(EUCOMID, EUcountryID, [governsOverEdgeColl, {name: 'governs over', level:'international'}],
                [governedByEdgeColl, {name: 'governed by', level:'international'}], db, aqlQuery);

            //Edge controlled by will be added from EP

            //More edges for EUCOM: judged over by \ judgesOver
            await checkExistsAndCreateEdgesTwoWay(EUCOMID, EUCoJID, [judgedOverBy, {name: 'judged over by', jurisdiction: 'European Union'}],
                [judgesOver, {name: 'judges over', jurisdiction: 'European Union'}], db, aqlQuery);

            console.log('edges created');

            await checkCreateNodeAndEdgesFromArrayByKey(DGArray, executiveBodiesColl, [partOf, {name: 'part of', type: 'directorate-general of'}],
                [hasPart, {name: 'has part', type: 'has directorate-general'}], EUCOMID, db, aqlQuery);

            console.log('All done, iteration: '+count);
            count++;
            asyncLoop(count)

        }
        catch (err) {
            console.log('CAUGHT ERR: \n'+err);
        }
    }
    else console.log('All done processing EU COM');
}

superagent.get(url)
    .end((err, res) => {
        if (err) console.error(err);

        else {
            console.log('retrieval succeeded in EUCom');
            const rawData = JSON.parse(res.text);
            data = rawData.results.bindings;
            console.log('data length: '+data.length );
            //iterate over results in async style
            asyncLoop(count)
        }
    });

// console.log(url);
