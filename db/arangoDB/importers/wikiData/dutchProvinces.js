import { db, aqlQuery } from '../../arangodb';
import  wdk  from 'wikidata-sdk';
import superagent from 'superagent';
import { getKey } from '../../utils/getKeyFromWikiDataURL';
import { compareObj } from '../../utils/compareObj';
import { decode } from '../../utils/decodeWindows1252';
import { checkCreateNodeAndEdges, checkCreateNodeAndEdgesByKey, checkExistsEdgeOrCreateOrUpdate, checkExistsAndCreateEdgesTwoWay,
    checkCreateNodeAndEdgesMultipleColl, checkCreateNodeAndEdgesFromArrayByKey } from '../../utils/createNodeAndEdges';
import { checkExistsByName, checkEdgeExists, checkExistsByKey, checkExistsMultipleColl } from '../../utils/checkExists';
import { checkExistsAndCreateNodeByKey, checkExistsAndCreateNodeByName} from '../../utils/checkExistsAndCreate';

const provincesColl = db.collection('provinces');
const partOf = db.edgeCollection('partOf');
const hasPart = db.edgeCollection('hasPart');

const municipalityColl = db.collection('municipalities');

const netherlandsID = "countries/Q55";

const url = wdk.sparqlQuery(`
SELECT DISTINCT ?item ?itemLabel ?province ?provinceLabel ?gemeenteKeys ?gemeenteLabels ?populationLabel WHERE {
  SERVICE wikibase:label { bd:serviceParam wikibase:language "en" .}
  ?item wdt:P36 wd:Q727 .
  ?item wdt:P131 wd:Q29999 .
  ?item wdt:P150 ?province .
  OPTIONAL { ?province wdt:P1082 ?population  }.
  {
    SELECT ?province (GROUP_CONCAT(?gemeenteLabel; SEPARATOR=";") As ?gemeenteLabels) (GROUP_CONCAT(?gemeente; SEPARATOR=";") As ?gemeenteKeys) 
    WHERE {
      ?province wdt:P31 wd:Q134390 .
      ?province wdt:P150 ?gemeente .
      SERVICE wikibase:label { bd:serviceParam wikibase:language "en" .
                             ?gemeente rdfs:label ?gemeenteLabel}
    }
    GROUP BY ?province
  }
} ORDER BY ?provinceLabel`);

const getProvinceData = (provinceRaw) => {
    let decodedName = decode(provinceRaw.provinceLabel.value);
    return {
        _key: getKey(provinceRaw.province.value),
        name: decodedName,
        population: typeof provinceRaw.populationLabel !== "undefined" ? provinceRaw.populationLabel.value : '',
        level: 'regional'
    }
};

let count = 0;
let data = [];

const getMunicipalityArray = (nameArr, urlArr, provinceName) => {
    let returnArr = [];
    for (let i = 0; i < nameArr.length; i++) {
        let decodedName = decode(nameArr[i]);
        returnArr.push({
            name: decodedName,
            _key: getKey(urlArr[i]),
            type: 'municipality',
            level: 'local',
            country: 'Netherlands',
            province: provinceName
        });
    }
    return returnArr;
};

async function asyncLoop(iteration) {
    console.log('Iteration: '+iteration);
    const provinceRaw = data[iteration];
    const provinceRefined = getProvinceData(provinceRaw);
    console.log('Province refined: '+JSON.stringify(provinceRefined));
    const municipalityLabelsArr = provinceRaw.gemeenteLabels.value.split(';');
    // console.log('municipalityLabelsArr.length '+municipalityLabelsArr.length);
    const municipalityKeysArr = provinceRaw.gemeenteKeys.value.split(';');
    // console.log('municipalityKeysArr.length '+ municipalityKeysArr.length);
    let municipalityArray = getMunicipalityArray(municipalityLabelsArr, municipalityKeysArr , provinceRefined.name);
    console.log('municipalityArray:');
    for (let mun of municipalityArray) {
        console.log('mun name: '+mun.name)
    }
    if (iteration < data.length) {
        try {
            let provinceID;
            let provinceMeta;
            console.log('Processing province '+provinceRefined.name);
            provinceMeta = await checkCreateNodeAndEdgesByKey(provinceRefined._key, provinceRefined.name, provincesColl,
                [partOf, {name: 'part of', type: 'province of', level: 'regional'}], [hasPart, {name: 'has part', type: 'has province', level: 'national'}],
                netherlandsID, db, aqlQuery, provinceRefined);
            
            provinceID = provinceMeta._id;
            console.log('Province ID: '+provinceID);

            await checkCreateNodeAndEdgesFromArrayByKey(municipalityArray, municipalityColl, [partOf, {name: 'part of', type: 'municipality of', level: 'local'}],
                [hasPart, {name: 'part of', type: 'has municipality', level: 'regional'}], provinceID, db, aqlQuery);

            console.log('Municipalities created');
            count++;
            asyncLoop(count);
        }
        catch (err) {
            console.error('CAUGHT ERROR \n'+err);
        }
    }
    else {
        console.log(`All done, completed ${iteration} iterations`);
    }
}

superagent.get(url)
    .end((err, res) => {
        if (err) console.error(err);

        else {
            console.log('retrieval succeeded in provinces');
            const rawData = JSON.parse(res.text);
            data = rawData.results.bindings;
            console.log('data length: '+data.length );
            //iterate over results in async style
            asyncLoop(count)
        }
    });

// console.log(url);

// console.log(decode('DÃ¡il Ã‰ireann'));
// console.log(decode('SkarsterlÃ¢n'));
// Dáil Éireann
// Skarsterlân

// let dail = decode('DÃ¡il Ã‰ireann');
// let skaster = decode('SkarsterlÃ¢n');
// console.log('dail: '+dail);
// console.log('skaster: '+skaster);


asyncLoop(count);

