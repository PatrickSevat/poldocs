/**
 * Created by Patrick on 04/08/2016.
 */
import { db, aqlQuery } from '../../arangodb';
import wdk from 'wikidata-sdk';
import superagent from 'superagent';
import Promise from 'promise';
import { compareObj } from '../../utils/compareObj';
import { getKey } from '../../utils/getKeyFromWikiDataURL';
import { checkExists, checkEdgeExists, checkExistsByKey, checkExistsMultipleColl } from '../../utils/checkExists';
import { checkCreateNodeAndEdges, checkCreateNodeAndEdgesByKey, checkCreateNodeAndEdgesMultipleColl} from '../../utils/createNodeAndEdges';

const municipalities = db.collection('municipalities');
const partOf = db.edgeCollection('partOf');
const hasPart= db.edgeCollection('hasPart');

const executiveBodies = db.collection('executiveBodies');

const peopleColl = db.collection('people');

const url = wdk.sparqlQuery(`SELECT DISTINCT ?item ?itemLabel ?mayorLabel ?mayor ?startTimeMayor ?posHeldLabel ?endTimeMayor ?populationLabel 
?startDate ?endDate ?replacedBy ?replacedByLabel ?replaces ?replacesLabel ?replacedByMayor ?replacedByMayorLabel ?replacesMayor ?replacesMayorLabel ?websiteLabel ?coords ?genderLabel WHERE {

  #Select all municipalities in the netherlands
  ?item wdt:P31 wd:Q2039348 .
  #Add some additional metadata about the municipality
  OPTIONAL {   ?item p:P31 ?municipalityStatement . 
  				OPTIONAL { ?municipalityStatement pq:P580 ?startDate. }
                OPTIONAL { ?municipalityStatement pq:P582 ?endDate. }
                OPTIONAL { ?municipalityStatement pq:P1366 ?replacedBy. }
                OPTIONAL { ?municipalityStatement pq:P1365 ?replaces. }         
           }
  OPTIONAL { ?item wdt:P856 ?website. }
  OPTIONAL { ?item wdt:P625 ?coords. }
  OPTIONAL { ?item wdt:P242 ?map. }
  OPTIONAL { ?item wdt:P1082 ?population . }
  
  #Retrieve the mayor if possible
  OPTIONAL { 
    ?item p:P6 ?mayorStatement . 		
    ?mayorStatement ps:P6 ?mayor .
    ?mayor wdt:P21 ?gender .
    #Get start and end time of the mayor as defined on the municipality page
    OPTIONAL { ?mayorStatement pq:P580 ?startTimeMayor }
    OPTIONAL { ?mayorStatement pq:P582 ?endTimeMayor }
    
    #Get start and end time of the mayor as defined on the mayor page
    OPTIONAL {?mayor p:P39 ?mayorPositionHeldStatement .
              ?mayorPositionHeldStatement ps:P39 ?posHeld .
              {?posHeld wdt:P279* wd:Q30185} UNION {?posHeld wdt:P31 wd:Q30185} .
              ?posHeld p:P31 ?posHeldStatement .
              OPTIONAL { ?mayorPositionHeldStatement pq:P580 ?startTimeMayor }
              OPTIONAL { ?mayorPositionHeldStatement pq:P582 ?endTimeMayor }
              OPTIONAL { ?mayorPositionHeldStatement pq:P1366 ?replacedByMayor. }
              OPTIONAL { ?mayorPositionHeldStatement pq:P1365 ?replacesMayor. }    
             }
           }
  
#Give labels in English with fallback for Dutch
  SERVICE wikibase:label { bd:serviceParam wikibase:language "en,nl,de,fr" . }
} ORDER BY DESC (?posHeldLabel)
`);

// Municipality saved or updated to collection municipalities, country: NL
// Edges: part of \country\ , \country\ has municipality

// Add legislative body, name: municipality name+ city council, nameNL gemeenteraad municipality name, type: local parliament, subtype: municipal council
// Edges: \municipality name city council
// & executive body, name municipality government, nameNL: college burgemeester en wethouders municipality name type: local government, subtype: 'college van burgemeester en wethouders', municipal government
// Add mayors + previous
// Mayor edges: \mayor\ member of \name municipality government\, \mayor\ headOfBody \name municipality government\ + dates
// Mayor edges: \name municipality government\ has member \mayor\, \name municipality government\ has head of body \mayor\ + dates
//

//Mayor replaces, AKA mayor that has been replaced: additional data: current: false

let count = 0;
let data;

const getMunicipalityData = (municipality) => {
    var obj = {
        _key: getKey(municipality.item.value),
        startDate: typeof municipality.startDate !== "undefined" ? municipality.startDate.value : '',
        name: municipality.itemLabel.value,
        wikiData: municipality.item.value,
        endDate: typeof municipality.endDate !== "undefined" ? municipality.endDate.value : '',
        population: typeof municipality.populationLabel !== "undefined" ? municipality.populationLabel.value : '',
        website: typeof municipality.websiteLabel !== "undefined" ? municipality.websiteLabel.value : '',
        coords: typeof municipality.coords !== "undefined" ? municipality.coords.value : '',
        type: 'municipality',
        country: ['Kingdom of the Netherlands', 'Netherlands']
    };
    return obj;
};

const asyncLoop = (iteration) => {
    console.log('1 iteration: '+iteration);
    setTimeout(function () {
        if (iteration < data.length) {
            const municipalityRaw = data[iteration];
            console.log('2 processing: '+municipality.itemLabel.value);
            let municipalityID;
            let mayorName = typeof municipalityRaw.mayorLabel !== "undefined" ? municipalityRaw.mayorLabel.value : '';
            let mayorGender = typeof municipalityRaw.genderLabel !== "undefined" ? municipalityRaw.genderLabel.value : '';
            let mayorKey = typeof municipalityRaw.mayor !== "undefined" ? getKey(municipalityRaw.mayor.value) : '';

            let replacesName = typeof municipalityRaw.replacesLabel !== "undefined" ? municipalityRaw.replacesLabel.value : '';
            let replacesKey = typeof municipalityRaw.replaces !== "undefined" ? getKey(municipalityRaw.replaces.value ): '';

            let replacedByName = typeof municipalityRaw.replacedByLabel !== "undefined" ? municipalityRaw.replacedByLabel.value : '';
            let replacedByKey = typeof municipalityRaw.replacedBy !== "undefined" ? getKey(municipalityRaw.replacedBy.value ): '';

            let replacedByMayorName = typeof municipalityRaw.replacedByMayorLabel !== "undefined" ? municipalityRaw.replacedByMayorLabel.value : '';
            let replacedByMayorKey = typeof municipalityRaw.replacedByMayor !== "undefined" ? getKey(municipalityRaw.replacedByMayor.value) : '';

            let replacesMayor = typeof municipalityRaw.replacesMayorLabel !== "undefined" ? municipalityRaw.replacesMayorLabel.value : '';
            let replacesMayorKey = typeof municipalityRaw.replacesMayor !== "undefined" ? getKey(municipalityRaw.replacesMayor.value) : '';

            let municipalityRefined = getMunicipalityData(municipalityRaw);

            checkExistsByKey(municipalityRefined._key, municipalities, db, aqlQuery).then(municipalityExists => {
                if (municipalityExists) {
                    let municipalityOld = municipalityExists[0];
                    municipalityID = municipalityOld._id;

                    //update municipality
                    let updateMunicipality = compareObj(municipalityOld, municipalityRefined);
                    municipalities.replace(municipalityID, updateMunicipality).then(updatedMunicipality => {
                        checkCreateNodeAndEdgesByKey(mayorKey, mayorName, peopleColl, [headOfExecutiveBodyEdge], [], municipalityID, db, aqlQuery, {
                            _id: mayorKey,
                            type: 'politician',
                            subType: 'mayor',
                            gender: mayorGender
                        }).then()
                    })

                }
                else {
                    municipalities.save(municipalityRefined).then(createdMunicipality => {
                        municipalityID = createdMunicipality._id;

                    })
                }
            }, err => console.log('ERROR in checkExists municipality: \n'+err));

            // checkExists(municipality.itemLabel.value, municipalities, db, aqlQuery)
            //
            // //based on muncipality exists, branch to create or update
            //     .then(arr => {
            //         //!arr country does not exist => create it
            //         if (!arr) {
            //             console.log('5 municipality does not exist');
            //             let data = getMunicipalityData(municipality);
            //             municipalities.save(data)
            //                 //    save municipalityID & check \ create replaces, mayor and replacedBy + edges
            //                 .then(municipality => {
            //                     console.log('6 municipality saved');
            //                     municipalityID = municipality._id;
            //                     mayor(mayorName, mayorWikiDataUrl, municipalityID)
            //                     .then(replaces(replacesName, municipalityID)
            //                     .then(replacedBy(replacedByName, municipalityID)
            //                     .then(
            //                         () => {
            //                             console.log('saved records & edges');
            //                             count ++;
            //                             setTimeout(asyncLoop(count), 500)
            //                         }, err => {
            //                             console.log('ERROR in final promise:');
            //                             console.log(err);
            //                         }
            //                     )))
            //                 });
            //         }
            //         //municipality exists => update it
            //         else {
            //             let municipalityCurrent = arr[0];
            //             municipalityID = municipalityCurrent._id;
            //             let municipalityNormalized = getMunicipalityData(municipality);
            //             //compareObject updates the old item with the new item returning updated attributes
            //             let updateObj = compareObj(municipalityNormalized, municipalityCurrent);
            //
            //             municipalities.replace(municipalityID, updateObj).then(updatedMunicipality => {
            //                 console.log('5 municipality already existed and updated');
            //                 mayor(mayorName, mayorWikiDataUrl, municipalityID)
            //                 .then(replaces(replacesName, municipalityID)
            //                 .then(replacedBy(replacedByName, municipalityID)
            //                 .then(
            //                     () => {
            //                         console.log('saved records & edges');
            //                         count ++;
            //                         setTimeout(asyncLoop(count), 500)
            //                     }, err => {
            //                         console.log('ERROR in final promise:');
            //                         console.log(err);
            //                     }
            //                 )))
            //             })
            //         }
            //     }, err => console.log('err in resolving checkExists promise: ' +err));
        }
        else console.log('all done'); 
    }, 500)
    
};

// make call to wikidata api
superagent.get(url)
    .end((err, res) => {
        if (err) console.error(err);
        else {
            console.log('retrieval succeeded in municipality');
            const rawData = JSON.parse(res.text);
            data = rawData.results.bindings;
            console.log('data length: '+data.length );
            //iterate over results in async style
            asyncLoop(count)
        }
    });



//
// const replaces = (replaceeName, replacerID) => {
//     return new Promise((resolve, reject) => {
//         //Check if replaceeMunicipality already exists in municipality coll
//         let replaceeID;
//         checkExists(replaceeName, municipalities, db, aqlQuery).then(arr => {
//             //if not create replacee municipality & edge
//             if (!arr && replaceeName !== '') {
//                 municipalities.save({
//                         name: replaceeName
//                     })
//                     .then(replacee => {
//                         //saved ege from replacer to replacee
//                         replaceeID = replacee._id;
//                         replacesEdge.save({}, replacerID, replaceeID)
//                             .then(replacedByEdge.save({}, replaceeID, replacerID)).then(edge => {
//                                 console.log('replacee Municipality+ edges created');
//                                 resolve(true)
//                             });
//                     })
//             }
//             //else if replaceeMunicipality name is incorrect\undefined resolve so we can move on
//             else if (replaceeName === '') {
//                 console.log('empty field in replaceeMunicipality');
//                 resolve(true);
//             }
//             //if replaceeMunicipality exists check for existing edge, if not create edge
//             else if (arr && replaceeName !== '') {
//                 replaceeID = arr[0]._id;
//                 checkEdgeExists(replacerID, replaceeID, replacesEdge, db, aqlQuery).then(edge => {
//                     if (edge) resolve();
//                     else {
//                         console.log(`9 replaceeMunicipality ${replaceeName} already existed, adding edge` );
//                         replacesEdge.save({}, replacerID, replaceeID)
//                             .then(checkEdgeExists(replaceeID, replacerID, replacedByEdge, db, aqlQuery).then(replacedByEdge => {
//                                 if (replacedByEdge) resolve();
//                                 else {
//                                     replacedByEdge.save({}, arr[0]._id, replacerID).then(edge => {
//                                         console.log('replacee Municipality+ edges created');
//                                         resolve();
//                                     })
//                                 }
//                             }))
//                     }
//                 })
//             }
//         }, err => {
//             console.log('ERR: '+err);
//             reject(err)
//         })
//     })
// };
//
// const replacedBy = (replacerName, replaceeID) => {
//     return new Promise((resolve, reject) => {
//         //Check if replacerMunicipality already exists in municipality coll
//         let replacerID;
//         checkExists(replacerName, municipalities, db, aqlQuery).then(arr => {
//             //if not create replacer municipality & edge
//             if (!arr && replacerName !== '') {
//                 municipalities.save({
//                         name: replacerName
//                     })
//                     .then(replacer => {
//                         //saved ege from replacer to replacee
//                         replacerID = replacer._id;
//                         replacedByEdge.save({}, replaceeID, replacerID)
//                             .then(replacesEdge.save({}, replacerID, replaceeID).then(edge => {
//                                         console.log('replacee Municipality+ edges created');
//                                         resolve();
//                                     })
//                             )
//                     })
//             }
//             //else if replacerMunicipality name is incorrect\undefined resolve so we can move on
//             else if (replacerName === '') {
//                 console.log('empty field in replacerMunicipality');
//                 resolve(true);
//             }
//             //if replacerMunicipality exists check for existing edge, if not create edge
//             else if (arr && replacerName !== '') {
//                 console.log('replacerMun exists, checking edges')
//                 replacerID = arr[0]._id;
//                 //check if replacedByEdge exists
//                 checkEdgeExists(replaceeID, replacerID, replacedByEdge, db, aqlQuery).then(edge => {
//                     console.log('edge length:'+edge.length);
//                     if (edge) resolve();
//                     else {
//                         console.log(`9 replacerMunicipality ${replacerName} already existed, adding edge` );
//                         replacedByEdge.save({}, replaceeID, replacerID)
//                             .then(checkEdgeExists(replacerID,replaceeID, replacesEdge, db, aqlQuery).then(replacesEdgeItem => {
//                                 if (replacesEdgeItem) resolve();
//                                 else {
//                                     replacesEdge.save({}, replacerID, replaceeID).then(edge => {
//                                         console.log('replacee Municipality+ edges created');
//                                         resolve();
//                                     })
//                                 }
//                             }, err => {
//                                 console.log('ERR: '+err);
//                                 reject(err)
//                             }))
//                     }
//                 }, err => {
//                     console.log('ERR: '+err);
//                     reject(err)
//                 })
//             }
//         }, err => {
//             console.log('ERR: '+err);
//             reject(err)
//         })
//     })
// };
//
//
// const mayor = (name, wikidataURL, municipalityID) => {
//     return new Promise((resolve, reject) => {
//         //check if mayor exists in people collection
//         checkExists(name, peopleColl, db, aqlQuery).then(arr => {
//             if (!arr && name !== '') {
//                 peopleColl.save({name, type: ['politician'], subTypes: ['mayor'], wikiData: wikidataURL }).then(mayor => {
//                     headEdge.save({}, mayor._id, municipalityID)
//                         .then(edge => {
//                             console.log('head + edge created');
//                             resolve()
//                         })
//                 })
//             }
//             else if (!arr && name === '') {
//                 console.log('empty field in head');
//                 resolve();
//             }
//             else {
//                 //update mayor
//                 let mayor = arr[0];
//                 if (mayor.type.indexOf('politician') === -1) mayor.type.push('politician');
//                 if (mayor.subtype.indexOf('mayor') === -1) mayor.subtype.push('mayor');
//                 mayor.wikiData = wikidataURL;
//                 peopleColl.replace(mayor._id, mayor).then(updatedMayor => {
//                 //    check if edge already exists
//                     checkEdgeExists(updatedMayor._id, municipalityID, headEdge, db, aqlQuery).then(edgeCheck => {
//                         if (edgeCheck) resolve();
//                         else {
//                             headEdge.save({}, arr[0]._id, municipalityID).then(
//                                 edge => {
//                                     console.log('updated mayor & created edge');
//                                     resolve()
//                                 }
//                             )
//                         }
//                     })
//                 });
//             }
//         }, err => reject(err))
//     })
// };

/*****************************************************************************
 * 
 *          TEST DATA & VARIABLES
 * 
 * 
 *****************************************************************************/