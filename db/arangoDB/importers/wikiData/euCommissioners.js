import { db, aqlQuery } from '../../arangodb';
import { decode } from '../../utils/decodeWindows1252';
import { getKey } from '../../utils/getKeyFromWikiDataURL';
import superagent from 'superagent';
import { checkCreateNodeAndEdges, checkCreateNodeAndEdgesByKey, checkExistsEdgeOrCreateOrUpdate, checkExistsAndCreateEdgesTwoWay,
    checkCreateNodeAndEdgesMultipleColl, checkCreateNodeAndEdgesFromArrayByKey, checkExistsAndCreateEdgesTwoWayFromArray } from '../../utils/createNodeAndEdges';
import { checkExistsByName, checkEdgeExists, checkExistsByKey, checkExistsMultipleColl, checkExistsByAttr } from '../../utils/checkExists';
import { checkExistsAndCreateNodeByKey, checkExistsAndCreateNodeByName} from '../../utils/checkExistsAndCreate';
import  wdk  from 'wikidata-sdk';

const peopleColl = db.collection('people');
const politicalParties = db.collection('politicalParties');
const executiveBodies = db.collection('executiveBodies');

const partOf = db.edgeCollection('partOf');
const hasPart = db.edgeCollection('hasPart');

const EUComID = 'executiveBodies/Q8880';

let count = 0;
let data = [];

const url = wdk.sparqlQuery(`
select distinct $person $personLabel $office $officeLabel $countryLabel $startDate $endDate $replacedBy $replacedByLabel $replaces $replacesLabel where {
  	$person wdt:P39/wdt:P279* wd:Q2661677 .
  	$person p:P39 $officeStatement .
	$officeStatement ps:P39 $office .
  	$office wdt:P279* wd:Q2661677 .
  	OPTIONAL { $officeStatement pq:P27 ?country. }
  	OPTIONAL { $officeStatement pq:P580 ?startDate. }
    OPTIONAL { $officeStatement pq:P582 ?endDate. }
    OPTIONAL { $officeStatement pq:P1366 ?replacedBy. }
    OPTIONAL { $officeStatement pq:P1365 ?replaces. }

  	SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
} ORDER BY $officeLabel`);

async function getCommissionerData(person) {
    let name = await decode(person.personLabel.value);
    let role = [typeof person.officeLabel !== 'undefined' ? person.officeLabel.value : ''];
    if (role[0] !== 'European Commissioner') role.push('European Commissioner' );
    return new Promise((resolve, reject) =>  resolve({
        name,
        _key: getKey(person.person.value),
        country: typeof person.countryLabel !== 'undefined' ? person.countryLabel.value : '',
        startDate: typeof person.startDate !== 'undefined' ? person.startDate.value : '',
        endDate: typeof person.endDate !== 'undefined' ? person.endDate.value : '',
        replaces: typeof person.replacesLabel !== 'undefined' ? person.replacesLabel.value : '',
        replacedBy: typeof person.replacedByLabel !== 'undefined' ? person.replacedByLabel.value : '',
        type: 'politician',
        subType: role
    }))
}

async function asyncLoop(iteration) {
    console.log('Iteration: '+iteration);
    if (iteration < data.length) {
        try {
            const commissionerRaw = data[iteration];
            const commissionerRefined = await getCommissionerData(commissionerRaw);
            console.dir(commissionerRefined);

            let commissionerID = await checkCreateNodeAndEdgesByKey(commissionerRefined._key, commissionerRefined.name, peopleColl, [partOf, {name: 'part of', level: 'international', type: 'commissioner of'}],
                [hasPart, {name: 'part of', level: 'international', type:'has commissioner'}], EUComID, db, aqlQuery, commissionerRefined);

            //Create replaces person
            if (typeof commissionerRaw.replacesLabel !== 'undefined' && typeof commissionerRaw.replaces !== 'undefined') {
                console.log('Adding replaces person');
                let replacesName = await decode(commissionerRaw.replacesLabel.value);
                let replacesKey = getKey(commissionerRaw.replaces.value);
                await checkCreateNodeAndEdgesByKey(replacesKey, replacesName, peopleColl, [partOf, {name: 'part of', level: 'international', type: 'commissioner of'}],
                    [hasPart, {name: 'part of', level: 'international', type:'has commissioner'}], EUComID, db, aqlQuery, 
                    {type: 'politician', subType: commissionerRefined.subType, endDate: commissionerRefined.startDate});
            }

            //Create replacedBy person
            if (typeof commissionerRaw.replacedByLabel !== 'undefined' && typeof commissionerRaw.replacedBy !== 'undefined') {
                console.log('Adding replaces person');
                let replacedByName = await decode(commissionerRaw.replacedByLabel.value);
                let replacedByKey = getKey(commissionerRaw.replacedBy.value);
                await checkCreateNodeAndEdgesByKey(replacedByKey, replacedByName, peopleColl, [partOf, {name: 'part of', level: 'international', type: 'commissioner of'}],
                    [hasPart, {name: 'part of', level: 'international', type:'has commissioner'}], EUComID, db, aqlQuery,
                    {type: 'politician', subType: commissionerRefined.subType, startDate: commissionerRefined.endDate});
            }
            
            count++;
            asyncLoop(count);
            
        //    TODO add national political party once MEPs is solved
        //    TODO fix names
        }
        catch (err) {
            console.log('CAUGHT ERR: \n'+err);
        }
    }
    else {
        console.log('All done processing Commissioners');
    }
}

superagent.get(url)
    .end((err, res) => {
        if (err) console.error(err);

        else {
            console.log('retrieval succeeded in EUCommissioners');
            const rawData = JSON.parse(res.text);
            data = rawData.results.bindings;
            console.log('data length: '+data.length );
            //iterate over results in async style
            asyncLoop(count)
        }
    });

// console.log(url);

// asyncLoop(count);