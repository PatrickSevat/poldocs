import { db, aqlQuery } from '../../arangodb';
import  wdk  from 'wikidata-sdk';
import superagent from 'superagent';
import Promise from 'promise'
import { compareObj } from '../../utils/compareObj';
import { checkCreateNodeAndEdges, checkCreateNodeAndEdgesMultipleColl } from '../../utils/createNodeAndEdges';
import { checkExists, checkEdgeExists, checkExistsMultipleColl } from '../../utils/checkExists';

const peopleColl = db.collection('people');

const politicalPartyColl = db.collection('politicalParties');
const memberOfEdgeColl = db.edgeCollection('memberOf');
const hasMemberEdgeColl = db.edgeCollection('hasMember');

const companiesColl = db.collection('companies');
const IGOcoll = db.collection('IGO');
const hasEmployeeEdgeColl = db.collection('hasEmployee');
const worksAtEdgeColl = db.collection('worksAt');

const publicOfficesColl = db.collection('publicOffices');
const holdsOrHeldOfficeEdgeColl = db.collection('holdsOrHeldOffice');
const officeFilledByEdgeColl = db.collection('officeFilledBy');

const replacesEdgeColl = db.collection('replaces');
const replacedByEdgeColl = db.collection('replacedBy');


const query = `SELECT DISTINCT ?politician ?politicianLabel ?_member_of_political_partyLabel ?_official_websiteLabel 
?_employerLabel ?_date_of_birth ?_sex_or_genderLabel ?_date_of_death ?office ?officeLabel ?_start_time ?_end_time 
?twitterLabel ?facebookLabel ?instagramLabel ?replacesLabel ?replacedByLabel  WHERE
{
  #select human which holds or has held office as NL MP
  ?politician wdt:P31 wd:Q5 .
  ?politician wdt:P39 wd:Q18887908.

  #other offices held by politician
  ?politician p:P39 ?officesStatement.
  ?officesStatement ps:P39 ?office .
  OPTIONAL { ?officesStatement pq:P580 ?_start_time. }
  OPTIONAL { ?officesStatement pq:P582 ?_end_time. }
  OPTIONAL { ?officesStatement pq:P1365 ?replaces . }
  OPTIONAL { ?officesStatement pq:P1366 ?replacedBy . }

  #Additional metadata
  OPTIONAL { ?politician wdt:P569 ?_date_of_birth. }
  OPTIONAL { ?politician wdt:P102 ?_member_of_political_party. }
  OPTIONAL { ?politician wdt:P856 ?_official_website. }
  OPTIONAL { ?politician wdt:P108 ?_employer. }
  OPTIONAL { ?politician wdt:P570 ?_date_of_death. }
  OPTIONAL { ?politician wdt:P21 ?_sex_or_gender. }
  OPTIONAL { ?politician wdt:P2002 ?twitter. }
  OPTIONAL { ?politician wdt:P2013 ?facebook. }
  OPTIONAL { ?politician wdt:P2003 ?instagram. }
  SERVICE wikibase:label { bd:serviceParam wikibase:language "nl,en". }
}
ORDER BY ?politicianLabel`;

let url = wdk.sparqlQuery(query);

const getPoliticianData = (politician) => {
    return {
        name: politician.politicianLabel.value,
        wikiData: politician.politician.value,
        politicalParty: (typeof politician._member_of_political_partyLabel === "undefined") ? '' : politician.politician._member_of_political_partyLabel.value,
        website: (typeof  politician._official_websiteLabel === "undefined") ? '' :  politician._official_websiteLabel.value,
        dateOfBirth: (typeof  politician._date_of_birth === "undefined") ? '' :  politician._date_of_birth.value ,
        dateOfDeath: (typeof  politician._date_of_death === "undefined") ? '' :   politician._date_of_death.value  ,
        twitter: (typeof  politician.twitterLabel === "undefined") ? '' : politician.twitterLabel.value,
        facebook: (typeof  politician.facebookLabel === "undefined") ? '' : politician.facebookLabel.value,
        instagram: (typeof  politician.instagramLabel === "undefined") ? '' : politician.instagramLabel.value,
        nationality: ['Kingdom of the Netherlands', 'Netherlands', 'Dutch'],
        type: ['politician'],
        subTypes: ['MP']
    };
};

let count = 0;
let data;


const asyncLoop = (iteration) => {
    setTimeout(() => {
        console.log('1 iteration: '+iteration);
        if (iteration < data.length) {
            const politician = data [iteration];
            console.log('2 processing: '+politician.itemLabel.value);
            let politicianID;

            let politicalPartyName = typeof politician._member_of_political_partyLabel !== "undefined" ? politician._member_of_political_partyLabel.value : '';
            let employerName = typeof politician._employerLabel !== "undefined" ? politician._employerLabel.value : '';
            let officeName = typeof politician.officeLabel !== "undefined" ? politician.officeLabel.value : '';
            let officeDates = {
                startTime: typeof politician._start_time.value !== "undefined" ? politician._start_time.value.value : '',
                endTime: typeof politician._end_time.value !== "undefined" ? politician._end_time.value.value : ''
            };
            let replacesName = typeof politician.replacesLabel !== "undefined" ? politician.replacesLabel.value : '';
            let replacedByName = typeof politician.replacedByLabel !== "undefined" ? politician.replacedByLabel.value : '';

            checkExists(politician.itemLabel.value, peopleColl, db, aqlQuery)
            .then(personExists => {
                //politician does not exits => create it
                if (!personExists) {
                    let politicianNormalized = getPoliticianData(politician);
                    peopleColl.save(politicianNormalized)
                    .then(savedPolitician => {
                        politicianID = savedPolitician._id;
                        checkCreateNodeAndEdges(politicalPartyName, politicalPartyColl, hasMemberEdgeColl, memberOfEdgeColl, politicianID, db, aqlQuery, {'country': 'Kingdom of the Netherlands'})
                        .then(checkCreateNodeAndEdgesMultipleColl(employerName, [companiesColl, politicalPartyColl, publicOfficesColl, IGOcoll, politicalPartyColl], hasEmployeeEdgeColl, worksAtEdgeColl, politicianID, db, aqlQuery))
                        .then(checkCreateNodeAndEdges(officeName, publicOfficesColl, officeFilledByEdgeColl, holdsOrHeldOfficeEdgeColl, politicianID, db, aqlQuery, officeDates))
                        .then(checkCreateNodeAndEdges(replacedByName, peopleColl, replacedByEdgeColl, replacesEdgeColl, politicianID, db, aqlQuery, {office: officeName}))
                        .then(checkCreateNodeAndEdges(replacesName, peopleColl, replacesEdgeColl, replacedByEdgeColl, politicianID, db, aqlQuery, {office: officeName}))
                        .then(nodeAndEdges => {
                            console.log('saved records & edges');
                            count ++;
                            setTimeout(asyncLoop(count), 500)
                        }, err => {
                            console.log('ERROR in final promise:');
                            console.log(err);
                        })
                    })
                }
                else if (personExists) {
                    let currentPolitician = personExists[0];
                    politicianID = currentPolitician._id;
                    let newPoliticianNormalized = getPoliticianData(politician);

                    let updateObj = compareObj(newPoliticianNormalized, currentPolitician);

                    peopleColl.replace(politicianID, updateObj).then(updatedPolitician => {
                        console.log('Politician already existed and updated');
                        checkCreateNodeAndEdges(politicalPartyName, politicalPartyColl, hasMemberEdgeColl, memberOfEdgeColl, politicianID, db, aqlQuery, {'country': 'Kingdom of the Netherlands'})
                            .then(checkCreateNodeAndEdgesMultipleColl(employerName, [companiesColl, politicalPartyColl, publicOfficesColl], hasEmployeeEdgeColl, worksAtEdgeColl, politicianID, db, aqlQuery))
                            .then(checkCreateNodeAndEdges(officeName, publicOfficesColl, hasEmployeeEdgeColl, worksAtEdgeColl, politicianID, db, aqlQuery, officeDates))
                            .then(checkCreateNodeAndEdges(replacedByName, peopleColl, replacedByEdgeColl, replacesEdgeColl, politicianID, db, aqlQuery, {office: officeName}))
                            .then(checkCreateNodeAndEdges(replacesName, peopleColl, replacesEdgeColl, replacedByEdgeColl, politicianID, db, aqlQuery, {office: officeName}))
                            .then(nodeAndEdges => {
                                console.log('saved records & edges');
                                count ++;
                                setTimeout(asyncLoop(count), 500)
                            }, err => {
                                console.log('ERROR in final promise:');
                                console.log(err);
                            })
                    })
                }
            }, err => console.log('err in resolving checkExists promise: ' +err))
        }
        else console.log('all done');
    },500)
};

// superagent.get(url)
//     .end((err, res) => {
//         if (err) console.error(err);
//         else {
//             console.log('retrieval succeeded in municipality');
//             const rawData = JSON.parse(res.text);
//             data = rawData.results.bindings;
//             console.log('data length: '+data.length );
//             //iterate over results in async style
//             asyncLoop(count)
//         }
//     });

// console.log(url);

// const test1 = db.collection('test1');
// const test2 = db.collection('test2');
// const test3 = db.collection('test3');
// checkExistsMultipleColl('iets', [test1, test2, test3], db, aqlQuery);

// checkExists('Mark Rutte', peopleColl, db, aqlQuery);

// console.log(peopleColl.name);