import { db, aqlQuery } from '../../arangodb';
import { decode } from '../../utils/decodeWindows1252';
import { getKey } from '../../utils/getKeyFromWikiDataURL';
import { compareObj } from '../../utils/compareObj';
import superagent from 'superagent';
import { checkCreateNodeAndEdges, checkCreateNodeAndEdgesByKey, checkExistsEdgeOrCreateOrUpdate, checkExistsAndCreateEdgesTwoWay,
    checkCreateNodeAndEdgesMultipleColl, checkCreateNodeAndEdgesFromArrayByKey, checkExistsAndCreateEdgesTwoWayFromArray } from '../../utils/createNodeAndEdges';
import { checkExistsByName, checkEdgeExists, checkExistsByKey, checkExistsMultipleColl, checkExistsByAttr } from '../../utils/checkExists';
import { checkExistsAndCreateNodeByKey, checkExistsAndCreateNodeByName} from '../../utils/checkExistsAndCreate';
import  wdk  from 'wikidata-sdk';

let query = `
SELECT DISTINCT ?politician ?politicianLabel ?_member_of_political_party 
?_member_of_political_partyLabel ?_official_websiteLabel ?_employer 
?_employerLabel ?_date_of_birth ?_sex_or_genderLabel ?_date_of_death ?office ?officeLabel ?_start_time ?_end_time 
WHERE
{
  #select human from NL or Kingdom NL which has occupation politician
  ?politician wdt:P31 wd:Q5 .
  { ?politician wdt:P27 wd:Q55 . } UNION { ?politician wdt:P27 wd:Q29999 . } 
  ?politician wdt:P106 wd:Q82955.

  #other offices held by politician
  ?politician p:P39 ?officesStatement.
  ?officesStatement ps:P39 ?office .
  OPTIONAL { ?officesStatement pq:P580 ?_start_time. }
  OPTIONAL { ?officesStatement pq:P582 ?_end_time. }

  #Additional metadata
  OPTIONAL { ?politician wdt:P569 ?_date_of_birth. }
  OPTIONAL { ?politician wdt:P102 ?_member_of_political_party. }
  OPTIONAL { ?politician wdt:P856 ?_official_website. }
  OPTIONAL { ?politician wdt:P108 ?_employer. }
  OPTIONAL { ?politician wdt:P570 ?_date_of_death. }
  OPTIONAL { ?politician wdt:P21 ?_sex_or_gender. }
  SERVICE wikibase:label { bd:serviceParam wikibase:language "en,nl". }
}
ORDER BY ?politicianLabel
`;

let url = wdk.sparqlQuery(query);

const peopleColl = db.collection('people');
const publicOffices = db.collection('publicOffices');
const politicalParties = db.collection('politicalParties');
const companies = db.collection('companies');

const partOf = db.edgeCollection('partOf');
const hasPart = db.edgeCollection('hasPart');
const holdsOrHeldOffice = db.edgeCollection('holdsOrHeldOffice');
const officeFilledBy = db.edgeCollection('officeFilledBy');


let count = 0;
let data = [];

function getPartyData(person) {
    if (typeof person._member_of_political_party !== 'undefined') {
        return {
            _key: typeof person._member_of_political_party !== 'undefined' ? getKey(person._member_of_political_party.value) : '',
            name: typeof person._member_of_political_partyLabel !== 'undefined' ? getKey(person._member_of_political_partyLabel.value) : '',
            country: ['Netherlands', 'Kingdom of the Netherlands']
        }
    }
    else return null;
}

function getOfficeDates(person) {
    return {
        officeName: person.officeLabel.value,
        holderName: person.politicianLabel.value,
        startTime: typeof person._start_time !== 'undefined' ? person._start_time.value : '',
        endTime: typeof person._end_time !== 'undefined' ? person._end_time.value : ''
    }
}

function getOfficeData(person) {
    return {
        _key: getKey(person.office.value),
        name: person.officeLabel.value
    }
}

function getEmployerData(person) {
    if (typeof person._employer !== 'undefined') {
        return {
            _key: getKey(person._employer.value),
            name: person._employerLabel.value
        }
    }
    else return null
}

function getPersonData(person) {
    return {
        _key: getKey(person.politician.value),
        name: person.politicianLabel.value,
        dateOfBirth: typeof person._date_of_birth !== 'undefined' ? person._date_of_birth.value : '',
        dateOfDeath: typeof person._date_of_death !== 'undefined' ? person._date_of_death.value : '',
        gender: typeof person._sex_or_genderLabel !== 'undefined' ? person._sex_or_genderLabel.value : '',
        wikiData: person.politician.value
    }
}

async function asyncLoop(iteration) {
    if (iteration < data.length) {
        console.log('\nIteration: '+iteration);
        const personRaw = data[iteration];
        const personRefined = getPersonData(personRaw);
        // console.dir(personRefined);
        
        const party = getPartyData(personRaw);
        // console.dir(party);
        
        const employer = getEmployerData(personRaw);
        // console.dir(employer)

        const office = getOfficeData(personRaw);
        const officeDates = getOfficeDates(personRaw);

        try {
            let personMeta = await checkExistsAndCreateNodeByKey(personRefined._key, peopleColl, db, aqlQuery, personRefined);
            let personID = personMeta._id;

            if (party) {
                let partyMeta = await checkCreateNodeAndEdgesByKey(party._key, party.name, politicalParties, [hasPart, {name: 'has part', type: 'has member'}],
                    [partOf, {name: 'part of', type: 'member of'}], personID, db, aqlQuery, party);
            }

            let officeMeta = await checkCreateNodeAndEdgesByKey(office._key, office.name, publicOffices, [officeFilledBy, officeDates], [holdsOrHeldOffice, officeDates], personID, db, aqlQuery, office );

            if (employer) {
                let companyData = await checkCreateNodeAndEdgesByKey(employer._key, employer.name, companies, [hasPart, {name: 'has part', type: 'employee of'}],
                    [partOf, {name: 'part of', type: 'employee of'}], personID, db, aqlQuery, employer);
            }

            console.log('\nPolitician done');
            count++;
            asyncLoop(count);
        }
        catch (err) {
            console.log('CAUGHT ERR: \n'+err);
        }    
    }
    else {
        console.log('\nALL DONE DUTCH POLITICIANS');
    }
}

superagent.get(url)
    .end((err, res) => {
        if (err) console.error(err);
        else {
            console.log('retrieval succeeded in dutch politicians');
            const rawData = JSON.parse(res.text);
            data = rawData.results.bindings;
            console.log('data length: '+data.length );
            //iterate over results in async style
            asyncLoop(count)
        }
    });

// console.log(url);

// asyncLoop(count);