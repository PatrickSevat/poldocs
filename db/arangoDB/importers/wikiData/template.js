import { db, aqlQuery } from '../../arangodb';
const documentColl = db.collection('documents');

documentColl.remove('someDocThatDoesntExist').then(success => {
    console.log('removed')
}, err => {
    console.log('err: \n'+err);
})