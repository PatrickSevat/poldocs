import { db, aqlQuery } from '../../arangodb';
import  wdk  from 'wikidata-sdk';
import superagent from 'superagent';
import Promise from 'promise'
import { compareObj } from '../../utils/compareObj';
import { getKey } from '../../utils/getKeyFromWikiDataURL';
import { checkCreateNodeAndEdges, checkCreateNodeAndEdgesMultipleColl, checkCreateNodeAndEdgesByKey } from '../../utils/createNodeAndEdges';
import { checkExists, checkEdgeExists, checkExistsMultipleColl, checkExistsByKey } from '../../utils/checkExists';

const peopleColl = db.collection('people');

const politicalPartyColl = db.collection('politicalParties');
const memberOfEdgeColl = db.edgeCollection('memberOf');
const hasMemberEdgeColl = db.edgeCollection('hasMember');

const companiesColl = db.collection('companies');
const IGOcoll = db.collection('IGO');
const hasEmployeeEdgeColl = db.edgeCollection('hasEmployee');
const worksAtEdgeColl = db.edgeCollection('worksAt');

const publicOfficesColl = db.collection('publicOffices');
const holdsOrHeldOfficeEdgeColl = db.edgeCollection('holdsOrHeldOffice');
const officeFilledByEdgeColl = db.edgeCollection('officeFilledBy');

const replacesEdgeColl = db.edgeCollection('replaces');
const replacedByEdgeColl = db.edgeCollection('replacedBy');


const query = `SELECT DISTINCT ?politician ?politicianLabel ?office ?officeLabel ?officeTypeLabel ?start_time ?end_time 
?replaces ?replacesLabel ?replacedBy ?replacedByLabel ?twitterLabel ?facebookLabel ?instagramLabel 
?_date_of_birth ?_date_of_death ?_member_of_political_partyLabel ?_official_websiteLabel ?_employer ?_sex_or_genderLabel
WHERE {
  ?politician wdt:P31 wd:Q5 .
  ?politician wdt:P39/wdt:P279* wd:Q19334526 .
  ?politician p:P39 ?officeStatement .
  ?officeStatement ps:P39 ?office .
  ?office wdt:P279 ?officeType .
  
  OPTIONAL { ?officeStatement pq:P580 ?start_time. }
  OPTIONAL { ?officeStatement pq:P582 ?end_time. }
  OPTIONAL { ?officeStatement pq:P1365 ?replaces . }
  OPTIONAL { ?officeStatement pq:P1366 ?replacedBy . }
  
  OPTIONAL { ?politician wdt:P569 ?_date_of_birth. }
  OPTIONAL { ?politician wdt:P102 ?_member_of_political_party. }
  OPTIONAL { ?politician wdt:P856 ?_official_website. }
  OPTIONAL { ?politician wdt:P108 ?_employer. }
  OPTIONAL { ?politician wdt:P570 ?_date_of_death. }
  OPTIONAL { ?politician wdt:P21 ?_sex_or_gender. }
  OPTIONAL { ?politician wdt:P2002 ?twitter. }
  OPTIONAL { ?politician wdt:P2013 ?facebook. }
  OPTIONAL { ?politician wdt:P2003 ?instagram. }
  
  SERVICE wikibase:label {
    bd:serviceParam wikibase:language "en" .
  	}
  
} ORDER BY ?politicianLabel`;

let url = wdk.sparqlQuery(query);

const getPoliticianData = (politician) => {
    let keyArr = politician.politician.value.split('/');
    return {
        _key: keyArr[keyArr.length-1],
        name: politician.politicianLabel.value,
        wikiData: politician.politician.value,
        gender: (typeof politician._sex_or_genderLabel === "undefined") ? '' : politician._sex_or_genderLabel.value,
        politicalParty: (typeof politician._member_of_political_partyLabel === "undefined") ? '' : politician._member_of_political_partyLabel.value,
        website: (typeof  politician._official_websiteLabel === "undefined") ? '' :  politician._official_websiteLabel.value,
        dateOfBirth: (typeof  politician._date_of_birth === "undefined") ? '' :  politician._date_of_birth.value ,
        dateOfDeath: (typeof  politician._date_of_death === "undefined") ? '' :   politician._date_of_death.value  ,
        twitter: (typeof  politician.twitterLabel === "undefined") ? '' : politician.twitterLabel.value,
        facebook: (typeof  politician.facebookLabel === "undefined") ? '' : politician.facebookLabel.value,
        instagram: (typeof  politician.instagramLabel === "undefined") ? '' : politician.instagramLabel.value,
        nationality: ['Kingdom of the Netherlands', 'Netherlands', 'Dutch'],
        type: ['politician'],
        subTypes: determineSubTypes(politician.officeTypeLabel.value, politician.officeLabel.value )
    };
};

const determineSubTypes = (subType, office) => {
    if (subType == ('defence minister' || 'finance minister' || 'foreign minister' || "interior minister"
        || 'minister of justice' || 'prime minister' || 'Public works minister' || 'transport minister' || 'deputy prime minister')) {
        return ['minister', subType]
    }
    else if (subType == 'minister of the Netherlands') return ['minister'];
    else if (subType == 'head of government' && office == 'mayor') return 'mayor';
    else if (subType == 'member of parliament' && office == 'member of the House of Representatives of the Netherlands') return 'MP';
    else if (subType == 'member of parliament' && office == 'member of the European Parliament') return 'MEP';
    else if (subType == 'position' && office == 'European Commissioner') return 'European Commissioner';

    else if (subType == 'senator') return ['senator'];
    else if (subType == 'diplomat') return ['diplomat'];
    else if (subType == 'Dutch municipal executive' || subType =='municipal councillor of Amsterdam') return ['Dutch municipal executive'];
    else return null;
};

let count = 0;
let data = [];

const asyncLoop = (iteration) => {
    setTimeout(() => {
        console.log('Iteration: '+iteration);
        if (iteration < data.length) {
            const politicianRaw = data[iteration];
            const politicianRefined = getPoliticianData(politicianRaw);
            console.log('Processing: '+politicianRefined.name);
            let politicianID;
            
            let politicalPartyName = typeof politicianRaw._member_of_political_partyLabel !== "undefined" ? politicianRaw._member_of_political_partyLabel.value : '';

            let employerName = typeof politicianRaw._employerLabel !== "undefined" ? politicianRaw._employerLabel.value : '';
            let officeName = typeof politicianRaw.officeLabel !== "undefined" ? politicianRaw.officeLabel.value : '';
            let officeKey = typeof  politicianRaw.office !== 'undefined' ? getKey(politicianRaw.office.value) : '';
            let officeDates = {
                startTime: typeof politicianRaw.start_time !== "undefined" ? politicianRaw.start_time.value : '',
                endTime: typeof politicianRaw.end_time !== "undefined" ? politicianRaw.end_time.value : ''
            };

            let replacesName = typeof politicianRaw.replacesLabel !== "undefined" ? politicianRaw.replacesLabel.value : '';
            let replacesKey = typeof politicianRaw.replaces !== "undefined" ? getKey(politicianRaw.replaces.value) : '';

            let replacedByName = typeof politicianRaw.replacedByLabel !== "undefined" ? politicianRaw.replacedByLabel.value : '';
            let replacedByKey = typeof politicianRaw.replacedBy !== "undefined" ? getKey(politicianRaw.replacedBy.value) : '';


            //check if politician already exists in db
            checkExistsByKey(politicianRefined._key, peopleColl, db, aqlQuery)
                .then(personExists => {
                    //politician does not exits => create it
                    if (!personExists) {
                        console.log('person does not exist');
                        peopleColl.save(politicianRefined)
                            .then(savedPolitician => {
                                console.log('saved politician');
                                politicianID = savedPolitician._id;
                                checkCreateNodeAndEdges(politicalPartyName, politicalPartyColl, hasMemberEdgeColl, memberOfEdgeColl, politicianID, db, aqlQuery, {'country': ['Kingdom of the Netherlands', 'Netherlands']})
                                    .then(politicalPartyChecked => {
                                        console.log('party checked');
                                        checkCreateNodeAndEdgesMultipleColl(employerName, [companiesColl, politicalPartyColl, publicOfficesColl, IGOcoll, politicalPartyColl], hasEmployeeEdgeColl, worksAtEdgeColl, politicianID, db, aqlQuery)
                                            .then(employersChecked => {
                                                console.log('employersChecked');
                                                checkCreateNodeAndEdgesByKey(officeKey, officeName, publicOfficesColl, [officeFilledByEdgeColl, officeDates], [holdsOrHeldOfficeEdgeColl,officeDates], politicianID, db, aqlQuery, {'country:': ['Kingdom of the Netherlands', 'Netherlands']})
                                                    .then(publicOfficeChecked => {
                                                        console.log('publicOfficeChecked');
                                                        checkCreateNodeAndEdgesByKey(replacedByKey, replacedByName, peopleColl, replacedByEdgeColl, replacesEdgeColl, politicianID, db, aqlQuery, {type: ['politician'], subType: officeName})
                                                            .then(replacedByChecked => {
                                                                console.log('replacedByChecked');
                                                                checkCreateNodeAndEdgesByKey(replacesKey, replacesName, peopleColl, replacesEdgeColl, replacedByEdgeColl, politicianID, db, aqlQuery, {type: 'politician', subType: officeName})
                                                                    .then(nodeAndEdges => {
                                                                        console.log('replacesChecked');
                                                                        console.log('saved records & edges');
                                                                        count ++;
                                                                        setTimeout(asyncLoop(count), 1000)
                                                                    }, err => {
                                                                        console.log('ERROR in final promise:');
                                                                        console.log(err);
                                                                    })
                                                            }, err => console.log(err))

                                                    }, err => console.log(err))
                                            }, err => console.log(err))
                                    }, err => console.log(err))
                            }, err => console.log(err));
                    }
                    else if (personExists) {
                        let currentPolitician = personExists[0];
                        politicianID = currentPolitician._id;
                        console.log('Person exists, ID: '+politicianID);
                        let newPoliticianNormalized = getPoliticianData(politicianRaw);
                        console.log('newPoliticianNormalized: '+JSON.stringify(newPoliticianNormalized));
                        let updateObj = compareObj(newPoliticianNormalized, currentPolitician);

                        peopleColl.replace(politicianID, updateObj).then(updatedPolitician => {
                            console.log('Politician already existed and updated');
                            checkCreateNodeAndEdges(politicalPartyName, politicalPartyColl, hasMemberEdgeColl, memberOfEdgeColl, politicianID, db, aqlQuery, {'country': ['Kingdom of the Netherlands', 'Netherlands']})
                                .then(politicalPartyChecked => {
                                    console.log('party checked');
                                    checkCreateNodeAndEdgesMultipleColl(employerName, [companiesColl, politicalPartyColl, publicOfficesColl, IGOcoll, politicalPartyColl], hasEmployeeEdgeColl, worksAtEdgeColl, politicianID, db, aqlQuery)
                                        .then(employersChecked => {
                                            console.log('employersChecked');
                                            checkCreateNodeAndEdgesByKey(officeKey, officeName, publicOfficesColl, [officeFilledByEdgeColl, officeDates], [holdsOrHeldOfficeEdgeColl,officeDates], politicianID, db, aqlQuery, {'country:': ['Kingdom of the Netherlands', 'Netherlands']})
                                                .then(publicOfficeChecked => {
                                                    console.log('publicOfficeChecked');
                                                    checkCreateNodeAndEdgesByKey(replacedByKey, replacedByName, peopleColl, replacedByEdgeColl, replacesEdgeColl, politicianID, db, aqlQuery, {office: officeName})
                                                        .then(replacedByChecked => {
                                                            console.log('replacedByChecked');
                                                            checkCreateNodeAndEdgesByKey(replacesKey, replacesName, peopleColl, replacesEdgeColl, replacedByEdgeColl, politicianID, db, aqlQuery, {office: officeName})
                                                                .then(nodeAndEdges => {
                                                                    console.log('replacesChecked');
                                                                    console.log('saved records & edges');
                                                                    count ++;
                                                                    setTimeout(asyncLoop(count), 1000)
                                                                }, err => {
                                                                    console.log('ERROR in final promise:');
                                                                    console.log(err);
                                                                })
                                                        }, err => console.log(err))

                                                }, err => console.log(err))
                                        }, err => console.log(err))
                                }, err => console.log(err))
                        }, err => console.log(err))
                    }
                }, err => console.log('err in resolving checkExists promise: ' +err))
        }
        else console.log('all done');
    },500)
};

superagent.get(url)
    .end((err, res) => {
        if (err) console.error(err);
        else {
            console.log('retrieval succeeded in municipality');
            const rawData = JSON.parse(res.text);
            data = rawData.results.bindings;
            console.log('data length: '+data.length );
            //iterate over results in async style
            asyncLoop(count)
        }
    });

// console.log(url);

// asyncLoop(count);