/**
 * Created by Patrick on 04/08/2016.
 */
import { db, aqlQuery } from '../../arangodb';
import  wdk  from 'wikidata-sdk';
import superagent from 'superagent';
import { getKey } from '../../utils/getKeyFromWikiDataURL';
import { compareObj } from '../../utils/compareObj';
import { decode } from '../../utils/decodeWindows1252';
import { checkCreateNodeAndEdges, checkCreateNodeAndEdgesByKey, checkExistsEdgeOrCreateOrUpdate, checkExistsAndCreateEdgesTwoWay,
    checkCreateNodeAndEdgesMultipleColl, checkCreateNodeAndEdgesFromArrayByKey } from '../../utils/createNodeAndEdges';
import { checkExistsByName, checkEdgeExists, checkExistsByKey, checkExistsMultipleColl } from '../../utils/checkExists';
import { checkExistsAndCreateNodeByKey, checkExistsAndCreateNodeByName} from '../../utils/checkExistsAndCreate';

const countriesColl = db.collection('countries');
const partOf = db.edgeCollection('partOf');
const hasPart = db.edgeCollection('hasPart');

//IGO hasPart country or country is part of IGO
const igoCollection = db.collection('IGO');

//Executive body governs over country and other way around
//Additional edges: partOf hasPart country
//Additional data: jurisdiction, type
const executiveBodiesColl = db.collection('executiveBodies');
const governsOverEdgeColl = db.edgeCollection('governsOver');
const governedByEdgeColl = db.edgeCollection('governedBy');

//Executive body controlled by legislative body and other way around
//Additional edges: partOf hasPart country
//Additional data: jurisdiction, type
const legislativeBodiesColl = db.collection('legislativeBodies');
const controlsEdgeColl = db.edgeCollection('controls');
const controlledByEdgeColl = db.edgeCollection('controlledBy');

//Judiciary checks both legislative and executive bodies
//Additional data: jurisdiction, type
const judiciariesColl = db.collection('judiciaries');
const judgesOver = db.edgeCollection('judgesOver');
const judgedOverBy = db.edgeCollection('judgedOverBy');
const overrules = db.edgeCollection('overrules');
const overruledBy = db.edgeCollection('overruledBy');

const EUJudiciaries = ['judiciaries/Q4951', 'judiciaries/Q740574', 'judiciaries/Q1142069', 'judiciaries/Q1518827'];

const urlNL = wdk.sparqlQuery(`SELECT DISTINCT ?item ?itemLabel ?memberOf ?memberOfKeys ?codeLabel ?waterLabel ?populationLabel ?giniLabel ?hdiLabel ?gdpLabel
?legislativeBody ?legislativeBodyLabel ?uniCameral ?uniCameralLabel ?lowerHouse ?lowerHouseLabel ?upperHouse ?upperHouseLabel ?executiveBody ?executiveBodyLabel
 ?judicialLabel WHERE {
  SERVICE wikibase:label { bd:serviceParam wikibase:language "en" .}
  ?item wdt:P36 wd:Q727 .
  ?item wdt:P131 wd:Q29999 .
  
   OPTIONAL { ?item wdt:P194 ?legislativeBody .
           	OPTIONAL { ?legislativeBody wdt:P527 ?lowerHouse .
                       ?lowerHouse wdt:P31/wdt:P279* wd:Q375928
                     }
             OPTIONAL { ?legislativeBody wdt:P527 ?upperHouse .
                       ?upperHouse wdt:P31/wdt:P279* wd:Q637846
                     }
            OPTIONAL { ?legislativeBody wdt:P150 ?lowerHouse .
                       ?lowerHouse wdt:P31/wdt:P279* wd:Q375928
                     }
             OPTIONAL { ?legislativeBody wdt:P150 ?upperHouse .
                       ?upperHouse wdt:P31/wdt:P279* wd:Q637846
                     }
           } 
  OPTIONAL { ?item wdt:P208 ?executiveBody . }         
  OPTIONAL { ?item wdt:P297 ?code . }         
  OPTIONAL { ?item wdt:P1082 ?population . }
  OPTIONAL { ?item wdt:P1125 ?gini . }
  OPTIONAL { ?item wdt:P1081 ?hdi . }
  OPTIONAL { ?item wdt:P2131 ?gdp . }
  OPTIONAL { ?item wdt:P2927 ?water . }
  OPTIONAL { ?item wdt:P209 ?judicial . }
   {
    SELECT ?item (GROUP_CONCAT(?memberOfLabel; SEPARATOR=";") As ?memberOf) (GROUP_CONCAT(?memberOf; SEPARATOR=";") As ?memberOfKeys) WHERE {
      ?item wdt:P36 wd:Q727 .
  	  ?item wdt:P131 wd:Q29999 .
      ?item wdt:P463 ?memberOf .
      SERVICE wikibase:label { bd:serviceParam wikibase:language "en" .
                             ?memberOf rdfs:label ?memberOfLabel}
    }
    GROUP BY ?item
  }
} ORDER BY ?itemLabel`);

//Contains languages
const url = wdk.sparqlQuery(`
SELECT DISTINCT ?item ?itemLabel ?memberOf ?memberOfKeys ?codeLabel ?waterLabel ?populationLabel ?giniLabel ?hdiLabel ?gdpLabel
?legislativeBody ?legislativeBodyLabel ?uniCameral ?uniCameralLabel ?lowerHouse ?lowerHouseLabel ?upperHouse ?upperHouseLabel ?executiveBody ?executiveBodyLabel
?judicial ?judicialLabel ?languageCodeLabels ?languageCodeKeys WHERE {
  SERVICE wikibase:label { bd:serviceParam wikibase:language "en" .}
  ?item wdt:P31 wd:Q185441 .
  
   OPTIONAL { ?item wdt:P194 ?legislativeBody .
           	OPTIONAL { ?legislativeBody wdt:P527 ?lowerHouse .
                       ?lowerHouse wdt:P31/wdt:P279* wd:Q375928
                     }
             OPTIONAL { ?legislativeBody wdt:P527 ?upperHouse .
                       ?upperHouse wdt:P31/wdt:P279* wd:Q637846
                     }
            OPTIONAL { ?legislativeBody wdt:P150 ?lowerHouse .
                       ?lowerHouse wdt:P31/wdt:P279* wd:Q375928
                     }
             OPTIONAL { ?legislativeBody wdt:P150 ?upperHouse .
                       ?upperHouse wdt:P31/wdt:P279* wd:Q637846
                     }
           } 
  OPTIONAL { ?item wdt:P208 ?executiveBody . }         
  OPTIONAL { ?item wdt:P297 ?code . }         
  OPTIONAL { ?item wdt:P1082 ?population . }
  OPTIONAL { ?item wdt:P1125 ?gini . }
  OPTIONAL { ?item wdt:P1081 ?hdi . }
  OPTIONAL { ?item wdt:P2131 ?gdp . }
  OPTIONAL { ?item wdt:P2927 ?water . }
  OPTIONAL { ?item wdt:P209 ?judicial . }
  
  {
    SELECT ?item (GROUP_CONCAT(?languageCodeLabel; SEPARATOR=";") As ?languageCodeLabels) (GROUP_CONCAT(?languageCode; SEPARATOR=";") As ?languageCodeKeys) WHERE {
      ?item wdt:P31 wd:Q185441 .
      ?item wdt:P37 ?language .
	?language p:P424 ?languageStatement .
	?languageStatement ps:P424 ?languageCode .
      SERVICE wikibase:label { bd:serviceParam wikibase:language "en" .
                             ?languageCode rdfs:label ?languageCodeLabel}
    }
    GROUP BY ?item
  }
  
   {
    SELECT ?item (GROUP_CONCAT(?memberOfLabel; SEPARATOR=";") As ?memberOf) (GROUP_CONCAT(?memberOf; SEPARATOR=";") As ?memberOfKeys) WHERE {
      ?item wdt:P31 wd:Q185441 .
      ?item wdt:P463 ?memberOf .
      SERVICE wikibase:label { bd:serviceParam wikibase:language "en" .
                             ?memberOf rdfs:label ?memberOfLabel}
    }
    GROUP BY ?item
  }
} ORDER BY ?itemLabel`);

async function getCountryData (countries) {
    let name = await decode(countries.itemLabel.value);
    return {
        _key: getKey(countries.item.value),
        code:  (typeof countries.codeLabel === "undefined") ? '' : countries.codeLabel.value,
        name,
        wikiData: countries.item.value,
        water: (typeof countries.waterLabel === "undefined") ? '' : countries.waterLabel.value,
        population: (typeof  countries.populationLabel === "undefined") ? '' :  countries.populationLabel.value,
        gini: (typeof  countries.giniLabel === "undefined") ? '' :  countries.giniLabel.value ,
        hdi: (typeof  countries.hdiLabel === "undefined") ? '' :   countries.hdiLabel.value  ,
        gdp: (typeof  countries.gdpLabel === "undefined") ? '' :   countries.gdpLabel.value,
        languageCodes: countries.languageCodeLabels.value.split(';')
    };
};

let count = 0;
let data = [];

const getMemberOfArray = (nameArr, urlArr) => {
    let returnArr = [];
    for (let i = 0; i < nameArr.length; i++) {
        returnArr.push({
            name: nameArr[i],
            _key: getKey(urlArr[i]),
            type: 'intergovenmental organisation',
            level: 'international'
        })
    }
    return returnArr;
};



//recursive async loop
async function asyncLoop (iteration) {

    console.log('Iteration: '+iteration);
    if (iteration < data.length) {
        const countryRaw = data[iteration];
        const countryRefined = await getCountryData(countryRaw);
        console.log('Processing: '+countryRefined.name);
        let countryMeta;
        let countryID;

        let executiveBodyMeta;
        let executiveBodyID;

        let legislativeBodyMeta;
        let legislativeBodyID;

        let judiciaryMeta;
        let judiciaryID;

        let executiveBodyName = typeof countryRaw.executiveBodyLabel !== "undefined" ? await decode(countryRaw.executiveBodyLabel.value) : '';
        let executiveBodyKey = typeof countryRaw.executiveBody !== "undefined" ? getKey(countryRaw.executiveBody.value): '';

        let legislativeBodyName = typeof countryRaw.legislativeBodyLabel !== "undefined" ? await decode(countryRaw.legislativeBodyLabel.value) : '';
        let legislativeBodyKey = typeof countryRaw.legislativeBody !== "undefined" ? getKey(countryRaw.legislativeBody.value) : '';

        let lowerHouseName = typeof countryRaw.lowerHouseLabel !== "undefined" ? await decode(countryRaw.lowerHouseLabel.value) : '';
        let lowerHouseKey = typeof countryRaw.lowerHouse !== "undefined" ? getKey(countryRaw.lowerHouse.value) : '';

        let upperHouseName = typeof countryRaw.upperHouseLabel !== "undefined" ? await decode(countryRaw.upperHouseLabel.value) : '';
        let upperHouseKey = typeof countryRaw.upperHouse !== "undefined" ? getKey(countryRaw.upperHouse.value) : '';

        let legislativeObj = {
            combined: {
                name: legislativeBodyName,
                _key: legislativeBodyKey
            },
            lower: {
                name: lowerHouseName,
                _key: lowerHouseKey
            },
            upper: {
                name: upperHouseName,
                _key: upperHouseKey
            }
        };

        let judiciaryName = typeof countryRaw.judicialLabel !== "undefined" ? await decode(countryRaw.judicialLabel.value) : '';
        let judiciaryKey = typeof countryRaw.judicial !== "undefined" ? getKey(countryRaw.judicial.value) : '';

        let memberOfArray = getMemberOfArray(countryRaw.memberOf.value.split(';'),countryRaw.memberOfKeys.value.split(';'));

        try {
            //Create or update country
            countryMeta = await checkExistsAndCreateNodeByKey(countryRefined._key, countriesColl, db, aqlQuery, countryRefined);

            countryID = countryMeta._id;

            //Create or update IGOs and create or update edges connecting IGOs with country
            await checkCreateNodeAndEdgesFromArrayByKey(memberOfArray, igoCollection, [hasPart, {name: 'has part', type: 'has member state', level: 'international'}],
                [partOf, {name: 'part of', type: 'member state of', level: 'international'}], countryID, db, aqlQuery);

            //Create executive body and create or updates edges connecting Exec Body governing country
            executiveBodyMeta = await checkCreateNodeAndEdgesByKey(executiveBodyKey, executiveBodyName, executiveBodiesColl,
                [governsOverEdgeColl, {name: 'governs over', level: 'national'}], [governedByEdgeColl, {name: 'governed by',level: 'national'}],
                countryID, db, aqlQuery, {type: 'national government', level: 'national', jurisdiction: countryRefined.name});

            executiveBodyID = executiveBodyMeta._id;
            console.log('Saved or updated executive body');

            //Create or update edges connecting exec body partOf to country
            await checkExistsAndCreateEdgesTwoWay(executiveBodyID, countryID, [partOf, {name: 'part of', type: 'executive body part of'}], [hasPart, {name: 'has part', type: 'has executive body'}], db, aqlQuery)

            //Create legislative bodies
            legislativeBodyMeta = await processLegislativeBodies(legislativeObj, countryID, executiveBodyID, countryRefined.name);
            legislativeBodyID = legislativeBodyMeta._id;

            //Create judiciary if available
            judiciaryMeta = await checkCreateNodeAndEdgesByKey(judiciaryKey, judiciaryName, judiciariesColl, [partOf, {name: 'part of', level: 'national', type:'judiciary part of'}],
                [hasPart, {name: 'has part',level: 'national',type: 'has judiciary'}], countryID, db, aqlQuery,
                {type: 'judiciary', subType:'highest national judiciary', level: 'national', jurisdiction: countryRefined.name});

            if (!judiciaryMeta) {
                console.log('All done, iteration: '+count);
                count++;
                asyncLoop(count);
            }

            else {
                judiciaryID = judiciaryMeta._id;

                // Create or update edges connecting judiciary to executiveBody and legislativeBodies
                await checkExistsAndCreateEdgesTwoWay(judiciaryID, executiveBodyID, [judgesOver, {name: 'judges over', jurisdiction: countryRefined.name}],
                    [judgedOverBy, {name: 'judged over by', jurisdiction: countryRefined.name}], db, aqlQuery);

                await checkExistsAndCreateEdgesTwoWay(judiciaryID, legislativeBodyID, [judgesOver, {name: 'judges over', jurisdiction: countryRefined.name}],
                    [judgedOverBy, {name: 'judged over by', jurisdiction: countryRefined.name}], db, aqlQuery);

                // add judicial hierarchy edges
                await checkExistsAndCreateEdgesTwoWay(judiciaryID, EUJudiciaries[0], [overruledBy, {name: 'overruled by'}],
                    [overrules, {name: 'overrules'}], db, aqlQuery);

                console.log('All done, iteration: '+count);
                count++;
                asyncLoop(count)
            }
        }
        catch (err) {
            console.log('CAUGHT ERR: \n'+err);
        }
    }
    else console.log('All done processing countries');
}

async function processLegislativeBodies (legislativeObj, countryID, executiveBodyID, countryName) {
    let combinedMeta;
    let lowerMeta;
    let upperMeta;
    let combinedID;
    let lowerID;
    let upperID;
    //If combined.name is '' we can skip, no legislative body provided
    if (legislativeObj.combined.name === '') {
        console.log('No legislative bodies');
        return new Promise((resolve, reject) => {
            resolve();
        });
    }
    else {
        try {
            //Unicameral legislature
            if (legislativeObj.lower.name == '') {
                //Create main legislative body
                combinedMeta = await checkCreateNodeAndEdgesByKey(legislativeObj.combined._key, legislativeObj.combined.name, legislativeBodiesColl,
                    [controlsEdgeColl, {name: 'controls', level: 'national'}], [controlledByEdgeColl, {name: 'controlled by', level: 'national'}], executiveBodyID, db, aqlQuery,
                    {name:legislativeObj.combined.name, type: 'national legislative body', subType: 'unicameral', level: 'national', jurisdiction: countryName});
                combinedID = combinedMeta._id;

                // create edges connecting it to country
                await checkExistsAndCreateEdgesTwoWay(combinedID, countryID, partOf, hasPart, db, aqlQuery);

                return new Promise((resolve, reject) => {
                    resolve(combinedMeta)
                });
            }

            // Bicameral legislature
            else {
                //Create main legislative body
                combinedMeta = await checkCreateNodeAndEdgesByKey(legislativeObj.combined._key, legislativeObj.combined.name, legislativeBodiesColl,
                    [controlsEdgeColl, {name: 'controls', level: 'national'}], [controlledByEdgeColl, {name: 'controlled by', level: 'national'}], executiveBodyID, db, aqlQuery,
                    {name:legislativeObj.combined.name, type: 'national legislative body', subType: 'bicameral', level: 'national', jurisdiction: countryName});
                combinedID = combinedMeta._id;

                // create edges connecting it to country
                await checkExistsAndCreateEdgesTwoWay(combinedID, countryID, [partOf, {name: 'part of',type: 'legislative body of', subType:'main legislative body of'}], [hasPart, {name: 'has part', type: 'has legislative body', subType:'has main legislative body'}], db, aqlQuery);

                // create lower House
                lowerMeta = await checkCreateNodeAndEdgesByKey(legislativeObj.lower._key, legislativeObj.lower.name, legislativeBodiesColl,
                    [controlsEdgeColl, {name: 'controls', level: 'national'}], [controlledByEdgeColl, {name: 'controlled by', level: 'national'}], executiveBodyID, db, aqlQuery,
                    {name:legislativeObj.lower.name, type: 'national legislative body', subType: 'lower house', level: 'national', jurisdiction: countryName});
                lowerID = lowerMeta._id;

                // create edges connecting lower house to main legislative body
                await checkExistsAndCreateEdgesTwoWay(lowerID, combinedID, [partOf, {name: 'part of', type: 'legislative body of', subType: 'lower house of'}], [hasPart, {name: 'has part', type: 'has legislative body', subType: 'has lower house'}], db, aqlQuery);

                // create upper House
                upperMeta = await checkCreateNodeAndEdgesByKey(legislativeObj.upper._key, legislativeObj.upper.name, legislativeBodiesColl,
                    [controlsEdgeColl, {name: 'controls', level: 'national'}], [controlledByEdgeColl, {name: 'controlled by', level: 'national'}], executiveBodyID, db, aqlQuery,
                    {name:legislativeObj.upper.name, type: 'national legislative body', subType: 'upper house', level: 'national', jurisdiction: countryName});
                upperID = upperMeta._id;

                // create edges connecting upper house to main legislative body
                await checkExistsAndCreateEdgesTwoWay(upperID, combinedID, [partOf, {name: 'part of', type:'legislative body of', subType: 'upper house of'}], [hasPart, {name: 'has part', type: 'has legislative body', subType: 'upper house of'}], db, aqlQuery)
                console.log('Created parliaments and edges');
                return new Promise((resolve, reject) => {
                    resolve(combinedMeta)
                })
            }
        }
        catch (err) {
            console.log('CAUGHT ERR in processLegislativeBody: \n'+err)
        }
    }
}

// make call to wikidata api
superagent.get(url)
    .end((err, res) => {
        if (err) console.error(err);

        else {
            console.log('retrieval succeeded in country');
            const rawData = JSON.parse(res.text);
            data = rawData.results.bindings;
            console.log('data length: '+data.length );
            //iterate over results in async style
            asyncLoop(count)
        }
    });

// console.log(url);

// console.log(decode('DÃ¡il Ã‰ireann'));
// Dáil Éireann

// asyncLoop(count);

/*****************************************************************************
 *
 *          TEST DATA & VARIABLES
 *
 *
 *****************************************************************************/