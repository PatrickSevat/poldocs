import { db, aqlQuery } from '../../arangodb';
import  wdk  from 'wikidata-sdk';
import superagent from 'superagent';
import { getKey } from '../../utils/getKeyFromWikiDataURL';
import { compareObj } from '../../utils/compareObj';
import { decodeUTF8 } from '../../utils/decodeUTF8';
import { checkCreateNodeAndEdges, checkCreateNodeAndEdgesByKey, checkExistsEdgeOrCreateOrUpdate, checkExistsAndCreateEdgesTwoWay,
    checkCreateNodeAndEdgesMultipleColl, checkCreateNodeAndEdgesFromArrayByKey } from '../../utils/createNodeAndEdges';
import { checkExistsByName, checkEdgeExists, checkExistsByKey, checkExistsMultipleColl } from '../../utils/checkExists';
import { checkExistsAndCreateNodeByKey, checkExistsAndCreateNodeByName} from '../../utils/checkExistsAndCreate';

const executiveBodiesColl = db.collection('executiveBodies');
const partOf = db.edgeCollection('partOf');
const hasPart = db.edgeCollection('hasPart');

const url = wdk.sparqlQuery(`SELECT DISTINCT ?item ?itemLabel ?dutchName ?inceptionLabel ?websiteLabel ?officeLabel ?countryLabel ?officeHolderLabel WHERE {
  { ?item wdt:P31 wd:Q3143387 } .
  OPTIONAL { ?item wdt:P571 ?inception}
  OPTIONAL { ?item wdt:P856 ?website}
  OPTIONAL { ?item wdt:P2388 ?office}
  OPTIONAL { ?item wdt:P1037 ?officeHolder}

  SERVICE wikibase:label {
    bd:serviceParam wikibase:language "en" .
  	}
  SERVICE wikibase:label {
    bd:serviceParam wikibase:language "nl" .
    ?item rdfs:label ?dutchName .
  	}
}  ORDER BY ?itemLabel`);

const cabinetOfNLID = 'executiveBodies/Q2479200';
let count = 0;
let data = [];

const getMinistryData = (ministry) => {
    let nameEng = typeof ministry.itemLabel !== 'undefined' ? ministry.itemLabel.value : '';
    let nameNL = typeof ministry.dutchName !== 'undefined' ? ministry.dutchName.value : '';
    return {
        name: nameEng,
        nameNL: nameNL,
        country: 'Netherlands',
        _key: getKey(ministry.item.value),
        inceptionDate: typeof ministry.inceptionLabel !== 'undefined' ? ministry.inceptionLabel.value : '',
        website: typeof ministry.websiteLabel !== 'undefined' ? ministry.websiteLabel.value : ''
    }
};

async function asyncLoop(iteration) {
    console.log('iteration: '+iteration);
    let ministryRaw = data[iteration];
    let ministryRefined = getMinistryData(ministryRaw);

    if (iteration < data.length) {
        try {
            await checkCreateNodeAndEdgesByKey(ministryRefined._key, ministryRefined.name, executiveBodiesColl,
                [partOf, {name: 'part of', type: 'ministry of', level: 'national'}], [hasPart, {name: 'has part', type: 'has ministry', level: 'national'}],
                cabinetOfNLID, db, aqlQuery, ministryRefined);

            console.log('Added all dutch ministries');
            count++;
            asyncLoop(count);
        }
        catch(err) {
            console.error('CAUGHT ERROR \n'+err);
        }

    }
    else {
        console.log('All done!');
    }
};

// make call to wikidata api
superagent.get(url)
    .end((err, res) => {
        if (err) console.error(err);
        else {
            console.log('retrieval succeeded in dutchMinistries');
            const rawData = JSON.parse(res.text);
            data = rawData.results.bindings;
            console.log('data length: '+data.length );
            //iterate over results in async style
            asyncLoop(count)
        }
    });

// console.log(url);
// asyncLoop(count);
