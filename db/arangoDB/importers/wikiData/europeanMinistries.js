import { db, aqlQuery } from '../../arangodb';
import  wdk  from 'wikidata-sdk';
import superagent from 'superagent';
import { getKey } from '../../utils/getKeyFromWikiDataURL';
import { compareObj } from '../../utils/compareObj';
import { decodeUTF8 } from '../../utils/decodeUTF8';
import { decideName } from '../../utils/decideName';
import { checkCreateNodeAndEdges, checkCreateNodeAndEdgesMultipleColl, checkCreateNodeAndEdgesByKey } from '../../utils/createNodeAndEdges';
import { checkExists, checkEdgeExists, checkExistsMultipleColl, checkExistsByKey } from '../../utils/checkExists';

/***
 *
 *
 * !!!IMPORTANT!!!
 * This query only goes 1 subclass deep from ministry
 * 
 * !!!IMPORTANT!!!
 * Adding more label services increases the time needed to retrieve query by factor 25 or more
 * 
 * */


const executiveBodiesColl = db.collection('executiveBodies');

const publicOfficesColl = db.collection('publicOffices');

const peopleColl = db.collection('people');
const headOfBodyEdgeColl = db.edgeCollection('headOfBody');
const officeFilledByEdgeColl = db.edgeCollection('officeFilledBy');

const url = wdk.sparqlQuery(`SELECT DISTINCT ?item ?itemLabel ?dutchName ?germanName ?inceptionLabel ?websiteLabel ?officeLabel ?countryLabel 
?officeHolder ?officeHolderLabel WHERE {
  {?item wdt:P31 wd:Q192350} UNION {?item wdt:P31/wdt:P279 wd:Q192350} .
  ?country wdt:P31 wd:Q185441 .
  ?item wdt:P17 ?country .
  OPTIONAL { ?item wdt:P571 ?inception}
  OPTIONAL { ?item wdt:P856 ?website}
  OPTIONAL { ?item wdt:P2388 ?office}
  OPTIONAL { ?item wdt:P1037 ?officeHolder}

  SERVICE wikibase:label {
    bd:serviceParam wikibase:language "en,nl,de,fr" .
  	}
}  ORDER BY ?itemLabel`);

let count = 0;
let data;
let skippedWikiDataIDs = [];

const getMinistryData = (ministry) => {
    let nameEng = typeof ministry.itemLabel !== 'undefined' ? ministry.itemLabel.value : '';
    let keyArr = ministry.item.value.split('/');
    return {
        _key: keyArr[keyArr.length-1],
        name: ministry.itemLabel.value,
        country: ministry.countryLabel.value,
        wikiData: ministry.item.value,
        inceptionDate: typeof ministry.inceptionLabel !== 'undefined' ? ministry.inceptionLabel.value : '',
        website: typeof ministry.websiteLabel !== 'undefined' ? ministry.websiteLabel.value : ''
    }
};

const asyncLoop = (iteration) => {
    setTimeout(() => {
        console.log('iteration: '+iteration);
        if (iteration < data.length) {
            let ministryRaw = data[iteration];
            let ministryRefined = getMinistryData(ministryRaw);
            console.log('refined key: '+ministryRefined._key);
            let officeName = typeof ministryRaw.officeLabel !== 'undefined' ? ministryRaw.officeLabel.value : '';
            let officeHolderName = typeof ministryRaw.officeHolderLabel !== 'undefined' ? ministryRaw.officeHolderLabel.value : '';
            let officeHolderRaw = typeof ministryRaw.officeHolder !== 'undefined' ? ministryRaw.officeHolder.value.split('/') : '';
            let officeHolderKey;
            if (officeHolderRaw instanceof Array) {
                officeHolderKey = officeHolderRaw[officeHolderRaw.length-1];
            }
            else officeHolderKey = '';
            //extra check for name due to wikidata timeout
            if (ministryRefined.name !== '') {
                //check if ministry exists in db
                checkExistsByKey(ministryRefined._key, executiveBodiesColl, db, aqlQuery).then(ministryExists => {
                    let ministryID;
                    let publicOfficeID;
                    //ministry does not exists
                    if (!ministryExists) {
                        console.log('ministry does not exist');
                        //create new ministry
                        executiveBodiesColl.save(ministryRefined).then(savedMinistry => {
                            //check public office related to ministry exists
                            if (officeName !== '') {
                                checkExists(officeName, publicOfficesColl, db, aqlQuery).then(officeExists => {
                                    //public office exists
                                    if (officeExists) {
                                        publicOfficeID = officeExists[0]._id;
                                        //check if public office holder (minister] exists + corresponding edges and create if necessary
                                        checkCreateNodeAndEdgesByKey(officeHolderKey, officeHolderName, peopleColl, headOfBodyEdgeColl, officeFilledByEdgeColl, publicOfficeID, db, aqlQuery, {_key: officeHolderID,type: ['politician'], subType: ['minister']})
                                            .then(allDone => {
                                                console.log('Created ministry & public office, checked officeHolder & edges');
                                                count++;
                                                setTimeout(asyncLoop(count), 500);
                                            }, err => {
                                                console.log('ERROR in final promise:');
                                                console.log(err);
                                            })
                                    }
                                    //    public office does not exist
                                    else {
                                        // create public office
                                        publicOfficesColl.save({name: officeName, country: ministryRefined.country}).then(savedPublicOffice => {
                                            publicOfficeID = savedPublicOffice._id;
                                            // check if public office holder (minister] exists + corresponding edges and create if necessary
                                            checkCreateNodeAndEdgesByKey(officeHolderKey, officeHolderName, peopleColl, headOfBodyEdgeColl, officeFilledByEdgeColl, publicOfficeID, db, aqlQuery, {_key: officeHolderID, type: ['politician'], subType: ['minister']})
                                                .then(allDone => {
                                                    console.log('Created ministry & public office, checked officeHolder & edges');
                                                    count++;
                                                    setTimeout(asyncLoop(count), 500);
                                                }, err => {
                                                    console.log('ERROR in final promise:');
                                                    console.log(err);
                                                })
                                        })
                                    }
                                })
                            }
                            else {
                                count++;
                                setTimeout(asyncLoop(count), 500)
                            }
                        }, err => console.log(err));
                    }
                    //    ministry already exists, update it with new data
                    else {
                        let ministryOld = ministryExists[0];
                        ministryID = ministryOld._id;
                        
                        let updateMinistry = compareObj(ministryRefined, ministryOld);
                        console.log('ministry exists, updating ID: '+ministryID);
                        console.log('update Obj:\n'+JSON.stringify(updateMinistry));
                        executiveBodiesColl.replace(ministryID, updateMinistry).then(updatedMinistry => {
                            console.log('updated ministry');
                            //check public office related to ministry exists
                            if (officeName !== '') {
                                checkExists(officeName, publicOfficesColl, db, aqlQuery).then(officeExists => {
                                    //public office exists
                                    if (officeExists) {
                                        publicOfficeID = officeExists[0]._id;
                                        //check if public office holder (minister] exists + corresponding edges and create if necessary
                                        checkCreateNodeAndEdgesByKey(officeHolderKey, officeHolderName, peopleColl, headOfBodyEdgeColl, officeFilledByEdgeColl, publicOfficeID, db, aqlQuery, {type: ['politician'], subType: ['minister']})
                                            .then(allDone => {
                                                console.log('Created ministry & public office, checked officeHolder & edges');
                                                count++;
                                                setTimeout(asyncLoop(count), 500);
                                            }, err => {
                                                console.log('ERROR in final promise:');
                                                console.log(err);
                                            })
                                    }
                                    //    public office does not exist
                                    else {
                                        if (officeName !== '') {
                                            // create public office
                                            publicOfficesColl.save({name: officeName, country: ministryRefined.country}).then(savedPublicOffice => {
                                                publicOfficeID = savedPublicOffice._id;
                                                // check if public office holder (minister] exists + corresponding edges and create if necessary
                                                checkCreateNodeAndEdgesByKey(officeHolderKey, officeHolderName, peopleColl, headOfBodyEdgeColl, officeFilledByEdgeColl, publicOfficeID, db, aqlQuery, {type: ['politician'], subType: ['minister']})
                                                    .then(allDone => {
                                                        console.log('Created ministry & public office, checked officeHolder & edges');
                                                        count++;
                                                        setTimeout(asyncLoop(count), 500);
                                                    }, err => {
                                                        console.log('ERROR in final promise:');
                                                        console.log(err);
                                                    })
                                            })
                                        }
                                        
                                    }
                                })
                            }
                            else {
                                count++;
                                setTimeout(asyncLoop(count), 500)
                            }
                        }, err => console.log(err))
                    }
                })
            }
            else {
                skippedWikiDataIDs.push(ministryRefined.wikiData);
                console.log('Invalid ministry name');
                count++;
                setTimeout(asyncLoop(count), 500);
            }
        }
        else {
            console.log('All done!');
            console.log('Skipped:');
            console.log(skippedWikiDataIDs);
        }
    },500)
};

// console.log(url);

// asyncLoop(count);

// make call to wikidata api
superagent.get(url)
    .end((err, res) => {
        if (err) console.error(err);
        else {
            console.log('retrieval succeeded in municipality');
            const rawData = JSON.parse(res.text);
            data = rawData.results.bindings;
            console.log('data length: '+data.length );
            //iterate over results in async style
            asyncLoop(count)
        }
    });

