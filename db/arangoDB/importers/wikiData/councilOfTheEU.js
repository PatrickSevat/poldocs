import { db, aqlQuery } from '../../arangodb';
import { decode } from '../../utils/decodeWindows1252';
import { getKey } from '../../utils/getKeyFromWikiDataURL';
import superagent from 'superagent';
import { checkCreateNodeAndEdges, checkCreateNodeAndEdgesByKey, checkExistsEdgeOrCreateOrUpdate, checkExistsAndCreateEdgesTwoWay,
    checkCreateNodeAndEdgesMultipleColl, checkCreateNodeAndEdgesFromArrayByKey, checkExistsAndCreateEdgesTwoWayFromArray } from '../../utils/createNodeAndEdges';
import { checkExistsByName, checkEdgeExists, checkExistsByKey, checkExistsMultipleColl, checkExistsByAttr } from '../../utils/checkExists';
import { checkExistsAndCreateNodeByKey, checkExistsAndCreateNodeByName} from '../../utils/checkExistsAndCreate';
import  wdk  from 'wikidata-sdk';

const politicalParties = db.collection('politicalParties');
const executiveBodies = db.collection('executiveBodies');
const legislativeBodies = db.collection('legislativeBodies');
const otherBodies = db.collection('otherBodies');

const partOf = db.edgeCollection('partOf');
const hasPart = db.edgeCollection('hasPart');

const controlsEdgeColl = db.edgeCollection('controls');
const controlledByEdgeColl = db.edgeCollection('controlledBy');

const EUComID = 'executiveBodies/Q8880';
const EUID = 'IGO/Q458';

let count = 0;
let data = [{
    "council": {
        "type": "uri",
        "value": "http://www.wikidata.org/entity/Q8896"
    },
    "config": {
        "type": "uri",
        "value": "http://www.wikidata.org/entity/Q203276"
    },
    "councilLabel": {
        "xml:lang": "en",
        "type": "literal",
        "value": "Council of the European Union"
    },
    "configLabel": {
        "xml:lang": "en",
        "type": "literal",
        "value": "General Affairs Council"
    }
}];

const url = wdk.sparqlQuery(`
SELECT DISTINCT $council $councilLabel $config $configLabel WHERE {
  $council wdt:P279* wd:Q8896 .
  $council wdt:P527 $config .
  OPTIONAL { $council wdt:P463 $member }
  OPTIONAL { $config wdt:P463 $member }
  SERVICE wikibase:label { bd:serviceParam wikibase:language "en" . }
}`);

const getCouncilRefined = (council) => {
    return {
        name: council.councilLabel.value,
        _key: getKey(council.council.value),
        wikiData: council.council.value,
        type: 'European legislative body',
        subType: 'bicameral',
        level: 'international',
        jurisdiction: 'European Union'
    }
};

const getConfigRefined = (council) => {
    return {
        name: council.configLabel.value,
        _key: getKey(council.config.value),
        wikiData: council.config.value,
        level: 'international',
        jurisdiction: 'European Union',
        type: 'configuration of the Council of the European Union'
    }
};

async function asyncLoop(iteration) {
    if (iteration < data.length) {
        const councilRaw = data[iteration];
        const councilRefined = getCouncilRefined(councilRaw);
        const configRefined = getConfigRefined(councilRaw);

        try {
            let councilMeta = await checkCreateNodeAndEdgesByKey(councilRefined._key, councilRefined.name, legislativeBodies,
                [partOf, {name: 'part of', type: 'legislative body of'}], [hasPart, {name: 'has part', type: 'has legislative body'}], EUID, db, aqlQuery, councilRefined);
            let councilID = councilMeta._id;

            await checkExistsAndCreateEdgesTwoWay(councilID, EUComID, [controlsEdgeColl, {name: 'controls', level: 'international'}],
                [controlledByEdgeColl, {name: 'controlled by', level: 'international'}], db, aqlQuery);

            let configMeta = await checkCreateNodeAndEdgesByKey(configRefined._key, configRefined.name, otherBodies,
                [partOf, {name: 'part of', type: 'configuration of'}], [hasPart, {name: 'has part', type: 'has configuration'}], councilID, db, aqlQuery, configRefined);
            let configID = configMeta._id;

            console.log('All done, iteration: '+count);
            count++;
            asyncLoop(count)
        }
        catch (err) {
            console.log('CAUGHT ERR: \n'+err);
        }
    }
    else {
        console.log('All done processing council of the EU');
    }
}

superagent.get(url)
    .end((err, res) => {
        if (err) console.error(err);

        else {
            console.log('retrieval succeeded in EUCom');
            const rawData = JSON.parse(res.text);
            data = rawData.results.bindings;
            console.log('data length: '+data.length );
            //iterate over results in async style
            asyncLoop(count)
        }
    });

// console.log(url);

// asyncLoop(count);