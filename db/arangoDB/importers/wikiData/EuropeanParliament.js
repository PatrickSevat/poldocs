/**
 * Created by Patrick on 11/08/2016.
 */

import { db, aqlQuery } from '../../arangodb';
import  wdk  from 'wikidata-sdk';
import superagent from 'superagent';
import noCache from 'superagent-no-cache';
import { getKey } from '../../utils/getKeyFromWikiDataURL';
import { compareObj } from '../../utils/compareObj';
import { decode } from '../../utils/decodeWindows1252';
import { checkCreateNodeAndEdges, checkCreateNodeAndEdgesByKey, checkExistsEdgeOrCreateOrUpdate, checkExistsAndCreateEdgesTwoWay,
    checkCreateNodeAndEdgesMultipleColl, checkCreateNodeAndEdgesFromArrayByKey } from '../../utils/createNodeAndEdges';
import { checkExistsByName, checkEdgeExists, checkExistsByKey, checkExistsMultipleColl } from '../../utils/checkExists';
import { checkExistsAndCreateNodeByKey, checkExistsAndCreateNodeByName} from '../../utils/checkExistsAndCreate';

const partOf = db.edgeCollection('partOf');
const hasPart = db.edgeCollection('hasPart');

//Executive body controlled by legislative body and other way around
//Additional edges: partOf hasPart country
//Additional data: jurisdiction, type
const legislativeBodiesColl = db.collection('legislativeBodies');
const controlsEdgeColl = db.edgeCollection('controls');
const controlledByEdgeColl = db.edgeCollection('controlledBy');

const politicalParties = db.collection('politicalParties');

const EUID = 'IGO/Q458';
const EUcountryID = 'countries/Q458';
const EUCoJID = 'judiciaries/Q4951';
const EUComID = 'executiveBodies/Q8880';

const url = wdk.sparqlQuery(`
SELECT DISTINCT ?legislativeBody ?legislativeBodyLabel ?EPParty ?EPPartyLabel ?nationalPartiesKeys ?nationalParties 
?nationalPartiesCountryKeys ?nationalPartiesCountries WHERE {
  ?eu wdt:P209 wd:Q4951 .
  ?eu wdt:P194 ?legislativeBody .
  ?legislativeBody wdt:P31 wd:Q6054776 .

  #European Parliament Parties
  OPTIONAL  {
    SELECT ?EPParty (GROUP_CONCAT(?nationalPartyLabel; SEPARATOR=";") As ?nationalParties) (GROUP_CONCAT(?nationalParty; SEPARATOR=";") As ?nationalPartiesKeys) 
           (GROUP_CONCAT(?nationalPartyCountry; SEPARATOR=";") As ?nationalPartiesCountryKeys) (GROUP_CONCAT(?nationalPartyCountryLabel; SEPARATOR=";") As ?nationalPartiesCountries) WHERE {
	         ?eu wdt:P194 ?legislativeBody .
      		 ?legislativeBody wdt:P31 wd:Q6054776 .
             ?EPParty wdt:P31/wdt:P279* wd:Q25796237  .      		 
      		 { ?nationalParty wdt:P463 ?EPParty } UNION { ?nationalParty wdt:P361 ?EPParty } .
      		 ?nationalParty wdt:P31/wdt:P279* wd:Q7278 .
      		 ?nationalParty wdt:P17 ?nationalPartyCountry .
             SERVICE wikibase:label { 
                bd:serviceParam wikibase:language "en" .
             	?nationalParty rdfs:label ?nationalPartyLabel .
               ?nationalPartyCountry rdfs:label ?nationalPartyCountryLabel
             }
     }
     GROUP BY ?EPParty
  }
  
  SERVICE wikibase:label { bd:serviceParam wikibase:language "en" . }
} ORDER BY ?itemLabel`);

let count = 0;
let data = [];

async function getNationalPartiesArray (nameArr, urlArr, countryUrlArr, countryNameArr) {
    let returnArr = [];
    for (let i = 0; i < nameArr.length; i++) {
        let decodedName = await decode(nameArr[i]);
        returnArr.push({
            name: decodedName,
            _key: getKey(urlArr[i]),
            countryKey: getKey(countryUrlArr[i]),
            country: getKey(countryNameArr[i]),
            type: 'political party',
            level: 'national'
        })
    }
    return returnArr;
}

async function getEPPartyData (party) {
    let decodedName = await decode(party.EPPartyLabel.value);
    console.log('decodedName: '+decodedName);
    return {
        _key: getKey(party.EPParty.value),
        name: party.EPPartyLabel.value,
        wikiData: party.EPParty.value,
        type: 'political party of the European Parliament',
        level: 'international'
    };
};

async function getEPData (EP) {
    let decodedName = await decode(EP.legislativeBodyLabel.value);
    return {
        _key: getKey(EP.legislativeBody.value),
        name: EP.legislativeBodyLabel.value,
        wikiData: EP.legislativeBody.value,
        type: 'European legislative body',
        subType: 'bicameral',
        level: 'international',
        jurisdiction: 'European Union'
    };
};

async function asyncLoop(iteration) {
    console.log('Iteration: '+iteration);
    if (iteration < data.length) {
        const EPRaw = data[iteration];
        const EPRefined = await getEPData(EPRaw);
        const EPPartyRefined = await getEPPartyData(EPRaw);
        

        console.log('Processing: '+EPPartyRefined.name);
        let EPMeta;
        let EPID;

        let nationalPartyArr;

        let nationalPartiesArr = getNationalPartiesArray(EPRaw.nationalParties.value.split(';'), EPRaw.nationalPartiesKeys.value.split(';'),
        EPRaw.nationalPartiesCountryKeys.value.split(';'), EPRaw.nationalPartiesCountries.value.split(';'));

        try {
            // console.log('trying');
            //Create or update EP & create has part \ part of EU
            EPMeta = await checkCreateNodeAndEdgesByKey(EPRefined._key, EPRefined.name, legislativeBodiesColl, [partOf, {name: 'part of', type: 'legislative body of'}],
                [hasPart, {name: 'has part', type: 'has legislative body'}], EUID, db, aqlQuery, EPRefined);

            EPID = EPMeta._id;

            //Create edges for EP: part of\has part EU
            await checkExistsAndCreateEdgesTwoWay(EPID, EUComID, [controlsEdgeColl, {name: 'controls', level: 'international'}],
                [controlledByEdgeColl, {name: 'controlled by', level: 'international'}], db, aqlQuery);

            //Create EP Party & edges connecting it to EP
            await checkCreateNodeAndEdgesByKey(EPPartyRefined._key, EPPartyRefined.name, politicalParties, [partOf, {name: 'part of', type: 'political party of'}],
                [hasPart, {name: 'has part', type: 'has political party'}], EPID, db, aqlQuery, EPPartyRefined);

            console.log('All done, iteration: '+count);
            count++;
            asyncLoop(count)

        }
        catch (err) {
            console.log('CAUGHT ERR: \n'+err);
        }
    }
    else console.log('All done processing EP');
}

superagent.get(url)
    .use(noCache)
    .end((err, res) => {
        if (err) console.error(err);

        else {
            console.log('retrieval succeeded in EUCom');
            const rawData = JSON.parse(res.text);
            data = rawData.results.bindings;
            console.log('data length: '+data.length );
            //iterate over results in async style
            asyncLoop(count)
        }
    });

// console.log(url);

// asyncLoop(count);

// let test = {
//     "EPParty": {
//         "type": "uri",
//         "value": "http://www.wikidata.org/entity/Q753711"
//     },
//     "EPPartyLabel": {
//         "xml:lang": "en",
//         "type": "literal",
//         "value": "European United Leftâ€“Nordic Green Left"
//     }
// };
// //
// let res = getEPPartyData(test);
// console.log(JSON.stringify(res));