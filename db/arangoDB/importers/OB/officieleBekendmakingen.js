/**
 * Created by Patrick on 11/04/2016.
 */
import { db, aqlQuery } from '../../arangodb';
import { dateFormatFunc, onlyAllowOneDateValue } from '../../utils/dateFormat'
import { getXMLData, parseXML } from '../../utils/OBUtils'
import { getDocumentData } from './OBmetadata'
import { createDocAndRelationships } from './OBrelationships'
import { extractFromPDFSession, extractFromXMLSession, removePDFfromLocalFileSystem } from './OBFullTextRetrieval'
import {logObjectWithoutFullText} from '../../utils/documentUtils'
import {checkExistsByAttr} from '../../../../db/arangoDB/utils/checkExists'
import addOBDocumentES from '../../../../db/elasticSearch/addDocumentElastic';
import lodash from 'lodash';

const lastOBImportCollection = db.collection('lastOBImport');
const documentsCollection = db.collection('documents');

let changelogArr = [];
let failedRetrievals = []; 
let session;
// let dateFormat = dateFormatFunc('/');
let dateFormat = '2017/02/02';
//TODO import 13 sept and on

import sftpClient from 'sftp-promises';
const client = new sftpClient();

/**
 * WORKFLOW
 * 1. Retrieve & parse changelog XML
 * 2. Search XML entries for metadata-owms & relevant doc types, starting from last processed doc from db
     * 3. retrieve metadata for relevant doc
     * 4. from metadata._id -ex. 'kst-781334'- retrieve type dir and document dir => {dateFormat}/kst/kst-781334
     * 5. check if {id}.xml is available in document dir
     * 6. retrieve fullText
     * 7. create document relationships TODO incl cited using fulltextsearch when PDF
     * 8. repeat from step 3 until all docs are checked
 * 9. save last processed doc
 * 10. close connection
     * */

//Download changelog.xml in changelog
async function getChangelog(dateFormat) {
    try {
        //Creates reusable session with sFTP server for whole file
        await createSession();
        console.log('session created');
        let changeLogXML = await getXMLData(dateFormat+'/changelog.xml', session);
        console.log('changelog XML retrieved');

        //changelogArr consists out of all paths of relevant documents
        changelogArr = await parseXML(changeLogXML, 'changelog');
        console.log('changelog.length pre slice: '+changelogArr.length);
        
        //removes items from changelogArr which were already processed this day. Needed for hourly document retrieval
        await removeAlreadyProcessedDocs();

        //asyncLoop is where the magic happens, each (new) doc is retrieved, processed and saved to db
        await asyncLoop(session);

        //Once all documents for this changelog are processed, close the session
        session.end();
        console.log('\n closed session');

        //Promise needed for batch, multiple day, import, unused for a single day import
        return new Promise ((resolve, reject) => {
            resolve()
        })
    }
    catch (err) {
        console.log('caught err in getChangelog');
        console.error(err);

        //Close session on error to avoid keeping too many sessions open
        session.end();

        //Resolve here, because the batch import can continue. Error can occur on saturday \ sunday: no changelog exists
        return new Promise ((resolve, reject) => {
            resolve()
        })
    }
}

async function asyncLoop(session) {
    try {
        //changelogArr consists out of all paths of potentially relevant metadata documents
        //newest entries are on top, so we start from the last and make our way to newest.
        
        for (let i = changelogArr.length-1; i >= 0; i--) {
        // for (let i = 390; i<405; i++) {
            let item = changelogArr[i];
            let path = item.paths.path;

            // Filter out items from changeLogArr which are:
            // 1. not a metadata_owms.xml document
            // 2. a metadata_owms.xml document, but of a wrong kind (gmb, stcrt, bgr, etc.)
            if (path.search('metadata_owms.xml') === -1 || path.search('ag') > -1 || path.search('stcrt') > -1 ||
                path.search('trb') > -1 || path.search('gmb') > -1 || path.search('prb') > -1 || path.search('wsb') > -1
                || path.search('bgr') !== -1 || path.search('cvdr') !== -1) {
                console.log('skipped '+i+', : '+path+' action: '+item.action);
                continue;
            }


            //What remains are metadata_owms.xml documents of these types:
            // kv = kamervragen zndr antwoord, kst = kamerstukken, ah = aanhangsels aka kamervragen met antwoord,
            // nds = niet dossierstukken, h = handelingen, blg = bijlagen, stb = staatsblad
            console.log('\npath '+i+': '+path+ ' action: '+item.action);

            //When a document status changes from 'Onopgemaakt' to 'opgemaakt'
            // we can remove the saved pdf from our local filesystem as it will be replaced by an HTML version
            if (item.action === 'verwijderd') {
                await checkIfPDFcanBeRemoved(path);
                continue;
            }
            
            // Get the metadata document in XML format, if this fails, log it to an temp array move on to next iteration
            let documentXML;
            await getXMLData(path, session).then((xml) => {
                documentXML = xml;
                console.log('retrieved metadata XML for: '+path);
            }, (err) => {
                documentXML = false;
                failedRetrievals.push(path);
            });
            
            if (!documentXML) continue; 
            
            // Parse the XML string to JSON
            let documentParsed = await parseXML(documentXML, 'document');
            
            // Process the metadata JSON into something we will save in our db
            let documentMeta = getDocumentData(documentParsed, path);
            console.log('document obj retrieved for: '+path);
            
            // Get fullText from .pdf or .xml
            // xml preferred due to post processing which allows for more metadata retrieval
            if (documentMeta.documentStatus == 'Onopgemaakt') {
                console.log('\nONOPGEMAAKT\n');
                let returnObj = await extractFromPDFSession(path, documentMeta.oriKey, session, true);
                documentMeta.fullText = typeof returnObj.fullText != 'undefined' ? returnObj.fullText : '';
                documentMeta.documentLinks.pdf = typeof returnObj.pdfPath != 'undefined' ? returnObj.pdfPath : '';
                console.log('\nFulltext PDF: '+typeof documentMeta.fullText === 'string' ? console.log(documentMeta.fullText.slice(0,50)) : console.log('undefined'));
            }
            else {
                let fullTextAndAdditionalMetadata = await extractFromXMLSession(path, documentMeta._key, session);
            
                //Fallback method to PDF
                if (fullTextAndAdditionalMetadata == null ) {
                    let returnObj = await extractFromPDFSession(path, documentMeta.oriKey, session, true);
                    documentMeta.fullText = typeof returnObj.fullText != 'undefined' ? returnObj.fullText : '';
                    documentMeta.documentLinks.pdf = typeof returnObj.pdfPath != 'undefined' ? returnObj.pdfPath : '';
                    console.log('\nFulltext XML failed, fallback to PDF\nFulltext PDF: '+typeof documentMeta.fullText === 'string' ? console.log(documentMeta.fullText.slice(0,50)) : console.log('undefined'));
                }
                //Normal XML retrieval    
                else {
                    documentMeta.fullText = fullTextAndAdditionalMetadata.fullText;
                    //TODO merge metadataFromXML with regular fields
                    documentMeta.html = fullTextAndAdditionalMetadata.html;
                    documentMeta.metadataFromXML = fullTextAndAdditionalMetadata.meta;
                    documentMeta = addMetadataFromXML(documentMeta, fullTextAndAdditionalMetadata.meta);
                    console.log('\nFulltext XML: '+typeof documentMeta.fullText === 'string' ? console.log(documentMeta.fullText.slice(0,50)) : console.log('undefined'));
                }
            }
            
            // logObjectWithoutFullText(documentMeta);
            
            //Save or update the document and after that create relationships between the doc and other nodes (such as authors or cited docs)
            await createDocAndRelationships(documentMeta, db, aqlQuery);
            await addOBDocumentES(documentMeta);
            console.log('created relationships for: '+path);
        }
        //After processing the last item in changelogArr save last processed doc path.
        //If we run this script again today we can skip the documents already processed
        await saveLastProcessedDoc();
        console.log('All done processing officiële bekendmakingen');

        //resolve so we can close session
        return new Promise((resolve, reject) => {
            resolve();
        })
    }
    catch (err) {
        console.log('ERR in asyncLoop OB: \n'+err);
        return new Promise((resolve, reject) => {
            reject(err);
        })
    }
}

async function createSession() {
    try {
        var config = {
            hostname: 'bestanden.officielebekendmakingen.nl',
            port: 22,
            username: 'anonymous',
            password: 'patrick@cr-8.nl'
        };

        await client.session(config).then(function (ftpSession) {
            session = ftpSession;
        });
        console.log('session opened');    
    }
    catch (err) {
        console.log("ERROR creating session: \n"+err)
    }
}

async function removeAlreadyProcessedDocs() {
    try {
        //Arangodb can't handles keys\IDs with slashes in name
        let dateFormatDashed = dateFormat.replace(/\//g, '-');
        console.log('dateFormatDashed: '+dateFormatDashed);

        // As this function will run hourly and the changelog is updated throughout the day,
        // let's check the latest object we have processed and slice the array if needed
        // Note: save\retrieve the unprocessed path here!
        //Search for a doc with key dateFormat eg '01/09/2016' or create it if not available
        //Look up by keys returns empty array, look up by ID throws an error
        let lastPathArr = await lastOBImportCollection.lookupByKeys([dateFormatDashed]);
        let lastPath;
        if (lastPathArr.length === 0) lastOBImportCollection.save({_key: dateFormatDashed, last: null});
        else lastPath = lastPathArr[0].last;

        if (lastPath) {
            console.log('lastPath: '+ lastPath);
            let index;
            for (let i = 0; i<changelogArr.length; i++) {
                if (changelogArr[i].paths.path == lastPath) {
                    index = i;
                    break;
                }
            }

            console.log('index for slice: '+index);
            changelogArr = changelogArr.slice(0, index);
            console.log('array.length after slice: '+changelogArr.length)
        }
    }
    catch (err) {
        console.log('ERROR in remove already processed docs IN officieleBekendmakingen.js: \n'+err)
    }
}

async function saveLastProcessedDoc() {
    try {
        let dateFormatDashed = dateFormat.replace(/\//g, '-');
        await lastOBImportCollection.replace(dateFormatDashed,
            {
                _key: dateFormatDashed,
                last: changelogArr[0].paths.path,
                countFailed: failedRetrievals.length,
                failed: failedRetrievals
            });
    }
    catch (err) {
        console.log('ERROR in save last processed doc IN officieleBekendmakingen.js')
    }
}

async function loopOverDays() {
    try {
        await getChangelog(dateFormat);

        dateFormat = incrementDate(dateFormat);

        if (dateFormat == '2016/09/09') {
            console.log('all done looping over days! \n' +
                'HOORAY');
        }
        else {
            loopOverDays();
        }
    }
    catch (err) {
        console.log('caught err in loop over days');
        console.log(err);
    }
}

function incrementDate(dateStr) {
    //Really rough date incrementer: if day >31, day =1 & month +=1, if month >12 month = 1 & year +=1
    let dateArr = dateStr.split('/');
    let year = Number(dateArr[0]);
    let month = Number(dateArr[1]);
    let day = Number(dateArr[2]);

    day += 1;

    if (day > 31) {
        day = 1;
        month +=1;
    }
    if (month > 12) {
        month = 1;
        year += 1;
    }
    if (day < 10) day = '0'+day;
    if (month <10) month = '0'+month;

    return year.toString()+'/'+month+'/'+day;
}

async function checkIfPDFcanBeRemoved(path) {
    try {
        console.log('checking remove');
        let oriKey = path.split('\\');
        oriKey = oriKey[oriKey.length-2];
        let doesItExist = await checkExistsByAttr(oriKey, 'oriKey', documentsCollection, db, aqlQuery);
        if (Array.isArray(doesItExist)) {
            await removePDFfromLocalFileSystem();
            console.log('pdf removed');
            Promise.resolve()
        }
        else
            Promise.resolve()
    }
    catch (err) {
        Promise.reject(err)
    }
}

function addMetadataFromXML(documentMeta, metaFromXML) {
    let returnDocMeta = documentMeta;
    let documentReferences = [];
    let externalReferences = [];
    let organisationNames = [];

    //Add references
    metaFromXML.references.map((ref, i) => {
        if (ref.soort == 'document')
            documentReferences.push(ref.doc);
        else if (ref.soort == 'URL')
            externalReferences.push(ref.doc);
    });

    //Add names & organisations to authors
    //TODO check if this is correct or we need another field with mentioned people \ organisations
    metaFromXML.organisations.map((org, i) => {
        organisationNames.push(org._);
        organisationNames.push(org.$.afkorting);
    });
    // returnDocMeta.authorsBeforeConcat = returnDocMeta.authors;
    // returnDocMeta.organisationNamesFromMetaXML = organisationNames;
    // returnDocMeta.authorsConcat = metaFromXML.names.concat(organisationNames);
    returnDocMeta.authors = returnDocMeta.authors.concat(metaFromXML.names);
    returnDocMeta.authors = returnDocMeta.authors.concat(organisationNames);
    returnDocMeta.authors = lodash.uniq(returnDocMeta.authors);

    returnDocMeta.documentRelationships.references = returnDocMeta.documentRelationships.references.concat(documentReferences);
    returnDocMeta.documentRelationships.references = lodash.uniq(returnDocMeta.documentRelationships.references );

    returnDocMeta.documentRelationships.legalBasis = returnDocMeta.documentRelationships.legalBasis.concat(metaFromXML.legalBasis);
    returnDocMeta.documentRelationships.legalBasis = lodash.uniq(returnDocMeta.documentRelationships.legalBasis);

    returnDocMeta.documentRelationships.externalReferences = externalReferences;

    //TODO add metaFromXML.politicalParties

    return returnDocMeta;
}


// loopOverDays() can be used for batch import
// loopOverDays();

// getChangelog is used for a normal, daily, import
getChangelog(dateFormat);






