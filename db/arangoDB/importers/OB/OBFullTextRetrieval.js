import underscoreStr from 'underscore.string'
import textract from 'textract';
import sftpClient from 'sftp-promises';
import {parseXML} from '../../utils/OBUtils'
import lodash from 'lodash';
import fs from 'fs';
import AdmZip from 'adm-zip';
import createDOMPurify from 'dompurify';
import jsdom from 'jsdom';

const window = jsdom.jsdom('', {
    features: {
        FetchExternalResources: false, // disables resource loading over HTTP / filesystem
        ProcessExternalResources: false // do not execute JS within script blocks
    }
}).defaultView;
const DOMPurify = createDOMPurify(window);


const client = new sftpClient();

async function extractFromXMLSession(path, id, session) {
    try {
        let xmlPath = path.substr(0,path.lastIndexOf('\\'))+'\\'+id+'.xml';
        let buffer = await client.getBuffer(xmlPath, session);
        let str = buffer.toString('utf8');
        let meta = await parseAndGetMeta(str);
        let html = await getHTML(path, id, session);
        return new Promise((resolve, reject) => {
            resolve({
                html,
                fullText: underscoreStr.stripTags(str),
                meta
            })
        })
    }
    catch (err) {
        console.log('Error in extract from XML: \n'+err);
        return new Promise ((resolve, reject) => {
            resolve(null);
        })
    }
}

async function getHTML(path, id, session) {
    try {
        let zipPath = path.substr(0,path.lastIndexOf('\\'))+'\\'+id+'.html.zip';
        let buffer = await client.getBuffer(zipPath, session);
        const zip = new AdmZip(buffer);
        const zipEntries = zip.getEntries();
        let entryIndex;
        zipEntries.forEach((item, itemIndex) => {
            if (item.entryName === id+'.html') {
                entryIndex = itemIndex;
            }
        });
        let html = zip.readAsText(zipEntries[entryIndex]);
        let sanitized = DOMPurify.sanitize(html, {SAFE_FOR_TEMPLATES: true, FORBID_TAGS: ['link']});
        sanitized = sanitized.replace(/(\s+(?=<))/g, '');
        return new Promise((resolve, reject) => {
            resolve(sanitized);
        })
    }
    catch (err) {
        return new Promise((resolve, reject) => {
            reject(html);
        })
    }
}

async function savePDFtoLocalFileSystem(oriKey, data) {
    return new Promise((resolve, reject) => {
        console.log('writing to path: '+'./public/pdf/'+oriKey+'.pdf');
        fs.writeFile('../../../public/pdf/'+oriKey+'.pdf', data, function (err) {
            if (err) {
                console.log('Err, could not open file for writing: \n'+err);
                reject(err);
            }

            else {
                console.log('saved PDF to local FS');
                resolve('/static/pdf/'+oriKey+'.pdf')
            }

        })
    })
}

async function removePDFfromLocalFileSystem(oriKey) {
    try {
        fs.unlink('../../../public/pdf/'+oriKey+'.pdf', function (err) {
            if (err) {
                console.log('ERROR in removing pdf from local file system: \n'+err);
                return new Promise((resolve, reject) => reject(err));
            }
            else
                return new Promise((resolve, reject) => resolve());
        })    
    }
    catch (err) {
        
    }
}

async function extractFromPDFSession(path, id, session, savePDF) {
    try {
        let pdfPath = path.substr(0,path.lastIndexOf('\\'))+'\\'+id+'.pdf';
        let buff = await client.getBuffer(pdfPath, session);
        console.log('buff retrieved');
        let returnObj = {};
        if (savePDF) {
            console.log('\nSAVING PDF\n'+pdfPath);
            returnObj.pdfPath = await savePDFtoLocalFileSystem(id, buff);
        }

        
        return new Promise((resolve, reject) => {
            textract.fromBufferWithMime('application/pdf', buff, {preserveLineBreaks: false, tesseract: {lang: 'nld'}}, (err, text) => {
                if (err) {
                    console.log(err);
                    resolve(returnObj);
                }
                else {
                    console.log('extractFromPDF: '+text.substr(0,50));
                    returnObj.fullText = text;
                    resolve(returnObj);    
                }
                // console.log(text);
                
            })    
        })
    }
    catch (err) {
        console.log('Error in extractfromPDF: \n'+err);
        return new Promise((resolve, reject) => {
            reject(err);
        })
    }
}

function findKey(obj,key){
    let arr = [];

    function iterate(obj, key) {
        for (let i in obj) {
            if (i == key) {
                if (key === 'extref' && !Array.isArray((obj[i])))
                    arr.push(obj[i]['$']);
                else if (key === 'extref' && Array.isArray((obj[i])))
                    obj[i].map((item) => {
                        arr.push(item['$'])
                    });
                else
                    arr.push(obj[i])
            }
            else if (typeof obj[i] === 'object')
                iterate(obj[i],key)
        }
    }
    iterate(obj,key);
    return lodash.uniqWith(arr, lodash.isEqual);
}

async function parseAndGetMeta(str) {
    try {
        let parsed = await parseXML(str, 'documentFullText');
        let references = findKey(parsed, 'extref');
        let names = findKey(parsed, 'achternaam');
        let organisations = findKey(parsed, 'organisatie');
        let politicalParties = findKey(parsed, 'politiek');
        let legalBasis = findKey(parsed, 'grondslag');

        return new Promise((resolve, reject) => {
            resolve({
                references,
                names,
                organisations,
                politicalParties,
                legalBasis
            })
        })
    }
    catch (err) {
        console.log('Err in OB Full Text Retrieval, parseAndGetMetaData: \n'+err);
        return new Promise((resolve, reject) => {
            reject(err);
        })
    }
}

export { extractFromPDFSession, extractFromXMLSession, removePDFfromLocalFileSystem }
