'use strict';
require('babel-polyfill');
require('babel-register')({
    presets: ['es2015', 'react'],
    plugins: ["syntax-async-functions", "transform-regenerator"]
});

require('./unzipTest');
