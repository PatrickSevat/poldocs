import {searchForDocuments} from '../../../utils/documentUtils'
import { db, aqlQuery } from '../../../arangodb';

searchForDocuments('COM', db, aqlQuery, null, {caseSensitive: false, searchType: 'equality'});
