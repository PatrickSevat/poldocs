import {parseXML, getXMLDataLocal} from '../../../utils/OBUtils'
import util from 'util';
import underscoreStr from 'underscore.string'

async function localFullTextTest() {
    let XMLstring = await getXMLDataLocal('../../../sources/kst-29323-104.xml');
    let parsed = await parseXML(XMLstring, 'documentFullText');
    // console.log(util.inspect(parsed, {showHidden: false, depth: null}));
    let references = findKey(parsed, 'extref');
    let names = findKey(parsed, 'naam');
    console.log(util.inspect(references, {showHidden: false, depth: null}));
    console.log(util.inspect(names, {showHidden: false, depth: null}));
    let organisations = findKey(parsed, 'organisatie');
    let politicalParties = findKey(parsed, 'politiek');
    let legalBasis = findKey(parsed, 'grondslag');
    return {
        fullText: underscoreStr.stripTags(XMLstring),
        meta: {
            references,
            names,
            organisations,
            politicalParties,
            legalBasis
        }
    }
}

function findKey(obj,key){
    let arr = [];

    function iterate(obj, key) {
        for (let i in obj) {
            if (i == key) {
                if (key === 'extref' && !Array.isArray((obj[i])))
                    arr.push(obj[i]['$']);
                else if (key === 'extref' && Array.isArray((obj[i])))
                    obj[i].map((item) => {
                        arr.push(item['$'])
                    });
                else
                    arr.push(obj[i]);
            }
            else if (typeof obj[i] === 'object')
                iterate(obj[i],key)
        }
    }
    iterate(obj,key);
    return arr;
}

localFullTextTest();

