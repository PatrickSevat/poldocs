import sftpClient from 'sftp-promises';
let session;
const client = new sftpClient();
const AdmZip = require('adm-zip');

async function createSession() {
    var config = {
        hostname: 'bestanden.officielebekendmakingen.nl',
        port: 22,
        username: 'anonymous',
        password: 'patrick@cr-8.nl'
    };

    await client.session(config).then(function (ftpSession) {
        session = ftpSession;
    });
    console.log('session opened');
}

async function saveHTMLSession(path, id, session) {
    try {
        let zipPath = path.substr(0,path.lastIndexOf('\\'))+'\\'+id+'.html.zip';
        let buffer = await client.getBuffer(zipPath, session);
        console.log('zip in buffer');
        const zip = new AdmZip(buffer);
        const zipEntries = zip.getEntries();
        zipEntries.forEach((item, itemIndex) => {
            if (item.entryName === id+'.html') {
                let html = zip.readAsText(zipEntries[itemIndex]);
                console.log(html.slice(0,50));
                Promise.resolve(html);
            }
        })
    }
    catch (err) {
        Promise.reject(err)
    }
}

async function unzipTest() {
    try {
        console.log('starting unzip test');
        await createSession();
        let buffer = await saveHTMLSession('\\2016\\11\\07\\kst\\kst-22054-279\\metadata.xml', 'kst-22054-279', session);
        console.log('buffer unzipped and read');
        session.end();
        Promise.resolve();
    }
    catch (err) {
        console.log(err);
    }
}

unzipTest();

