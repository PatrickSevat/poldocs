import { checkCreateNodeAndEdges, checkCreateNodeAndEdgesByKey, checkExistsEdgeOrCreateOrUpdate, checkExistsAndCreateEdgesTwoWay,
    checkCreateNodeAndEdgesMultipleColl, checkCreateNodeAndEdgesFromArrayByKey, checkExistsAndCreateEdgesTwoWayFromArray } from '../../utils/createNodeAndEdges';
import { checkExistsAndCreateNodeByKey, checkExistsAndCreateNodeByName} from '../../utils/checkExistsAndCreate';
import {searchForDocuments, fromArrToID, fromIDToArr, fromArrToArr, inverseDocumentRelationship} from '../../utils/documentUtils'
import {searchForDossiers} from '../../utils/OBUtils'
import underscoreStr from 'underscore.string';
import {nowToISOString} from '../../utils/dateFormat'
import util from 'util'


async function createDocAndRelationships(document, db, aqlQuery) {
    const documentsColl = db.collection('documents');
    const dossierColl = db.collection('dossiers');
    const peopleColl = db.collection('people');
    const beleidsagendaColl = db.collection('taxonomieBeleidsagenda');

    const partOfEdge = db.edgeCollection('partOf');
    const hasPartEdge = db.edgeCollection('hasPart');
    const authorOfEdge = db.edgeCollection('authorOf');
    const authoredByEdge = db.edgeCollection('authoredBy');
    const documentRelationshipEdge = db.edgeCollection('documentRelationships');
    const dossierRelationshipsEdge = db.edgeCollection('dossierRelationships');
    const askedByEdge = db.edgeCollection('askedBy');
    const askedToEdge = db.edgeCollection('askedTo');

    try {
        //Save doc
        let savedDoc = await checkExistsAndCreateNodeByKey(document._key, documentsColl, db, aqlQuery, document);
        console.log('\nsaved doc');
        let savedDocID = savedDoc._id;

        //Save author edges
        if (document.authors !== '') {
            let authordIDs = await searchForDocuments(document.authors, db, aqlQuery,
                ['people', 'legislativeBodies', 'executiveBodies', 'otherBodies', 'publicOffices', 'IGO'], {caseSensitive: false, searchType: 'equality'} );
            let fromToAuthorArr = fromArrToID(authordIDs, savedDocID);
            await checkExistsAndCreateEdgesTwoWayFromArray(fromToAuthorArr, [authorOfEdge, {type: 'author of'}],
                [authoredByEdge, {type: 'authored by'}], db, aqlQuery);
            console.log('saved author edges');
        }

        //Create edge between question and answer
        if (document._key.search('ah-') > -1) {
            await checkExistsAndCreateEdgesTwoWay(savedDocID, 'documents/kv-tk-'+document.questionID,
                [documentRelationshipEdge, {type: 'answers'}], [documentRelationshipEdge, {type: 'answeredBy'}], db, aqlQuery);
            console.log('\ncreated question and answer edges');

            //Add answerer as author, only when question is answered (type = ah\aanhangsel)
            if (document.askedTo !== '') {
                let askedToArr = [];
                if (Array.isArray(document.askedTo)) askedToArr = document.askedTo;
                else askedToArr.push(document.askedTo);

                let askedToIDArr = await searchForDocuments(askedToArr, db, aqlQuery, ['people'], {caseSensitive: false, searchType: 'equality'});
                let fromToArr = fromIDToArr(savedDocID, askedToIDArr);
                await checkExistsAndCreateEdgesTwoWayFromArray(fromToArr, [authoredByEdge, {type: 'authored by'}],
                    [authorOfEdge, {type: 'author of'}], db, aqlQuery);
                console.log('created asker as author edges')
            }
        }

        //Create edges between asker and answerer
        if (document.askedTo !== '' && document.askedBy !== '' && (document._key.search('ah') > -1 || document._key.search('kv') > -1)) {
            let askedToArr = [];
            if (Array.isArray(document.askedTo)) askedToArr = document.askedTo;
            else askedToArr.push(document.askedTo);

            let askedByArr = [];
            if (Array.isArray(document.askedBy)) askedByArr = document.askedBy;
            else askedByArr.push(document.askedBy);

            let askedToIDArr = await searchForDocuments(askedToArr, db, aqlQuery, ['people'], {caseSensitive: false, searchType: 'equality'});
            let askedByIDArr = await searchForDocuments(askedByArr, db, aqlQuery, ['people'], {caseSensitive: false, searchType: 'equality'});

            let fromToArr = fromArrToArr(askedByIDArr, askedToIDArr);
            await checkExistsAndCreateEdgesTwoWayFromArray(fromToArr, [askedToEdge, {document: savedDocID}],
                [askedByEdge, {document: savedDocID}], db, aqlQuery);
            console.log('created edges between asker and answerer')
        }

        //Special treatment for bijlages\attachments
        if (document._key.search('blg-') >-1) {
            let docID;
            if (document.dossier !== '' && document.ondernummer !== '') docID = 'documents/kst-'+document.dossier+'-'+document.ondernummer;
            else if (Array.isArray(document.documentRelationships.attachmentOf) && document.documentRelationships.attachmentOf.length > 0)
                docID = 'documents/'+document.documentRelationships.attachmentOf[0];

            if (docID) {
                await checkExistsAndCreateEdgesTwoWay(savedDocID, docID,
                    [documentRelationshipEdge, {type: 'attachment'}], [documentRelationshipEdge, {type: 'attachmentOf'}], db, aqlQuery);
                console.log('\ncreated blg edges');
            }
        }
        
        //Add doc to dossier, create dossier if neccessary
        if (document.dossier !== '' && document._key.search('blg-') === -1) {
            if (Array.isArray(document.dossier)) {
                for (let i =0; i<document.dossier.length; i++) {
                    let dossier = getDossierObj(document, i);
                    let dossierMeta = await checkCreateNodeAndEdgesByKey(document.dossier[i], document.dossierTitle[i], dossierColl, [hasPartEdge, {type: 'dossier has part', ondernummer: document.subDossier[i]}],
                        [partOfEdge, {type: 'part of dossier', ondernummer: document.subDossier[i]}], savedDocID, db, aqlQuery, dossier);

                //    Check if dossier contains a reference to a Eurlex dossier
                    await checkEurlexDossier(dossier, db, dossierRelationshipsEdge, dossierMeta);
                }
            }
            else {
                let dossier = getDossierObj(document, false);
                let dossierMeta = await checkCreateNodeAndEdgesByKey(document.dossier, document.dossierTitle, dossierColl, [hasPartEdge, {type: 'dossier has part', ondernummer: document.subDossier}],
                    [partOfEdge, {type: 'part of dossier', ondernummer: document.subDossier}], savedDocID, db, aqlQuery, dossier)
                
                await checkEurlexDossier(dossier, db, dossierRelationshipsEdge, dossierMeta);
            }
            console.log('\ncreated edges and possibly dossier');
        }
        if (document.KIWI !== '') {
            await checkCreateNodeAndEdgesByKey('kiwi-'+document.KIWI, 'kiwi-'+document.KIWI, dossierColl, [hasPartEdge, {type: 'dossier has part'}],
                [partOfEdge, {type: 'part of dossier'}], savedDocID, db, aqlQuery,
                {_key: document.dossier, name: document.dossierTitle});
            console.log('\ncreated KIWI');
        }

        //Add categories links to taxonomieBeleidsagenda
        if (document.categories.beleidsagenda.length > 0) {
            let idArr = [];
            //categories is always an array
            document.categories.beleidsagenda.forEach((item) => {
                idArr.push('taxonomieBeleidsagenda/'+underscoreStr.cleanDiacritics(item.top.replace(/\s/g, '-')));
                idArr.push('taxonomieBeleidsagenda/'+underscoreStr.cleanDiacritics(item.top.replace(/\s/g, '-'))
                        +'-'+underscoreStr.cleanDiacritics(item.sub.replace(/\s/g, '-')))
            });

            let filteredArr = Array.from(new Set(idArr));
            let fromToArr = fromIDToArr(savedDocID, filteredArr);
            console.log('\nFrom to category arr:');
            console.log(fromToArr);
            await checkExistsAndCreateEdgesTwoWayFromArray(fromToArr, [partOfEdge, {type: 'part of category'}],
                [hasPartEdge, {type: 'category has document'}], db, aqlQuery);
            console.log('created category edges');
        }

        //Add document relationship edges
        for (let relationship in document.documentRelationships) {
            if (document.documentRelationships[relationship].length === 0) {}
            else {
                let IDarr = [];
                if (relationship === 'adviesRvS') 
                    IDarr = [document.documentRelationships[relationship].replace(/\//g, '-')];

                //behandeldDossier is special: consists of dossier number and ondernummers
                //dossier relationships are added here, kamerstukken based on ondernummers are added to IDarr    
                else if (relationship === 'behandeldDossier') {
                    let dossiers = [];
                    let kamerstukken = [];
                    let arr = [];
                    if (Array.isArray(document.documentRelationships[relationship])) arr = document.documentRelationships[relationship];
                    else arr.push(document.documentRelationships[relationship]);
                    arr.forEach((item) => {
                        let split = item.split(';');
                        dossiers.push(split[0]);
                        kamerstukken.push('kst-'+split[0]+'-'+split[1]);
                    });
                    dossiers = Array.from(new Set(dossiers));
                    let fromDocToRelArr = fromIDToArr(savedDocID, dossiers, 'dossiers/');
                    console.log('\nfromdoctorelArr, relationship: '+relationship);
                    console.dir(fromDocToRelArr);
                    await checkExistsAndCreateEdgesTwoWayFromArray(fromDocToRelArr, [partOfEdge, {type: 'part of dossier'}],
                        [hasPartEdge, {type: 'dossier has part'}], db, aqlQuery);
                    console.log('created doc is part of dossier edges');
                    IDarr = kamerstukken;
                }

                else if (relationship === 'implements') {
                    let arr = [];
                    if (Array.isArray(document.documentRelationships[relationship])) arr = document.documentRelationships[relationship];
                    else arr.push(document.documentRelationships[relationship]);
                    IDarr = await searchForDocuments(arr, db, aqlQuery, ['documents'], 'celexKey', {caseSensitive: false, searchType: 'equality'});
                }

                else if (relationship === 'legalBasis') {
                    // TODO   Skipped for now
                }
                else 
                    IDarr = getIDArr(document.documentRelationships[relationship]);

                if (IDarr.length > 0) {
                    let fromDocToRelArr = fromIDToArr(savedDocID, IDarr, 'documents/');
                    console.log('\nFrom doc to rel arr, relatinship: '+relationship);
                    console.dir(fromDocToRelArr);
                    await checkExistsAndCreateEdgesTwoWayFromArray(fromDocToRelArr, [documentRelationshipEdge, {type: relationship}],
                        [documentRelationshipEdge, {type: inverseDocumentRelationship(relationship)}], db, aqlQuery);
                    console.log('created edges for: '+relationship)
                }
            }
        }
        
        //Relationships derived from extra metadata retrieved from full text XML
        //TODO determine if we also want to add names, organisations, parties and legal basis
        if (typeof document.metadataFromXML !== 'undefined') {
            if (document.metadataFromXML.references.length > 0) {
                let IDarr = [];
                document.metadataFromXML.references.map((item) => {
                    if (item.soort == 'document' && item.doc.search('dossier/') === -1)
                        IDarr.push(item.doc)
                });
                let fromToArr = fromIDToArr(savedDocID, IDarr, 'documents/');
                await checkExistsAndCreateEdgesTwoWayFromArray(fromToArr, [documentRelationshipEdge, {type: 'references'}],
                    [documentRelationshipEdge, {type: 'isReferencedBy'}], db, aqlQuery);
                console.log('created metadataXML reference edges')
            }
        }
        
        //TODO check fulltext for references, limited to EU references
        if (document.fullText !== '') {
            
        }

        console.log('\nFinished createDocAndRelationships for: '+document._key);
        Promise.resolve();
    }
    catch (err) {
        console.log('\nCaught err in createDocAndRelationships');
        console.error(err);
        Promise.reject(err);
    }
}

function getDossierObj(document, i) {
    let key = document.dossier;
    let title = document.dossierTitle;
    if (i !== false) {
        key = key[i];
        title = title[i];
    }
    return {
        _key: key,
        title,
        level: 'national',
        source: 'officieleBekendmakingen',
        country: 'NL',
        dateImported: nowToISOString(),
        events: [{
        date: typeof document.documentDate != 'undefined' ? document.documentDate : document.dateImported,
        eventLabel: 'Document added to dossier',
        eventLabelNL: 'Document toegevoegd aan dossier',
        eventOrganisation: document.authors,
        documentID: document._key,
        attachment: document.documentRelationships.attachment, 
        categories: document.categories,
        type: 'National dossier'
        }]
    }
}

async function checkEurlexDossier(dossier, db, dossierRelationshipsEdge, dossierMeta) {
    try {
        if (dossier.title.search(/richtlijn|verordening|besluit/gi) > -1 ) {


            // Search for legislation
            // Captures format 1998/123/EG && 2013/48/EU => (EU) 2013/48 in title
            let legislationMatches = getLegislationMatches(dossier.title);
            // Search for COM preparatory acts
            // Captures format COM (2013) 821 && 2013 COM 0821, COM (2016) 26 => documents.subTitle searchable: COM/2016/026
            let preparatoryActMatches = getPreparatoryActMatches(dossier.title);

            //items in matches have this format: {searchCollection: 'documents', searchAttr: 'title', searchValues:[]}
            let matches = legislationMatches.concat(preparatoryActMatches);
            if (matches.length === 0)
                return new Promise((resolve, reject) => resolve());

            console.log('matches');
            console.log(matches);
            let relatedDossierIDs = await searchForDossiers(matches, db);
            console.log('relatedDossierIDs');
            console.log(relatedDossierIDs);

            if (typeof relatedDossierIDs == 'undefined' || relatedDossierIDs.length == 0 )
                return new Promise((resolve, reject) => resolve());

            let fromToArr = fromArrToID(relatedDossierIDs, dossierMeta._id);
            await checkExistsAndCreateEdgesTwoWayFromArray(fromToArr, [dossierRelationshipsEdge, {type: 'inter level dossier', subType: 'Eurlex dossier to OB dossier'}],
                [dossierRelationshipsEdge, {type: 'inter level dossier', subType: 'OB dossier from Eurlex dossier'}]);
            console.log('Eurlex to national dossiers edges created');

            //TODO remove Error once it works properly
            throw new Error('created dossier relationship: check if it went correctly based on matches and relatedDossiers!');
            return new Promise((resolve, reject) => resolve())

        //    TODO add metadata to document with connected Eurlex dossier
        }    
    }
    catch (err) {
        return new Promise((resolve, reject) => reject(err))
    }
    
}

function getIDArr(relationship) {
    let returnArr = [];
    if (Array.isArray(relationship)) {
        relationship.forEach((rel) => {
            if (rel.search('/') == -1) {
                let split = rel.split(';');
                split.forEach((item) => {
                    item.search('-') > -1 ? returnArr.push(item) : null;
                })
            }
            else {
                console.log('filtered link from relationship')
            }
        })
    }
    else {
        let split = relationship.split(';');
        split.forEach((item) => {
            item.search('-') > -1 ? returnArr.push(item) : null;
        })
    }
    return returnArr;
}

function getLegislationMatches(searchStr) {
    let matches = [];
    // Search for legislation

    //Capture regulations \ verordening in this format: Verordening (EU) nr. 596/2014 => 2014/596/EU
    // format 2014/596/EU will be found in doc.subTitle of imported docs after 29 sept, 
    // 30sept adjustment was made to make subTitle 'C/2016/4796' => 'C/2016/4796 2016/4796/EU' so searchable for this cause
    let regExp0 = (/\(\w{2,8}\)\snr\.\s\d{1,4}\/\d{2,4}/gi);
    let regExp0Matches = searchStr.match(regExp0);
    

    // Captures format YYYY/1(234)/EU && YYYY/1(234)/EG : 2013/48/EU => (EU) 2013/48 in title
    let regExp1 = /(\d{2,4}\/\d{2,4}\/\w{2,8})/gi;
    let regExp1Matches = searchStr.match(regExp1);
    if (!regExp1Matches && !regExp0Matches)
        return matches;

    if (regExp0Matches) {
        regExp0Matches.map((match) => {
            let matchReplached = match.replace(/(\()|\s|(nr\.)/gi, '');
            let split = matchReplached.split(/\)|\//gi);
            let joined = split[2]+'/'+split[1]+'/'+split[0];

            let titleReplaced = match.replace(/nr\.\s/i, '');
            let splitSpace = titleReplaced.split(' ');
            let IDAndYearSplit = splitSpace[1].split('/');
            let joinedTitle = splitSpace[0]+' '+IDAndYearSplit[1]+'/'+IDAndYearSplit[0];
            matches.push({searchCollection: 'documents', searchAttr: 'subTitle', searchValues: [joined]});
            matches.push({searchCollection: 'documents', searchAttr: 'title', searchValues: [joinedTitle]});
        });
    }

    if (regExp1Matches) {
        regExp1Matches.map((match) => {
            let matchSplit = match.split('/');
            matches.push({searchCollection: 'documents', searchAttr: 'title', searchValues: ['\('+matchSplit[2]+'\) '+matchSplit[0]+'/'+matchSplit[1]]});
            matches.push({searchCollection: 'documents', searchAttr: 'subTitle', searchValues: [match]})
        });
    }
    
    console.log('legislation matches:');
    console.dir(matches);
    return matches
}

function getPreparatoryActMatches(searchStr) {
    let matches = [];
    // Search for COM preparatory acts
    // Captures format COM (2013) 821 && COM(2013)821  && 2013 COM 0821, COM (2016) 26, JOIN(2015)32
    // => documents.subTitle searchable: COM/2016/026
    let regExp2 = /([A-Z]{2,8}\s?\(\d{2,4}\)\s?\d{1,4})|(\d{2,4}\s?[A-Z]{2,8}\s?\d{1,4})/g;
    let regExp2Matches = searchStr.match(regExp2);
    if (!regExp2Matches)
        return matches;

    regExp2Matches.map((match) => {
        let split;
        if (match.search(/\(/) > -1) {
            split = match.split(/\(|\)/g)
        }
        else
            split = match.split(' ');

        split = split.map((item) => item.replace(/\s/g, ''));

        let rejoin = '';
        let used = [];
        split.map((item, i) => {
            if (isNaN(Number(item))) {
                rejoin += item;
                used.push(i);
            }
        });
        console.log('rejoin phase 1: '+rejoin);
        split.map((item, i) => {
            if ((item[0] === '2' && item[1] === '0') || (item[0] === '1' && item[1] === '9')) {
                rejoin+=('/'+item);
                used.push(i);
            }
        });
        console.log('rejoin phase 2: '+rejoin);
        split.map((item, i) => {
            if (used.indexOf(i) === -1) {
                rejoin+=('/'+item)
            }
        });

        //    Add search values with leading zero if needed
        let searchValues = [rejoin];
        let rejoinSplit = rejoin.split('/');
        if (rejoinSplit[2].length < 4) {
            let rejoinSplit2LeadingZero = rejoinSplit[2];
            for (let i = 0; i < (4-rejoinSplit[2].length); i++) {
                rejoinSplit2LeadingZero = '0'+rejoinSplit2LeadingZero;
                searchValues.push(rejoinSplit2LeadingZero);
            }
        }
        
        matches.push({searchCollection: 'documents', searchAttr: 'subTitle', searchValues: [searchValues]});

        console.log('prep acts matches:');
        console.log(util.inspect(matches, false, null));
        return matches;
    });
}

export {createDocAndRelationships}