import { db, aqlQuery } from '../../arangodb';
import { checkExistsByAttr } from '../../utils/checkExists';
import { typeofUndefined, typeofUndefinedTwoObj } from '../../utils/typeofUndefined';
import { dateFormatFunc, nowToISOString, strToISOString } from '../../utils/dateFormat'
import {parseString, Parser} from 'xml2js';
import fs from 'fs';
import util from 'util';
import underscoreStr from 'underscore.string';

// const parser = new Parser({explicitArray: false, ignoreAttrs: true});
//
// function getXMLData(config, url) {
//
//     /**
//      * Uncomment for actual import
//      * TODO add test for Whoosh connect to sftp server
//      * */
//     // return new Promise((resolve, reject) => {
//     //     Whoosh.connect(config, function (err, conn) {
//     //         if (err) throw new Error('ERROR connecting to Officiele Bekendmaking SFTP server: \n'+err);
//     //         conn.getContent(url, function (err, content, stats) {
//     //             if(err) throw new Error('ERROR in retrieving SFTP path: \n'+url+'\nERR: \n'+err);
//     //             conn.disconnect();
//     //             resolve(content);
//     //         })
//     //     })
//     // })
//
//     /**
//      * Uncomment for local dev file: ../sources/changelog.xml
//      * */
//     return new Promise((resolve, reject) => {
//         fs.readFile('../sources/metadata_owms-h.xml', function (err, data) {
//             if (err) reject(err);
//             // console.log('data retrieved');
//             resolve(data);
//         })
//     })
// }
//
// async function getMetadata(path) {
//     try {
//         let metadataXML = await getXMLData(path);
//         console.log('Metadata retrieved');
//         parser.parseString(metadataXML, function (err, result) {
//             if (err) throw err;
//             // let wstream = fs.createWriteStream('metadata.json');
//             // wstream.write(JSON.stringify(result));
//             // wstream.end(() => {
//             //     console.log('done writing')
//             // });
//             // console.log(util.inspect(result, false, null))
//           
//             let document = getDocumentData(result['owms-metadata']);
//             console.dir(document)
//             Promise.resolve(document);
//         });
//     }
//     catch (err) {
//         throw new Error('Error in officiële bekendmakingen: \n'+err);
//     }
// }
//
// getMetadata()

function mergeArguments() {
    let arr = [];
    let args = Array.from(arguments);
    // console.log('args: '+args);
    args.forEach((arg) => {
        // console.log(arg);
        if (arg !== '' && typeof arg === 'string') arr.push(arg);
        else if (arg !== '' && Array.isArray(arg)) arr.concat(arg);
    });
    if (arr.length > 0) return Array.from(new Set(arr));
    else return arr;
}

function getCategories(categories) {
    if (categories === '') return [];
    let returnArr = [];
    let arr = Array.isArray(categories) ? categories : [categories];
    arr.forEach((item) => {
        let itemArr = item.split('|');
        returnArr.push({top: underscoreStr.trim(itemArr[0]), sub: underscoreStr.trim(itemArr[1])})
    });
    return returnArr;
}

function getDocumentData(owmsMetadata, path, logging) {
    let owmskern = typeofUndefined(owmsMetadata, 'owmskern');
    let owmsmantel = typeofUndefined(owmsMetadata, 'owmsmantel');
    let oep = typeofUndefined(owmsMetadata, 'oep');
    let document = {};

    //owmskern
    document._key = typeofUndefined(owmskern, 'dcterms:identifier');
    document.path = path;
    document._key.replace(/\//g, '-');
    //document.documentLinks placed below cause its dependent upon documentStatus
    document.title = typeofUndefined(owmskern, 'dcterms:title');
    document.language = convertStringToArray(typeofUndefined(owmskern, 'dcterms:language'));
    document.type = convertStringToArray(typeofUndefined(owmskern, 'dcterms:type')); 
    document.dateModified = strToISOString(typeofUndefined(owmskern, 'dcterms.modified'));
    // document.temporal = typeofUndefined(owmskern, 'dcterms:temporal');
    // document.jurisdiction = convertStringToArray(typeofUndefined(owmskern, 'dcterms:spatial'));
    // document.spatial = typeofUndefined(owmskern, 'dcterms:spatial');
    document.level = 'national';
    document.source = 'officieleBekendmakingen';
    document.country = 'NL';

    //Author relationships both org & people possible
    document.authors = mergeArguments(typeofUndefined(owmskern, 'dcterms:creator'),
        typeofUndefinedTwoObj(owmsmantel, oep, 'overheidop:indiener'), typeofUndefined(owmskern, 'overheid:authority'),
        typeofUndefinedTwoObj(owmsmantel, oep, 'overheid:isRatifiedBy'), typeofUndefinedTwoObj(owmsmantel, oep, 'dcterms:contributor'));

    //owmsmantel \ oep
    document.questionID = typeofUndefinedTwoObj(owmsmantel, oep, 'overheidop:vraagnummer');
    document.answerID = typeofUndefinedTwoObj(owmsmantel, oep, 'overheidop:aanhangselNummer');
    // document.summary = typeofUndefinedTwoObj(owmsmantel, oep, 'dcterms:abstract');
    // console.log('summary: '+document.summary);
    document.subTitle = mergeArguments(typeofUndefinedTwoObj(owmsmantel, oep, 'dcterms:alternative'),
        typeofUndefinedTwoObj(owmsmantel, oep, 'overheid:abbrevation'), typeofUndefinedTwoObj(owmsmantel, oep, 'overheidop:documenttitel'));
    document.organisationType = typeofUndefinedTwoObj(owmsmantel, oep, 'overheid:organisationType');
    // console.log('orgType: '+document.organisationType);
    document.KIOtype = typeofUndefinedTwoObj(owmsmantel, oep, 'overheidop:KIOtype');
    // console.log('kio: '+document.KIOtype);
    // document.audience = typeofUndefinedTwoObj(owmsmantel, oep, 'dcterms:audience');
    // console.log('audience: '+document.audience);
    document.availableDate = strToISOString(typeofUndefinedTwoObj(owmsmantel, oep, 'dcterms:available'));
    document.documentDate = strToISOString(typeofUndefinedTwoObj(owmsmantel, oep, 'dcterms:issued'));
    document.parliamentaryYear = typeofUndefinedTwoObj(owmsmantel, oep, 'overheidop:vergaderjaar');
    // document.dateCreated = strToISOString(typeofUndefinedTwoObj(owmsmantel, oep, 'dcterms:created'));
    // console.log('available: '+document.availableDate);
    // document.conformsTo = typeofUndefinedTwoObj(owmsmantel, oep, 'dcterms:conformsTo');
    // document.coverage = typeofUndefinedTwoObj(owmsmantel, oep, 'dcterms:coverage');
    // console.log('created: '+document.dateCreated);
    // document.description = typeofUndefinedTwoObj(owmsmantel, oep, 'dcterms:description');
    // document.subject = typeofUndefinedTwoObj(owmsmantel, oep, 'dcterms:subject');
    document.entryIntoForceDate = strToISOString(typeofUndefinedTwoObj(owmsmantel, oep, 'dcterms:valid'));
    document.endOfValidityDate = strToISOString(typeofUndefinedTwoObj(owmsmantel, oep, 'overheidop:materieelUitgewerkt'));
    document.version = typeofUndefinedTwoObj(owmsmantel, oep, 'overheidop:versieInformatie');
    document.dateMeeting = strToISOString(typeofUndefinedTwoObj(owmsmantel, oep, 'overheidop:datumVergadering'));
    document.documentStatus = typeofUndefinedTwoObj(owmsmantel, oep, 'overheidop:documentStatus');
    // Manifestations omitted in favour of documentLinks
    // document.manifestations = mergeArguments(typeofUndefinedTwoObj(owmsmantel, oep, 'dcterms:format'), typeofUndefinedTwoObj(owmsmantel, oep, 'dcterms:isFormatOf'),
    //     typeofUndefinedTwoObj(owmsmantel, oep, 'dcterms:hasFormat'));
    
    //Dossier attr
    document.dossier = typeofUndefinedTwoObj(owmsmantel, oep, 'overheidop:dossiernummer'); 
    document.dossier = document.dossier.replace(/\s/g, '-');
    if (document.dossier.search(';') > -1) document.dossier = document.dossier.split(';');
    document.dossier = convertStringToArray(document.dossier);
    
    document.subDossier = typeofUndefinedTwoObj(owmsmantel, oep, 'overheidop:ondernummer');
    document.subDossier = convertStringToArray(document.subDossier);
    document.dossierTitle = typeofUndefinedTwoObj(owmsmantel, oep, 'overheidop:dossiertitel');
    document.KIWI = typeofUndefinedTwoObj(owmsmantel, oep, 'overheidop:ketenID');
    // console.log('kiwi: '+document.KIWI);

    //Documents relationships
    document.documentRelationships = {};

    //"Bijlage 1; stb-2007-27-bijlage1"
    document.documentRelationships.attachment = mergeArguments(typeofUndefinedTwoObj(owmsmantel, oep, 'dcterms:hasPart'),typeofUndefinedTwoObj(owmsmantel, oep, 'overheidop:bijlage'));
    document.documentRelationships.attachment = convertStringToArray(document.documentRelationships.attachment);
    document.documentRelationships.attachmentOf = mergeArguments(typeofUndefinedTwoObj(owmsmantel, oep, 'dcterms:isPartOf'), typeofUndefinedTwoObj(owmsmantel, oep, 'overheidop:hoofddocument'));
    document.documentRelationships.attachmentOf = convertStringToArray(document.documentRelationships.attachmentOf);

    //replaces \ replacedBy used for reprints
    //"Staatscourant 2010, 4051 herdruk 1; stcrt-2010-4051-h1"
    document.documentRelationships.replaces = convertStringToArray(typeofUndefinedTwoObj(owmsmantel, oep, 'dcterms:replaces'));
    document.documentRelationships.isReplacedBy = convertStringToArray(typeofUndefinedTwoObj(owmsmantel, oep, 'dcterms:isReplacedBy'));
    
    //requires \ requiredBy is used for rectifications ex value: Staatsblad 2010, 726 verbeterblad 1; stb-2010-726-v1
    document.documentRelationships.requires = convertStringToArray(typeofUndefinedTwoObj(owmsmantel, oep, 'dcterms:requires'));
    document.documentRelationships.isRequiredBy = convertStringToArray(typeofUndefinedTwoObj(owmsmantel, oep, 'dcterms:isRequiredBy'));

    //cannot find any uses of references
    document.documentRelationships.references = convertStringToArray(typeofUndefinedTwoObj(owmsmantel, oep, 'dcterms:references'));

    // Related to takes this form: Staatsblad 2007, 27; stb-2007-27
    document.documentRelationships.relation = convertStringToArray(typeofUndefinedTwoObj(owmsmantel, oep, 'dcterms:relation'));

    // Build a check if type = aanhangsel => use document.questionID to build a relationship
    // Using overheidop:aanhangsel won't work because the updated question is not present in changelog.xml
    // document.documentRelationships.aanhangsel = typeofUndefinedTwoObj(owmsmantel, oep, 'overheidop:aanhangsel');

    //W03.14.0273/II/K/B
    document.documentRelationships.adviesRvS = convertStringToArray(typeofUndefinedTwoObj(owmsmantel, oep, 'overheidop:adviesRvS')); 

    // ex behandeld dossier: 24515;333 => dossier: '24515', ondernummer: '333', corresponding ID of referenced doc: 'kst-24515-333'
    document.documentRelationships.behandeldDossier = convertStringToArray(typeofUndefinedTwoObj(owmsmantel, oep, 'overheidop:behandeldDossier')); 
    //Implements a CELEX num
    document.documentRelationships.implements = convertStringToArray(typeofUndefinedTwoObj(owmsmantel, oep, 'overheidop:implementeertEUbesluit'));
    document.documentRelationships.legalBasis = convertStringToArray(typeofUndefinedTwoObj(owmsmantel, oep, 'dcterms:source'));

    //people relationships
    if ((document.type == 'Aanhangsel van de Handelingen' || document.type == 'Kamervragen zonder Antwoord') ||
    (document.type.indexOf('Aanhangsel van de Handelingen') > -1 || document.type.indexOf('Kamervragen zonder Antwoord') > -1)) {
        document.askedBy = convertStringToArray(typeofUndefinedTwoObj(owmsmantel, oep, 'overheidop:indiener'));
        document.askedTo = convertStringToArray(typeofUndefinedTwoObj(owmsmantel, oep, 'overheidop:ontvanger'));
    }

    if (document.type == 'Aanhangsel van de Handelingen' || document.type.indexOf('Aanhangsel van de Handelingen') > -1) {
        if (document.askedTo === '') {}
        else if (typeof document.askedTo === 'string')
            document.authors.push(document.askedTo);
        else if (Array.isArray(document.askedTo))
            document.authors = document.authors.concat(document.askedTo);
    }


    //categories
    document.categories = {
        beleidsagenda: getCategories(typeofUndefinedTwoObj(owmsmantel, oep, 'overheid:category')),
        eurovoc: []
    };
    
    //For onopgemaakt documents preprocess their key to their final key so we don't end up with two version of the same doc
    if (document.documentStatus === 'Onopgemaakt') {
        document.oriKey = document._key;
        document._key = checkOnopgemaakt(document);
        document.documentLinks = {"pdf": '/static/pdf/'+document.oriKey+'.pdf'}
    }
    else {
        document.documentLinks = {
            "pdf": 'https://zoek.officielebekendmakingen.nl/'+document._key+'.pdf',
            "html": 'https://zoek.officielebekendmakingen.nl/'+document._key+'.html',
            "odt": 'https://zoek.officielebekendmakingen.nl/'+document._key+'.odt',
            "xml": 'https://zoek.officielebekendmakingen.nl/'+document._key+'.xml'
        }
    }
    
    document.dateImported = nowToISOString();

    return document;

    //Skipped metadata
    //document.startPage = overheidop:startpagina
    //document.endPage = overheidop:eindpagina
    //document.handelingenItemNummer = overheidop:handelingenItemNummer
    //overheidop:publicationIssue = identifier => skipped, overheidop:publicationName = document.type => skipped
    //document.dateSubmit = overheidop:datumIndiening
    //document.dateReceived = overheidop:datumOntvangst
    //dcterms.publisher = always same value so no use
}

function checkOnopgemaakt(document) {
    if (document.documentStatus !== 'Onopgemaakt')
        return document._key;

    if ((Array.isArray(document.type) && document.type.indexOf('Bijlage') > -1) || (document.type === 'Bijlage'))
        return document._key;

    //    Process onopgemaakte kamerstukken
    else if (document.dossier !== '' && document.subDossier !== '') {
        if ((Array.isArray(document.type) && document.type.indexOf('Kamerstuk') > -1) ||
            (typeof document.type == 'string' && document.type === 'Kamerstuk')) {
            let subDossierStr = typeof document.subDossier === 'undefined' ? '' : '-'+document.subDossier;
            return 'kst-'+document.dossier+subDossierStr;
        }
        else return document._key;
    }
    else if (document.questionID !== '') {
        let kamer = document.authors.indexOf('Eerste Kamer der Staten-Generaal') ? 'ek' : 'tk';
        let parliamentaryYearWithoutDash = document.parliamentaryYear.replace('-', '');
        if ((Array.isArray(document.type) && document.type.indexOf('Kamervragen zonder Antwoord') > -1)
            || (document.type == 'Kamervragen zonder Antwoord')) {
                return 'kv-'+kamer+'-'+document.questionID;
        }
        else if ((Array.isArray(document.type) && document.type.indexOf('Aanhangsel van de Handelingen') > -1)
            || (document.type == 'Aanhangsel van de Handelingen')) {
                return 'ah-'+kamer+'-'+parliamentaryYearWithoutDash+'-'+document.answerID;
        }
        else
            return document._key;
    }
    // Process niet-dossier-stukken
    else if ((Array.isArray(document.type) && document.type.indexOf('Niet-dossierstuk') > -1) || document.type == 'Niet-dossierstuk') {
        //TODO find an nds item which is onopgemaakt and check if we can derive final key
        return document._key;
    }
}

function convertStringToArray(string) {
    if (Array.isArray(string))
        return string;
    else if (string == '')
        return [];
    else if (typeof string == 'string')
        return [string];
    else 
        return [];
}

export {getDocumentData}

/**
 *

 http://standaarden.overheid.nl/oep/technische-documentatie

 !!!
 IMPORTANT attr without edges
 !!!

 DC.identifier
 OVERHEIDop.aanhangselNummer = ID for Aanhangsels AKA Kamervragen met Antwoord
 OVERHEIDop.vraagnummer = ID for Kamervragen zonder Antwoord

 DC.spatial

 DC.type
 OVERHEIDop.documentStatus

 DC.title
 DCTERMS.abstract
 DCTERMS.alternative ==> alt title
 OVERHEIDop.documenttitle

 DCTERMS.available ==> date publication
 DCTERMS.issued => document date
 OVERHEIDop.datumIndiening
 OVERHEIDop.datumOntvangst
 OVERHEIDop.datumVergadering

 OVERHEIDop.datumTotstandkoming ==> date or dates of agreeing on treaty
 OVERHEIDop.plaatsTotstandkoming ==> place or places where treaty has been agreed
 OVERHEIDop.verdragnummer ==> ID of treaty as determined by dutch ministry of foreign affairs

 DCTERMS.temporal ==> specific time, example: closed road

 OVERHEIDop.datumOndertekening
 OVERHEIDop.jaargang
 OVERHEIDop.publicationIssue
 OVERHEIDop.startdatum
 OVERHEIDop.einddatum



 !!!
 IMPORTANT attr which can create edges
 !!!

 OVERHEIDop.aanhangsel => Q&A to Q without A (Kamervragen zonder Antwoord]

 OVERHEIDop.behandeldDossier
 OVERHEIDop.dossiernummer
 OVERHEIDop.dossiertitel
 OVERHEIDop.ondernummer => ID of dossier in which doc is published

 OVERHEIDop.ketenID => optional ID used for tracking legislative process

 DC.creator ==> Always gov organisation, multiple values possible, can be a non governmental org
 OVERHEID.authority => governmental body, responsible for doc. Example: report created by external bureau = creator, commissioned by gov org => authority
 OVERHEID.organisationType => governmental body
 OVERHEIDop.indiender => MP which asked question
 OVERHEIDop.ontvanger => politician in office who receives question

 OVERHEID.category => subject from TaxonomieBeleidsagenda
 OVERHEID.adviesRvS => refers to doc by RvS

 DC.source => refers to existing law as legal basis ==> link to wetten.overheid.nl

 DCTERMS.hasPart ==> attachments
 DCTERMS.partOf ==> attachment of !!!NOT USED!!!
 OVERHEIDop.bijlage
 OVERHEIDop.externeBijlage

 DCTERMS.replaces => <meta name="DCTERMS.replaces" content="Staatscourant 2010, 4051; stcrt-2010-4051">
 DCTERMS.isReplacedBy => <meta name="DCTERMS.isReplacedBy" content="Staatscourant 2010, 4051 herdruk 1; stcrt-2010-4051-h1">

 DCTERMS.isRequiredBy ==> old doc is rectified by this reference <meta name="DCTERMS.isRequiredBy" content="Staatsblad 2010, 726 verbeterblad 1; stb-2010-726-v1">
 DCTERMS.requiredBy ==> reference rectifies old doc

 OVERHEIDop.implementeertEUbesluit ==> refers to CELEX ID

 OVERHEIDop.publicationName
 OVERHEIDop.publicationIssue

 DCTERMS.publisher => organisation that publishes doc
 DCTERMS.relation => Referenced doc: <meta name="DCTERMS.relation" content="Advies Raad van State; stcrt-2008-6748">

 OVERHEIDop.gemeentenaam
 OVERHEIDop.provincie
 * */