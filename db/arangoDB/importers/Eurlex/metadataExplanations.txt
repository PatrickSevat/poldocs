/** Metadata to retrieve:
 * MANIFESTATION_TYPE => available formats,
 *
 * AU => entity that produced the act
 * AS => co authors,
 * CREATED_BY => ,
 * RAPPORTEUR => MEP, MCoR, MEESC who reports on act in question
 *
 * TI_DISPLAY => title & subtitle,
 *
 * CODE_PROC => work part of dossier
 *
 * LP_DOSSIER_TI_DISPLAY => procedure title,
 * LP_DOSSIER_ID => ,
 * LP_INITIATING => CELEX number of initiating proposal doc
 * LP_LEGAL_BASIS_DISPLAY
 * LP_ADOPTED_ACT
 * LP_ADOPTED_FL => boolean if procedure is closed and adopted
 * LP_PENDING_FL => procedure pending boolean
 * LP_WITHDRAWN_FL => withdrawn boolean
 * LP_FILE_TYPE => type of proposal initiating procedure: Ex: council framework decision, draft act, recommendation for an opinion, many more
 *
 * DN => documentNumber in CELEX FORMAT! can be used to rebuild document ID: http://eur-lex.europa.eu/content/help/faq/intro.html#help10
 * PS_ID => IDENTIFIER: PS_ID=COM_2014_0082_FIN; PS_ID="20150409-007:C(2015)2355/804650"; PS_ID=JOL_2015_099_R_0006
 *
 * SUM => summary of CASE LAW!,
 * DD => Date document,
 * IF => date in force
 * RP => date of reply
 * EV => date of end validity
 * TP => date of transposition (date on which directives must be implemented by member states]
 *
 * CI_DISPLAY => instruments cited in CELEX FORMAT, only cited docs, NO LEGAL BASIS, NO AMENDMENTS
 * MS_DISPLAY => amended to in CELEX numbers
 * MD_DISPLAY => amended by in CELEX,
 * EA_DISPLAY => CELEX numbers of acts that will be amended if preperatory act succeeds & also earlier preperatory acts & amendments to preperatory acts,
 * LB_DISPLAY => Legal basis + art, paragraph etc
 * AF => Political group,
 * ASKED_BY => ,
 * CELLAR_ID
 * VV => in force boolean
 * */

 Query:

SELECT
      MANIFESTATION_TYPE, AU, AS, CREATED_BY, RAPPORTEUR,
      DN, PS_ID, WORK_IDS_DISPLAY, VV,
      ASKED_BY,
      TI_DISPLAY, TE, TEXT_INDEX, TI_DISPLAY, SUM,
      LB_DISPLAY, CI_DISPLAY, MS_DISPLAY, MD_DISPLAY, EA_DISPLAY,
      CODE_PROC, PROC_NUM, PROC_GR_DISPLAY, PROC_TYP, PART_OF_DOSSIER,
      DC, DC_DOM, DC_MTH, DC_TT,


      LP_DOSSIER_TI_DISPLAY, LP_DOSSIER_ID, LP_PROC_REFERENCE, LP_DOCUMENT_REF, LP_INITIATING_DISPLAY, LP_ADOPTED_ACT_DISPLAY, LP_ACTING_BODY, LP_AGENT, LP_ADOPTED_FL, LP_AUTHORISING_AGENT,
        LP_BODY_PENDING_CODED, LP_BODY_RL_ADDR, LP_BODY_RL_ADDR_FOR, LP_BODY_RL_ADDR_INFO, LP_BODY_RL_ASSOC, LP_BODY_RL_AUT_OPINION, LP_BODY_RL_AUT_REPORT, LP_BODY_RL_CONSULT,
         LP_BODY_RL_JOINT_RESP, LP_BODY_RL_MAND_CONSULT, LP_BODY_RL_OPT_CONSULT, LP_BODY_RL_PRI_RESP, LP_BODY_ROLE, LP_BODY_ROLE_EXP, LP_CODE_LEGAL_BASIS_ART,
         LP_DC, LP_DC_MTH, LP_DC_TT, LP_DECISION, LP_DECISION_MODE,
         LP_EVENT_DATE_DISPLAY, LP_EVENT_DOC_CELEX, LP_EVENT_ID, LP_EVENT_TYPE, LP_FILE_TYPE, LP_ID, LP_INITIATING_DISPLAY, LP_INTER_CODE_TYPE, LP_INTER_CODE_NUM, LP_INTER_CODE_YEAR, LP_LEGAL_BASIS_DISPLAY,
         LP_PERSON_DRAFTSMAN, LP_PERSON_JOINT_RESP, LP_PERSON_RAPP, LP_PERSON_RESP, LP_PROC_DISPLAY, LP_PROC_TYP, LP_SECONDARY_LEG,
         LP_WITHDRAWN_FL, LP_PENDING_FL
    WHERE
(DTS_SUBDOM = LEGAL_PROCEDURE  ) AND  ((DD >= 01/01/2016 <= 31/12/2016 OR PD >= 01/01/2016 <= 31/12/2016) OR (IF >= 01/01/2016 <= 31/12/2016 OR EV >= 01/01/2016 <= 31/12/2016 OR SG >= 01/01/2016 <= 31/12/2016 OR DL >= 01/01/2016 <= 31/12/2016) OR XC >= 01/01/2012 <= 31/12/2012 OR XA >= 01/01/2012 <= 31/12/2012) ORDER BY XA DESC