import soap from 'soap'
const url = 'http://eur-lex.europa.eu/eurlex-ws?wsdl';
import superagent from 'superagent';
const soapOptions= {'forceSoap12Headers': true};
import {password} from './../../../../secret/password';
import util from 'util';
import fs from 'fs';
import Promise from 'promise';

import { db, aqlQuery } from '../../arangodb';
import { checkExistsAndCreateEdgesTwoWayFromArray } from '../../utils/createNodeAndEdges';
import { checkExistsAndCreateNodeByKey} from '../../utils/checkExistsAndCreate';
import { searchForDocuments, fromArrToID, fromIDToArr, inverseDocumentRelationship } from '../../utils/documentUtils';
import {dateFormatFunc, strToISOString, nowToISOString } from '../../utils/dateFormat'
import {typeofUndefined } from '../../utils/typeofUndefined'
import { retrieveAuthors, getManifestations, checkCelexKey, getType, getFullText, getDocumentRelationships,
    getEurovocIDs, getDossierData, getDocumentLinks, createSubTitle, convertStringToArray } from '../../utils/EurlexUtils'


const documentsColl = db.collection('documents');

const partOfEdge = db.edgeCollection('partOf');
const hasPartEdge = db.edgeCollection('hasPart');
const authorOfEdge = db.edgeCollection('authorOf');
const authoredByEdge = db.edgeCollection('authoredBy');
const consultsEdge = db.edgeCollection('consults');
const consultedByEdge = db.edgeCollection('consultedBy');
const documentRelationshipsEdge = db.edgeCollection('documentRelationships');

// let dateFormat = '2016/10/06';
let dateFormat = dateFormatFunc('/');
let language3 = 'nld';
let language2 = 'nl';

/**
 * Use this query for daily updates
 * */

//query searches in parl questions, preperatory acts, international agreements, treaties and legislative procedures
let query =
    `
    SELECT
      MANIFESTATION_TYPE, AU, AS, CREATED_BY, RAPPORTEUR,
      DN, PS_ID, WORK_IDS_DISPLAY, VV,
      ASKED_BY, 
      TI, TE, TEXT_INDEX, TI_DISPLAY, 
      LB_DISPLAY, CI_DISPLAY, MS_DISPLAY, MD_DISPLAY, EA_DISPLAY,
      CODE_PROC, PROC_GR_DISPLAY, PART_OF_DOSSIER, 
      DC, DC_DOM, DC_MTH, DC_TT,
      DD, DH, DL, EV, TP, IF, RP
    WHERE  
    ((DTS_SUBDOM = PRE_ACTS OR LEGISLATION OR INTER_AGREE OR TREATIES) AND 
        ((DD = ${dateFormat} OR PD = ${dateFormat}) OR ((IF = ${dateFormat} OR EV = ${dateFormat} OR NF = ${dateFormat} OR SG = ${dateFormat} OR TP = ${dateFormat} OR LO = ${dateFormat} OR DH = ${dateFormat} OR DL = ${dateFormat} OR RP = ${dateFormat} OR VO = ${dateFormat} OR DB = ${dateFormat} ) OR 
        (DH = ${dateFormat} OR DL = ${dateFormat} OR RP = ${dateFormat} OR VO = ${dateFormat} OR DB = ${dateFormat}))
        OR XC = ${dateFormat} OR XA = ${dateFormat})
    )     
    AND CASE_LAW_SUMMARY = false AND COMPOSE = ${language3} ORDER BY XA DESC`;


let docs2016query = `
SELECT MANIFESTATION_TYPE, AU, AS, CREATED_BY, RAPPORTEUR, DN, PS_ID, WORK_IDS_DISPLAY, VV, ASKED_BY, TI_DISPLAY, TE, TEXT_INDEX, TI_DISPLAY, LB_DISPLAY, CI_DISPLAY, MS_DISPLAY, MD_DISPLAY, EA_DISPLAY, CODE_PROC, PROC_GR_DISPLAY, PART_OF_DOSSIER, DC, DC_DOM, DC_MTH, DC_TT, DD, DH, DL, EV, TP, IF, RP WHERE ((DTS_SUBDOM = PRE_ACTS OR LEGISLATION OR INTER_AGREE OR TREATIES) AND DD >= 01/01/2016 ) AND CASE_LAW_SUMMARY = false AND COMPOSE = eng ORDER BY XA DESC
`;

let bulkArr = [];

var pageNumber = 1;
let pageSize = 100;
let numResults;

function getQuery(pageNum) {
    return {
        // 'expertQuery': queryDossier,
        'expertQuery': query,
        'page': pageNum,
        'pageSize': pageSize,
        'searchLanguage': language2
    };
}

// let wLogstream = fs.createWriteStream('import2016docsLog.txt');

makeEurlexCall();
// console.log(query);

function makeEurlexCall() {

    let eurlexQuery = getQuery(pageNumber);

    // let wstream = fs.createWriteStream(dateFormatFunc('_')+'-'+pageNumber+'.json');
    soap.createClient(url, soapOptions, function (err, client) {
        if (err) console.error(err);
        console.log('retrieving page: '+pageNumber);
        client.addSoapHeader('<wsse:Security soap:mustUnderstand="true" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"><wsse:UsernameToken wsu:Id="UsernameToken-2783C6C748D7FA98E414596887803756"><wsse:Username>nsevatpa</wsse:Username><wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'+password+'</wsse:Password></wsse:UsernameToken></wsse:Security>');
        client.doQuery(eurlexQuery, function (err, result) {
            if (err) console.log(err);
            // console.log(util.inspect(result, false, null));
            
            bulkArr = result.result;
            numResults = result.totalhits;
            console.log('\nretrieving batch '+pageNumber+', number of hits for this batch: '+result['numhits']+', total hits: '+result['totalhits']);

            // wstream.write(JSON.stringify(result));
            // wstream.end(() => {
            //     console.log('done writing');
            //     setTimeout(() => {
            //         if (pageNumber*pageSize < numResults) {
            //             pageNumber++;
            //             makeEurlexCall();
            //         }
            //         else {
            //             console.log('\nALL DONE WRITING TO LOCAL!')
            //         }
            //     },1000)
            // });
            
            // asyncLoop(true);
            asyncLoop()
        })
    });
}

function getLocalFile(path) {
    return new Promise((resolve, reject) => {
        fs.readFile(path, 'utf8', (err, res) => {
            if (err) reject(err);
            else {
                console.log('data retrieved');
                resolve(JSON.parse(res))
            }
        });
    })
}

async function processLocalFile(batchNum) {
    bulkArr = await getLocalFile('./2016_09_01-'+batchNum+'.json');
    numResults = bulkArr.totalhits;
    bulkArr = bulkArr.result;
    
    // bulkArr = bulkArr.slice(8,9);
    asyncLoop();
    // asyncLoop();
}

// processLocalFile(pageNumber);

async function asyncLoop(logging) {
    try {
        for (let i = 0; i < bulkArr.length; i++) {
            console.log('\ndocument iteration: ' + i+', page number: '+pageNumber+', totalhits: '+numResults);
            // await waitASecond();
            const data = bulkArr[i].content.NOTICE;

            if (typeof data.MANIFESTATION === 'undefined' && typeofUndefined(bulkArr[i], 'document_link') === '') {
                console.error('No manifestations & no document link');
            }
            else {
                let saveWorkObj = {};
                //TODO Fix dutch!
                saveWorkObj.language = ['eng'];
                saveWorkObj.cellarKey = bulkArr[i].reference.slice(bulkArr[i].reference.search(':') + 1, bulkArr[i].reference.lastIndexOf('_')) || data.WORK.URI.IDENTIFIER;
                saveWorkObj._key = saveWorkObj.cellarKey;
                if (logging) console.log('cellarKey: ' + saveWorkObj.cellarKey);
                saveWorkObj.celexKey = checkCelexKey(typeofUndefined(data.WORK, 'ID_CELEX', 'VALUE'));
                if (logging) console.log('celexKey: ' + saveWorkObj.celexKey);
                saveWorkObj.documentLinks = getDocumentLinks(typeofUndefined(bulkArr[i], 'document_link'));
                saveWorkObj.type = getType(saveWorkObj.celexKey);
                saveWorkObj.title = data.EXPRESSION[0].EXPRESSION_TITLE.VALUE.replace(/#/g, ' ');
                saveWorkObj.fullText = typeof bulkArr[i].content.CONTENT_URL !== 'undefined' ? getFullText(bulkArr[i].content.CONTENT_URL) : '';
                if (logging) console.log('fullText\n' + saveWorkObj.fullText.slice(0, 50));
                saveWorkObj.documentDate = typeofUndefined(data.WORK, 'WORK_DATE_DOCUMENT', 'VALUE');
                if (logging) console.log('DD: ' + saveWorkObj.documentDate);
                saveWorkObj.subTitle = convertStringToArray(typeofUndefined(data.EXPRESSION[0], 'EXPRESSION_SUBTITLE', 'VALUE'));
                //    This is needed for example with regulations: Eurlex metadat subtitle = C/2016/4796, internally we need 2016/4796/EU
                //TODO watch this closely, currently -29 sept 16- 10% still don't get a subTitle which they should
                saveWorkObj.subTitleID = createSubTitle(saveWorkObj);
                //DONE: if subTitle is empty: retrieve from fullText
                //saveWorkObj.type = 'Preparatory acts'
                //EN Official Journal of the European Union C 353/168 P8_TA(2015)0199
                //EN Official Journal of the European Union C 303/147

                //Done in case of saveWorkObj.type == 'EU Legislation' && saveWorkObj.subTitle == '' => retrieve identifier from title:
                //Council Directive (EU) 2016/856 of 25 May 2016 amending Directive 2006/112/EC => 2016/856/EU

                if (logging) console.log('title: ' + saveWorkObj.title);
                if (logging) console.log('subtitle: ' + saveWorkObj.subTitle);
                saveWorkObj.authors = retrieveAuthors(data.WORK.CREATED_BY, data.WORK.WORK_CREATED_BY_AGENT);
                if (logging) console.log('authors: ' + saveWorkObj.authors);
                // Omitted manifestations in favour of document links
                saveWorkObj.manifestations = getManifestations(typeofUndefined(data, 'MANIFESTATION'));
                if (logging) console.log('manifest: ');
                if (logging) console.dir(saveWorkObj.manifestations);
                //TODO add Inverse MD relationships?
                saveWorkObj.documentRelationships = getDocumentRelationships(data);
                if (logging) console.dir(saveWorkObj.documentRelationships);
                saveWorkObj.dossier = await getDossierData(typeofUndefined(data, 'INVERSE'), db, documentsColl, aqlQuery);
                if (logging) console.log('dossierID: ' + saveWorkObj.dossierIDDashed);
                saveWorkObj.reportedBy = convertStringToArray(typeofUndefined(data.WORK, 'REPORTED_BY', 'PREFLABEL'));
                // if (logging) console.log('reportedBy: '+saveWorkObj.reportedBy);
                saveWorkObj.inForce = typeof data.WORK['RESOURCE_LEGAL_IN-FORCE'] !== 'undefined' ? data.WORK['RESOURCE_LEGAL_IN-FORCE'].VALUE : '';
                saveWorkObj.categories = {
                    eurovoc: getEurovocIDs(typeofUndefined(data.WORK, 'WORK_IS_ABOUT_CONCEPT_EUROVOC')),
                    beleidsagenda: []
                };
                if (logging) console.log('Eurovoc: ' + saveWorkObj.eurovoc);
                saveWorkObj.endOfValidityDate = typeofUndefined(data.WORK, 'RESOURCE_LEGAL_DATE_END-OF-VALIDITY', 'VALUE');
                saveWorkObj.entryIntoForceDate = typeofUndefined(data.WORK, 'RESOURCE_LEGAL_DATE_ENTRY-INTO-FORCE', 'VALUE');
                saveWorkObj.deadline = typeofUndefined(data.WORK, 'RESOURCE_LEGAL_DATE_DEADLINE', 'VALUE');
                saveWorkObj.dateImported = nowToISOString();
                saveWorkObj.level = 'international';
                saveWorkObj.source = 'Eurlex';
                saveWorkObj.country = 'EU';
                if (logging) console.log('Save work obj passed, iteration: ' + i);

                let savedObj = await checkExistsAndCreateNodeByKey(saveWorkObj._key, documentsColl, db, aqlQuery, saveWorkObj);
                console.log('\nSaved to documentsColl, key: ' + savedObj._id);

                //retrieve author IDs based on the names in saveWorkObj.authors
                //TODO searchForDocuments assumes that all authors are already in DB, build length check, possibly return both name and ID
                if (logging) console.log(saveWorkObj.authors);
                console.log(saveWorkObj.authors);
                let authorIDsArr = await searchForDocuments(saveWorkObj.authors, db, aqlQuery, null, {caseSensitive: false, searchType: 'equality'});
                if (logging) console.log('AuthorIDsArr: ' + authorIDsArr);
                console.log('AuthorIDsArr: ' + authorIDsArr);
                let fromToAuthorArr = fromArrToID(authorIDsArr, savedObj._id);
                console.log(fromToAuthorArr);
                // create edges from author(s] to document
                await checkExistsAndCreateEdgesTwoWayFromArray(fromToAuthorArr, [authorOfEdge, {type: 'author of'}],
                    [authoredByEdge, {type: 'authored by'}], db, aqlQuery);
                if (logging) console.log('\n saved author edges');


                //for each document relationship which isn't '' create edges
                if (logging) console.log('\nstarting doc relationships');
                for (let relationship in saveWorkObj.documentRelationships) {
                    if (saveWorkObj.documentRelationships[relationship] === '') {
                    }
                    else if (Array.isArray(saveWorkObj.documentRelationships[relationship])) {
                        console.log('\nprocessing relationship: '+relationship);
                        let fromToDocRelArr = fromIDToArr(savedObj._id, saveWorkObj.documentRelationships[relationship], 'documents/');
                        console.log(fromToDocRelArr);
                        if (logging) console.dir(fromToDocRelArr);
                        await checkExistsAndCreateEdgesTwoWayFromArray(fromToDocRelArr, [documentRelationshipsEdge, {type: relationship}],
                            [documentRelationshipsEdge, {type: inverseDocumentRelationship(relationship)}], db, aqlQuery);
                        console.log('done processing relationship: '+relationship);
                    }
                }
                if (logging) console.log('\ncompleted doc relationships');


                // Add to dossier if applicable
                //dossierIDDashed used to contain the interinstitutional reference, but NOW contains the CELLAR
                if (saveWorkObj.dossier.length > 0) {
                    let dossierInterInstIDArr = await searchForDocuments(saveWorkObj.dossier, db, aqlQuery, ['dossiers'], {caseSensitive: false, searchType: 'equality', searchAttr: 'interinstitutionalReference'});
                    console.log('dossierInterInstIDArr'+dossierInterInstIDArr);
                    console.log('isArray + typeof dossierInterInstIDArr'+Array.isArray(dossierInterInstIDArr)+' '+ typeof dossierInterInstIDArr);
                    let dossierCellarIDArr = await searchForDocuments(saveWorkObj.dossier, db, aqlQuery, ['dossiers'], {caseSensitive: false, searchType: 'equality', searchAttr: '_key'});
                    
                    let combinedIDArr = dossierCellarIDArr.concat(dossierInterInstIDArr);
                    let fromRelToDocArr = fromArrToID(combinedIDArr, savedObj._id);
                    await checkExistsAndCreateEdgesTwoWayFromArray(fromRelToDocArr,[hasPartEdge, {type: 'dossier has part'}],
                        [partOfEdge, {type: 'part of dossier'}], db, aqlQuery);
                    if (logging) console.log('created edges (and dossier)')
                }


                //Reported by TODO Skipped for now
                // if (saveWorkObj.reportedBy !== '') {
                //     let reporterIDs = await searchForDocuments(saveWorkObj.reportedBy)
                // }
            }
        }
        console.log('\ncompleted iterating over documents. Completed: ' + (((pageNumber - 1) * 100) + bulkArr.length) + ' out of ' + numResults);

        if (((pageNumber - 1) * pageSize) + bulkArr.length < numResults) {
            console.log('\n Old page number: ' + pageNumber);
            pageNumber++;
            console.log('New page number: ' + pageNumber);
            makeEurlexCall();
            // processLocalFile(pageNumber);
        }

        else {
            console.log('\n ALL DONE! \n finished '+(((pageNumber-1)*100)+bulkArr.length)+' documents');
        
        }
    
    }
    catch (err) {
        console.error(err);
    }
}



/* Example SOAP response:
 * How does it work?
 * Work = document we are retrieving
 * Expression: language we are retrieving the document in
 * Manifestation: the document format (pdf, xml, etc) in which the document can be retrieved
 * SameAs: other identifiers and direct link to document (direct links can also be retrieved via CELLAR, see below)
 * CELLAR link based on below example: 'http://publications.europa.eu/resource/cellar/1ea17146-eff9-11e5-8529-01aa75ed71a1
 * */

/*
 * CELLAR, types of notices:
 * Tree: metadata of the work, metadata of all expressions (languages), metadat of all manifestations (pdf, xml, html, etc.)
 * Branch: metadata of the work, metadata of selected expression (language) and metadat of all manifestations for that language
 * Object: metadata of an selected object. Object can be Work, Expression or Manifestation
 * Identifier: list of synonyms of object. Object can be Work, Expression or Manifestation
 * RDF-object: RDF response for object. Object can be Work, Expression, Manifestation, Dossier, Event or Agent
 * RDF-tree: RDF response for work, dossier or Agent
 * */