/**
 * Hierarchy work-expression-manifestation-content stream
        - a Work => document
        - Expressions => document in language
        - Manifestations => document in language in format
        - Content streams => an actual representation of the Manifestation
 */
import { db, aqlQuery } from '../../arangodb';
import {parseString, Parser} from 'xml2js';
import superagent from 'superagent';
import util from 'util';

const parser = new Parser({explicitArray: true});

//cellarID from EurLex:  nld_cellar:b9caef45-6459-11e6-9b08-01aa75ed71a1_nl

let psName = 'cellar';
let psID = '47cc5480-0612-11e6-b713-01aa75ed71a1'; // {work-id}.{exprid}.{man-id}/{cs-id} example: '550e8400-e29b-41d4-a716-446655440000.0001.03/DOC_1'
let lang = 'eng';
let resourceURI = `http://publications.europa.eu/resource/${psName}/${psID}`;

async function retrieveFullText (resourceURI, lang, mimeType) {
    try {
        let resultRaw = await superagent
            .get(resourceURI)
            .set({Accept: mimeType, 'Accept-Language': lang})
            .buffer();
        let result = resultRaw.text;
        console.log(result);
    }
    catch (err) {
        console.log(err);
    }
}

async function retrieveMimeTypes(resourceURI, lang) {
    try {
        let resultRaw = await superagent
            .get(resourceURI)
            .set({Accept: 'application/xml;notice=branch', 'Accept-Language': lang})
            .buffer();
        let result = resultRaw.text;
        let parsedRes;
        parser.parseString(result, (err, parsed) => {

            //WORK related variables
            // let citedWorks = parsed.notice[0].work[0].WORK_CITES_WORK;
            // let workCreatedBy = parsed.notice[0].work[0].CREATED_BY;
            // let workCreatedByAgent = parsed.notice[0].work[0].WORK_CREATED_BY_AGENT;
            // let dateDocument = parsed.notice[0].work[0].DATE_DOCUMENT;
            // let workID = parsed.notice[0].work[0].IDENTIFIER;
            //
            // //INVERSE related variables
            // let related = parsed.notice[0].inverse[0].RESOURCE_LEGAL_RELATED_TO_RESOURCE_LEGAL;
            // let related2 = parsed.notice[0].inverse[0].WORK_RELATED_TO_WORK;

            //EXPRESSION related variables
            let title = parsed.NOTICE.EXPRESSION[0].EXPRESSION_TITLE[0].VALUE[0];
            let subTitle = parsed.NOTICE.EXPRESSION[0].EXPRESSION_SUBTITLE[0].VALUE[0];

            //MANIFESTATIONS related variables
            let manifestations = parsed.NOTICE.MANIFESTATION;

            console.log(`title: ${title}, subtitle: ${subTitle}`);
            
            console.log('manifestations:');
            console.log(util.inspect(manifestations, false, null));
        })
    }
    catch (err) {
        console.log(err);
    }
}

// retrieveFullText(resourceURI);
retrieveMimeTypes(resourceURI, 'eng');

/**
 *
 * <NOTICE decoding="eng" type="branch">
 <WORK>
 <URI>...</URI>
 <SAMEAS>...</SAMEAS>
 <SAMEAS>...</SAMEAS>
 <CREATED_BY type="concept">
 <URI>
 <VALUE>
 http://publications.europa.eu/resource/authority/corporate-body/COM
 </VALUE>
 <IDENTIFIER>COM</IDENTIFIER>
 <TYPE>corporate-body</TYPE>
 </URI>
 <OP-CODE>COM</OP-CODE>
 <IDENTIFIER>COM</IDENTIFIER>
 <PREFLABEL>European Commission</PREFLABEL>
 <ALTLABEL>Commission</ALTLABEL>
 </CREATED_BY>
 <RESOURCE_LEGAL_NUMBER_NATURAL_CELEX type="data">...</RESOURCE_LEGAL_NUMBER_NATURAL_CELEX>
 <TITLE type="data">...</TITLE>
 <WORK_CITES_WORK type="link">
 <URI>...</URI>
 <SAMEAS>...</SAMEAS>
 <SAMEAS>
 <URI>
 <VALUE>
 http://publications.europa.eu/resource/comnat/COM_2016_0176_FIN
 </VALUE>
 <IDENTIFIER>COM_2016_0176_FIN</IDENTIFIER>
 <TYPE>comnat</TYPE>
 </URI>
 </SAMEAS>
 </WORK_CITES_WORK>
 <WORK_IS_ABOUT_CONCEPT_EUROVOC type="concept_facet">
 <WORK_IS_ABOUT_CONCEPT_EUROVOC_CONCEPT type="concept">
 <URI>...</URI>
 <IDENTIFIER>3017</IDENTIFIER>
 <PREFLABEL>transmission network</PREFLABEL>
 <ALTLABEL>data-transmission network</ALTLABEL>
 <ALTLABEL>Euronet</ALTLABEL>
 <ALTLABEL>telecommunications network</ALTLABEL>
 <ALTLABEL>broadcasting network</ALTLABEL>
 <ALTLABEL>Transpac</ALTLABEL>
 </WORK_IS_ABOUT_CONCEPT_EUROVOC_CONCEPT>
 <WORK_IS_ABOUT_CONCEPT_EUROVOC_TT type="concept">
 <URI>...</URI>
 <IDENTIFIER>4361</IDENTIFIER>
 <PREFLABEL>communications systems</PREFLABEL>
 </WORK_IS_ABOUT_CONCEPT_EUROVOC_TT>
 <WORK_IS_ABOUT_CONCEPT_EUROVOC_DOM type="concept">
 <URI>...</URI>
 <IDENTIFIER>32</IDENTIFIER>
 <PREFLABEL>32 EDUCATION AND COMMUNICATIONS</PREFLABEL>
 </WORK_IS_ABOUT_CONCEPT_EUROVOC_DOM>
 <WORK_IS_ABOUT_CONCEPT_EUROVOC_MTH type="concept">
 <URI>...</URI>
 <IDENTIFIER>3226</IDENTIFIER>
 <PREFLABEL>3226 communications</PREFLABEL>
 </WORK_IS_ABOUT_CONCEPT_EUROVOC_MTH>
 </WORK_IS_ABOUT_CONCEPT_EUROVOC>
 <IS_ABOUT type="concept">
 <URI>...</URI>
 <IDENTIFIER>5674</IDENTIFIER>
 <PREFLABEL>EU initiative</PREFLABEL>
 <ALTLABEL>European Union initiative</ALTLABEL>
 <ALTLABEL>Community initiative</ALTLABEL>
 <ALTLABEL>initiative of the European Union</ALTLABEL>
 </IS_ABOUT>
 <IS_ABOUT type="concept">
 <URI>
 <VALUE>...</VALUE>
 <IDENTIFIER>TECN</IDENTIFIER>
 <TYPE>fd_070</TYPE>
 </URI>
 <OP-CODE>TECN</OP-CODE>
 <IDENTIFIER>TECN</IDENTIFIER>
 <PREFLABEL>Technology</PREFLABEL>
 </IS_ABOUT>
 <LASTMODIFICATIONDATE type="date">
 <VALUE>2016-04-21T10:56:10.139+02:00</VALUE>
 <YEAR>2016</YEAR>
 <MONTH>04</MONTH>
 <DAY>21</DAY>
 </LASTMODIFICATIONDATE>
 <WORK_HAS_EXPRESSION type="link">...</WORK_HAS_EXPRESSION>
 <WORK_HAS_EXPRESSION type="link">...</WORK_HAS_EXPRESSION>
 <WORK_HAS_EXPRESSION type="link">...</WORK_HAS_EXPRESSION>
 <DATE type="date">...</DATE>
 <DATE_DOCUMENT type="date">...</DATE_DOCUMENT>
 <VERSION type="data">
 <VALUE>final</VALUE>
 </VERSION>
 <WORK_IS_ABOUT_CONCEPT_EUROVOC type="concept_facet">...</WORK_IS_ABOUT_CONCEPT_EUROVOC>
 <WORK_CITES_WORK type="link">
 <URI>...</URI>
 <SAMEAS>
 <URI>
 <VALUE>
 http://publications.europa.eu/resource/celex/12012P007
 </VALUE>
 <IDENTIFIER>12012P007</IDENTIFIER>
 <TYPE>celex</TYPE>
 </URI>
 </SAMEAS>
 </WORK_CITES_WORK>
 <WORK_HAS_EXPRESSION type="link">...</WORK_HAS_EXPRESSION>
 <WORK_HAS_EXPRESSION type="link">...</WORK_HAS_EXPRESSION>
 <WORK_ID_DOCUMENT type="data">...</WORK_ID_DOCUMENT>
 <YEAR type="data">
 <VALUE>2016</VALUE>
 </YEAR>
 <WORK_CITES_WORK type="link">
 <URI>...</URI>
 <SAMEAS>
 <URI>
 <VALUE>
 http://publications.europa.eu/resource/comnat/COM_2015_0626_FIN
 </VALUE>
 <IDENTIFIER>COM_2015_0626_FIN</IDENTIFIER>
 <TYPE>comnat</TYPE>
 </URI>
 </SAMEAS>
 <SAMEAS>...</SAMEAS>
 </WORK_CITES_WORK>
 <WORK_CITES_WORK type="link">
 <URI>...</URI>
 <SAMEAS>
 <URI>
 <VALUE>
 http://publications.europa.eu/resource/com/COM_2012_449
 </VALUE>
 <IDENTIFIER>COM_2012_449</IDENTIFIER>
 <TYPE>com</TYPE>
 </URI>
 </SAMEAS>
 <SAMEAS>...</SAMEAS>
 <SAMEAS>...</SAMEAS>
 </WORK_CITES_WORK>
 <WORK_ID_DOCUMENT type="data">
 <VALUE>comnat:COM_2016_0178_FIN</VALUE>
 </WORK_ID_DOCUMENT>
 <IS_ABOUT type="concept">...</IS_ABOUT>
 <WORK_DATE_DOCUMENT type="date">
 <VALUE>2016-04-19</VALUE>
 <YEAR>2016</YEAR>
 <MONTH>04</MONTH>
 <DAY>19</DAY>
 </WORK_DATE_DOCUMENT>
 <WORK_HAS_EXPRESSION type="link">...</WORK_HAS_EXPRESSION>
 <WORK_HAS_EXPRESSION type="link">...</WORK_HAS_EXPRESSION>
 <IS_ABOUT type="concept">...</IS_ABOUT>
 <WORK_HAS_EXPRESSION type="link">...</WORK_HAS_EXPRESSION>
 <WORK_HAS_EXPRESSION type="link">...</WORK_HAS_EXPRESSION>
 <RESOURCE_LEGAL_IS_ABOUT_SUBJECT-MATTER type="concept_level">
 <RESOURCE_LEGAL_IS_ABOUT_SUBJECT-MATTER_1 type="concept">
 <URI>...</URI>
 <OP-CODE>INFQ</OP-CODE>
 <IDENTIFIER>INFQ</IDENTIFIER>
 <PREFLABEL>Electronic data processing</PREFLABEL>
 </RESOURCE_LEGAL_IS_ABOUT_SUBJECT-MATTER_1>
 </RESOURCE_LEGAL_IS_ABOUT_SUBJECT-MATTER>
 <WORK_PART_OF_COLLECTION_DOCUMENT type="concept">...</WORK_PART_OF_COLLECTION_DOCUMENT>
 <WORK_CITES_WORK type="link">
 <URI>
 <VALUE>
 http://publications.europa.eu/resource/cellar/fdd70f57-7032-4121-92ae-ccf8ef68c15b
 </VALUE>
 <IDENTIFIER>fdd70f57-7032-4121-92ae-ccf8ef68c15b</IDENTIFIER>
 <TYPE>cellar</TYPE>
 </URI>
 <SAMEAS>
 <URI>
 <VALUE>
 http://publications.europa.eu/resource/oj/JOL_2008_218_R_0030_01
 </VALUE>
 <IDENTIFIER>JOL_2008_218_R_0030_01</IDENTIFIER>
 <TYPE>oj</TYPE>
 </URI>
 </SAMEAS>
 <SAMEAS>...</SAMEAS>
 <SAMEAS>...</SAMEAS>
 </WORK_CITES_WORK>
 <RESOURCE_LEGAL_YEAR type="data">
 <VALUE>2016</VALUE>
 </RESOURCE_LEGAL_YEAR>
 <WORK_HAS_EXPRESSION type="link">...</WORK_HAS_EXPRESSION>
 <WORK_HAS_EXPRESSION type="link">...</WORK_HAS_EXPRESSION>
 <WORK_CITES_WORK type="link">
 <URI>
 <VALUE>
 http://publications.europa.eu/resource/cellar/f5ad287a-f31e-450f-ad66-c727d4c34681
 </VALUE>
 <IDENTIFIER>f5ad287a-f31e-450f-ad66-c727d4c34681</IDENTIFIER>
 <TYPE>cellar</TYPE>
 </URI>
 <SAMEAS>
 <URI>
 <VALUE>
 http://publications.europa.eu/resource/comnat/COM_2013_0048_FIN
 </VALUE>
 <IDENTIFIER>COM_2013_0048_FIN</IDENTIFIER>
 <TYPE>comnat</TYPE>
 </URI>
 </SAMEAS>
 <SAMEAS>...</SAMEAS>
 <SAMEAS>...</SAMEAS>
 </WORK_CITES_WORK>
 <WORK_IS_ABOUT_CONCEPT_EUROVOC type="concept_facet">...</WORK_IS_ABOUT_CONCEPT_EUROVOC>
 <IS_ABOUT type="concept">...</IS_ABOUT>
 <WORK_CITES_WORK type="link">
 <URI>...</URI>
 <SAMEAS>
 <URI>
 <VALUE>
 http://publications.europa.eu/resource/comnat/COM_2012_0045_FIN
 </VALUE>
 <IDENTIFIER>COM_2012_0045_FIN</IDENTIFIER>
 <TYPE>comnat</TYPE>
 </URI>
 </SAMEAS>
 <SAMEAS>...</SAMEAS>
 <SAMEAS>...</SAMEAS>
 </WORK_CITES_WORK>
 <WORK_HAS_EXPRESSION type="link">...</WORK_HAS_EXPRESSION>
 <IDENTIFIER type="data">...</IDENTIFIER>
 <WORK_HAS_EXPRESSION type="link">...</WORK_HAS_EXPRESSION>
 <WORK_HAS_EXPRESSION type="link">...</WORK_HAS_EXPRESSION>
 <WORK_CREATED_BY_AGENT type="concept">
 <URI>
 <VALUE>
 http://publications.europa.eu/resource/authority/corporate-body/COM
 </VALUE>
 <IDENTIFIER>COM</IDENTIFIER>
 <TYPE>corporate-body</TYPE>
 </URI>
 <OP-CODE>COM</OP-CODE>
 <IDENTIFIER>COM</IDENTIFIER>
 <PREFLABEL>European Commission</PREFLABEL>
 <ALTLABEL>Commission</ALTLABEL>
 </WORK_CREATED_BY_AGENT>
 <RESOURCE_LEGAL_IS_ABOUT_CONCEPT_DIRECTORY-CODE type="concept_level">...</RESOURCE_LEGAL_IS_ABOUT_CONCEPT_DIRECTORY-CODE>
 <WORK_IS_ABOUT_CONCEPT_EUROVOC type="concept_facet">...</WORK_IS_ABOUT_CONCEPT_EUROVOC>
 <IS_ABOUT type="concept">...</IS_ABOUT>
 <WORK_CITES_WORK type="link">
 <URI>...</URI>
 <SAMEAS>...</SAMEAS>
 <SAMEAS>
 <URI>
 <VALUE>
 http://publications.europa.eu/resource/oj/JOL_2007_108_R_0001_01
 </VALUE>
 <IDENTIFIER>JOL_2007_108_R_0001_01</IDENTIFIER>
 <TYPE>oj</TYPE>
 </URI>
 </SAMEAS>
 <SAMEAS>...</SAMEAS>
 </WORK_CITES_WORK>
 <IDENTIFIER type="data">
 <VALUE>comnat:COM_2016_0178_FIN</VALUE>
 </IDENTIFIER>
 <WORK_CITES_WORK type="link">
 <URI>...</URI>
 <SAMEAS>
 <URI>
 <VALUE>
 http://publications.europa.eu/resource/comnat/COM_2015_0192_FIN
 </VALUE>
 <IDENTIFIER>COM_2015_0192_FIN</IDENTIFIER>
 <TYPE>comnat</TYPE>
 </URI>
 </SAMEAS>
 <SAMEAS>...</SAMEAS>
 <SAMEAS>...</SAMEAS>
 </WORK_CITES_WORK>
 <WORK_CITES_WORK type="link">
 <URI>...</URI>
 <SAMEAS>
 <URI>
 <VALUE>
 http://publications.europa.eu/resource/oj/JOL_2011_088_R_0045_01
 </VALUE>
 <IDENTIFIER>JOL_2011_088_R_0045_01</IDENTIFIER>
 <TYPE>oj</TYPE>
 </URI>
 </SAMEAS>
 <SAMEAS>...</SAMEAS>
 <SAMEAS>...</SAMEAS>
 </WORK_CITES_WORK>
 <WORK_HAS_EXPRESSION type="link">...</WORK_HAS_EXPRESSION>
 <ID_SECTOR type="data">
 <VALUE>5</VALUE>
 </ID_SECTOR>
 <WORK_HAS_EXPRESSION type="link">...</WORK_HAS_EXPRESSION>
 <RESOURCE_LEGAL_DATE_DISPATCH type="date">
 <VALUE>2016-04-19</VALUE>
 <YEAR>2016</YEAR>
 <MONTH>04</MONTH>
 <DAY>19</DAY>
 <ANNOTATION>...</ANNOTATION>
 <ANNOTATION>...</ANNOTATION>
 </RESOURCE_LEGAL_DATE_DISPATCH>
 <WORK_CITES_WORK type="link">
 <URI>...</URI>
 <SAMEAS>...</SAMEAS>
 <SAMEAS>
 <URI>
 <VALUE>
 http://publications.europa.eu/resource/com/COM_2012_029
 </VALUE>
 <IDENTIFIER>COM_2012_029</IDENTIFIER>
 <TYPE>com</TYPE>
 </URI>
 </SAMEAS>
 <SAMEAS>...</SAMEAS>
 </WORK_CITES_WORK>
 <WORK_HAS_EXPRESSION type="link">...</WORK_HAS_EXPRESSION>
 <WORK_IS_ABOUT_CONCEPT_EUROVOC type="concept_facet">...</WORK_IS_ABOUT_CONCEPT_EUROVOC>
 <IS_ABOUT type="concept">...</IS_ABOUT>
 <IS_ABOUT type="concept">...</IS_ABOUT>
 <WORK_HAS_EXPRESSION type="link">...</WORK_HAS_EXPRESSION>
 <IS_ABOUT type="concept">...</IS_ABOUT>
 <IS_ABOUT type="concept">...</IS_ABOUT>
 <CREATED_BY type="concept">
 <URI>...</URI>
 <OP-CODE>CNECT</OP-CODE>
 <IDENTIFIER>CNECT</IDENTIFIER>
 <PREFLABEL>
 Directorate-General for Communications Networks, Content and Technology
 </PREFLABEL>
 <ALTLABEL>DG Communications Networks, Content and Technology</ALTLABEL>
 </CREATED_BY>
 <WORK_HAS_RESOURCE-TYPE type="concept">
 <URI>...</URI>
 <OP-CODE>COMMUNIC</OP-CODE>
 <IDENTIFIER>COMMUNIC</IDENTIFIER>
 <PREFLABEL>Communication</PREFLABEL>
 </WORK_HAS_RESOURCE-TYPE>
 <RESOURCE_LEGAL_TYPE type="data">...</RESOURCE_LEGAL_TYPE>
 <SERVICE_RESPONSIBLE type="data">...</SERVICE_RESPONSIBLE>
 <WORK_CITES_WORK type="link">
 <URI>...</URI>
 <SAMEAS>
 <URI>
 <VALUE>...</VALUE>
 <IDENTIFIER>SWD_2015_0111_FIN</IDENTIFIER>
 <TYPE>comnat</TYPE>
 </URI>
 </SAMEAS>
 <SAMEAS>...</SAMEAS>
 <SAMEAS>
 <URI>
 <VALUE>
 http://publications.europa.eu/resource/swd/SWD_2015_0111
 </VALUE>
 <IDENTIFIER>SWD_2015_0111</IDENTIFIER>
 <TYPE>swd</TYPE>
 </URI>
 </SAMEAS>
 </WORK_CITES_WORK>
 <WORK_IS_ABOUT_CONCEPT_EUROVOC type="concept_facet">...</WORK_IS_ABOUT_CONCEPT_EUROVOC>
 <WORK_CITES_WORK type="link">
 <URI>
 <VALUE>
 http://publications.europa.eu/resource/cellar/e0df1a5a-01ea-11e4-831f-01aa75ed71a1
 </VALUE>
 <IDENTIFIER>e0df1a5a-01ea-11e4-831f-01aa75ed71a1</IDENTIFIER>
 <TYPE>cellar</TYPE>
 </URI>
 <SAMEAS>
 <URI>
 <VALUE>
 http://publications.europa.eu/resource/com/COM_2014_508
 </VALUE>
 <IDENTIFIER>COM_2014_508</IDENTIFIER>
 <TYPE>com</TYPE>
 </URI>
 </SAMEAS>
 <SAMEAS>
 <URI>
 <VALUE>
 http://publications.europa.eu/resource/celex/52014DC0442
 </VALUE>
 <IDENTIFIER>52014DC0442</IDENTIFIER>
 <TYPE>celex</TYPE>
 </URI>
 </SAMEAS>
 <SAMEAS>
 <URI>
 <VALUE>
 http://publications.europa.eu/resource/comnat/COM_2014_0442_FIN
 </VALUE>
 <IDENTIFIER>COM_2014_0442_FIN</IDENTIFIER>
 <TYPE>comnat</TYPE>
 </URI>
 </SAMEAS>
 </WORK_CITES_WORK>
 <RESOURCE_LEGAL_IS_ABOUT_SUBJECT-MATTER type="concept_level">
 <RESOURCE_LEGAL_IS_ABOUT_SUBJECT-MATTER_1 type="concept">
 <URI>
 <VALUE>
 http://publications.europa.eu/resource/authority/fd_070/TECN
 </VALUE>
 <IDENTIFIER>TECN</IDENTIFIER>
 <TYPE>fd_070</TYPE>
 </URI>
 <OP-CODE>TECN</OP-CODE>
 <IDENTIFIER>TECN</IDENTIFIER>
 <PREFLABEL>Technology</PREFLABEL>
 </RESOURCE_LEGAL_IS_ABOUT_SUBJECT-MATTER_1>
 </RESOURCE_LEGAL_IS_ABOUT_SUBJECT-MATTER>
 <WORK_HAS_EXPRESSION type="link">...</WORK_HAS_EXPRESSION>
 <WORK_IS_ABOUT_CONCEPT_EUROVOC type="concept_facet">...</WORK_IS_ABOUT_CONCEPT_EUROVOC>
 <WORK_IS_ABOUT_CONCEPT_EUROVOC type="concept_facet">...</WORK_IS_ABOUT_CONCEPT_EUROVOC>
 <WORK_CREATED_BY_AGENT type="concept">...</WORK_CREATED_BY_AGENT>
 <WORK_IS_ABOUT_CONCEPT_EUROVOC type="concept_facet">...</WORK_IS_ABOUT_CONCEPT_EUROVOC>
 <WORK_HAS_EXPRESSION type="link">...</WORK_HAS_EXPRESSION>
 <WORK_IS_ABOUT_CONCEPT_EUROVOC type="concept_facet">...</WORK_IS_ABOUT_CONCEPT_EUROVOC>
 <IS_ABOUT type="concept">...</IS_ABOUT>
 <RESOURCE_LEGAL_ID_CELEX type="data">...</RESOURCE_LEGAL_ID_CELEX>
 <WORK_HAS_EXPRESSION type="link">...</WORK_HAS_EXPRESSION>
 <WORK_CITES_WORK type="link">
 <URI>
 <VALUE>
 http://publications.europa.eu/resource/cellar/0da3b161-b97b-4ea9-9e3c-676d534b7407
 </VALUE>
 <IDENTIFIER>0da3b161-b97b-4ea9-9e3c-676d534b7407</IDENTIFIER>
 <TYPE>cellar</TYPE>
 </URI>
 <SAMEAS>
 <URI>
 <VALUE>
 http://publications.europa.eu/resource/celex/12012P008
 </VALUE>
 <IDENTIFIER>12012P008</IDENTIFIER>
 <TYPE>celex</TYPE>
 </URI>
 </SAMEAS>
 </WORK_CITES_WORK>
 <DATE_CREATION_LEGACY type="date">...</DATE_CREATION_LEGACY>
 <IDENTIFIER type="data">...</IDENTIFIER>
 <DOCUMENT_COM_OTHER_EC_SERVICE_RESPONSIBLE type="data">...</DOCUMENT_COM_OTHER_EC_SERVICE_RESPONSIBLE>
 <WORK_CITES_WORK type="link">
 <URI>
 <VALUE>
 http://publications.europa.eu/resource/cellar/6c9b0d4b-831f-47fe-b551-ac13604d9797
 </VALUE>
 <IDENTIFIER>6c9b0d4b-831f-47fe-b551-ac13604d9797</IDENTIFIER>
 <TYPE>cellar</TYPE>
 </URI>
 <SAMEAS>...</SAMEAS>
 <SAMEAS>...</SAMEAS>
 <SAMEAS>...</SAMEAS>
 </WORK_CITES_WORK>
 <WORK_HAS_EXPRESSION type="link">...</WORK_HAS_EXPRESSION>
 <IS_ABOUT type="concept">...</IS_ABOUT>
 <RESOURCE_LEGAL_HAS_TYPE_ACT_CONCEPT_TYPE_ACT type="concept">...</RESOURCE_LEGAL_HAS_TYPE_ACT_CONCEPT_TYPE_ACT>
 <WORK_HAS_EXPRESSION type="link">...</WORK_HAS_EXPRESSION>
 <WORK_CITES_WORK type="link">...</WORK_CITES_WORK>
 <IS_ABOUT type="concept">...</IS_ABOUT>
 <ID_CELEX type="data">...</ID_CELEX>
 <TYPE>DC</TYPE>
 <TYPE>cdm:work</TYPE>
 <TYPE>cdm:resource_legal</TYPE>
 </WORK>
 <INVERSE complete="true">
 <RESOURCE_LEGAL_RELATED_TO_RESOURCE_LEGAL type="link">
 <URI>
 <VALUE>
 http://publications.europa.eu/resource/cellar/6bf401a1-0612-11e6-b713-01aa75ed71a1
 </VALUE>
 <IDENTIFIER>6bf401a1-0612-11e6-b713-01aa75ed71a1</IDENTIFIER>
 <TYPE>cellar</TYPE>
 </URI>
 <SAMEAS>...</SAMEAS>
 <SAMEAS>...</SAMEAS>
 <ANNOTATION>...</ANNOTATION>
 </RESOURCE_LEGAL_RELATED_TO_RESOURCE_LEGAL>
 <WORK_RELATED_TO_WORK type="link">...</WORK_RELATED_TO_WORK>
 <RESOURCE_LEGAL_RELATED_TO_RESOURCE_LEGAL type="link">...</RESOURCE_LEGAL_RELATED_TO_RESOURCE_LEGAL>
 <WORK_RELATED_TO_WORK type="link">
 <URI>...</URI>
 <SAMEAS>...</SAMEAS>
 <SAMEAS>...</SAMEAS>
 </WORK_RELATED_TO_WORK>
 </INVERSE>
 <EXPRESSION>
 <URI>...</URI>
 <SAMEAS>...</SAMEAS>
 <EXPRESSION_MANIFESTED_BY_MANIFESTATION type="link">...</EXPRESSION_MANIFESTED_BY_MANIFESTATION>
 <EXPRESSION_USES_LANGUAGE type="concept">...</EXPRESSION_USES_LANGUAGE>
 <TITLE type="data">...</TITLE>
 <EXPRESSION_MANIFESTED_BY_MANIFESTATION type="link">...</EXPRESSION_MANIFESTED_BY_MANIFESTATION>
 <TITLE type="data">...</TITLE>
 <LANG type="data">...</LANG>
 <LANG type="data">...</LANG>
 <LASTMODIFICATIONDATE type="date">...</LASTMODIFICATIONDATE>
 <EXPRESSION_TITLE type="data">
 <VALUE>
 COMMUNICATION FROM THE COMMISSION TO THE EUROPEAN PARLIAMENT, THE COUNCIL, THE EUROPEAN ECONOMIC AND SOCIAL COMMITTEE AND THE COMMITTEE OF THE REGIONS European Cloud Initiative - Building a competitive data and knowledge economy in Europe
 </VALUE>
 </EXPRESSION_TITLE>
 <EXPRESSION_BELONGS_TO_WORK type="link">...</EXPRESSION_BELONGS_TO_WORK>
 <EXPRESSION_MANIFESTED_BY_MANIFESTATION type="link">...</EXPRESSION_MANIFESTED_BY_MANIFESTATION>
 <EXPRESSION_SUBTITLE type="data">
 <VALUE>COM/2016/0178 final</VALUE>
 </EXPRESSION_SUBTITLE>
 </EXPRESSION>
 <MANIFESTATION manifestation-type="docx">
 <URI>
 <VALUE>
 http://publications.europa.eu/resource/cellar/47cc5480-0612-11e6-b713-01aa75ed71a1.0009.01
 </VALUE>
 <IDENTIFIER>47cc5480-0612-11e6-b713-01aa75ed71a1.0009.01</IDENTIFIER>
 <TYPE>cellar</TYPE>
 </URI>
 <SAMEAS>...</SAMEAS>
 <MANIFESTATION_MANIFESTS_EXPRESSION type="link">...</MANIFESTATION_MANIFESTS_EXPRESSION>
 <MANIFESTATION_HAS_ITEM>...</MANIFESTATION_HAS_ITEM>
 <MANIFESTATION_TYPE type="data">
 <VALUE>docx</VALUE>
 </MANIFESTATION_TYPE>
 <LASTMODIFICATIONDATE type="date">
 <VALUE>2016-04-19T11:37:06.254+02:00</VALUE>
 <YEAR>2016</YEAR>
 <MONTH>04</MONTH>
 <DAY>19</DAY>
 </LASTMODIFICATIONDATE>
 <TYPE>docx</TYPE>
 </MANIFESTATION>
 <MANIFESTATION manifestation-type="xhtml">...</MANIFESTATION>
 <MANIFESTATION manifestation-type="pdf">...</MANIFESTATION>
 </NOTICE>*/