import soap from 'soap'
const url = 'http://eur-lex.europa.eu/eurlex-ws?wsdl';
const soapOptions= {'forceSoap12Headers': true};
import {password} from './../../../../secret/password';
import util from 'util';
import fs from 'fs';
import Promise from 'promise';

import { db, aqlQuery } from '../../arangodb';
import { checkCreateNodeAndEdges, checkCreateNodeAndEdgesByKey, checkExistsEdgeOrCreateOrUpdate, checkExistsAndCreateEdgesTwoWay,
    checkCreateNodeAndEdgesMultipleColl, checkCreateNodeAndEdgesFromArrayByKey, checkExistsAndCreateEdgesTwoWayFromArray } from '../../utils/createNodeAndEdges';
import { checkExistsByName, checkEdgeExists, checkExistsByKey, checkExistsMultipleColl, checkExistsByAttr } from '../../utils/checkExists';
import { checkExistsAndCreateNodeByKey, checkExistsAndCreateNodeByName} from '../../utils/checkExistsAndCreate';
import { searchForDocuments, fromArrToID, fromIDToArr, inverseDocumentRelationship, logObjectWithoutFullText, searchDocumentsSubTitle } from '../../utils/documentUtils';
import {nowToISOString, dateFormatFunc } from '../../utils/dateFormat'
import {typeofUndefined } from '../../utils/typeofUndefined'
import { retrieveAuthors, getLegalBasis, getManifestations, checkCelexKey, getType, getFullText, getAddressees, getDossierDocIDs,
    getEventContainsWork, getURIfromArr, getEurovocDossierIDs, getDocumentRelationships, getEventObject, getEventObjectLogged,
    getEurovocIDs, translateCOMDocumentCodes, filterNonCelex } from '../../utils/EurlexUtils'

const documentsColl = db.collection('documents');
const dossierColl = db.collection('dossiers');

const partOfEdge = db.edgeCollection('partOf');
const hasPartEdge = db.edgeCollection('hasPart');
const authorOfEdge = db.edgeCollection('authorOf');
const authoredByEdge = db.edgeCollection('authoredBy');
const consultsEdge = db.edgeCollection('consults');
const consultedByEdge = db.edgeCollection('consultedBy');
const documentRelationshipsEdge = db.edgeCollection('documentRelationships');

let dateFormat = dateFormatFunc('/');
// let dateFormat = '2016/10/06';
let language3 = 'eng';
let language2 = 'en';

let bulkArr = [];
let pageNumber = 1;
let pageSize = 50;
let year = 2016;
let numResults;
// let numResults = 462;

//TODO write function that adds year automatically, now just done manually
let queryDossierImport2010till2016 = `
SELECT
LP_DOSSIER_TI_DISPLAY, LP_DOSSIER_ID, LP_PROC_REFERENCE, LP_DOCUMENT_REF, LP_INITIATING_DISPLAY, LP_ADOPTED_ACT_DISPLAY, LP_ACTING_BODY, LP_AGENT, LP_ADOPTED_FL, LP_AUTHORISING_AGENT, 
        LP_BODY_PENDING_CODED, LP_BODY_RL_ADDR, LP_BODY_RL_ADDR_FOR, LP_BODY_RL_ADDR_INFO, LP_BODY_RL_ASSOC, LP_BODY_RL_AUT_OPINION, LP_BODY_RL_AUT_REPORT, LP_BODY_RL_CONSULT,
         LP_BODY_RL_JOINT_RESP, LP_BODY_RL_MAND_CONSULT, LP_BODY_RL_OPT_CONSULT, LP_BODY_RL_PRI_RESP, LP_BODY_ROLE, LP_BODY_ROLE_EXP, LP_CODE_LEGAL_BASIS_ART, 
         LP_DC, LP_DC_MTH, LP_DC_TT, LP_DECISION, LP_DECISION_MODE, 
         LP_EVENT_DATE_DISPLAY, LP_EVENT_DOC_CELEX, LP_EVENT_ID, LP_EVENT_TYPE, LP_FILE_TYPE, LP_ID, LP_INITIATING_DISPLAY, LP_INTER_CODE_TYPE, LP_INTER_CODE_NUM, LP_INTER_CODE_YEAR, LP_LEGAL_BASIS_DISPLAY,
         LP_PERSON_DRAFTSMAN, LP_PERSON_JOINT_RESP, LP_PERSON_RAPP, LP_PERSON_RESP, LP_PROC_DISPLAY, LP_PROC_TYP, LP_SECONDARY_LEG,  
         LP_WITHDRAWN_FL, LP_PENDING_FL
    WHERE
DTS_SUBDOM = LEGAL_PROCEDURE AND LP_INTER_CODE_YEAR = ${year} 
AND (LP_INTER_CODE_TYPE_CODED = CNS OR SLP OR ACC OR CNC OR SYN OR COD OR OLP OR APP OR AVC OR CNB OR NLE OR PRT)`;

let queryDossier = `
SELECT
      LP_DOSSIER_TI_DISPLAY, LP_DOSSIER_ID, LP_PROC_REFERENCE, LP_DOCUMENT_REF, LP_INITIATING_DISPLAY, LP_ADOPTED_ACT_DISPLAY, LP_ACTING_BODY, LP_AGENT, LP_ADOPTED_FL, LP_AUTHORISING_AGENT, 
        LP_BODY_PENDING_CODED, LP_BODY_RL_ADDR, LP_BODY_RL_ADDR_FOR, LP_BODY_RL_ADDR_INFO, LP_BODY_RL_ASSOC, LP_BODY_RL_AUT_OPINION, LP_BODY_RL_AUT_REPORT, LP_BODY_RL_CONSULT,
         LP_BODY_RL_JOINT_RESP, LP_BODY_RL_MAND_CONSULT, LP_BODY_RL_OPT_CONSULT, LP_BODY_RL_PRI_RESP, LP_BODY_ROLE, LP_BODY_ROLE_EXP, LP_CODE_LEGAL_BASIS_ART, 
         LP_DC, LP_DC_MTH, LP_DC_TT, LP_DECISION, LP_DECISION_MODE, 
         LP_EVENT_DATE_DISPLAY, LP_EVENT_DOC_CELEX, LP_EVENT_ID, LP_EVENT_TYPE, LP_FILE_TYPE, LP_ID, LP_INITIATING_DISPLAY, LP_INTER_CODE_TYPE, LP_INTER_CODE_NUM, LP_INTER_CODE_YEAR, LP_LEGAL_BASIS_DISPLAY,
         LP_PERSON_DRAFTSMAN, LP_PERSON_JOINT_RESP, LP_PERSON_RAPP, LP_PERSON_RESP, LP_PROC_DISPLAY, LP_PROC_TYP, LP_SECONDARY_LEG,  
         LP_WITHDRAWN_FL, LP_PENDING_FL
    WHERE
(DTS_SUBDOM = LEGAL_PROCEDURE  ) AND (XC = ${dateFormat} OR XA = ${dateFormat} OR LP_EVENT_DATE = ${dateFormat}) ORDER BY XA DESC`;

function getQuery(pageNum) {
    return {
        // 'expertQuery': queryDossier,
        'expertQuery': queryDossier,
        'page': pageNum,
        'pageSize': pageSize,
        'searchLanguage': language2
    };
}

let client;

function createClient() {
    return new Promise((resolve, reject) => {
        soap.createClient(url, soapOptions, function (err, createdClient) {
            if (err) reject(err);
            client = createdClient;
            client.addSoapHeader('<wsse:Security soap:mustUnderstand="true" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"><wsse:UsernameToken wsu:Id="UsernameToken-2783C6C748D7FA98E414596887803756"><wsse:Username>nsevatpa</wsse:Username><wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'+password+'</wsse:Password></wsse:UsernameToken></wsse:Security>');
            resolve(createdClient);
        });    
    })
}

function getBulkArr(query) {
    return new Promise((resolve, reject) => {
        client.doQuery(query, function (err, result) {
            if (err) reject(err);
            // console.log(util.inspect(result, false, null));
            console.log('\nretrieving batch '+query.page+', number of hits for this batch: '+result['numhits']+', total hits: '+result['totalhits']+' date: '+dateFormat);
            numResults = result.totalhits;
            resolve(result.result);
        })    
    })
}

async function batchController() {
    try {
        // let wstream = fs.createWriteStream('dossier-2016-'+pageNumber+'-50.json');
        client = await createClient();
        let query = getQuery(pageNumber);
        bulkArr = await getBulkArr(query);

        if (!bulkArr) {
            console.log('no results');
            return;
        }
            
        
        await asyncLoopDossiers(false);
        // wstream.write(JSON.stringify(result));
        // wstream.end(() => {
        //     console.log('done writing');
        //     // asyncLoopDossiers(false);
        // });
        if (((pageNumber-1)*pageSize)+bulkArr.length < numResults) {
            console.log('Old pagenum: '+pageNumber);
            pageNumber++;
            console.log('New pagenum: '+pageNumber);
            batchController();
        }
        else {
            console.log('\nALL DONE')
        }
    }
    catch (err) {
        console.log('CAUGHT ERR IN batchController:');
        console.log(err);
    }
}

batchController();

function getLocalFile(path) {
    return new Promise((resolve, reject) => {
        fs.readFile(path, 'utf8', (err, res) => {
            if (err) reject(err);
            else {
                console.log('data retrieved');
                resolve(JSON.parse(res))
            }
        });
    })
}

async function startDossierTestLoop() {
    bulkArr = await getLocalFile('dossier-2013-1-50.json');
    // console.log(typeof bulkArr);
    bulkArr = bulkArr.result;
    // bulkArr = bulkArr.slice(23,24);
    // console.log(Array.isArray(bulkArr));
    // console.log(bulkArr.length);
    // console.dir(bulkArr);
    asyncLoopDossiers();
}

// startDossierTestLoop();

let IDarr = [];

async function asyncLoopDossiers(logging) {
    try {
        for (let i = 0; i<bulkArr.length; i++) {
            console.log('Iteration: '+i+', pageNumber'+pageNumber);
            const data = bulkArr[i].content.NOTICE;
            // console.log('\nData:');
            // console.dir(data);
            // const dossier = getDossier(data);
            const dossier = getDossier(data, logging);
            // console.log(util.inspect(dossier, false, null));
            dossier.cellar = typeofUndefined(bulkArr[i], 'reference');
            dossier.cellar = dossier.cellar.slice(dossier.cellar.search(':')+1, dossier.cellar.lastIndexOf('_'));
            dossier._key = dossier.cellar;
            dossier.level = 'international';
            dossier.source = 'Eurlex';
            dossier.dateImported = nowToISOString();
            dossier.country = 'EU';
            console.log('getDossier passed: '+dossier._key+' interinsti: '+dossier.interinstitutionalReference);
            if (IDarr.indexOf(dossier._key) === -1)
                IDarr.push(dossier._key);
            else if (IDarr.indexOf(dossier._key) > -1)
                throw new Error('ERROR duplicate key processed');

            let dossierSaved = await checkExistsAndCreateNodeByKey(dossier._key, dossierColl, db, aqlQuery, dossier);
            console.log('save completed: '+dossier.interinstitutionalReference+ ' cellar: '+dossier._key);
            //Search initiating doc by dossier.initiatedByCelex cross referencing documents.celex
            //Creating the initiating document in advance is not possible due to absence of cellar key
            // console.log(typeof dossier.initiatedByCelex);
            console.log(dossier.initiatedByCelex);
            if (dossier.initiatedByCelex && dossier.initiatedByCelex !== '' && dossier.initiatedByCelex.length !== 0) {
                var initiatedIDsArr = await searchForDocuments(dossier.initiatedByCelex, db, aqlQuery, ['documents'], 'celex');
                // console.log('initiated IDs arr retrieved, length: '+initiatedIDsArr.length);
                if (initiatedIDsArr.length > 0) {
                    await checkExistsAndCreateEdgesTwoWay(dossierSaved._id, initiatedIDsArr[0], [hasPartEdge, {type: 'dossier initiated by'}],
                        [partOfEdge, {type: 'initiates dossier'}], db, aqlQuery);
                    // console.log('initiated IDs arr edges created');
                }    
            }
            

            //Search adopted act by dossier.adoptedAct cross referencing documents.celex
            //adopted possibly has multiple values
            if (dossier.adoptedAct && dossier.adoptedAct !== '' && dossier.adoptedAct.length !== 0) {
                var adoptedIDsArr = await searchForDocuments(dossier.adoptedAct, db, aqlQuery, ['documents'], 'celex');
                // console.log('adoptedIDsArr retrieved, length: '+adoptedIDsArr.length);
                if (adoptedIDsArr.length > 0) {
                    let fromToArr = fromIDToArr(dossierSaved._id, adoptedIDsArr);
                    await checkExistsAndCreateEdgesTwoWayFromArray(fromToArr, [hasPartEdge, {type: 'adopted act'}],
                        [partOfEdge, {type: 'adopted from legislative procedure'}], db, aqlQuery);
                    // console.log('adoptedIDs edges created');
                }
            }
            
            //TODO add eurovoc
            //TODO add rapporteur and responsible person

            //Loop over event and create edges where necessary
            // console.log('calling asyncEventLoop');
            await asyncEventLoop(dossier.events, dossierSaved._id, logging);
            // console.log('asyncEventLoop finished');
            
            console.log('Finished processing iteration '+i+' in Dossiers, pageNumber'+pageNumber+'\n'+', total hits: '+numResults);
            
            if (i === bulkArr.length -1) console.log('All done dossier iterations');
        }
        return new Promise((resolve, reject) => resolve())
    }
    catch (err) {
        return new Promise((resolve, reject) => reject(err))
    }
}

//TODO rewrite this function cleaner, much repeated code
async function asyncEventLoop(events, dossierID, logBool) {
    try {
        for (let i=0; i<events.length; i++) {
            if (logBool) console.log('event iteration: '+i+', out of: '+events.length);
            let event = events[i];
            if (i == 6) console.dir(event); 

            if (event.eventContainsWork !== '') {
                let celexOnlyArr = filterNonCelex(event.eventContainsWork);
                let COMcodeOnlyArr = translateCOMDocumentCodes(event.eventContainsWork);
                if (logBool) console.log('celex only workIDs: '+celexOnlyArr);
                if (logBool) console.log('com only doc IDs: '+COMcodeOnlyArr);
                let eventIDsArr = [];
                let eventIDsArr2 = [];
                if (celexOnlyArr.length > 0) eventIDsArr = await searchForDocuments(celexOnlyArr, db, aqlQuery, ['documents'], 'celex');
                if (COMcodeOnlyArr.length > 0) eventIDsArr2 = await searchDocumentsSubTitle(COMcodeOnlyArr, db, aqlQuery);
                let combinedEventIDsArr = eventIDsArr.concat(eventIDsArr2);
                if (logBool) console.log('combined documents IDs: '+combinedEventIDsArr);
                if (combinedEventIDsArr.length > 0) {
                    let fromToArr = fromIDToArr(dossierID, combinedEventIDsArr);
                    await checkExistsAndCreateEdgesTwoWayFromArray(fromToArr, [hasPartEdge, {type: 'dossier event contains work'}],
                        [partOfEdge, {type: 'work part of dossier event'}], db, aqlQuery);
                    if (logBool) console.log('created eventcontainsWork edges');
                }
            }
            if (event.documentID !== '') {
                let celexOnlyArr = filterNonCelex(event.documentID);
                let COMcodeOnlyArr = translateCOMDocumentCodes(event.documentID);
                if (logBool) console.log('celex only doc IDs: '+celexOnlyArr);
                if (logBool) console.log('com only doc IDs: '+COMcodeOnlyArr);
                let eventIDsArr = [];
                let eventIDsArr2 = [];
                if (celexOnlyArr.length > 0) eventIDsArr = await searchForDocuments(celexOnlyArr, db, aqlQuery, ['documents'], 'celex');
                if(COMcodeOnlyArr.length > 0) eventIDsArr2 = await searchDocumentsSubTitle(COMcodeOnlyArr, db, aqlQuery);
                // console.log('pre concat');
                let combinedEventIDsArr = eventIDsArr.concat(eventIDsArr2);
                if (logBool) console.log('combined documents IDs: '+combinedEventIDsArr);
                if (combinedEventIDsArr.length > 0) {
                    let fromToArr = fromIDToArr(dossierID, combinedEventIDsArr);
                    await checkExistsAndCreateEdgesTwoWayFromArray(fromToArr, [hasPartEdge, {type: 'dossier event contains work'}],
                        [partOfEdge, {type: 'work part of dossier event'}], db, aqlQuery);
                    if (logBool) console.log('create documentIDs edges');
                }
            }
            if (event.rapporteur !== '') {
                let personIDArr = await searchForDocuments(event.rapporteur, db, aqlQuery, ['people']);
                if (logBool) console.log('rapporteur ID: '+personIDArr);
                if (typeof personIDArr[0] !== 'undefined') {
                    await checkExistsAndCreateEdgesTwoWay(dossierID, personIDArr[0], [partOfEdge, {type: 'event has rapporteur'}],
                        [hasPartEdge, {type: 'rapporteur for dossier event'}], db, aqlQuery);
                    if (logBool) console.log('created rapporteur edges');
                }

            }
            if (event.responsiblePerson !== '') {
                let personIDArr = await searchForDocuments(event.responsiblePerson, db, aqlQuery, ['people']);
                if (logBool) console.log('responsible personID: '+personIDArr);
                if (typeof personIDArr[0] !== 'undefined') {
                    await checkExistsAndCreateEdgesTwoWay(dossierID, personIDArr[0], [partOfEdge, {type: 'event has responsible person'}],
                        [hasPartEdge, {type: 'responsible person for dossier event'}], db, aqlQuery);
                    if (logBool) console.log('created responsible person edges');
                }
            }
            if (event.optionallyConsults !== '') {
                let consultsIDArr = await searchForDocuments(event.optionallyConsults, db, aqlQuery, ['legislativeBodies', 'executiveBodies', 'otherBodies', 'IGO']);
                if (logBool) console.log('consultsIDarr: '+consultsIDArr);
                if (consultsIDArr.length > 0) {
                    let fromToArr = fromIDToArr(dossierID, consultsIDArr);
                    await checkExistsAndCreateEdgesTwoWayFromArray(fromToArr, [consultsEdge, {type: 'consults'}],
                        [consultedByEdge, {type: 'consulted by dossier'}], db, aqlQuery);
                    if (logBool) console.log('consults edges created');
                }
            }
            if (event.mandatorilyConsults !== '') {
                let consultsIDArr = await searchForDocuments(event.mandatorilyConsults, db, aqlQuery, ['legislativeBodies', 'executiveBodies', 'otherBodies', 'IGO']);
                if (logBool) console.log('manda consults id arr: '+consultsIDArr);
                if (consultsIDArr.length > 0) {
                    let fromToArr = fromIDToArr(dossierID, consultsIDArr);
                    await checkExistsAndCreateEdgesTwoWayFromArray(fromToArr, [consultsEdge, {type: 'consults'}],
                        [consultedByEdge, {type: 'consulted by dossier'}], db, aqlQuery);
                    if (logBool) console.log('manda consults edges created');
                }
            }
            if (i === events.length-1) {
                // console.log('Iterated through all events');
                return new Promise((resolve, reject) => {
                    resolve()
                })
            }
        }
    }
    catch (err) {
        console.error(err);
    }
}

function getEvents(events, logBool) {
    let returnArr = [];
    if (Array.isArray(events)) {
        // console.log('events.length: '+events.length);
        events.forEach((event, index) => {
            let eventObj;
            if (logBool) eventObj  = getEventObjectLogged(event);
            else eventObj = getEventObject(event);
            // console.log('eventObj index '+index+' passed\n');
            // console.log(eventObj);
            returnArr.push(eventObj)
        });
        return returnArr;
    }
    else return getEventObject(events)
}

function getDossier(data, logBool) {
    let dossier = {};
    dossier.interinstitutionalReference = typeofUndefined(data, 'DOSSIER', 'PROCEDURE_CODE_INTERINSTITUTIONAL_REFERENCE_PROCEDURE', 'VALUE');
    dossier.interinstitutionalReference = dossier.interinstitutionalReference.replace(/\//g, '-'); //  '2012/0253/COD' => '2012-0253-COD'
    if (logBool) console.log('\nDossier ID: '+dossier.interinstitutionalReference);
    dossier.title = 'Procedure '+dossier.interinstitutionalReference.replace(/-/g, '/');
    let prepAct = typeofUndefined(data.DOSSIER, 'DOSSIER_INITIATED_BY_ACT_PREPARATORY');
    let embeddedNotice = typeofUndefined(prepAct, 'EMBEDDED_NOTICE');
    dossier.subTitle = typeofUndefined(embeddedNotice, 'EXPRESSION[0]', 'EXPRESSION_TITLE', 'VALUE');
    if (logBool) console.log('Dossier title: '+dossier.title);
    if (logBool) console.log('Dossier subtitle: '+dossier.subtitle);
    
    
    dossier.initiatedByCelex = getURIfromArr(prepAct);
    if (logBool) console.log('Dossier initiatedBy: '+dossier.initiatedByCelex);
    
    //Based-on only exists for old dossiers
    dossier.legalBasis = typeofUndefined(data.DOSSIER, 'BASIS_LEGAL', 'VALUE');
    if (dossier.legalBasis === '') dossier.legalBasis = getURIfromArr(typeofUndefined(data.DOSSIER, 'BASED_ON'));
    if (logBool) console.log('Dossier legal basis: '+dossier.legalBasis);
    // adoptedAct returns celex key
    dossier.adoptedAct = typeof data.DOSSIER.DOSSIER_PRODUCES_RESOURCE_LEGAL !== 'undefined' ? getURIfromArr(data.DOSSIER.DOSSIER_PRODUCES_RESOURCE_LEGAL) : '';
    if (logBool) console.log('Dossier adopted act: '+dossier.adoptedAct);
    
    
    dossier.type = 'dossier';
    dossier.subType = typeof data.DOSSIER.DOSSIER_IS_ABOUT_CONCEPT_TYPE_DOSSIER !== 'undefined' ? data.DOSSIER.DOSSIER_IS_ABOUT_CONCEPT_TYPE_DOSSIER.PREFLABEL : '';
    if (logBool) console.log('Dossier subType: '+dossier.subType);
    dossier.procedure = typeof data.DOSSIER.PROCEDURE_CODE_INTERINSTITUTIONAL_HAS_TYPE_CONCEPT_TYPE_PROCEDURE_CODE_INTERINSTITUTIONAL !== 'undefined' ?
        data.DOSSIER.PROCEDURE_CODE_INTERINSTITUTIONAL_HAS_TYPE_CONCEPT_TYPE_PROCEDURE_CODE_INTERINSTITUTIONAL.PREFLABEL : '';
    if (logBool) console.log('Dossier procedure: '+dossier.procedure);
    dossier.year = typeofUndefined(data.DOSSIER, 'PROCEDURE_CODE_INTERINSTITUTIONAL_YEAR_PROCEDURE', 'VALUE');
    if (logBool) console.log('Dossier year: '+dossier.year);
    dossier.adoptedBoolean = typeof data.DOSSIER['DOSSIER_ADOPTED-PROPOSAL'] !== 'undefined' ? data.DOSSIER['DOSSIER_ADOPTED-PROPOSAL'].VALUE : 'false';
    if (logBool) console.log('Dossier adopted boolean: '+dossier.adoptedBoolean);
    dossier.pendingBoolean = typeof data.DOSSIER['DOSSIER_PENDING-PROPOSAL'] !== 'undefined' ? data.DOSSIER['DOSSIER_PENDING-PROPOSAL'].VALUE : 'false';
    if (logBool) console.log('Dossier pending boolean: '+dossier.pendingBoolean);
    dossier.withdrawnBoolean = typeof data.DOSSIER['DOSSIER_WITHDRAWN-PROPOSAL'] !== 'undefined' ? data.DOSSIER['DOSSIER_WITHDRAWN-PROPOSAL'].VALUE : 'false';
    if (logBool) console.log('Dossier withdrawn boolean: '+dossier.withdrawnBoolean);
    dossier.eurovocIDs = getEurovocDossierIDs(embeddedNotice);
    if (logBool) console.log('Dossier Eurovoc IDs: '+dossier.eurovocIDs);
    dossier.events = getEvents(data.EVENT, logBool);
    if (logBool) console.log('events');
    if (logBool) console.log(dossier.events.length);
    return dossier;
}