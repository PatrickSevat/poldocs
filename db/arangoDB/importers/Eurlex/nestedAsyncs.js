async function depth1() {
    for (let i1 = 0; i1 < 3; i1++) {
        console.log('i1: '+i1);
        await depth2();
    }
    console.log('all done depth 1');
}

async function depth2() {
    for (let i2 = 0; i2 < 3; i2++) {
        console.log('i2: '+i2);
        await depth3();
        if (i2 === 2) {
            return new Promise(function(resolve, reject) {
                resolve();
            })
        }
    }
}

function depth3() {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            console.log('resolving depth 3');
            resolve();
        }, 500)
    })
}

depth1();