import Promise from 'promise';
import {checkExistsByName, checkExistsByKey, checkEdgeExists, checkExistsMultipleColl} from './checkExists';
import {compareObj} from './compareObj';
import { onlyAllowOneDateValue } from './dateFormat'

//TODO see if Upsert helps here https://docs.arangodb.com/3.0/AQL/Operations/Upsert.html

async function checkExistsEdgeOrCreateOrUpdate (edgeColl, fromEdgeID, toEdgeID, db, aqlQuery) {
    let collName;
    let additionalInfo;
    let newSaveObj;
    try {
        //First determine if additional data is defined
        //if there is an array, position 0 is the edgeCollection, position 1 is additional dataObject
        if (Array.isArray(edgeColl)) {
            collName = edgeColl[0];
            additionalInfo = edgeColl[1];
        }
        else {
            collName = edgeColl;
        }

        let existingEdgesArr = await checkEdgeExists(fromEdgeID, toEdgeID, collName, db, aqlQuery);

        //Update edge
        if (existingEdgesArr && existingEdgesArr.length > 0) {
            let correctEdge;

            //This check should not be necessary
            existingEdgesArr.forEach((existingEdge) => {
                if (existingEdge._from == fromEdgeID && existingEdge._to == toEdgeID ) {
                    correctEdge = existingEdge
                }
            });
            if (additionalInfo) {
                newSaveObj = compareObj(correctEdge, additionalInfo);
                let savedObj = await collName.replace(correctEdge._id, newSaveObj);
                return new Promise((resolve, reject) => {
                    // console.log('Updated edge with additional info');
                    resolve(savedObj);
                })
            }
            else return;
        }
        //    Create edge
        else {
            let saveObj = {};
            saveObj._to = toEdgeID;
            saveObj._from = fromEdgeID;
            if (additionalInfo) Object.assign(saveObj, additionalInfo);
            let savedObj = await collName.save(saveObj);
            return new Promise((resolve, reject) => {
                // console.log('created edge, possibly with additional info');
                resolve(savedObj);
            })
        }
    }
    catch (err) {
        return new Promise((resolve, reject) => {
            reject(err);
        })
    }
    
}

async function checkExistsAndCreateEdgesTwoWay (nodeID, otherNodeID, edge1FromNodeIDToOtherNode, edge2FromOtherNodeToNodeID, db, aqlQuery) {
    try {
        await checkExistsEdgeOrCreateOrUpdate(edge1FromNodeIDToOtherNode, nodeID, otherNodeID, db, aqlQuery);
        await checkExistsEdgeOrCreateOrUpdate(edge2FromOtherNodeToNodeID, otherNodeID, nodeID, db, aqlQuery);
        return new Promise((resolve, reject) => {
            // console.log('edges created');
            resolve()
        })
    }
    catch (err) {
        return new Promise((resolve, reject) => {
            reject(err);
        })    
    }
}

//fromToArray assumes this format [{_from: "x1", _to: "y1"}, {_from: "xn", _to: "yn"}]
async function checkExistsAndCreateEdgesTwoWayFromArray (fromToArray, edgeColl1FromNodeNameToOtherNode, edgeColl2FromOtherNodeToNodeName, db, aqlQuery) {
    if (fromToArray.length === 0) return;
    try {
        for (let i = 0; i < fromToArray.length; i++) {
            // console.log('TwoWayFromArr iteration: '+i);
            // await waitASecond();
            let nodeID = fromToArray[i]._from;
            let otherNodeID = fromToArray[i]._to;
            await checkExistsAndCreateEdgesTwoWay(nodeID, otherNodeID, edgeColl1FromNodeNameToOtherNode, edgeColl2FromOtherNodeToNodeName, db, aqlQuery);
            if (i === fromToArray.length -1) {
                return new Promise ((resolve, reject) => {
                    console.log('created '+fromToArray.length+' edges');
                    resolve()
                })
            }
        }
    }
    catch (err) {
        return new Promise ((resolve, reject) => {
            console.log('Err in checkExistsEdgesTwoWayFromArray');
            reject(err)
        })
    }
};

const checkCreateNodeAndEdges = (nodeName, nodeColl, edgeColl1FromNodeNameToOtherNode, edgeColl2FromOtherNodeToNodeName,
                            otherNodeID, db, aqlQuery, standardSaveObj) => {
    return new Promise((resolve, reject) => {

        //Let's start by checking if the node already exists
        checkExistsByName(nodeName, nodeColl, db, aqlQuery).then(nodeExistsArr => {
            let nodeID;
            let savedNodeMeta;
            /**
             * This is the simple one, node does not yet exist and therefore we can assume that edges dont exist yet either
             * **/
            //node does not exist and name is correct
            if (!nodeExistsArr && nodeName !== '') {
                // save node is the object that's going to be saved
                // default mode is just saving nodeName, if standardSaveObj is defined it is merged with name
                let saveNode;
                if (standardSaveObj && typeof standardSaveObj.name === "undefined")
                    saveNode = Object.assign({}, standardSaveObj, {"name": nodeName});
                else
                    saveNode = {"name": nodeName};

                console.log(`Saving into ${nodeColl.name} this object: \n`+JSON.stringify(saveNode));

                //save node object into collection
                nodeColl.save(saveNode).then(savedItem => {
                    console.log('saved');
                    nodeID = savedItem._id;
                    savedNodeMeta = savedItem;
                    //Ok, node is saved, let's turn to the edges
                    checkExistsEdgeOrCreateOrUpdate(edgeColl1FromNodeNameToOtherNode, nodeID, otherNodeID, db, aqlQuery)
                        .then(checkExistsEdgeOrCreateOrUpdate(edgeColl2FromOtherNodeToNodeName, otherNodeID, nodeID, db, aqlQuery))
                        .then(edge2 => {
                            console.log('Edges created');
                            resolve(savedNodeMeta);
                        }, err => {
                            console.log('ERROR in edges: '+err);
                            reject(err);
                        })
                    
                })
            }

            /**
             * This is another simple one, node name is invalid, resolve & move on
             * **/
            //    node name is invalid, resolve so we can move on!
            else if (!nodeExistsArr && nodeName === '') {
                console.error(`Node name ${nodeName} is invalid`);
                resolve();
            }
                
            //    node already exists 
            else if (nodeExistsArr && nodeName !== '') {
                nodeID = nodeExistsArr[0]._id;
                console.log(`Nodename ${nodeName} already existed in ${nodeColl.name}`);

                //with standardSaveObj provided, an update to node is required
                if (standardSaveObj) {

                    //compareObj compares the existing object with the new object and returns a new object with additional data
                    let updateObj = compareObj(nodeExistsArr[0], standardSaveObj);
                    updateObj = onlyAllowOneDateValue(updateObj);

                    //Let's update the collection with the new data we got from comparing both objects
                    nodeColl.replace(nodeID, updateObj).then(updatedObj => {

                        // Now do the edges check 
                        checkExistsEdgeOrCreateOrUpdate(edgeColl1FromNodeNameToOtherNode, nodeID, otherNodeID, db, aqlQuery)
                            .then(checkExistsEdgeOrCreateOrUpdate(edgeColl2FromOtherNodeToNodeName, otherNodeID, nodeID, db, aqlQuery))
                            .then(edge2 => {
                                console.log('Edges created');
                                resolve(updateObj);
                            }, err => {
                                console.log('ERROR in edges: '+err);
                                reject(err);
                            })
                    })
                }
                //No updated required
                else {
                    checkExistsEdgeOrCreateOrUpdate(edgeColl1FromNodeNameToOtherNode, nodeID, otherNodeID, db, aqlQuery)
                        .then(checkExistsEdgeOrCreateOrUpdate(edgeColl2FromOtherNodeToNodeName, otherNodeID, nodeID, db, aqlQuery))
                        .then(edge2 => {
                            console.log('Edges created');
                            resolve(nodeExistsArr[0]);
                        }, err => {
                            console.log('ERROR in edges: '+err);
                            reject(err);
                        })
                }
            }
        })
    })
};

const checkCreateNodeAndEdgesByKey = (nodeKey, nodeName, nodeColl, edgeColl1FromNodeNameToOtherNode, edgeColl2FromOtherNodeToNodeName,
                                 otherNodeID, db, aqlQuery, standardSaveObj) => {
    return new Promise((resolve, reject) => {
        try {
            checkExistsByKey(nodeKey, nodeColl, db, aqlQuery).then(nodeExistsArr => {
                let nodeID;
                let savedNodeMeta;
                //node does not exist and name is correct
                if (nodeExistsArr.length === 0 && nodeName !== '' && nodeKey !== '') {
                    // save node is the object that's going to be saved
                    // standard is saving nodeName, if standardSaveObj is defined it is merged with name
                    let saveNode;
                    if (standardSaveObj) {
                        console.log(`nodeName: ${nodeName}, standardSaveObj.name: ${standardSaveObj.name}`);
                        saveNode = Object.assign({}, standardSaveObj, {"name": nodeName, _key: nodeKey});
                        saveNode = onlyAllowOneDateValue(saveNode);
                    }

                    else
                        saveNode = {"name": nodeName, _key: nodeKey};

                    console.log(`Saving into ${nodeColl.name} this object: \n`+JSON.stringify(saveNode));
                    //save obj into collection
                    nodeColl.save(saveNode).then(savedItem => {
                        console.log('saved');
                        nodeID = savedItem._id;
                        savedNodeMeta = savedItem;
                        //check the edges
                        checkExistsEdgeOrCreateOrUpdate(edgeColl1FromNodeNameToOtherNode, nodeID, otherNodeID, db, aqlQuery)
                            .then(checkExistsEdgeOrCreateOrUpdate(edgeColl2FromOtherNodeToNodeName, otherNodeID, nodeID, db, aqlQuery))
                            .then(edge2 => {
                                console.log('Edges created');
                                resolve(savedNodeMeta);
                            }, err => {
                                console.log('ERROR in edges: '+err);
                                reject(err);
                            })

                    })
                }

                //    node name is invalid return
                else if ((nodeExistsArr.length === 0 && nodeName === '') || (!nodeExistsArr && nodeKey === '')) {
                    console.error(`Node name ${nodeName} or node key ${nodeKey} is invalid`);
                    resolve();
                }

                //    node already exists
                else if (nodeExistsArr.length > 0 && nodeName !== '' && nodeKey !== '') {
                    nodeID = nodeExistsArr[0]._id;
                    console.log(`Nodename ${nodeName} already existed in ${nodeColl.name}`);
                    // with standardSaveObj an update is required
                    if (standardSaveObj) {
                        let updateObj = compareObj(standardSaveObj, nodeExistsArr[0]);
                        updateObj = onlyAllowOneDateValue(updateObj);
                        
                        console.log(`Updating into ${nodeColl.name} this object: \n`+JSON.stringify(updateObj));
                        nodeColl.replace(nodeID, updateObj).then(updatedObj => {
                            checkExistsEdgeOrCreateOrUpdate(edgeColl1FromNodeNameToOtherNode, nodeID, otherNodeID, db, aqlQuery)
                                .then(checkExistsEdgeOrCreateOrUpdate(edgeColl2FromOtherNodeToNodeName, otherNodeID, nodeID, db, aqlQuery))
                                .then(edge2 => {
                                    console.log('Edges checked');
                                    resolve(updateObj);
                                }, err => {
                                    console.log('ERROR in edges: '+err);
                                    reject(err);
                                })
                        })
                    }
                    //    no updated required
                    else {
                        checkExistsEdgeOrCreateOrUpdate(edgeColl1FromNodeNameToOtherNode, nodeID, otherNodeID, db, aqlQuery)
                            .then(checkExistsEdgeOrCreateOrUpdate(edgeColl2FromOtherNodeToNodeName, otherNodeID, nodeID, db, aqlQuery))
                            .then(edge2 => {
                                console.log('Edges created');
                                resolve(nodeExistsArr[0]);
                            }, err => {
                                console.log('ERROR in edges: '+err);
                                reject(err);
                            })
                    }
                }
            })
        }
        catch(err) {
            console.error(err);
        }
    })
};

/**
 * IMPORTANT: As this function creates and/or updates multiple nodes, it returns an array containing nodeMeta objects
 * */
const checkCreateNodeAndEdgesFromArrayByKey = (nodeArray, nodeColl, edgeColl1FromNodeNameToOtherNode, edgeColl2FromOtherNodeToNodeName, otherNodeID, db, aqlQuery) => {
    console.log('nodeArr.length: '+nodeArray.length);
    return new Promise((resolve, reject) => {
        let iteration = 0;
        let nodeMetaArr = [];
        const asyncArrayLoop = (obj, nodeColl, edgeColl1FromNodeNameToOtherNode, edgeColl2FromOtherNodeToNodeName, otherNodeID, db, aqlQuery) => {
            if (iteration < nodeArray.length) {
                console.log('obj in async array loop:\n'+JSON.stringify(obj));
                checkCreateNodeAndEdgesByKey(obj._key, obj.name, nodeColl, edgeColl1FromNodeNameToOtherNode, edgeColl2FromOtherNodeToNodeName, otherNodeID, db, aqlQuery, obj)
                    .then(nodeMeta => {
                        nodeMetaArr.push(nodeMeta);
                        console.log('Finished iteration in asyncArrayLoop');
                        iteration++;
                        asyncArrayLoop(nodeArray[iteration], nodeColl, edgeColl1FromNodeNameToOtherNode, edgeColl2FromOtherNodeToNodeName, otherNodeID, db, aqlQuery);
                    }, err => {
                        console.log('ERROR in checkCreateNodeAndEdgesByKey INSIDE asyncArrayLoop INSIDE checkCreateNodeAndEdgesFromArrayByKey');
                        reject(err);
                    })    
            }
            else {
                console.log(`All done in checkExistsAndCreateFromArray, checked or saved ${iteration} objects in ${nodeColl.name}`);
                resolve(nodeMetaArr);
            }
        };

        asyncArrayLoop(nodeArray[iteration], nodeColl, edgeColl1FromNodeNameToOtherNode, edgeColl2FromOtherNodeToNodeName, otherNodeID, db, aqlQuery);

    });
};

//Same as checkCreateNodeAndEdges but takes an array as nodeCollArr to check in multiple collections
//If nodeName is not found in collections, it is created in the first collection in nodeCollArr
const checkCreateNodeAndEdgesMultipleColl = (nodeName, nodeCollArr, edgeColl1FromNodeNameToOtherNode, edgeColl2FromOtherNodeToNodeName,
                                 otherNodeID, db, aqlQuery, standardSaveObj) => {
    return new Promise((resolve, reject) => {
        checkExistsMultipleColl(nodeName, nodeCollArr, db, aqlQuery).then(nodeExistsArr => {
            let nodeID;
            let savedNodeMeta;
            //node does not exist in collections and name is correct
            if (!nodeExistsArr && nodeName !== '') {
                // save node is the object that's going to be saved
                // standard is saving nodeName, if standardSaveObj is defined it is merged with name
                let saveNode;
                if (standardSaveObj && typeof standardSaveObj.name !== "undefined")
                    saveNode = Object.assign({}, standardSaveObj, {"name": nodeName});
                else
                    saveNode = {"name": nodeName};

                console.log(`Saving into ${nodeCollArr[0].name} this object: \n`+JSON.stringify(saveNode));
                //save obj into collection
                nodeCollArr[0].save(saveNode).then(savedItem => {
                    console.log('saved');
                    nodeID = savedItem._id;
                    savedNodeMeta = savedItem;
                    checkExistsEdgeOrCreateOrUpdate(edgeColl1FromNodeNameToOtherNode, nodeID, otherNodeID, db, aqlQuery)
                        .then(checkExistsEdgeOrCreateOrUpdate(edgeColl2FromOtherNodeToNodeName, otherNodeID, nodeID, db, aqlQuery))
                        .then(edge2 => {
                            console.log('Edges created');
                            resolve(savedNodeMeta);
                        }, err => {
                            console.log('ERROR in edges: '+err);
                            reject(err);
                        })
                })
            }

            //    node name is invalid return
            else if (!nodeExistsArr && nodeName === '') {
                console.error(`Node name ${nodeName} is invalid`);
                resolve();
            }

            //    node already exists
            else if (nodeExistsArr && nodeName !== '') {
                nodeID = nodeExistsArr[0]._id;
                console.log(`Nodename ${nodeName} already existed in ${nodeCollArr[0].name}`);
                //with standardsaveObj an update is required
                if (standardSaveObj) {
                    let updateObj = compareObj(nodeExistsArr[0], standardSaveObj);
                    nodeExistsArr[0].replace(nodeID, updateObj).then(updatedObj => {
                        checkExistsEdgeOrCreateOrUpdate(edgeColl1FromNodeNameToOtherNode, nodeID, otherNodeID, db, aqlQuery)
                            .then(checkExistsEdgeOrCreateOrUpdate(edgeColl2FromOtherNodeToNodeName, otherNodeID, nodeID, db, aqlQuery))
                            .then(edge2 => {
                                console.log('Edges created');
                                resolve();
                            }, err => {
                                console.log('ERROR in edges: '+err);
                                reject(err);
                            })
                    })
                }
                //No update required
                else {
                    checkExistsEdgeOrCreateOrUpdate(edgeColl1FromNodeNameToOtherNode, nodeID, otherNodeID, db, aqlQuery)
                        .then(checkExistsEdgeOrCreateOrUpdate(edgeColl2FromOtherNodeToNodeName, otherNodeID, nodeID, db, aqlQuery))
                        .then(edge2 => {
                            console.log('Edges created');
                            resolve();
                        }, err => {
                            console.log('ERROR in edges: '+err);
                            reject(err);
                        })
                }
            }
        })
    })
};

export {checkCreateNodeAndEdges, checkCreateNodeAndEdgesMultipleColl, checkExistsAndCreateEdgesTwoWay,
    checkCreateNodeAndEdgesByKey, checkCreateNodeAndEdgesFromArrayByKey, checkExistsEdgeOrCreateOrUpdate, checkExistsAndCreateEdgesTwoWayFromArray};

