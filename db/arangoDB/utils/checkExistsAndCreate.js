/**
 * Created by Patrick on 09/08/2016.
 */
import Promise from 'promise';
import { checkExistsByName, checkExistsByKey } from './checkExists'
import { compareObj } from './compareObj'
import { onlyAllowOneDateValue } from './dateFormat'

const checkExistsAndCreateNodeByName = (name, collName, db, aqlQuery, standardSaveObj) => {
    return new Promise((resolve, reject) => {
        checkExistsByName(name, collName, db, aqlQuery)
            .then(existsArr => {
                if (existsArr) {
                    let oldItem = existsArr[0];
                    let updateItem;
                    if (standardSaveObj) {
                        updateItem = compareObj(oldItem, standardSaveObj);
                        if (typeof standardSaveObj.dateModified != 'undefined')
                            updateItem.dateModified = standardSaveObj.dateModified;
                    }
                    else updateItem = oldItem;
                    collName.replace(oldItem._id, updateItem).then(updatedItem => {
                        console.log(`Updated ${name} in ${collName.name} `);
                        resolve(updateItem);
                    }, err => {
                        console.log(`ERROR updating ${name} in ${collName.name} WITHIN checkExistsAndCreateNodeByName`);
                        reject(err);
                    })
                }
                else {
                    let saveObj = Object.assign({}, standardSaveObj, {name});
                    collName.save(saveObj).then(savedObj => {
                        console.log(`Saved ${name} in ${collName.name} `);
                        resolve(savedObj);
                    }, err => {
                        console.log(`ERROR saving ${name} in ${collName.name} `);
                        reject(err);
                    })
                }
            }, err => {
                console.log(`ERROR checking ${name} exists in ${collName.name} WITHIN checkExistsAndCreateNodeByName`);
                reject(err);
            })
    })
};

const checkExistsAndCreateNodeByKey = (key, collName, db, aqlQuery, standardSaveObj) => {
    return new Promise((resolve, reject) => {
        
        //Check if doc already exists
        checkExistsByKey(key, collName, db, aqlQuery)
            .then(existsArr => {
                console.log('existsArr:');
                console.log(existsArr);
                
                //doc already exists
                if (existsArr.length > 0) {
                    let oldItem = existsArr[0];
                    let updateItem;

                    if (standardSaveObj) {
                        //For documents from Officiële Bekendmakingen: if documentStatus changes delete old doc and save new doc
                        if (standardSaveObj.source === 'officieleBekendmakingen' && oldItem.documentStatus !== standardSaveObj.documentStatus) {
                            collName.remove(oldItem._key).then(() => {
                                collName.save(standardSaveObj).then(() => {
                                    standardSaveObj._id = 'documents/'+standardSaveObj._key;
                                    resolve(standardSaveObj);    
                                },(err) => {
                                    console.log('Error saving doc after removing old doc in checkExistsAndCreateNodeByKey: '+err);
                                    reject(err)
                                } );
                            }, (err) => {
                                console.log('Error removing doc in checkExistsAndCreateNodeByKey: '+err);
                                reject(err)
                            });
                        }
                        // Else compare old and new doc and save aggregated result   
                        else {
                            updateItem = compareObj(oldItem, standardSaveObj);
                        }
                    }
                    else updateItem = oldItem;
                    
                     // Check updateItem for date attributes with array value. Needed for items like dateImported which otherwise would stack in an array
                    updateItem = onlyAllowOneDateValue(updateItem);

                    collName.replace(oldItem._id, updateItem).then(updatedItem => {
                        console.log(`Updated ${updateItem._key} in ${collName.name} `);
                        resolve(updateItem);
                    }, err => {
                        console.log(`ERROR updating ${oldItem.name} in ${collName.name} WITHIN checkExistsAndCreateNodeByName`);
                        reject(err);
                    })
                }
                //    Doc does not yet exists, create it
                else {
                    let saveObj;
                    if (standardSaveObj) saveObj = Object.assign({}, standardSaveObj, {_key: key});
                    else saveObj = {_key: key};
                    collName.save(saveObj).then(savedObj => {
                        console.log(`Saved ${key} in ${collName.name} `);
                        resolve(compareObj(savedObj, saveObj));
                    }, err => {
                        console.log(`ERROR saving ${key} in ${collName.name} `);
                        reject(err);
                    })
                }
            }, err => {
                console.log(`ERROR checking ${key} exists in ${collName.name} WITHIN checkExistsAndCreateNodeByName`);
                reject(err);
            })
    })
};

export { checkExistsAndCreateNodeByKey, checkExistsAndCreateNodeByName }