import lodash from 'lodash'

//TODO find an alternative for eval
function typeofUndefined(containingObj, attrToCheck, additionalAttrs) {
    //Catch errs in Eurlex
    if (typeof containingObj.ANNOTATION !== 'undefined') {
        if (typeof containingObj.ANNOTATION.ERROR_MESSAGE !== 'undefined') return 'ERROR: corrupted data from source'
    }
    else if (typeof containingObj[attrToCheck] !== 'undefined') {
        if ((typeof containingObj[attrToCheck].ANNOTATION !== 'undefined')) {
            if (typeof containingObj[attrToCheck].ANNOTATION.ERROR_MESSAGE !== 'undefined') return 'ERROR: corrupted data from source'    
        }
    }
    //Normal function
    let path = Array.from(arguments).slice(1);
    return lodash.get(containingObj, path, '')
}

function typeofUndefinedTwoObj(obj1, obj2, attrToCheck, additionalAttrs) {
    //Catch corrupted data in Eurlex
    if (typeof obj1.ANNOTATION !== 'undefined' || typeof obj2.ANNOTATION !== 'undefined') {
        if (typeof obj1.ANNOTATION.ERROR_MESSAGE !== 'undefined' || typeof obj2.ANNOTATION.ERROR_MESSAGE !== 'undefined')
            return 'ERROR: corrupted data from source'
    }
    else if (typeof obj1[attrToCheck] !== 'undefined') {
        if ((typeof obj1[attrToCheck].ANNOTATION !== 'undefined')) {
            if (typeof obj1[attrToCheck].ANNOTATION.ERROR_MESSAGE !== 'undefined')
                return 'ERROR: corrupted data from source'
        }
    }
    else if (typeof obj2[attrToCheck] !== 'undefined') {
        if ((typeof obj2[attrToCheck].ANNOTATION !== 'undefined')) {
            if (typeof obj2[attrToCheck].ANNOTATION.ERROR_MESSAGE !== 'undefined')
                return 'ERROR: corrupted data from source'
        }
    }
    //Normal function
    if (typeof obj1[attrToCheck] !== 'undefined') {
        let additional = 'obj1[attrToCheck]';
        for (let i = 3; i<arguments.length; i++) {
            additional += ('.'+arguments[i])
        }
        return(eval(additional))
    }
    else if (typeof obj2[attrToCheck] !== 'undefined') {
        let additional = 'obj2[attrToCheck]';
        for (let i = 3; i<arguments.length; i++) {
            additional += ('.'+arguments[i])
        }
        // console.log(additional);
        return(eval(additional))
    }
    else return ''
}

export {typeofUndefined, typeofUndefinedTwoObj}