/**
 * Created by Patrick on 08/08/2016.
 */
const getKey = (url) => {
    if (url !== '') {
        let arr = url.split('/');
        return arr[arr.length-1];
    }
    else return ''
};

export {getKey}