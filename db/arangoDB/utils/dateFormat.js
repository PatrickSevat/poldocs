function dateFormat(seperator) {
    let currentDate = new Date();
    return timestampToYYYYMMDD(currentDate, seperator)
}

function timestampToYYYYMMDD(timestamp, seperator) {
    let date = new Date(timestamp);
    let currentYear = date.getFullYear();
    let currentMonth = date.getMonth();
    let currentDay = date.getDate();

    currentMonth += 1;
    currentMonth = currentMonth < 10 ? '0'+currentMonth : currentMonth;
    currentDay = currentDay < 10 ? '0'+currentDay : currentDay;

    return currentYear+seperator+currentMonth+seperator+currentDay;
}

function nowToISOstring() {
    let now = new Date;
    return now.toISOString();
}

function strToISOString(dateStr) {
    if (dateStr == '') return null;
    let date = new Date(dateStr);
    let numDate = date.getTime();

    if (!isNaN(numDate))
        return date.toISOString();
    else
        return null;
}

function onlyAllowOneDateValue(obj) {
    let dateNames = ['documentDate', 'dateModified', 'availableDate', 'dateCreated',
        'entryIntoForceDate', 'endOfValidityDate', 'deadline', 'dateMeeting', 'dateImported'];

    dateNames.map((name) => {
        let sorted;
        if (typeof obj[name] !== 'undefined' && Array.isArray(obj[name])) {
            sorted = obj[name].sort((a,b) => {
                return new Date(b) - new Date(a);
            });
            for (let i = 0; i <sorted.length; i++) {
                if (sorted[i] !== null && sorted[i] !== '') {
                    obj[name] = sorted[0];
                    break;
                }
            }
        }
    });

    return obj;
}

exports.onlyAllowOneDateValue = onlyAllowOneDateValue;

exports.timestampToYYYYMMDD = timestampToYYYYMMDD;

exports.dateFormatFunc = dateFormat;

exports.nowToISOString = nowToISOstring;

exports.strToISOString = strToISOString;