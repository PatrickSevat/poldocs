
//TODO amount of collections to be searched can be reduced by looking at WORK_CREATED_BY_AGENT.TYPE = 'corporate body',
//check of there is an equivalent for OB \ ORI
/**
 * @param optionalOptionsObj values: searchAttr = string, caseSensitive = bool, searchType = 'RegExp' || 'equality'
 * @param searchValuesArr can be an array or a string
 * */
function searchForDocuments(searchValuesArr, db, aqlQuery, collNameArr, optionalOptionsObj) {
    return new Promise((resolve, reject) => {
        if (searchValuesArr.length === 0) resolve([]);
        let collectionNameArr = [];
// publicOffices.name !!!be careful minister can be many countries!!!,
        if (!collNameArr) {
            collectionNameArr = ['countries', 'legislativeBodies', 'executiveBodies', 'people', 'IGO', 'otherBodies',
                'publicOffices', 'politicalParties', 'companies'];
        }
        else
            collectionNameArr = collNameArr;

        let searchAttr;
        let caseSensitive = false;
        let searchType = 'RegExp';

        if (optionalOptionsObj) {
            searchAttr = typeof optionalOptionsObj.searchAttr === 'undefined'? 'name' : optionalOptionsObj.searchAttr;
            caseSensitive = typeof optionalOptionsObj.caseSensitive === 'undefined'? false : optionalOptionsObj.caseSensitive;
            searchType = typeof optionalOptionsObj.searchType === 'undefined'? 'RegExp' : optionalOptionsObj.searchType;
        }

        let query = '';
        let append = `\nlet appended = union(`;

        collectionNameArr.forEach((collName, collIndex) => {
            query += `\nlet ${collName} = (
                    for ${collName}Item in ${collName}`;

            if (searchType == 'equality') query += `\nlet arr = (TO_ARRAY(${collName}Item.${searchAttr})) \nfor name in arr\nfilter `;
            else if (searchType == 'RegExp') query += '\n filter';

            if (Array.isArray(searchValuesArr)) {
                searchValuesArr.forEach((authorName, authorIndex) => {
                    authorName = authorName.replace(/'/g, "\\'");
                    let authorSearchName;
                    if (caseSensitive) {
                        authorSearchName = authorName;
                    }
                    else {
                        authorSearchName = authorName.toLowerCase();
                        if (searchType == 'RegExp') {
                            authorSearchName = '\\\\b'+authorSearchName+'\\\\b';
                        }
                    }

                    if (authorIndex == searchValuesArr.length-1) {
                        if (searchType == 'RegExp') {
                            //AQL regex query asks for caseInsensitiveness as third argument
                            query += `\nREGEX_TEST(${collName}Item.${searchAttr}, '${authorSearchName}' , ${!caseSensitive})`;
                        }
                        else if (searchType == 'equality') {
                            if (!caseSensitive) {
                                query += `LOWER(name) =='${authorSearchName}'`
                            }
                            else {
                                query += `filter name =='${authorSearchName}'`
                            }
                            
                        }
                    }
                    else {
                        if (searchType == 'RegExp') {
                            //AQL regex query asks for caseInsensitiveness as third argument
                            query += `\nREGEX_TEST(${collName}Item.${searchAttr}, '${authorSearchName}' , ${!caseSensitive}) ||`;
                        }
                        else if (searchType == 'equality') {
                            if (!caseSensitive) {
                                query += `LOWER(name) =='${authorSearchName}' ||`
                            }
                            else {
                                query += `name =='${authorSearchName}' ||`
                            }

                        }
                    } 
                });    
            }
            else if (typeof searchValuesArr === 'string') {
                let authorSearchName;
                if (caseSensitive) {
                    authorSearchName = searchValuesArr;
                }
                else {
                    authorSearchName = searchValuesArr.toLowerCase();
                    if (searchType == 'RegExp') {
                        authorSearchName = '\\\\b'+authorSearchName+'\\\\b';
                    }
                }
                if (searchType == 'RegExp') query += `\nREGEX_TEST(${collName}Item.${searchAttr}, '${authorSearchName}' , ${!caseSensitive})`;
                else if (searchType == 'equality' && !caseSensitive) query += `LOWER(name) =='${authorSearchName}'`;
                else if (searchType == 'equality' && caseSensitive) query += `name =='${authorSearchName}'`
            }
            

            query += `\n return ${collName}Item \n)`;
            if (collIndex == collectionNameArr.length-1) append += `${collName})`;
            else append += `${collName},`
        });
        if (Array.isArray(collectionNameArr) && collectionNameArr.length > 1) {
            query += append;
            query += `\nreturn appended`;
        }
        else {
            query += `\n return ${collectionNameArr[0]}`
        }

        // console.log(query);

        db.query(query).then(cursor => {
            let resArr = [];
            if (cursor) {
                // console.log('cursor true');
                cursor.all().then(arr => {
                    // console.dir(arr[0]);
                    if (Array.isArray(arr) && arr[0].length > 0) {
                        // console.log('arr is array');
                        arr[0].forEach((item) => {
                            resArr.push(item._id);
                        });
                        resolve(resArr);
                    }
                    else resolve(resArr);
                })
            }
            else {
                console.log('cursor false');
                resolve(resArr);
            }
        }, err => {
            reject(err)
        })
    })
}

function searchDocumentsSubTitle(searchValuesArr, db, aqlQuery) {
    return new Promise(function(resolve, reject){
        if (searchValuesArr.length === 0) resolve([]);
        let query = `for doc in documents
    let subTitle = doc.subTitle
    filter 
    `;
        searchValuesArr.forEach((item, index) => {
            if (index !== searchValuesArr.length -1) {
                query += `contains(subTitle, ${searchValuesArr[index]}) || `;
            }
            else query += `contains(subTitle, ${searchValuesArr[index]})`
        });
        query += `\n return doc._id`;

        db.query(query).then(cursor => {
            let resArr = [];
            if (cursor) {
                // console.log('cursor true');
                cursor.all().then(arr => {
                    // console.dir(arr[0]);
                    if (Array.isArray(arr) && arr[0].length > 0) {
                        // console.log('arr is array');
                        arr[0].forEach((item) => {
                            resArr.push(item._id);
                        });
                        resolve(resArr);
                    }
                    else resolve(resArr);
                })
            }
            else {
                console.log('cursor false');
                resolve(resArr);
            }
        }, err => {
            reject(err)
        })    
    })
}

function fromArrToID(fromArr, toID, prefix) {
    let uniqueFromArr = Array.from(new Set(fromArr));
    let returnArr = [];
    for (let i = 0; i < uniqueFromArr.length; i++) {
        if (!prefix) returnArr.push({_from: uniqueFromArr[i], _to: toID});
        else returnArr.push({_from: (prefix+uniqueFromArr[i]), _to: toID});
    }
    return returnArr
}

function fromIDToArr(fromID, toArr, prefix) {
    let uniqueToArr = Array.from(new Set(toArr));
    let returnArr = [];
    for (let i = 0; i < uniqueToArr.length; i++) {
        if (!prefix) returnArr.push({_from: fromID, _to: uniqueToArr[i]});
        else returnArr.push({_from: fromID, _to: (prefix+uniqueToArr[i])});
    }
    return returnArr
}

function fromArrToArr(fromArr, toArr) {
    let uniqueFromArr = Array.from(new Set(fromArr));
    let uniqueToArr = Array.from(new Set(toArr));
    let returnArr = [];
    uniqueFromArr.forEach((fromItem, fromIndex) => {
        uniqueToArr.forEach((toItem, toIndex) => {
            returnArr.push({_from: fromItem, _to: toItem})
        })
    })
    return returnArr;
}

function inverseDocumentRelationship(relationship) {
    const translateObj = {
        legalBasis: 'legalBasisFor',
        references: 'isReferencedBy',
        proposesAmend: 'proposesAmendBy',
        amends: 'amendedBy',
        requires: 'isRequiredBy',
        correctedBy: 'corrects',
        relation: 'relation',
        repeals: 'repealedBy',
        repealsImplicitly: 'repealedByImplicitly',
        completes: 'completedBy',
        rendersObsolete: 'renderedObsoleteBy',
        derogates: 'derogatedBy',
        extendsApplication: 'extendedApplicationBy',
        confirms: 'confirmedBy',
        tacklesSimilarQuestion: 'tacklesSimilarQuestion',
        interpretesAuthoritatively: 'interpretedAuthoritativelyBy',
        addsTo: 'addedBy',
        implements: 'implementedBy',
        extendsValidity: 'extendedValidityBy',
        replaces: 'isReplacedBy',
        incorporates: 'incorporatedBy',
        reestablishes: 'reestablishedBy',
        partiallySuspends: 'partiallySuspendedBy',
        defersApplication: 'deferredApplicationBy',
        relatedQuestion: 'relatedQuestion',
        containsEESCopinion: 'EESCOpinionPartOf',
        containsCORopinion: 'COROpinionPartOf',
        containsEPopinion: 'EPOpinionPartOf',
        partiallyRefersTo: 'partiallyReferredBy',
        adopts: 'adoptedBy',
        partiallyAdopts: 'partiallyAdoptedBy',
        attachment: 'attachmentOf',
        attachmentOf: 'attachment',
        reprintedBy: 'reprints',
        adviesRvS: 'adviesRvS',
        behandeldDossier: 'dossierBehandeldDoor'
    };
    return translateObj[relationship]
}

function logObjectWithoutFullText(saveWorkObj) {
    for (let key in saveWorkObj) {
        if (saveWorkObj.hasOwnProperty(key)) {
            if (key !== 'fullText') {
                console.log('Key: '+key);
                console.dir(saveWorkObj[key]);
            }
            else {
                console.log('Key: '+key);
                console.log(saveWorkObj[key].slice(0,50));
            }    
        }
    }
}

export {searchForDocuments, fromArrToID, fromIDToArr, fromArrToArr, inverseDocumentRelationship, logObjectWithoutFullText,
searchDocumentsSubTitle}

