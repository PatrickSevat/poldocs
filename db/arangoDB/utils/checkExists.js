// promise resolves with arr if doc exists, no arr when doc does not exist
import Promise from 'promise';

//TODO see if upsert helps here https://docs.arangodb.com/3.0/AQL/Operations/Upsert.html
const checkExistsByAttr = (attrVal, attr, collName, useDB, useAql) => {
    console.log('checking if exists, attr: '+attr+', attrVal: '+attrVal+', collName: '+collName.name);
    return new Promise((resolve, reject) => {
        if (attrVal !== '') {
            useDB.query(useAql`
        FOR x IN ${collName}
        FILTER x.${attr} == ${attrVal}
        RETURN x`
            ).then(cursor => {
                let arr = [];
                cursor.each((item) => arr.push(item)).then(() => {
                    console.log('all pushed for attr: '+attr+', attrVal: '+attrVal+', arr.length: '+arr.length);
                    //return with arr if results found, return without argument if no results
                    if (arr.length > 0) resolve(arr);
                    else resolve();
                });
                //    resolve on err, because cursor is unavailable, means no results, this is an extra check, unsure why sometimes an err is thrown
            }, err => {
                console.error(err);
                resolve()
            })
        }
        else {
            //name to check for is undefined
            console.log(`${attrVal} is ''`);
            resolve()
        }
    })
};


//TODO refactor byName and byID into checkExistsByAttr
//Checks if name exists in collName
const checkExistsByName = (name, collName, useDB, useAql) => {
    console.log('checking if exists, name: '+name+', collName: '+collName.name);
    return new Promise((resolve, reject) => {
        if (name !== '') {
            useDB.query(useAql`
        FOR x IN ${collName}
        FILTER x.name == ${name}
        RETURN x`
            ).then(cursor => {
                let arr = [];
                cursor.each((item) => arr.push(item)).then(() => {
                    console.log('all pushed for: '+name+', arr.length: '+arr.length);
                    //return with arr if results found, return without argument if no results
                    if (arr.length > 0) resolve(arr);
                    else resolve(arr[0]);
                });
                //    resolve on err, because cursor is unavailable, means no results, this is an extra check, unsure why sometimes an err is thrown
            }, err => {
                console.error(err);
                resolve()
            })
        }
        else {
            //name to check for is undefined
            console.log(`${name} is ''`);
            resolve()
        }

    })
};

const checkExistsByKey = (key, collName, useDB, useAql) => {
    console.log('checking if exists, key: '+key+', collName: '+collName.name);
    return new Promise((resolve, reject) => {
        if (key !== '') {
            // console.log('key not undefined');
            useDB.query(useAql`
        FOR x IN ${collName}
        FILTER x._key == ${key}
        RETURN x`
            ).then(cursor => {
                let arr = [];
                cursor.each((item) => arr.push(item)).then(() => {
                    console.log('all pushed for: '+key+', arr.length: '+arr.length);
                    //return with arr if results found, return without argument if no results
                    if (arr.length > 0) resolve(arr);
                    else resolve([]);
                });
                //    resolve on err, because cursor is unavailable, means no results, this is an extra check, unsure why sometimes an err is thrown
            }, err => {
                console.log(`${key} does not exist in ${collName.name}`);
                console.error(err);
                resolve([])
            })
        }
        else {
            //name to check for is undefined
            console.log(`${key} is ''`);
            resolve()
        }

    })
};

//same as checkExists() only extra argument for to and from fields
const checkEdgeExists = (fromID, toID, collName, useDB, useAql) => {
    return new Promise((resolve, reject) => {
        if (typeof fromID !== "undefined" && fromID !== '' && typeof toID !== "undefined" && toID !== '' ) {
            useDB.query(useAql`FOR x IN ${collName}
            FILTER x._from == ${fromID} AND x._to == ${toID}
            return x`
            ).then(cursor => {
                let arr = [];
                cursor.each((item) => arr.push(item)).then(() => {
                    // console.log('all pushed in edge exists, arr.length: '+arr.length);
                    if (arr.length > 0) resolve(arr);
                    else resolve();
                })
            }, err => {
                console.error(err);
                resolve()
            })
        }
        else {
            console.log('ERROR: Check edge exists has empty or undefined arguments');
            resolve()
        }
    })
};

//Same as checkExists but takes an array of collections to test against
const checkExistsMultipleColl = (name, collNameArr, useDB, useAql) => {
    console.log('checking if exists, name: '+name+' in multiple collections ');
    return new Promise((resolve, reject) => {
        if (name !== '') {
            let letQueries = '';
            let returnQuery = 'let union = union(';
            for(let i = 0; i < collNameArr.length; i++) {
                letQueries += `\nlet ${collNameArr[i].name} = (
                for ${collNameArr[i].name} in ${collNameArr[i].name}
                filter ${collNameArr[i].name}.name == "${name}"
                return ${collNameArr[i].name}
                )`;
                if (i !== collNameArr.length -1) {
                    returnQuery += `${collNameArr[i].name}, `
                }
                else if (i === collNameArr.length -1) {
                    returnQuery += `${collNameArr[i].name}) \n return union`;
                }
            }
            let fullQuery = letQueries+returnQuery;
            useDB.query(fullQuery).then(cursor => {
                let arr = [];
                cursor.each((item) => arr.push(item)).then(() => {
                    console.log('all pushed for: '+name+', arr.length: '+arr[0].length);
                    console.log(arr[0]);
                    //return with arr if results found, return without argument if no results
                    if (arr.length > 0) resolve(arr[0]);
                    else resolve();
                });
                //    resolve on err, because cursor is unavailable, means no results, this is an extra check, unsure why sometimes an err is thrown
            }, err => {
                console.log(`4 8 11 13 ${name} does not exist in ${collName.name}`);
                console.error(err);
                resolve()
            })
        }
        else {
            //name to check for is undefined
            console.log(`4 8 11 13 ${name} is ''`);
            resolve()
        }

    })
};

export {checkExistsByName, checkEdgeExists, checkExistsMultipleColl, checkExistsByKey, checkExistsByAttr}