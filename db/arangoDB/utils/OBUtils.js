import {parseString, Parser} from 'xml2js';
import sftpClient from 'sftp-promises';
import fs from 'fs';

const client = new sftpClient();
const parser = new Parser({explicitArray: false, ignoreAttrs: true});
const parserFT = new Parser({explicitArray: false, ignoreAttrs: false});

var config = {
    hostname: 'bestanden.officielebekendmakingen.nl',
    port: 22,
    username: 'anonymous',
    password: 'patrick@cr-8.nl'
};

async function getXMLData(url, session) {
    try {
        console.log(url);
        let buffer = await client.getBuffer(url, session);
        let str = buffer.toString('utf8');
        // console.log('extractFromXML: '+str.substr(0,50));
        return new Promise((resolve, reject) => {
            resolve(str);
        });
    }
    catch (err) {
        console.log('Error in extract from XML: \n'+err);
        return new Promise((resolve, reject) => {
            reject(err);
        });
    }
}

async function getXMLDataLocal(url) {
    try {
        console.log(url);
        return new Promise((resolve, reject) => {
            fs.readFile(url, 'utf8', function (err, data) {
                if (err) reject(err);
                // console.log('data retrieved');
                resolve(data);
            })
        })
    }
    catch (err) {
        console.log('Error in extract from XML: \n'+err);
        return new Promise((resolve, reject) => {
            reject(err);
        });
    }
}

function parseXML(XMLstring, type) {
    if (type === 'changelog') {
        // console.log(XMLstring.substr(0,200));
        return new Promise ((resolve, reject) => {
            parser.parseString(XMLstring, function (err, result) {
                if (err) reject(err);
                let array = result.changelog.changes.change;
                // console.log('array.length at start: '+array.length);
                resolve (array);
            });
        })
    }
    else if (type === 'document') {
        // console.log(XML.substr(0,200));
        return new Promise ((resolve, reject) => {
            parser.parseString(XMLstring, function (err, result) {
                if (err) reject(err);
                let obj = result['owms-metadata'];
                // console.log('array.length at start: '+array.length);
                resolve (obj);
            });
        })
    }
    else if (type === 'documentFullText') {
        // console.log(XML.substr(0,200));
        return new Promise ((resolve, reject) => {
            parserFT.parseString(XMLstring, function (err, result) {
                if (err) reject(err);
                // console.log('array.length at start: '+array.length);
                resolve (result);
            });
        })
    }
}

async function searchForDossiers(matches, db) {
    try {
        if (matches.length == 0)
            return new Promise((resolve, reject) => resolve());

        let docQuery= `
for doc in documents
filter 
        `;

        /**
         * matches:
         [ { searchCollection: 'documents',
    searchAttr: 'title',
    searchValues: [ 'EG 2004 49' ] },
         { searchCollection: 'documents',
           searchAttr: 'title',
           searchValues: [ 'EG 95 18' ] },
         { searchCollection: 'documents',
           searchAttr: 'title',
           searchValues: [ 'EG 2001 14' ] },
         { searchCollection: 'documents',
           searchAttr: 'title',
           searchValues: [ 'EG 2008 57' ] },
         { searchCollection: 'documents',
           searchAttr: 'title',
           searchValues: [ 'EU 2012 34' ] } ]

         matchItem:
         { searchCollection: 'documents',
           searchAttr: 'title',
           searchValues: [ 'EG 2004 49' ] }


         * */
        
        matches.map((matchItem, matchItemIndex) => {
            if (!Array.isArray(matchItem.searchValues)) {
                console.log('matchItem: ');
                console.log(matchItem);
                if (matchItemIndex === matches.length -1) {
                    // \\\\s|\\\\] is used as a replacement for \b word boundary. To make sure that COM/2016/026 does not match COM/2016/0261 
                    docQuery += `regex_test(doc.${matchItem.searchAttr}, '${matchItem}(\\\\s|\\\\])', true)`;
                } 
                else {
                    docQuery += `regex_test(doc.${matchItem.searchAttr}, '${matchItem}(\\\\s|\\\\])', true)  || `;
                }
            }
            else {
                matchItem.searchValues.map((searchValue, searchValueIndex) => {
                    // Properly escape forward slash
                    let replacedValue = searchValue.replace(/\//g, '\\\\/');
                    replacedValue = replacedValue.replace(/\(/g, '\\(');
                    replacedValue = replacedValue.replace(/\)/g, '\\)');
                    if (matchItemIndex === matches.length-1 && searchValueIndex === matchItem.searchValues.length -1)
                        docQuery += `regex_test(doc.${matchItem.searchAttr}, '${replacedValue}(\\\\s|\\\\])', true)`;
                    else
                        docQuery += `regex_test(doc.${matchItem.searchAttr}, '${replacedValue}(\\\\s|\\\\])', true) || `;
                })    
            }
        });

        docQuery += `
\nlet celexKey = doc.celexKey

for dossier in dossiers
filter contains(celexKey, dossier.adoptedAct[0]) || contains(celexKey, dossier.initiatedByCelex[0])
return dossier        
        `;
        console.log(docQuery);
        
        return new Promise((resolve, reject) => {
            db.query(docQuery).then((cursor) => {
                if (cursor) {
                    cursor.all().then((arr) => {
                        console.log('cursor.all arr: '+arr);
                        if (arr)
                            resolve(arr);
                        else
                            resolve([]);
                    });
                }
                else
                    resolve([]);
            });    
        })
        
        

    }
    catch (err) {
        return new Promise((resolve, reject) => reject(err))
    }

}

export {getXMLData, parseXML, getXMLDataLocal, searchForDossiers}