import {typeofUndefined} from './typeofUndefined'
import {strToISOString, nowToISOString} from './dateFormat'
import {checkExistsByKey} from './checkExists'

const retrieveAuthors = (createdBy, createdByAgent ) => {
    let authorArr = [];
    //TODO refactor into arguments loop
    // console.dir('created by:'+createdBy);
    // console.dir('created by:'+createdByAgent);
    if (createdBy) {
        if (typeof createdBy.SAMEAS !== 'undefined') return ['European Union', 'EU'];
        else if (createdBy.IDENTIFIER === 'OP_DATPRO') return [];
        else if (Array.isArray(createdBy)) {
            // console.log('createdBy Arr');
            createdBy.forEach(function (item) {
                // console.log('createdBy pushing: '+item.PREFLABEL+', '+item.IDENTIFIER);
                authorArr.push(item.PREFLABEL, item.IDENTIFIER);
            })
        }
        else {
            // console.log('createdBy non-array');
            authorArr.push(createdBy.PREFLABEL, createdBy.IDENTIFIER);
        }
    }
    if (createdByAgent) {
        if (Array.isArray(createdByAgent)) {
            // console.log('createdByAgent Arr');
            createdBy.forEach(function (item) {
                // console.log('createdByAgent pushing: '+item.PREFLABEL+', '+item.IDENTIFIER);
                authorArr.push(item.PREFLABEL, item.IDENTIFIER);
            })
        }
        else  {
            // console.log('createdByAgent non-array');
            authorArr.push(createdByAgent.PREFLABEL, createdByAgent.IDENTIFIER);
        }   
    }
    // console.log('Array from new Set');
    // console.log(Array.from(new Set(authorArr)));
    return Array.from(new Set(authorArr));
};

const getLegalBasis = (legalBasis) => {
    if (typeof legalBasis === 'undefined') return [];
    let legalArr = [];
    if (Array.isArray(legalBasis)) {
        // console.log('\nlegalbasis arr');
        legalBasis.forEach(function (item) {
            legalArr.push(typeofUndefined(item, 'URI', 'IDENTIFIER'))
        })
    }
    else {
        legalArr.push(typeofUndefined(legalBasis, 'URI', 'IDENTIFIER'))
    }
    return legalArr.filter((item) => {
        if (item !== '' && item.search('ERROR') === -1) return item;
    });
};

//TODO rebuild into array of possible manifestations
//TODO use that for html retrieval
const getManifestations = (manifestationsArr) => {
    if (manifestationsArr === '') return '';
    let returnObj = {};
    let linkArr = [];
    if (Array.isArray(manifestationsArr)) {
        manifestationsArr.forEach(function (manifestation) {
            if (typeof manifestation.MANIFESTATION_HAS_ITEM === 'undefined') {}
            else if (Array.isArray(manifestation.MANIFESTATION_HAS_ITEM)) {
                manifestation.MANIFESTATION_HAS_ITEM.forEach(function (item) {
                    
                    linkArr.push(typeofUndefined(item, 'URI', 'VALUE'));
                });
            }
            else {
                linkArr.push(typeofUndefined(manifestation.MANIFESTATION_HAS_ITEM, 'URI', 'VALUE'));

            }
            let fileFormat = typeof manifestation.MANIFESTATION_TYPE !== 'undefined' ? manifestation.MANIFESTATION_TYPE.VALUE : 'unknown';
            returnObj[fileFormat] = linkArr
        });
    }
    else {
        if (typeof manifestationsArr.MANIFESTATION_HAS_ITEM === 'undefined') {}
        else if (Array.isArray(manifestationsArr.MANIFESTATION_HAS_ITEM)) {
            manifestationsArr.MANIFESTATION_HAS_ITEM.forEach(function (item) {
                linkArr.push(typeofUndefined(item, 'URI', 'VALUE'));
            });
        }
        else {
            linkArr.push(typeofUndefined(manifestationsArr.MANIFESTATION_HAS_ITEM, 'URI', 'VALUE'));
        }
        let fileFormat = typeof manifestationsArr.MANIFESTATION_TYPE !== 'undefined' ? manifestationsArr.MANIFESTATION_TYPE.VALUE : 'unknown';
        returnObj[fileFormat] = linkArr
    }
    return returnObj
};

const checkCelexKey = (id) => {
    if (id.search('R\\(') > -1) return id.slice(0, id.search('R\\('));
    else if (id.search('\\(')> -1) return id.slice(0, id.search('\\('));
    else return id;
};

const getType = (celexKey) => {
    if (celexKey === '') return '';
    let typeObj = {
        '1': 'Treaties',
        '2': 'International Agreements',
        '3': 'EU Legislation',
        '4': 'Complementary Legislation',
        '5': 'Preparatory acts',
        '6': 'Jurisprudence',
        '7': 'National implementing decisions',
        '8': 'National case law',
        '9': 'Parliamentary questions',
        '0': 'Consolidated EU Legislation',
        'E': 'EFTA documents'
    };
    return typeObj[celexKey[0]]
};

const getFullText = (fullText) => {
    if (Array.isArray(fullText)) {
        let str = '';
        fullText.forEach(function (obj) {
            str += obj.DRECONTENT
        });
        return str;
    }
    else return fullText.DRECONTENT;
};

function getAddressees(addressees) {
    if (Array.isArray(addressees)) {
        let arr = [];
        addressees.forEach((addressee) => {
            arr.push(addressee.PREFLABEL);
        });
        return arr;
    }
    else {
        return convertStringToArray(typeofUndefined(addressees, 'PREFLABEL'));
    }
}

function getDossierDocIDs (docIDs) {
    if (Array.isArray(docIDs)) {
        let arr = [];
        docIDs.forEach((id) => {
            if (id.VALUE) arr.push(id.VALUE);
        });
        return arr;
    }
    else {
        return [docIDs.VALUE];
    }
}

function getEventContainsWork(containsWork) {
    let linkArr = [];
    if (Array.isArray(containsWork)) {
        containsWork.forEach((workArr) => {
            if (Array.isArray(workArr.SAMEAS)) {
                workArr.SAMEAS.forEach((uri) => {
                    linkArr.push(uri.URI.IDENTIFIER)
                })
            }
            else {
                linkArr.push(workArr.SAMEAS.URI.IDENTIFIER);
            }
        })
    }
    else {
        if (Array.isArray(containsWork.SAMEAS)) {
            containsWork.SAMEAS.forEach((uri) => {
                linkArr.push(uri.URI.IDENTIFIER);
            })
        }
        else {
            linkArr.push(containsWork.SAMEAS.URI.IDENTIFIER);
        }
    }
    // console.log('linkArr: '+linkArr);
    
    return Array.from(new Set(linkArr))
}

function getURIfromArr(arr) {
    if (arr == '') return '';
    let celexArr = [];
    // console.log('\n arr in getURIfromArr');
    // console.dir(arr);
    if (Array.isArray(arr)) {
        // console.log('toplevel is array');
        arr.forEach((item) => {
            let sameas = item.SAMEAS;
            if (Array.isArray(sameas)) {
                sameas.forEach((uri) => {
                    let link = typeofUndefined(uri.URI, 'IDENTIFIER');
                    if (typeofUndefined(uri.URI, 'TYPE') === 'celex' && link != null) 
                        celexArr.push(link);
                });
            }
            else if (sameas.URI.TYPE === 'celex' && sameas.URI.IDENTIFIER != null) 
                celexArr.push(sameas.URI.IDENTIFIER);
        })
    }
    else {
        let sameas = arr.SAMEAS;
        if (Array.isArray(sameas)) {
            sameas.forEach((uri) => {
                let link = typeofUndefined(uri.URI, 'IDENTIFIER');
                if (typeofUndefined(uri.URI, 'TYPE') === 'celex' && link != null) 
                    celexArr.push(link);
            });
        }
        else if (sameas.URI.TYPE === 'celex' && sameas.URI.IDENTIFIER != null) 
            celexArr.push(sameas.URI.IDENTIFIER);
    }
    return Array.from(new Set(celexArr));
}

function getEurovocDossierIDs(embeddedNotice) {
    if (embeddedNotice === '' || typeofUndefined(embeddedNotice, 'WORK') === '') return [];
    let eurovoc = embeddedNotice.WORK.WORK_IS_ABOUT_CONCEPT_EUROVOC;
    
    // console.log('\n Eurovoc:');
    // console.dir(eurovoc);
    if (Array.isArray(eurovoc)) {
        let arr = [];
        eurovoc.forEach((item) => {
            // console.log(item.WORK_IS_ABOUT_CONCEPT_EUROVOC_CONCEPT.IDENTIFIER);
            arr.push(item.WORK_IS_ABOUT_CONCEPT_EUROVOC_CONCEPT.IDENTIFIER)
        });
        return Array.from(new Set(arr));
    }
    else {
        return eurovoc.WORK_IS_ABOUT_CONCEPT_EUROVOC_CONCEPT.IDENTIFIER
    }
}

function getDocumentRelationships(data) {
    console.log('doc relationships called ');
    let returnObj = {};
    returnObj.legalBasis = typeof data.WORK.RESOURCE_LEGAL_BASED_ON_RESOURCE_LEGAL !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_BASED_ON_RESOURCE_LEGAL) : [];
    // console.log('legalbasis: '+legalBasis);
    returnObj.references = typeof data.WORK.WORK_CITES_WORK !== 'undefined' ? getLegalBasis(data.WORK.WORK_CITES_WORK) : [];
    // console.log('cited work: '+references);
    returnObj.proposesAmend = typeof data.WORK.RESOURCE_LEGAL_PROPOSES_TO_AMEND_RESOURCE_LEGAL !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_PROPOSES_TO_AMEND_RESOURCE_LEGAL) : [];
    // console.log('amending: '+amending);
    returnObj.amends = typeof data.WORK.RESOURCE_LEGAL_AMENDS_RESOURCE_LEGAL !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_AMENDS_RESOURCE_LEGAL) : [];
    // console.log('amends: '+amends);
    returnObj.corrects = typeof data.WORK.RESOURCE_LEGAL_CORRECTS_RESOURCE_LEGAL !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_CORRECTS_RESOURCE_LEGAL) : [];
    // console.log('corrects: '+corrects);
    returnObj.relation = typeof data.WORK.RESOURCE_LEGAL_RELATED_TO_RESOURCE_LEGAL !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_RELATED_TO_RESOURCE_LEGAL) : [];
    // console.log('relation: '+relation);
    returnObj.repeals = typeof data.WORK.RESOURCE_LEGAL_REPEALS_RESOURCE_LEGAL !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_REPEALS_RESOURCE_LEGAL) : [];
    // console.log('repeals: '+repeals);
    returnObj.repealsImplicitly = typeof data.WORK.RESOURCE_LEGAL_IMPLICITLY_REPEALS_RESOURCE_LEGAL !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_IMPLICITLY_REPEALS_RESOURCE_LEGAL) : [];
    returnObj.completes = typeof data.WORK.RESOURCE_LEGAL_COMPLETES_RESOURCE_LEGAL !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_COMPLETES_RESOURCE_LEGAL) : [];
    returnObj.rendersObsolete = typeof data.WORK.RESOURCE_LEGAL_RENDERS_OBSOLETE_RESOURCE_LEGAL !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_RENDERS_OBSOLETE_RESOURCE_LEGAL) : [];
    returnObj.derogates = typeof data.WORK.RESOURCE_LEGAL_DEROGATES_RESOURCE_LEGAL !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_DEROGATES_RESOURCE_LEGAL) : [];
    returnObj.extendsApplication = typeof data.WORK.RESOURCE_LEGAL_EXTENDS_APPLICATION_RESOURCE_LEGAL  !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_EXTENDS_APPLICATION_RESOURCE_LEGAL) : [];
    returnObj.confirms = typeof data.WORK.RESOURCE_LEGAL_CONFIRMS_RESOURCE_LEGAL  !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_CONFIRMS_RESOURCE_LEGAL) : [];
    returnObj.tacklesSimilarQuestion = typeof data.WORK.RESOURCE_LEGAL_TACKLES_SIMILAR_QUESTION_AS_RESOURCE_LEGAL  !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_TACKLES_SIMILAR_QUESTION_AS_RESOURCE_LEGAL) : [];
    returnObj.interpretesAuthoritatively = typeof data.WORK.RESOURCE_LEGAL_INTERPRETES_AUTHORITATIVELY_RESOURCE_LEGAL  !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_INTERPRETES_AUTHORITATIVELY_RESOURCE_LEGAL) : [];
    returnObj.addsTo = typeof data.WORK.RESOURCE_LEGAL_ADDS_TO_RESOURCE_LEGAL  !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_ADDS_TO_RESOURCE_LEGAL) : [];
    returnObj.implements = typeof data.WORK.RESOURCE_LEGAL_IMPLEMENTS_RESOURCE_LEGAL  !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_IMPLEMENTS_RESOURCE_LEGAL) : [];
    returnObj.extendsValidity = typeof data.WORK.RESOURCE_LEGAL_EXTENDS_VALIDITY_OF_RESOURCE_LEGAL  !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_EXTENDS_VALIDITY_OF_RESOURCE_LEGAL) : [];
    returnObj.replaces = typeof data.WORK.RESOURCE_LEGAL_REPLACES_RESOURCE_LEGAL  !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_REPLACES_RESOURCE_LEGAL) : [];
    returnObj.incorporates = typeof data.WORK.RESOURCE_LEGAL_INCORPORATES_RESOURCE_LEGAL  !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_INCORPORATES_RESOURCE_LEGAL) : [];
    returnObj.reestablishes = typeof data.WORK.RESOURCE_LEGAL_REESTABLISHES_RESOURCE_LEGAL  !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_REESTABLISHES_RESOURCE_LEGAL) : [];
    returnObj.partiallySuspends = typeof data.WORK.RESOURCE_LEGAL_PARTIALLY_SUSPENDS_RESOURCE_LEGAL  !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_PARTIALLY_SUSPENDS_RESOURCE_LEGAL) : [];
    returnObj.defersApplication = typeof data.WORK.RESOURCE_LEGAL_DEFERS_APPLICATION_OF_RESOURCE_LEGAL  !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_DEFERS_APPLICATION_OF_RESOURCE_LEGAL) : [];
    returnObj.relatedQuestion = typeof data.WORK.RESOURCE_LEGAL_RELATED_QUESTION_TO_RESOURCE_LEGAL  !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_RELATED_QUESTION_TO_RESOURCE_LEGAL) : [];
    returnObj.containsEESCopinion= typeof data.WORK.RESOURCE_LEGAL_CONTAINS_EESC_OPINION_ON_RESOURCE_LEGAL  !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_CONTAINS_EESC_OPINION_ON_RESOURCE_LEGAL) : [];
    returnObj.containsEPopinion= typeof data.WORK.RESOURCE_LEGAL_CONTAINS_EESC_OPINION_ON_RESOURCE_LEGAL  !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_CONTAINS_EESC_OPINION_ON_RESOURCE_LEGAL) : [];
    returnObj.containsCORopinion= typeof data.WORK.RESOURCE_LEGAL_CONTAINS_COR_OPINION_ON_RESOURCE_LEGAL  !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_CONTAINS_COR_OPINION_ON_RESOURCE_LEGAL) : [];
    returnObj.partiallyRefersTo= typeof data.WORK.RESOURCE_LEGAL_PARTIALLY_REFERS_TO_RESOURCE_LEGAL  !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_PARTIALLY_REFERS_TO_RESOURCE_LEGAL) : [];
    returnObj.adopts = typeof data.WORK.RESOURCE_LEGAL_ADOPTS_RESOURCE_LEGAL  !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_ADOPTS_RESOURCE_LEGAL) : [];
    returnObj.partiallyAdopts = typeof data.WORK.RESOURCE_LEGAL_PARTIALLY_ADOPTS_RESOURCE_LEGAL !== 'undefined' ? getLegalBasis(data.WORK.RESOURCE_LEGAL_PARTIALLY_ADOPTS_RESOURCE_LEGAL) : [];
    return returnObj;
}

function getEventObject(event) {
    let date = strToISOString(event.EVENT_LEGAL_DATE.VALUE);
    // console.log('Event date: '+date);
    let eventContainsWork = typeof event.EVENT_LEGAL_CONTAINS_WORK !== 'undefined' ? getEventContainsWork(event.EVENT_LEGAL_CONTAINS_WORK) : [];
    // console.log('Event contains work: '+eventContainsWork);
    let eventLabel = typeof event.EVENT_LEGAL_HAS_TYPE_CONCEPT_TYPE_EVENT_LEGAL !== 'undefined' ? convertStringToArray(event.EVENT_LEGAL_HAS_TYPE_CONCEPT_TYPE_EVENT_LEGAL.PREFLABEL) : [];
    // console.log('Event label: '+eventLabel);
    let decision =  typeof event.EVENT_LEGAL_USES_CONCEPT_TYPE_DECISION !== 'undefined' ? convertStringToArray(event.EVENT_LEGAL_USES_CONCEPT_TYPE_DECISION.PREFLABEL) : [];
    // console.log('Event decision: '+decision);
    let eventOrganisation = typeof event.EVENT_LEGAL_INITIATED_BY_INSTITUTION !== 'undefined' ? convertStringToArray(event.EVENT_LEGAL_INITIATED_BY_INSTITUTION.PREFLABEL) : [];
    // console.log('Event org: '+eventOrganisation);
    let eventSubOrganisation = typeof event.EVENT_LEGAL_RESPONSIBILITY_OF_INSTITUTION !== 'undefined' ? getArray(event.EVENT_LEGAL_RESPONSIBILITY_OF_INSTITUTION.ALTLABEL, event.EVENT_LEGAL_RESPONSIBILITY_OF_INSTITUTION.PREFLABEL) : [];
    // console.log('Event sub org: '+eventSubOrganisation);
    let documentID = typeof event.EVENT_LEGAL_DOCUMENT_REFERENCE !== 'undefined' ? getDossierDocIDs(event.EVENT_LEGAL_DOCUMENT_REFERENCE) : [];
    // console.log('Event doc ID '+documentID);
    let addressees = typeof event['EVENT_LEGAL_FORMALLY-ADDRESSES_INSTITUTION'] !== 'undefined' ? getAddressees(event['EVENT_LEGAL_FORMALLY-ADDRESSES_INSTITUTION']) : [];
    // console.log('Event addressees: '+addressees);
    let responsiblePerson = typeof event.EVENT_LEGAL_RESPONSIBILITY_OF_PERSON !== 'undefined' ? getPersons(event.EVENT_LEGAL_RESPONSIBILITY_OF_PERSON) : [];
    // console.log('Event responsible person: '+responsiblePerson);
    let rapporteur = typeof event.REPORTED_BY !== 'undefined' ? getPersons(event.REPORTED_BY) : [];
    // console.log('Event rapporteur: '+rapporteur);
    let eventType =typeof event.EVENT_LEGAL_USES_CONCEPT_MODE_DECISION !== 'undefined' ? convertStringToArray(event.EVENT_LEGAL_USES_CONCEPT_MODE_DECISION.PREFLABEL) : [];
    // console.log('Event type: '+eventType);
    let mandatorilyConsults = getAddressees(typeofUndefined(event, 'EVENT_LEGAL_MANDATORILY-CONSULTS_INSTITUTION'));
    let optionallyConsults = getAddressees(typeofUndefined(event, 'EVENT_LEGAL_OPTIONALLY-CONSULTS_INSTITUTION'));
    return {
        date, eventContainsWork, eventLabel, decision, eventOrganisation, eventSubOrganisation, 
        documentID, addressees, responsiblePerson, rapporteur, eventType, mandatorilyConsults, optionallyConsults
    }
}

function getEventObjectLogged(event) {
    let date = strToISOString(event.EVENT_LEGAL_DATE.VALUE);
    console.log('Event date: '+date);
    let eventContainsWork = typeof event.EVENT_LEGAL_CONTAINS_WORK !== 'undefined' ? getEventContainsWork(event.EVENT_LEGAL_CONTAINS_WORK) : '';
    console.log('Event contains work: '+eventContainsWork);
    let eventLabel = typeof event.EVENT_LEGAL_HAS_TYPE_CONCEPT_TYPE_EVENT_LEGAL !== 'undefined' ? event.EVENT_LEGAL_HAS_TYPE_CONCEPT_TYPE_EVENT_LEGAL.PREFLABEL : '';
    console.log('Event label: '+eventLabel);
    let decision =  typeof event.EVENT_LEGAL_USES_CONCEPT_TYPE_DECISION !== 'undefined' ? event.EVENT_LEGAL_USES_CONCEPT_TYPE_DECISION.PREFLABEL : '';
    console.log('Event decision: '+decision);
    let eventOrganisation = typeof event.EVENT_LEGAL_INITIATED_BY_INSTITUTION !== 'undefined' ? event.EVENT_LEGAL_INITIATED_BY_INSTITUTION.PREFLABEL : '';
    console.log('Event org: '+eventOrganisation);
    let eventSubOrganisation = typeof event.EVENT_LEGAL_RESPONSIBILITY_OF_INSTITUTION !== 'undefined' ? getArray(typeofUndefined(event.EVENT_LEGAL_RESPONSIBILITY_OF_INSTITUTION, 'ALTLABEL'), typeofUndefined(event.EVENT_LEGAL_RESPONSIBILITY_OF_INSTITUTION, 'PREFLABEL')) : '';
    console.log('Event sub org: '+eventSubOrganisation);
    let documentID = typeof event.EVENT_LEGAL_DOCUMENT_REFERENCE !== 'undefined' ? getDossierDocIDs(event.EVENT_LEGAL_DOCUMENT_REFERENCE) : '';
    console.log('Event doc ID '+documentID);
    let addressees = typeof event['EVENT_LEGAL_FORMALLY-ADDRESSES_INSTITUTION'] !== 'undefined' ? getAddressees(event['EVENT_LEGAL_FORMALLY-ADDRESSES_INSTITUTION']) : '';
    console.log('Event addressees: '+addressees);
    let responsiblePerson = typeof event.EVENT_LEGAL_RESPONSIBILITY_OF_PERSON !== 'undefined' ? getPersons(event.EVENT_LEGAL_RESPONSIBILITY_OF_PERSON) : '';
    console.log('Event responsible person: '+responsiblePerson);
    let rapporteur = typeof event.REPORTED_BY !== 'undefined' ? getPersons(event.REPORTED_BY) : '';
    console.log('Event rapporteur: '+rapporteur);
    let eventType =typeof event.EVENT_LEGAL_USES_CONCEPT_MODE_DECISION !== 'undefined' ? event.EVENT_LEGAL_USES_CONCEPT_MODE_DECISION.PREFLABEL : '';
    console.log('Event type: '+eventType);
    let mandatorilyConsults = getAddressees(typeofUndefined(event, 'EVENT_LEGAL_MANDATORILY-CONSULTS_INSTITUTION'));
    console.log('mand consults: '+mandatorilyConsults);
    let optionallyConsults = getAddressees(typeofUndefined(event, 'EVENT_LEGAL_OPTIONALLY-CONSULTS_INSTITUTION'));
    console.log('opt consults: '+optionallyConsults);
    return {
        date, eventContainsWork, eventLabel, decision, eventOrganisation, eventSubOrganisation,
        documentID, addressees, responsiblePerson, rapporteur, eventType, mandatorilyConsults, optionallyConsults
    }
}

function convertStringToArray(string) {
    if (Array.isArray(string))
        return string;
    else if (string == '')
        return [];
    else if (typeof string == 'string')
        return [string];
    else
        return [];
}

function getPersons(persons) {
    let personsArr = [];
    if (Array.isArray(persons)) {
        persons.forEach((person) => {
            personsArr.push(person.SAMEAS.URI.IDENTIFIER)
        })
    }
    else personsArr.push(persons.SAMEAS.URI.IDENTIFIER);
    
    return personsArr;
}

function getArray(arr, str) {
    if (!Array.isArray(arr)) return [];
    let newArr = arr;
    newArr.push(str);
    return newArr;
}

function getEurovocIDs(eurovocArr) {
    if(eurovocArr === '') return [];
    if (!Array.isArray(eurovocArr)) return eurovocArr.WORK_IS_ABOUT_CONCEPT_EUROVOC_CONCEPT.IDENTIFIER;
    let idArr = [];
    eurovocArr.forEach((concept) => {
        for (let key in concept) {
            if ( key.search('EUROVOC') > -1) {

                //console.log(key)
                if(concept[key] !== null && typeof concept[key].IDENTIFIER != 'undefined' && concept[key].IDENTIFIER != null)
                    idArr.push(concept[key].IDENTIFIER);
            }
        }
    });
    return Array.from(new Set(idArr));
}

async function getDossierData(inverse, db, documentsColl, aqlQuery) {
    // console.log('getdossierdata called');
    // console.log(inverse);
    let dossierIDArr= [];
    let workPartDossier = typeofUndefined(inverse, 'WORK_PART_OF_DOSSIER');
    
    if (Array.isArray(workPartDossier)) {
        workPartDossier.forEach((item) => {
            if (typeofUndefined(item, 'URI', 'TYPE') === 'cellar') {
                let IDWithSlashes = typeofUndefined(item, 'URI', 'IDENTIFIER');

                if (IDWithSlashes.length > 30) {
                    checkExistsByKey(IDWithSlashes, documentsColl, db, aqlQuery).then((resArray) => {
                        if (resArray.length > 0)
                            dossierIDArr.push(resArray[0].interinstitutionalReference);
                        else
                            dossierIDArr.push(IDWithSlashes.replace(/\//g, '-'))
                    },
                        err => Promise.reject(err))
                }
                else
                    dossierIDArr.push(IDWithSlashes.replace(/\//g, '-'))
            }
        })
    }
    else {
        if (typeofUndefined(workPartDossier, 'URI', 'TYPE') === 'cellar') {
            let IDWithSlashes = typeofUndefined(workPartDossier, 'URI', 'IDENTIFIER'); // '388ac40f-a3e3-11e5-b528-01aa75ed71a1'
            // console.log(IDWithSlashes);
            if (IDWithSlashes.length > 30) {
                checkExistsByKey(IDWithSlashes, documentsColl, db, aqlQuery).then((resArray) => {
                        if (resArray.length > 0)
                            dossierIDArr.push(resArray[0].interinstitutionalReference);
                        else
                            dossierIDArr.push(IDWithSlashes.replace(/\//g, '-'))
                    },
                    err => Promise.reject(err))
            }
            else
                dossierIDArr.push(IDWithSlashes.replace(/\//g, '-'))

        }
    }
    return dossierIDArr;
    
}

function getDocumentLinks(links) {
    if (links === '') return '';
    let linkObj = {};
    if (Array.isArray(links)) {
        links.forEach((link) => {
            linkObj[link.attributes.type] = link['$value']
            })
        }
    else linkObj[links.attributes.type] = links['$value']
    return linkObj;
}

//COM_2016_0544_FIN \ COM/2016/544/FINAL => 'COM/2016/0544 final' Notice the diff in 544 vs 0544
// Done because this is how it appears in document.subTitle
function translateCOMDocumentCodes(codeArr) {
    let translatedArr = [];
    codeArr.forEach((code) => {
        if (code.search('COM') > -1 && (code.match(/_/g) || [] ).length === 3 || (code.match(/\//g) || [] ).length === 3) {
            if (code.search('_') > -1) {
                let split = code.split('_');
                if (split[2].length == 1) split[2] === '000'+split[2];
                else if (split[2].length == 2) split[2] === '00'+split[2];
                else if (split[2].length == 3) split[2] === '0'+split[2];
                return split[0]+'/'+split[1]+'/'+split[2]
            }
            else {
                let split = code.split('/');
                if (split[2].length == 1) split[2] === '000'+split[2];
                else if (split[2].length == 2) split[2] === '00'+split[2];
                else if (split[2].length == 3) split[2] === '0'+split[2];
                return split[0]+'/'+split[1]+'/'+split[2]
            }
        }
    });
    return translatedArr
}

function filterNonCelex(linkArr) {
    let filteredArr = [];
    linkArr.forEach((link) => {
        if (link.length >= 9 && link.length <= 11 && link.search('_') === -1 && link.search('/') === -1) filteredArr.push(link)
    });
    return filteredArr;
}

function createSubTitle(documentObj) {
    if (documentObj.type === 'Preparatory acts') {
        // Official Journal of the European Union (2016/C 353/23) ...
        // P8_TA(2015)0199 => 2016/C 353/23 - P8_TA(2015)0199
        let formatC = documentObj.fullText.match(/\(\d{4}\/C\s?\d{1,4}\/\d{1,4}\)/);
        if (!formatC)
            return [];

        formatC[0] = formatC[0].replace(/(\()|(\))/g, '');
        let formatP =documentObj.fullText.match(/(P\d_TA\(\d{4}\)\d{4,6})/);

        if (formatC)
            return (formatP) ? formatC[0]+' - '+formatP[0] : formatC[0];

        else {
            //  fullText (2001/C 96 E/10) => 2001/C 96 E/10
            let fullTextGoodFormat = documentObj.fullText.match(/\(\d{4}\/C\s?\d{1,4}\s\S{1,4}\/\d{1,4}\)/);
            if (fullTextGoodFormat)
                return fullTextGoodFormat[0].replace(/(\()|(\))/, '');

            //    Check fulltext for COM reference => COM(2004) 485 final => COM(2004)485
            let fullTextGoodFormat2 = documentObj.fullText.match(/\w{1,5}\(\d{2,4}\)\s\d{1,4}/);
            if (fullTextGoodFormat2)
                return fullTextGoodFormat2[0].replace(' ', '');
        }
    }
    else if (documentObj.type === 'EU Legislation') {
        //Council Directive (EU) 2016/856 of 25 May 2016 amending Directive 2006/112/EC => 2016/856/EU
        let oldFormat = documentObj.title.match(/\(\w{2,6}\)\s\d{4}\/\d{1,4}/);
        // 2004/618/EC: Council Decision of 11 August 2004 on the conclusion of an Agreement => 2004/618/EC
        let goodFormat = documentObj.title.match(/\d{2,4}\/\d{1,4}\/\w{1,6}/);
        if (!oldFormat && !goodFormat)
            return [];
        else if (!oldFormat && goodFormat) {
            return goodFormat[0];
        }
        console.log('oldformat:');
        console.dir(oldFormat);
        console.log('title:');
        console.log(documentObj.title);
        oldFormat = oldFormat[0].replace(/(\()|(\))/g, '');
        let split = oldFormat.split(' ');
        let split1 = split[1].split('/');
        split1.unshift(split[0]);
        return split1[1]+'/'+split1[2]+'/'+split1[0];
    }
    else
        return [];
}

export { retrieveAuthors, getLegalBasis, getManifestations, checkCelexKey, getType, getFullText, getAddressees, getDossierDocIDs,
getEventContainsWork, getURIfromArr, getEurovocDossierIDs, getDocumentRelationships, getEventObject, getEurovocIDs, getEventObjectLogged,
getDossierData, getDocumentLinks, translateCOMDocumentCodes, filterNonCelex, createSubTitle, convertStringToArray}