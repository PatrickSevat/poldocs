//Check for Q-values and filter them, if non Q-value return first hit
const decideName = (nameArr) => {
    for (let i = 0; i<nameArr.length; i++) {
        let pos0 = nameArr[i].slice(0,1);
        let num = nameArr[i].slice(1);
        if (pos0 !== 'Q' && isNaN(num) && nameArr[i] !== '') {
            return nameArr[i];
        }
        else if (i === nameArr.length -1) return ''
    } 
};

export {decideName}