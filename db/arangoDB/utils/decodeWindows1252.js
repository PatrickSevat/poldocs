/**
 * Created by Patrick on 11/08/2016.
 */

const decode = (string) => {
    return new Promise((resolve, reject) => {
        try {
            var iconv = require("iconv-lite");
            var encode = iconv.encode(string, 'windows-1252');
            var decoded = iconv.decode(new Buffer(encode), 'utf8');
            resolve(decoded);
        }
        catch (err) {
            console.log('CAUGHT ERR in decode: \n'+err);
            reject(err);
        }
    })

};

export {decode}