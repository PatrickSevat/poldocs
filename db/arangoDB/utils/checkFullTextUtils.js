let queryAllNames = `
let persons = (
    for x in people
    return {name: TO_ARRAY(x.name), id: x._id}
)
let executiveBodies = (
    for x in executiveBodies
    return {name: TO_ARRAY(x.name), id: x._id}
)
let legislativeBodies = (
    for x in legislativeBodies
    return {name: TO_ARRAY(x.name), id: x._id}
)
let companies = (
    for x in companies
    return {name: TO_ARRAY(x.name), id: x._id}
    )
let IGOs = (
    for x in IGO
    return {name: TO_ARRAY(x.name), id: x._id}
)
let countries = (
    for x in countries
    return {name: TO_ARRAY(x.name), id: x._id}
)
let judiciaries = (
    for x in judiciaries
    return {name: TO_ARRAY(x.name), id: x._id}
)
let otherBodies = (
    for x in otherBodies
    return {name: TO_ARRAY(x.name), id: x._id}
)
let parties = (
    for x in politicalParties
    return {name: TO_ARRAY(x.name), id: x._id}
    )
let offices = (
    for x in publicOffices
    return {name: TO_ARRAY(x.name), id: x._id}
)
let gemeenten = (
    for x in municipalities
    return {name: TO_ARRAY(x.name), id: x._id}
    )
let provinces = (
    for x in provinces
    return {name: TO_ARRAY(x.name), id: x._id}
)

let appended = union(persons, executiveBodies, legislativeBodies, companies, IGOs, countries, judiciaries, otherBodies, parties, offices, gemeenten, provinces)

return appended`;

let richtlijnRegExp = new RegExp('\\bRichtlijn\\s\\d+\\/\\d+\\/\\w+');
let positions = ['Hoge Vertegenwoordiger', ]

function match(str, arr) {
    console.log(Date.now());
    let returnArr = [];

    arr.forEach((obj) => {
        obj.name.forEach((name) => {
            if (name.indexOf(' ')> -1 && str.indexOf(name) > -1) returnArr.push(obj);
            else {
                var regex = new RegExp("\\b(?:" + name+ ")\\b", "gi");
                if (str.match(regex)) returnArr.push(obj)
            }
        })
    });
    console.log(Date.now());
    console.dir(returnArr);
}