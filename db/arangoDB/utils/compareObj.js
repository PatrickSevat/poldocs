import isEqual from 'lodash.isequal';

const compareObj = (moreAttrObj, lessAttrObj ) => {
    let keysArr = [];
    let diffArr = [];
    let returnObj = {};

    //Get all different keys in both obj
    for (let key in moreAttrObj) keysArr.push(key);
    for (let key2 in lessAttrObj) {
        if (keysArr.indexOf(key2) === -1) keysArr.push(key2)
    }

    //Iterate all keys in keysArr to determine best value
    for (let attr in keysArr) {

        //check if either is undefined
        if (typeof moreAttrObj[keysArr[attr]] === "undefined") returnObj[keysArr[attr]] = lessAttrObj[keysArr[attr]];
        else if (typeof lessAttrObj[keysArr[attr]] === "undefined") returnObj[keysArr[attr]] = moreAttrObj[keysArr[attr]];

        //    check if equal
        else if (lessAttrObj[keysArr[attr]] === moreAttrObj[keysArr[attr]] ) returnObj[keysArr[attr]] = moreAttrObj[keysArr[attr]];

        //    check if attr is obj itself
        else if (!Array.isArray(lessAttrObj[keysArr[attr]]) && typeof lessAttrObj[keysArr[attr]] === 'object' &&
            !Array.isArray(moreAttrObj[keysArr[attr]]) && typeof moreAttrObj[keysArr[attr]] === 'object') {
                returnObj[keysArr[attr]] = compareObj(moreAttrObj[keysArr[attr]], lessAttrObj[keysArr[attr]])
        }
            
        //    TODO create a possibility if one key is obj, other is str or arr

        //    check if one is ''
        else if (lessAttrObj[keysArr[attr]] === '' && moreAttrObj[keysArr[attr]] !== '') returnObj[keysArr[attr]] = moreAttrObj[keysArr[attr]];
        else if (moreAttrObj[keysArr[attr]] === '' && lessAttrObj[keysArr[attr]] !== '') returnObj[keysArr[attr]] = lessAttrObj[keysArr[attr]];

        //    both have values, return both to returnObj as ARRAY
        else if (moreAttrObj[keysArr[attr]] !== '' && lessAttrObj[keysArr[attr]] !== '') {

            //both are arrays, loop over both and save unique instances
            if (moreAttrObj[keysArr[attr]] instanceof Array && lessAttrObj[keysArr[attr]] instanceof Array) {
                let newArr= [];
                for (let value in moreAttrObj[keysArr[attr]]) {

                    //We have an array of objects or array of arrays
                    if (typeof moreAttrObj[keysArr[attr]][value] === 'object') {
                        //Iterate over newArr to see if one of its obj is same as the obj we are checking against
                        if (newArr.length === 0) newArr.push(moreAttrObj[keysArr[attr]][value]);
                        else {
                            let equal = false;
                            newArr.forEach((item) => {
                                if(isEqual(item, moreAttrObj[keysArr[attr]][value])) equal = true;
                            });
                            if (!equal) newArr.push(moreAttrObj[keysArr[attr]][value])
                        }
                    }
                    // Not an array of objects
                    else {
                        if (newArr.indexOf(moreAttrObj[keysArr[attr]][value]) === -1) newArr.push(moreAttrObj[keysArr[attr]][value])
                    }

                }
                for (let value2 in lessAttrObj[keysArr[attr]]) {
                    if (typeof lessAttrObj[keysArr[attr]][value2] === 'object') {
                        if (newArr.length === 0) newArr.push(lessAttrObj[keysArr[attr]][value2])
                        else {
                            let equal2 = false;
                            newArr.forEach((item) => {
                                if(isEqual(item, lessAttrObj[keysArr[attr]][value2])) equal2 = true;
                            });
                            if (!equal2) newArr.push(lessAttrObj[keysArr[attr]][value2]);
                        }
                    }
                    else {
                        if (newArr.indexOf(lessAttrObj[keysArr[attr]][value2]) === -1) newArr.push(lessAttrObj[keysArr[attr]][value2])
                    }

                }
                returnObj[keysArr[attr]] = newArr
            }

            //    one is Array, add other to Array
            else if ((moreAttrObj[keysArr[attr]] instanceof Array)) {
                returnObj[keysArr[attr]] = moreAttrObj[keysArr[attr]];
                if (returnObj[keysArr[attr]].indexOf(lessAttrObj[keysArr[attr]]) === -1) returnObj[keysArr[attr]].push(lessAttrObj[keysArr[attr]])
            }
            else if ((lessAttrObj[keysArr[attr]] instanceof Array )) {
                returnObj[keysArr[attr]] = lessAttrObj[keysArr[attr]];
                if (returnObj[keysArr[attr]].indexOf(moreAttrObj[keysArr[attr]]) === -1) returnObj[keysArr[attr]].push(moreAttrObj[keysArr[attr]])
            }

            //    neither is array, push them both to new array
            else {
                let newArr2 =[];
                newArr2.push(lessAttrObj[keysArr[attr]]);
                newArr2.push(moreAttrObj[keysArr[attr]]);
                returnObj[keysArr[attr]] = newArr2;
            }
        }
    }
    // console.log(JSON.stringify(returnObj));
    return returnObj;
};


export  {compareObj} ;

/*****************************************************************************
 *
 *          TEST DATA & VARIABLES
 *      TODO add a document with array containing objects
 *
 *****************************************************************************/

const test1 = {
    "item":  "http://www.wikidata.org/entity/Q491999",
    "coords": "Point(5.22 52.75)",
    "itemLabel":"Andijk2"
};

const test2 = {
    "item":  "http://www.wikidata.org/entity/Q491999",
    "endDate":  "2011-01-01T00:00:00Z",
    "coords": "Point(5.22 52.75)",
    "itemLabel": ["Andijk", "Andijk2"],
    "replacedByLabel": "Medemblik"
};



const expectedResult = {"item":"http://www.wikidata.org/entity/Q491999","endDate":"2011-01-01T00:00:00Z","coords":"Point(5.22 52.75)","itemLabel":["Andijk","Andijk2"],"replacedByLabel":"Medemblik"};


