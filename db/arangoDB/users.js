import bcrypt from 'bcrypt-nodejs';
import {db} from './arangodb';
import { typeofUndefined } from './utils/typeofUndefined';

const usersColl = db.collection('users');

const saltFactor = 10;
const maxLogins = 10;
const lockOutTime = 2 * 60 * 60 * 1000; //= 2 hours

/**
 * userObj:
 * {_key: req.body.username,
        email: req.body.email,
        password: req.body.password}

 * Added in createNewUser:
 * userObj.logInAttempts
 * userObj.lockedUntil
 * */

function bcryptGenSaltPromise(saltFactor) {
    return new Promise((resolve, reject) => {
        bcrypt.genSalt(saltFactor, (err, salt) => {
            if (err) reject(err);
            else resolve(salt);
        })
    })
}

function bcryptHashPromise(password, salt) {
    return new Promise((resolve, reject) => {
        bcrypt.hash(password, salt, null, (err, hash) => {
            if (err) reject(err);
            else resolve(hash);
        })
    })
}

async function createNewUser(userObj) {
    try {
        console.log('userObj in createNewUser');
        console.dir(userObj);

        let salt = await bcryptGenSaltPromise(saltFactor);
        let hash = await bcryptHashPromise(userObj.password, salt);

        userObj.password = hash;
        userObj.logInAttempts = 0;
        userObj.lockedUntil = 0;

        let userMeta = await usersColl.save(userObj);
        return new Promise((resolve, reject) => {
            resolve(userMeta);
        });

    }
    catch (err) {
        let arangoErr = typeofUndefined(err, 'response', 'body', 'errorMessage');
        console.log('ArangoErr in catch err createNewUser: \n'+arangoErr);

        if (arangoErr === 'cannot create document, unique constraint violated') {
            return new Promise((resolve, reject) => {
                reject('usernameTaken');
            })
        }
        else return new Promise((resolve, reject) => {
            reject(err);
        })
    }
}

async function getAuthenticated(username, candidatePassword) {
    let userWithHashedPW;
    try {
        userWithHashedPW = await usersColl.document(username);

        // check if user is locked out
        if (userWithHashedPW.lockedUntil > Date.now()) {
            userWithHashedPW.logInAttempts += 1;
            return new Promise((resolve, reject) => {
                reject('lockedOut');
            })
        }
        else {
            let pwMatch = await bcryptComparePromise(candidatePassword, userWithHashedPW.password);

            if (pwMatch) {
                if (userWithHashedPW.logInAttempts === 0 && userWithHashedPW.lockedUntil === 0) {
                    return new Promise((resolve, reject) => {
                        resolve(userWithHashedPW);
                    })
                }
                else {
                    userWithHashedPW.logInAttempts = 0;
                    userWithHashedPW.lockedUntil = 0;
                    let replacedUser = await usersColl.replace(userWithHashedPW._key, userWithHashedPW, {waitForSync: true});
                    return new Promise((resolve, reject) => {
                        resolve(userWithHashedPW);
                    })
                }
            }
        }
    }
    catch (err) {
        let arangoErr = typeofUndefined(err, 'response', 'body', 'errorMessage');
        console.log('ArangoErr in catch err getAuth: \n'+arangoErr);
        if (arangoErr === 'document not found') {
            return new Promise((resolve, reject) =>{
                console.log('rejecting unknownUsername');
                reject('unknownUsername');
            })
        }
        if (err === 'wrongPassword') {
            userWithHashedPW.logInAttempts += 1;
            if (userWithHashedPW.logInAttempts >= 5) {
                //Lock for 15 min
                userWithHashedPW.lockedUntil = (Date.now() + (15*60*1000));
                await usersColl.replace(userWithHashedPW._key, userWithHashedPW);
                return new Promise((resolve, reject) => {
                    reject('lockedOut');
                })
            }
            else {
                await usersColl.replace(userWithHashedPW._key, userWithHashedPW);
                return new Promise((resolve, reject) => {
                    reject('wrongPassword');
                })
            }
        }
        else {
            console.log(err);
            return new Promise((resolve, reject) =>{
                reject(err);
            })    
        }
        
    }
}

function bcryptComparePromise(candidatePW, hash) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(candidatePW, hash, (err, isMatch) => {
            if (err) {
                console.log('bcrypt compare failed: \n'+err);
                reject(err);  
            } 
            if (isMatch) resolve(true);
            else if (!isMatch) {
                reject('wrongPassword');
            }
        })
    })
}

export {createNewUser, getAuthenticated}
