import superagent from 'superagent';
import config from '../../../config.json';

export default function getHomepageSuggestES(query) {
    return new Promise((resolve, reject) => {
        superagent.post(`${config.ES_HOST}:${config.ES_PORT}/_search`)
            .send({
                "query": {
                    "query_string": {
                        "query": query+'*',
                        "fields": ["title^10", "fullText", "name"],
                        "default_operator": "AND"
                    }
                },
                "sort": {
                    "_score": "desc"
                },
                "size": 10
            })
            .end((err, res) => {
                if (err) {
                    console.log('err retrieving results from ES', err.error);
                    reject(err);
                }

                const results = res.body.hits.hits.map(item => {return item._source});
                console.log(results);
                resolve(results);
            })
    })


}