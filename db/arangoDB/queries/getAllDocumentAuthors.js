import {db, aqlQuery} from '../arangodb'
let query = `
let authorArr = (
 for doc in documents
 return TO_ARRAY(doc.authors)
)

let authorArrFiltered = unique(flatten(authorArr))

for author in authorArrFiltered
    filter author != null && !contains(author, ':') && !contains(author, '/') && author != ""
    let authorDotReplaces0 = substitute(author, '.', ' ')
    let authorDotReplaces = substitute(authorDotReplaces0, '-', ' ')
    
    let people = (
        for x in fulltext(people, 'name', authorDotReplaces)
        return {name: x.name, id: x._id}
    )
    
    let otherBodies = (
        for x in fulltext(otherBodies, 'name', authorDotReplaces)
        return {name: x.name, id: x._id}
    )
    
    
    let legislativeBodies = (
        for x in fulltext(legislativeBodies, 'name', authorDotReplaces)
        return {name: x.name, id: x._id}
    )
    
    
    let executiveBodies = (
        for x in fulltext(executiveBodies, 'name', authorDotReplaces)
        return {name: x.name, id: x._id}
    )
    
    
    
    let judiciaries = (
        for x in fulltext(judiciaries, 'name', authorDotReplaces)
        return {name: x.name, id: x._id}
    )
    
    let countries = (
        for x in fulltext(countries, 'name', authorDotReplaces)
        return {name: x.name, id: x._id}
    )
    
    let nameID = union(countries, judiciaries, executiveBodies, legislativeBodies, otherBodies,  people)
    
    return {author, nameID}
        

`;

function getAllDocumentAuthors() {
    return new Promise((resolve, reject) => {
        db.query(query).then((cursor) => {
            cursor.all().then((arr) => {
                if (arr)
                    resolve(arr);
                else
                    resolve([]);
            })
        }, err => {
            console.log(err);
            reject(err);
        })
    })

}

exports.getAllAuthorsQuery = query;

exports.getAllDocumentAuthors = getAllDocumentAuthors;

/**
 option2 via documents:

 let authorArr = (
 for doc in documents
 return TO_ARRAY(doc.authors)
 )

 let authorArrFiltered = unique(flatten(authorArr))

 for author in authorArrFiltered
 filter author != null && !contains(author, ':') && !contains(author, '/') && author != ""
 let authorDotReplaces0 = substitute(author, '.', ' ')
 let authorDotReplaces = substitute(authorDotReplaces0, '-', ' ')

 let people = (
 for x in fulltext(people, 'name', authorDotReplaces)
 return {name: x.name, id: x._id}
 )

 let otherBodies = (
 for x in fulltext(otherBodies, 'name', authorDotReplaces)
 return {name: x.name, id: x._id}
 )


 let legislativeBodies = (
 for x in fulltext(legislativeBodies, 'name', authorDotReplaces)
 return {name: x.name, id: x._id}
 )


 let executiveBodies = (
 for x in fulltext(executiveBodies, 'name', authorDotReplaces)
 return {name: x.name, id: x._id}
 )



 let judiciaries = (
 for x in fulltext(judiciaries, 'name', authorDotReplaces)
 return {name: x.name, id: x._id}
 )

 let countries = (
 for x in fulltext(countries, 'name', authorDotReplaces)
 return {name: x.name, id: x._id}
 )

 let nameID = union(countries, judiciaries, executiveBodies, legislativeBodies, otherBodies,  people)

 return {author, nameID}




 * */