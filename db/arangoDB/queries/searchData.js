import {db, aqlQuery} from '../arangodb'

function searchData(query) {
    return new Promise((resolve, reject) => {
        db.query(query).then((cursor) => {
            cursor.all().then((arr) => {
                resolve(arr);
            })
        }, (err) => {
            reject(err)
        })
    })
}

export {searchData}