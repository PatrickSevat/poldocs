import {db, aqlQuery} from '../arangodb'

function getHomepageSuggest(query) {
    return new Promise((resolve, reject) => {
        
        let dbQuery = getDBQuery(query);
        // console.log(dbQuery);
        db.query(dbQuery).then((cursor) => {
            cursor.all().then((arr) => {
                resolve(arr);
            })
        }, (err) => {
            reject(err)
        })
    })
}

export {getHomepageSuggest}

function getDBQuery(searchValue) {
    return `
let countryNameNLD = (
    for y in fulltext(countries, 'nameNL', '${searchValue}', 10)
    return {
        foundIn: 'name',
        type: 'country',
        weight: 10,
        id: y._id,
        name: y.nameNL,
        lang: 'nld'
    }
)
let countryNameENG = (
    for z in fulltext(countries, 'name', '${searchValue}', 10)
    return {
        foundIn: 'name',
        type: 'country',
        weight: 10,
        id: z._id,
        name: z.name,
        lang: 'eng'
    }
)
let docFullText = (
    for x in fulltext(documents, 'fullText', '${searchValue}', 10)
    let index = contains(lower(x.fullText), lower('${searchValue}'), true)
    let index2 = (index-75) < 0 ? 0 : (index-75)
    let substr = substring(x.fullText, index2, 150)
    return {
        foundIn: 'full text',
        type: 'document',
        weight: 1,
        id: x._id,
        title: x.title,
        authors: x.authors,
        extract: substr,
        lang: x.language
    }
)

let subTitle = (
    for x in fulltext(documents, 'subTitle', '${searchValue}', 10)
    let index = contains(lower(x.subTitle), lower('${searchValue}'), true)
    let index2 = (index-75) < 0 ? 0 : (index-75)
    let substr = substring(x.subTitle, index2, 150)
    return {
        foundIn: 'subTitle',
        type: 'document',
        weight: 5,
        id: x._id,
        title: x.title,
        authors: x.authors,
        extract: substr,
        lang: x.language
    }
)

let subTitleEqual = (
    for x in documents
    filter contains(x.subTitle, '${searchValue}')
    return {
        foundIn: 'subTitle',
        type: 'document',
        weight: 5,
        id: x._id,
        title: x.title,
        authors: x.authors,
        lang: x.language
    }
)

let title = (
    for x in fulltext(documents, 'title', '${searchValue}', 10)
    let index = contains(lower(x.title), lower('${searchValue}'), true)
    let index2 = (index-75) < 0 ? 0 : (index-75)
    let substr = substring(x.title, index2, 150)
    return {
        foundIn: 'title',
        type: 'document',
        weight: 10,
        id: x._id,
        title: x.title,
        authors: x.authors,
        extract: substr,
        lang: x.language
    }
)

let docAltTitle = (
    for x in fulltext(documents, 'altTitle', '${searchValue}', 10)
    let index = contains(lower(x.altTitle), lower('${searchValue}'), true)
    let index2 = (index-75) < 0 ? 0 : (index-75)
    let substr = substring(x.altTitle, index2, 150)
    return {
        foundIn: 'altTitle',
        type: 'document',
        weight: 5,
        id: x._id,
        title: x.title,
        authors: x.authors,
        extract: substr,
        lang: x.language
    }
)

let ID = (
    for x in documents
    let replaceSlashWithDash = substitute('${searchValue}', '/', '-')
    filter x._key == replaceSlashWithDash
    return {
        foundIn: 'ID',
        type: 'document',
        weight: 100,
        id: x._id,
        title: x.title,
        authors: x.authors,
        lang: x.language
    }
)

let celex = (
    for x in documents
    filter x.celexKey == '${searchValue}'
    LIMIT 10
    return {
        foundIn: 'celexKey',
        type: 'document',
        weight: 10,
        id: x._id,
        title: x.title,
        authors: x.authors,
        lang: x.language
    }
)

let type = (
    for x in fulltext(documents, 'type', '${searchValue}', 10)
    return {
        foundIn: 'type',
        type: 'document',
        weight: 1,
        id: x._id,
        title: x.title,
        authors: x.authors,
        lang: x.language
    }
)

let docAuthor = (
    for x in fulltext(documents, 'authors', '${searchValue}', 10)
    return {
        foundIn: 'authors',
        type: 'document',
        weight: 2,
        id: x._id,
        title: x.title,
        authors: x.authors,
        lang: x.language
    }
)

let dossierInterinstitutionalRef = (
    for x in dossiers
    filter x.interinstitutionalReference != null && x.interinstitutionalReference != ''
    let slashes = substitute(x.interinstitutionalReference,'-', '/')
    filter x.interinstitutionalReference == '${searchValue}' || slashes == '${searchValue}'
    return {
        foundIn: 'interinstitutional reference',
        type: 'dossiers',
        weight: 100,
        id: x._id,
        title: x.title,
        subType: x.subType,
        subTitle: x.subTitle
    }
)

let dossierTitle = (
    for x in fulltext(dossiers, 'title', '${searchValue}', 10)
    let index = contains(lower(x.title), lower('${searchValue}'), true)
    let index2 = (index-75) < 0 ? 0 : (index-75)
    let substr = substring(x.title, index2, 150)
    return {
        foundIn: 'title',
        type: 'dossiers',
        weight: 15,
        id: x._id,
        title: x.title,
        subType: x.subType,
        subTitle: x.subTitle,
        extract: substr
    }
)

let dossierSubTitle = (
    for x in fulltext(dossiers, 'subTitle', '${searchValue}', 10)
    let index = contains(lower(x.subTitle), lower('${searchValue}'), true)
    let index2 = (index-75) < 0 ? 0 : (index-75)
    let substr = substring(x.subTitle, index2, 150)
    return {
        foundIn: 'subTitle',
        type: 'dossiers',
        weight: 15,
        id: x._id,
        title: x.title,
        subType: x.subType,
        subTitle: x.subTitle,
        extract: substr
    }
)

let dossierName = (
    for x in fulltext(dossiers, 'name', '${searchValue}', 10)
    let index = contains(lower(x.name), lower('${searchValue}'), true)
    let index2 = (index-75) < 0 ? 0 : (index-75)
    let substr = substring(x.name, index2, 150)
    return {
        foundIn: 'name',
        type: 'dossiers',
        weight: 15,
        id: x._id,
        name: x.name,
        extract: substr
    }
)

let dossierSubType = (
    for x in fulltext(dossiers, 'subType', '${searchValue}', 10)
    return {
        foundIn: 'subType',
        type: 'dossiers',
        weight: 1,
        id: x._id,
        title: x.title,
        subType: x.subType,
        subTitle: x.subTitle
    }
)

let execBodyNameENG = (
    for x in fulltext(executiveBodies, 'name', '${searchValue}', 10)
    return {
        foundIn: 'name',
        type: 'executive bodies',
        weight: 100,
        id: x._id,
        name: x.name,
        lang: 'ENG'
    }
)

let execBodyNameNLD = (
    for x in fulltext(executiveBodies, 'nameNL', '${searchValue}', 10)
    return {
        foundIn: 'nameNL',
        type: 'executive bodies',
        weight: 100,
        id: x._id,
        name: x.nameNL,
        lang: 'NLD'
    }
)

let IGONameENG = (
    for x in fulltext(IGO, 'name', '${searchValue}', 10)
    return {
        foundIn: 'name',
        type: 'IGO',
        weight: 100,
        id: x._id,
        name: x.name,
        lang: 'ENG'
    }
)

let judiciariesNameENG = (
    for x in fulltext(judiciaries, 'name', '${searchValue}', 10)
    return {
        foundIn: 'name',
        type: 'judiciaries',
        weight: 100,
        id: x._id,
        name: x.name,
        lang: 'ENG'
    }
)

let judiciariesNameNLD = (
    for x in fulltext(judiciaries, 'nameNL', '${searchValue}', 10)
    return {
        foundIn: 'name',
        type: 'judiciaries',
        weight: 100,
        id: x._id,
        name: x.name,
        lang: 'NLD'
    }
)

let legislativeBodiesNameENG = (
    for x in fulltext(legislativeBodies, 'name', '${searchValue}', 10)
    return {
        foundIn: 'name',
        type: 'legislative bodies',
        weight: 100,
        id: x._id,
        name: x.name,
        lang: 'ENG'
    }
)

let otherBodiesNameENG = (
    for x in fulltext(otherBodies, 'name', '${searchValue}', 10)
    return {
        foundIn: 'name',
        type: 'other bodies',
        weight: 100,
        id: x._id,
        name: x.name,
        lang: 'ENG'
    }
)

let otherBodiesNameNLD = (
    for x in fulltext(otherBodies, 'nameNL', '${searchValue}', 10)
    return {
        foundIn: 'name',
        type: 'other bodies',
        weight: 100,
        id: x._id,
        name: x.name,
        lang: 'NLD'
    }
)

let peopleName = (
    for x in fulltext(people, 'name', '${searchValue}', 10)
    return {
        foundIn: 'name',
        gender: x.gender,
        type: 'people',
        weight: 100,
        id: x._id,
        name: x.name
    }
)

let partyName = (
    for x in fulltext(politicalParties, 'name', '${searchValue}', 10)
    return {
        foundIn: 'name',
        type: 'political parties',
        weight: 100,
        id: x._id,
        name: x.name
    }
)

return {ID,title,subTitle,subTitleEqual,docAltTitle,celex,type,docFullText,docAuthor,
        dossierInterinstitutionalRef,dossierTitle,dossierSubType,dossierSubTitle,
        execBodyNameENG, execBodyNameNLD,
        countryNameNLD,countryNameENG,
        IGONameENG,
        judiciariesNameENG, judiciariesNameNLD,
        legislativeBodiesNameENG,
        otherBodiesNameENG, otherBodiesNameNLD,
        peopleName,
        partyName
    }



 `
}