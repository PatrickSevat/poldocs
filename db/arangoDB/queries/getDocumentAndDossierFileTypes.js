import {db, aqlQuery} from '../arangodb'

function getDocumentAndDossierFileTypes() {
    return new Promise((resolve, reject) => {
        let query = `
let docTypesNational = (
    for doc in documents
    filter doc.level == 'national'
    return doc.type
)

let docTypesInternational = (
    for doc in documents
    filter doc.level == 'international'
    return doc.type
)

let dossierTypesInternational = (
    for dos in dossiers
    filter dos.level == 'international'
    return dos.procedure
)

return {
    documents: {
        national: unique(flatten(docTypesNational)), 
        international: unique(flatten(docTypesInternational))
        },
    dossiers: {
        national: ['national dossier'],
        international: unique(flatten(dossierTypesInternational))
    }        
}
`;
        db.query(query).then((cursor) => {
            cursor.all().then((arr) => {
                if (arr)
                    resolve(arr);
                else
                    resolve([]);
            })
        }, err => {
            console.log(err);
            reject(err);
        })
    })

}

exports.getDocumentAndDossierFileTypes = getDocumentAndDossierFileTypes;