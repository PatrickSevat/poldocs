import {db, aqlQuery} from '../arangodb'

//TODO optimize queries with LIMIT, offset, limit
//TODO optimize dossier query alot!

function getRecentDocuments() {
    return new Promise((resolve, reject) => {
        db.query(`
for doc in documents

filter (doc.dateModified > '2016-09-01' || doc.dateImported > '2016-09-01')
let newer = (doc.dateModified >= doc.dateImported) ? doc.dateModified : doc.dateImported
SORT newer DESC
LIMIT 250

let returnDocNoFullText = unset(doc, 'fullText')

return returnDocNoFullText  
        `
        ).then((cursor) => {
            cursor.all().then((arr) => {
                resolve(arr);
            })
        }, (err) => {
            reject(err)
        })
    })
}

function getRecentDossiers() {
    return new Promise((resolve, reject) => {
        db.query(recentDossierQuery).then((cursor) => {
            cursor.all().then((arr) => {
                resolve(arr);
            })
        }, (err) => {
            reject(err)
        })
    })
}

let recentDossierQuery = `for dossier in dossiers

filter dossier.dateImported > DATE_SUBTRACT(DATE_NOW(), 7, 'days') 
SORT dossier.dateImported DESC
LIMIT 250

let eventOrganisations = (
    for event in dossier.events
    filter event.eventOrganisation != ''
    return event.eventOrganisation
)

let eventSubOrganisations = (
    for event in dossier.events
    filter event.eventSubOrganisation != ''
    return event.eventSubOrganisation
)

let authors = union(eventOrganisations, eventSubOrganisations)
let authorsFlat = flatten(authors)
let authorsObj = {authors: authorsFlat}

let events = (
    for event in dossier.events
    let newEvent = (
        let arr = []
        let eventContainsWorkAndDocumentID = UNION_DISTINCT(TO_ARRAY(event.eventContainsWork), TO_ARRAY(event.documentID))
        for work in to_array(eventContainsWorkAndDocumentID)
        filter work != '' 
                
        let celexKey = (
            for doc in documents
            filter (work == doc.celexKey)
            return doc
            )    
                
        let subTitle = (
            let workReplaced = substitute(work, '_', ' ')
            for doc in fulltext(documents, 'subTitle', workReplaced)
            return doc
            )
            
        let key = (
            for doc in documents
            filter doc._key == work
            return doc
        )    
        return union(celexKey, subTitle, key)
    )
    return merge(event, {work: flatten(newEvent)})
)

let typeObj = {type: dossier.procedure == null ? 'National dossier' : dossier.procedure}

let eurovoc = (
    for euro in TO_ARRAY(dossier.eurovocIDs)
    return DOCUMENT(concat('eurovoc/desc-',euro))['label-en']
)

let beleidsagenda = (
    filter dossier.country != 'EU'
    for event in dossier.events
    return DOCUMENT(concat('documents/',event.documentID))['categories']['beleidsagenda']
)

let categoriesObj = {categories: {eurovoc: eurovoc, beleidsagenda: flatten(beleidsagenda)}}

let relatedDossiers = (
    for rel in dossierRelationships
    filter rel._to == dossier._id || rel._from == dossier._id 
    let dossierToRetrieve = rel._to == dossier._id ? rel._from : rel._to
    let relatedDossier = DOCUMENT(dossierToRetrieve)
    return merge(rel, {relatedDossier: relatedDossier})
)

let returnDossier = merge(dossier, authorsObj, typeObj, categoriesObj, {relatedDossiers: relatedDossiers}, {events: events})

return returnDossier`;

export {getRecentDocuments, getRecentDossiers}
