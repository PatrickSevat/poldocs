import {db, aqlQuery} from '../arangodb'

function getRelations(id) {
    let query = `
for rel in dossierRelationships
filter rel._to == '${id}' || rel._from == '${id}'
return rel
`;
    db.query(query).then((cursor) => {
        cursor.all((arr) => {
            if (arr)
                return new Promise((resolve, reject) => resolve(arr));
            else
                return new Promise((resolve, reject) => resolve([]));
        })
    }, err => {
        return new Promise((resolve, reject) => reject(err));
    })
}

exports.getRelations = getRelations;