/**
 * Created by Patrick on 01/08/2016.
 */
const Database = require('arangojs').Database;
import { arangoUsername, arangoPassword } from '../../secret/arangoCredentials';

let ipAndPort = '127.0.0.1:8529';
if (typeof process.env.DB_LINK_PORT_8529_TCP_ADDR != 'undefined')
    ipAndPort = process.env.DB_LINK_PORT_8529_TCP_ADDR+':'+process.env.DB_LINK_PORT_8529_TCP_PORT;

const db = new Database(`http://${arangoUsername}:${arangoPassword}@${ipAndPort}`);


//TODO revert to poldocs
db.useDatabase('poldocs-es');

var aqlQuery = require('arangojs').aqlQuery;

exports.db = db;
exports.aqlQuery = aqlQuery;
